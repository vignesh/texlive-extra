---
title: WEB programs in “TeX Live 2021”
colorlinks: true
---

## Introduction

Welcome to “All Things `WEB`” in “TeX Live.”

Here you find the documented source code of all major `WEB` and `CWEB` programs
in readable format.  The “named” entries represent the unmodified base sources,
the attached `[chg]` links point to “only the changed sections (or modules),”
so you may want to study these pairs in parallel.  Care has been taken to keep
the section numbers for the main parts intact.

Note that all `WEB` programs are mogrified from Pascal code to C code before
compilation, so further modifications and amendments will occur “for the real
thing,” i.e., the production of the executable `WEB` programs.  (The `CWEB`
programs do not require these extra modifications, they get linked directly to
the external runtime environment.)

If you believe to have found anything that remains technically, historically,
typographically, or politically incorrect, please notify the maintainers at
[the tex-k mailing list](mailto:tex-k@tug.org).

## Canonical Knuthian WEB programs

* `errata/` \[
  [The Errors of TeX](errata/errorlog.pdf) \|
  [The TeXbook, 1^st^\ printing](errata/errata.one.pdf) \|
  [The TeXbook, 2^nd^\ printing](errata/errata.two.pdf) \|
  [Computers&Typesetting, 15\ June\ 1987](errata/errata.three.pdf) \|
  [Computers&Typesetting, 20\ February\ 1989](errata/errata.four.pdf) \|
  [Computers&Typesetting, 30\ September\ 1989](errata/errata.five.pdf) \|
  [Computers&Typesetting, 31\ December\ 1990](errata/errata.six.pdf) \|
  [Computers&Typesetting, 15\ March\ 1992](errata/errata.seven.pdf) \|
  [Computers&Typesetting, pre-Millennium\ ed.](errata/errata.eight.pdf) \|
  [The TeXbook, September 1996](errata/errata.nine.pdf) \|
  [Computers&Typesetting, 1^st^\ Millennium\ ed.](errata/errata.ten.pdf) \|
  [Computers&Typesetting, 2^nd^\ Millennium\ ed.](errata/errata.eleven.pdf) \|
  [Computers&Typesetting, present](errata/errata.pdf) \]
* `etc/` \[
  [`VFtoVP`](etc/vftovp.pdf)~[chg](etc/vftovp-changes.pdf)~ \|
  [`VPtoVF`](etc/vptovf.pdf)~[chg](etc/vptovf-changes.pdf)~ \]
* `mf/` \[
  [`Metafont`](mf/mf.pdf)~[chg](mf/mf-changes.pdf)~ \|
  [`TRAPMAN`](mf/trapman.pdf) \]
* `mfware/` \[
  [`GFtype`](mfware/gftype.pdf)~[chg](mfware/gftype-changes.pdf)~ \|
  [`GFtoPK`](mfware/gftopk.pdf)~[chg](mfware/gftopk-changes.pdf)~ \|
  [`GFtoDVI`](mfware/gftodvi.pdf)~[chg](mfware/gftodvi-changes.pdf)~ \|
  [`MFT`](mfware/mft.pdf)~[chg](mfware/mft-changes.pdf)~ \]
* `tex/` \[
  [`GLUE`](tex/glue.pdf) \|
  [`TeX`](tex/tex.pdf)~[chg](tex/tex-changes.pdf)~ \|
  [`TRIPMAN`](tex/tripman.pdf) \]
* `texware/` \[
  [`POOLtype`](texware/pooltype.pdf)~[chg](texware/pooltype-changes.pdf)~ \|
  [`TFtoPL`](texware/tftopl.pdf)~[chg](texware/tftopl-changes.pdf)~ \|
  [`PLtoTF`](texware/pltotf.pdf)~[chg](texware/pltotf-changes.pdf)~ \|
  [`DVItype`](texware/dvitype.pdf)~[chg](texware/dvitype-changes.pdf)~ \]
* `web/` \[
  [`WEBMAN`](web/webman.pdf) \|
  [`WEAVE`](web/weave.pdf)~[chg](web/weave-changes.pdf)~ \|
  [`TANGLE`](web/tangle.pdf)~[chg](web/tangle-changes.pdf)~ \]

## Complementary WEB programs

* `other/` \[
  [`DVIcopy`](other/dvicopy.pdf)~[chg](other/dvicopy-changes.pdf)~ \|
  [`PatGen`](other/patgen.pdf)~[chg](other/patgen-changes.pdf)~ \|
  [`PKtoGF`](other/pktogf.pdf)~[chg](other/pktogf-changes.pdf)~ \|
  [`PKtype`](other/pktype.pdf)~[chg](other/pktype-changes.pdf)~ \]
* `bibtex/` \[ [`BibTeX`](bibtex/bibtex.pdf)~[chg](bibtex/bibtex-changes.pdf)~ \]
* `pdftex/` \[ [`pdfTeX`](pdftex/pdftex.pdf)~[chg](pdftex/pdftex-changes.pdf)~ \]
* `xetex/` \[ [`XeTeX`](xetex/xetex.pdf)~[chg](xetex/xetex-changes.pdf)~ \]

## CWEB programs

* `cweb/` \[
  [`CWEBMAN`](cweb/cwebman.pdf) \|
  [`COMMON`](cweb/common.pdf)~[chg](cweb/common-changes.pdf)~ \|
  [`CTANGLE`](cweb/ctangle.pdf)~[chg](cweb/ctangle-changes.pdf)~ \|
  [`CWEAVE`](cweb/cweave.pdf)~[chg](cweb/cweave-changes.pdf)~ \|
  [`CTWILL`](cweb/ctwill.pdf) \]
* `ctie/` \[ [`CTIE`](ctie/ctie.pdf)~[chg](ctie/ctie-changes.pdf)~ \]
* `tie/` \[ [`TIE`](tie/tie.pdf)~[chg](tie/tie-changes.pdf)~ \]

## Happy C/WEB Programming!

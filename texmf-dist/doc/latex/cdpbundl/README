(Version indicator: 2018 Feb 08)

The C.D.P. Bundle was originally developed for a free association named
"C.D.P." (more on it in the documentation), to typeset its official letters
with the appropriate letterhead; but more generally, it can be used to
typeset business letters bearing a letterhead of your choice and formatted
according to Italian style conventions.  Its modular structure, however,
provides you with building blocks of increasing level, by means of which
you should be able to adapt the letter format to a wide variety of styles.
It is also possible to write letters divided into sections and paragraphs,
to include floating figures and tables, and to use the commands
"\tableofcontents", "\listoffigures", and "\listoftables" to compile the
relevant indexes.  A single input file can contain several letters, and
each letter will have its own table of contents, etc., independent of the
other ones.

The C.D.P. Bundle may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.

For more information, read the following files:

00readme.txt        --  start by reading this file
cdpbundl-doc.pdf    --  overview of the C.D.P. Bundle (PDF)
cdp-ver-0-36.pdf    --  instructions specific to version 0.36 (PDF)

Other files that make up the distribution are:

manifest.txt    --  legal stuff
cdpbundl.dtx    --  main source file
cdpbundl.ins    --  installation script

See the file `manifest.txt' for a more precise specification of what
files constitutes a LPPL-compliant distribution.

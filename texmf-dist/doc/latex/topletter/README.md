#TOPletter class 
##version 0.3.0 of 2018/07/10  
Copyright 2015 Marco Torchiano  
Apache License 2.0 

The TOPletter bundle contains everything is needed for typesetting a letter using LaTeX and conforming to the official [Corporate Image](http://www.politocomunica.polito.it/corporate_image/marchio_e_identita_visiva) guidelines for [Politecnico di Torino](http://www.polito.it). The template can be used for letter both in Italian and English.

To understand how you can write an official letter, [here](https://github.com/mtorchiano/TOPLetter/blob/master/EsempioLettera.tex) is the source for the letter and [here](https://github.com/mtorchiano/TOPLetter/raw/master/EsempioLettera.pdf) you can see the result.




extract v1.9a
-------------

This package can be used to (conditionally) extract specific
commands and environments from a source file and write them to a
target file. This can be done without significant changes to the
source document, but labels inside the source can be used for more
flexibility. The package also provides environments to write code
directly to the target file or use code in both the source and the
target file. These tools allow one to generate ready-to-run files
from a source document, containing only the extracted material, in
an otherwise ordinary LaTeX run. See for more information the pdf
documentation of this package.


Installation
------------

First check whether this package can be installed by the distribution
that you use, since that will simplify installation. For manual
installations, the structure and supposed location in a TDS compliant
TeX-tree is shown in the overview below. In words, extract.sty should
be available to LaTeX. Don't forget to update your filename database
after installing the files.

CTAN:/macros/latex/contrib/extract: official location
 |
 |- readme     : should not be installed
 |- extract.dtx: /source/latex/extract
 |- extract.pdf: /doc/latex/extract
 `- extract.sty: /tex/latex/extract

The package includes pregenerated run and doc files, but you can
reproduce them from the source if necessary. See the first lines
of extract.dtx for information how to do this. Example files can
be produced by running the file extract.dtx with LaTeX once.

See the documentation of your LaTeX distribution or the TeX
Frequently Asked Questions for more information on installing
extract into your LaTeX distribution
(http://www.tex.ac.uk/cgi-bin/texfaq2html?label=instpackages).


License
-------

Copyright (C) 2004-2019 Hendri Adriaens

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2003/12/01 or later.

This work has the LPPL maintenance status "maintained".

This Current Maintainer of this work is Hendri Adriaens.

This work consists of the file extract.dtx and the derived files
extract.sty, xtrex1.tex, xtrex2.tex, xtrex3.tex, xtrex4.tex,
xtrex5.tex, xtrex6.tex and xtrex7.tex.

The following files constitute the extract package and must be
distributed as a whole: readme, extract.dtx, extract.pdf and
extract.sty.

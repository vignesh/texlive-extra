# Readme for the package cascade

Author: F. Pantigny (`fpantigny@wanadoo.fr`).

CTAN page: `https://ctan.org/pkg/nicematrix`

## License
The LaTeX extension `cascade` is distributed under the LPPL 1.3 license.

## Presentation


The LaTeX package `cascade` provides a command `\Cascade` to do contructions to present mathematical
demonstrations with successive braces for the deductions.


## Installation

The package `cascade` is present in the distributions MiKTeX, TeXLive and MacTeX.

For a manual installation:

* put the files `cascade.ins` and `cascade.dtx` in the same directory; 
* run `latex cascade.ins`.

The file `cascade.sty` will be generated.

The file `cascade.sty` is the only file necessary to use the extension `cascade`. 
You have to put it in the same directory as your document or (best) in a `texmf` tree. 

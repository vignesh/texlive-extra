# [Unreleased]

# [1.0.0] - 2020-03-17

## Added
- CHANGELOG file (following https://keepachangelog.com/en/1.0.0/).
- Semantic versionning (following https://semver.org/).

## Changed
- Compilation date displayed only if the issue number is not specified
  (`gztarticle` classonly).
- Prevent column breaks within items of "Comité de rédaction" (at the price of
  unbalanced columns).

## Fixed
- Track changes in `expl3`.
- Superflous uppercases removed.
- Index directive in `latexmk` config file modernized.

# [0.98] - 2018-04-09

## Changed
- Support for `biblatex` 3.8 changes.
- Track changes in `expl3`.
- Special editions implemented.

## Fixed
- Several bug fixes.

# [0.96] - 2017-04-07

## Changed
- Figures and tables:
  - with recurrent label and number but without any caption,
  - with caption but without any recurrent label nor numbered,
  implemented.
- Frames without any label, number nor caption implemented.
- Track changes in `expl3`.

# [0.9] - 2015-05-02

- Initial CTAN release of the `gzt` bundle.

----------------------------------------------------------------------------------------
etsvthor --- A package containing some useful abbreviations for members of e.t.s.v. Thor
E-mail: renatedebets98@gmail.com
Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt
----------------------------------------------------------------------------------------

This package provides some abbreviations which are very useful when typing meeting notes or other documents. Since this package basically only contains "\def"-commands, the code is extremely easy to read and self-explanatory. 



Version history
--------------

v1.0: First upload to CTAN!! :) 
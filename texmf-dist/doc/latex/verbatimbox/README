The style file verbatimbox has been created for the report writer's
convenience.

Do you need to include blocks of verbatim text inside figures, tables or
footnotes?  The verbatim environment is not compatible with some of
these other environments.  But now, the verbbox environment allows you
to input verbatim text and store it in a LaTeX box.  That box then
becomes generally usable in all other LaTeX environments, and can be
simply recalled.

This style therefore allows for a greater use of the verbatim
environment within other LaTeX constructs where verbatim was previously
inaccessible.

Input to the verbatim box can either be as text typed into the verbbox
or myverbbox environments, or else as the contents of a file, provided
as an argument to the \verbfilebox command.  In either case, an optional
argument may be provided to modify the text appearance, for example, by
changing the font size of the verbatim text placed into the box.

The \addvbuffer command, while supporting this package, also provides a
flexible way to add vertical buffer space above and below most LaTeX
objects, which can be handy in a variety of applications.

						-Steven Segletes

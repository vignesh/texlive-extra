Copyright (C) 2010 by Ryan Reich <ryan.reich@gmail.com>
-------------------------------------------------------

This file may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.2
of this license or (at your option) any later version.
The latest version of this license is in:

   http://www.latex-project.org/lppl.txt

and version 1.2 or later is part of all distributions of LaTeX 
version 1999/12/01 or later.

------------------------------------------------------
Package description
------------------------------------------------------

This package provides some powerful functions for drawing Young
tableaux and Young diagrams, both skew and justified, and with
colored boxes.

------------------------------------------------------
Package installation
------------------------------------------------------

Run:

> latex ytableau.ins

to get ytableau.sty, and if you like, also run

> latex ytableau.dtx

to get the ytableau.dvi (or pdflatex to get ytableau.pdf), which
includes some code samples (and the commented source).  This should
have been distributed with this README.

To regenerate the index and the changelog, after running the above
commands, you should do:

> makeindex -s gind.ist -o ytableau.ind ytableau.idx
> makeindex -s gglo.ist -o ytableau.gls ytableau.glo

and then compile ytableau.dtx again (possibly several times to get the
cross-references correct).
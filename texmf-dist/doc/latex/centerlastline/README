centerlastline --- last line centered (spanish paragraph)
=========================================================

Versions
1.0 -> initial version

Copyright (C) 2020 Miguel V. S. Frasson (mvsfrasson@gmail.com)

LICENSE
=======

This file is the README file, part of the `centerlastline' package.

This package may be distributed under the terms of the LaTeX Project
Public License, as described in lppl.txt in the base LaTeX
distribution, either version 1.3 or (at your option) any later
version.

You can obtain a copy of the lppl.txt file from the internet on
http://www.latex-project.org/lppl.txt

USAGE
=====

The package _centerlastline_ provides command \centerlastline, which
sets paragraph style to typeset with no indentation and last line
centered, an arrangemente known as _Spanish paragraph_. An example

{\centerlastline
A big paragraph to be formatted with no indentation and last line
centered. It is a not-so-frequent composition, useful to finish a
large paragraph of text, at the end of chapters, prologues, back cover
of books, etc.  Be careful to finish the paragraph with a blanck line
or \verb+\par+, to the paragraph style take effect.\par}

It can be used as an environment too, as in

\begin{centerlastline}
A paragraph to be formatted as a Spanish paragraph.  In environment
form, user doesn't have to take care of last paragraph.  The
environment taks care of everything.
\end{centerlastline}




  *File List*
   DOCSRC....   --  -- --   --    
   README.tex  2012/03/18   --    make README.pdf
 fifinddo.tex  2011/11/19   --    documenting fifinddo.sty
  makedoc.tex  2012/11/30   --    documenting makedoc.sty
 niceverb.tex  2014/03/28   --    documenting niceverb.sty
mdoccheat.tex  2011/01/26   --    cheat sheet for `makedoc.sty'
wikicheat.tex  2011/01/26   --    cheatsheet for wiki.sty
 srcfiles.tex  2012/11/27   --    file infos -> SrcFILEs.txt
      RUN....   --  -- --   --    
 arseneau.tex  2012/03/18   --    some packages by D. Arseneau (ul)
   substr.tex  2012/03/18   --    documentation for substr.sty
 copyfile.tex  2011/09/13   --    copy*ing/converting (UL)
 fdtxttex.tex  2011/09/13   --    txt to TeX by dialogue (UL)
 fdtxttex.tpl  2011/09/13   --    fifinddo correction template
  makedoc.tpl  2011/09/14   --    makedoc preprocessing template
   RUNUSE....   --  -- --   --    
    atari.cfg  2011/09/13   --    sample for copyfile.tex
    atari.fdf  2010/08/29   --    Atari German to ISO-8859 with blog.sty
 u8atablg.fdf  2011/09/12   --    UTF-8->Atari/blog with fdtxttex
 fddial0g.sty  2011/09/13  v0.2   dialogues with fifinddo (UL)
 copyfile.cfg  2011/09/13   --    initializing copyfile.tex
 fdtxttex.cfg  2011/09/12   --    initializing fdtxttex.tex
      USE....   --  -- --   --    
 fifinddo.sty  2012/11/17  v0.61  filtering TeX(t) files by TeX (UL)
  makedoc.sty  2012/08/28  v0.52  TeX input from *.sty (UL)
  makedoc.cfg  2013/03/25   --    documentation settings
 mdoccorr.cfg  2012/11/13   --    `makedoc' local typographical corrections
 niceverb.sty  2015/11/21  v0.62  minimize doc markup (UL)
     wiki.sty  2008/07/02  v0.2   LaTeX through Wiki markup
   BUNDLE....   --  -- --   --    
 nicetext.RLS  2015/11/21  r0.67  `niceverb.sty' v0.62 \qtdnverb fixed
  ***********

 List made at 2015/11/21, 02:43
 from script file srcfiles.tex


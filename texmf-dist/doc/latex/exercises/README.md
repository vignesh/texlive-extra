This is the distribution directory for exercises, a LaTeX package to typeset exercices and solutions.
The package allows you to customize the layout of those environments.
The optional points in exercises can be added automatically. In addition you’re able to hide the solutions.

This is exercises version 1.1, 17 Mai 2020. exercises is distributed 
under the LaTeX Project Public License (LPPL).


*** Source files:

exercises.sty	The style file
exercises.dtx	The documentation file
exercises.ins  The installation file


*** Formatted versions of the documentation:

exercises.pdf


Roger Jud (rogerjud@outlook.com)



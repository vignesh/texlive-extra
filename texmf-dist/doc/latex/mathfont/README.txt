This README file contains information about the LaTeX2e package
"mathfont" installation version 1.6. Besides this README, the
installation contains the following files:

1. mathfont_code.dtx: the primary file. Typeset this file with
LaTeX to generate mathfont.sty and complete the installation.

2. mathfont_code.pdf: documentation of the package code.

3. mathfont_symbol_list.pdf: a typeset list of symbols that
mathfont can act on.

4. mathfont_user_guide.pdf: user-level documentation.




Copyright 2018-2019 by Conrad Kosowsky

This file may be distributed and modified under the terms of the
LaTeX Public Project License, version 1.3c or any later version.
The most recent version of this license is available online at

           https://www.latex-project.org/lppl/.

This work has the LPPL status "maintained," and the current
maintainer is the package author, Conrad Kosowsky. He can be
reached at kosowsky.latex@gmail.com. The work consists of the
following items:

  (1) the base file mathfont_code.dtx;

  (2) the package code contained in mathfont.sty;

  (3) the derived files mathfont_symbol_list.tex,
mathfont_user_guide.tex, mathfont_heading.tex, and
mathfont_doc_patch.tex, and mathfont_index_warning.tex;

  (4) the pdf documentation files mathfont_code.pdf,
mathfont_symbol_list.pdf, and mathfont_user_guide.pdf;

  (5) all other files created through the configuration
process including mathfont_code.ind; and

  (6) this README.txt file.

For more information, see the original mathfont.dtx file. To
install mathfont on your computer, run mathfont_code.dtx through
LaTeX and place the derived file mathfont.sty in a directory
searchable by TeX.


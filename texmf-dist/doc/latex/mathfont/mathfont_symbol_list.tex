%%
%% This is file `mathfont_symbol_list.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% mathfont_code.dtx  (with options: `chars')
%% 
%% This file is file from version 1.6 of the LaTeX package "mathfont,"
%% to be used in conjunction with the XeTeX or LuaTeX engines.
%% 
%% Copyright 2018-2019 by Conrad Kosowsky
%% 
%% This file may be distributed and modified under the terms of the
%% LaTeX Public Project License, version 1.3c or any later version.
%% The most recent version of this license is available online at
%% 
%%            https://www.latex-project.org/lppl/.
%% 
%% This work has the LPPL status "maintained," and the current
%% maintainer is the package author, Conrad Kosowsky. He can be
%% reached at kosowsky.latex@gmail.com. The work consists of the
%% following items:
%% 
%%   (1) the base file mathfont_code.dtx;
%% 
%%   (2) the package code contained in mathfont.sty;
%% 
%%   (3) the derived files mathfont_symbol_list.tex,
%% mathfont_user_guide.tex, mathfont_heading.tex, and
%% mathfont_doc_patch.tex;
%% 
%%   (4) the pdf documentation files mathfont_code.pdf,
%% mathfont_symbol_list.pdf, and mathfont_user_guide.pdf;
%% 
%%   (5) all other files created through the configuration process
%% such as mathfont.idx and mathfont.ind; and
%% 
%%   (6) the associated README.txt file.
%% 
%% For more information, see the original mathfont.dtx file. To
%% install mathfont on your computer, run mathfont_code.dtx through
%% LaTeX and place the derived file mathfont.sty in a directory
%% searchable by TeX.
%% 
\documentclass[12pt,twoside]{article}
\makeatletter
\let\@@imath\imath
\let\@@jmath\jmath
\usepackage[margin=72.27pt]{geometry}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{mathfont}
\mathfont[greekupper]{Courier New}
\mathfont[greeklower,delimiters,bigops]{Times New Roman}
\mathfont[agreekupper,extsymbols,bb,cal,frak,bcal,bfrak]{Symbola}
\mathfont[agreeklower=roman]{Didot Bold}
\mathfont[hebrew]{New Peninim MT}
\mathfont[cyrillicupper]{EB Garamond}
\mathfont[cyrilliclower=roman]{Comic Sans MS}
\mathfont[symbols]{Arial}
\mathfont[arrows,extbigops]{STIXGeneral}
\mathfont[diacritics,lower]{Baskerville}
\usepackage{shortvrb}
\MakeShortVerb{|}
\pretolerance=20
\hyphenpenalty=10
\exhyphenpenalty=5
\brokenpenalty=0
\finalhyphendemerits=300
\doublehyphendemerits=500
\raggedcolumns
\parskip=0pt
\smallskipamount=2pt plus 1pt minus 1pt
\multicolsep=0pt
\premulticols=0pt
\begin{document}

\def\documentname{Symbol List}
\input mathfont_heading.tex

The \textsf{mathfont} package acts on some 300 alphanumeric characters and 500 general math symbols, and this document lists all such symbols besides Latin characters and Arabic numerals. The characters are organized by keyword, and when the user calls |\mathfont| on one of the keyword classes below, the package acts on every control sequence listed under that keyword. It changes the math-mode font of the character-commands that already exist in \LaTeX, and for the control sequences that do not exist in \LaTeX, it defines them to be new characters for use in math mode. Unlike most character-providing packages, \textsf{mathfont} does not provide extra symbols by default, and users can access additional control sequences only once they act |\mathfont| on some keyword-option. Of course, typesetting these symbols depends on having a font that contains them, and most major unicode fonts lack many or most of the symbols in this document. Choose your font wisely! Finally, as stated in the user guide, the \texttt{delimiters}, \texttt{bigops}, and |\big|\argtext{symbol} (such as |\bigvee|) characters do not in general change size in different mathematical contexts. I hope to address this limitation in future package updates. For documentation of user-level commands, see |mathfont_user_guide.pdf|, and for version history and code implementation, see |mathfont_code.pdf|. Both documentation files are included with the \textsf{mathfont} installation and available on \textsc{ctan}.

This document does not contain tables for the keywords |upper|, |lower|, and |digits|. The first of these keywords contains all capital Latin letters, and the second contains all lower-case Latin letters as well as the ``mathematical i'' $\@@imath$ coded with |\imath| and ``mathematical j'' $\@@jmath$ coded with |\jmath|. The |digits| category contains the digits 0 through 9. Unlike the \LaTeX\ kernel, \textsf{mathfont} declares both |\imath| and |\jmath| as math alphabet characters, so the package's local font-change commands will adjust the font of these two symbols.

\catcode`\|=12
\parskip=1pt

\blockheader{diacritics}{Accent}{Baskerville}

\begin{multicols}{3}
\makeaccent{\acute}
\makeaccent{\aacute}
\makeaccent{\dot}
\makeaccent{\ddot}
\makeaccent{\grave}
\makeaccent{\breve}
\makeaccent{\hat}
\makeaccent{\check}
\makeaccent{\bar}
\makeaccent{\mathring}
\makeaccent{\tilde}
\end{multicols}

\blockheader{greekupper}{Upper-Case Greek}{Courier New}

\begin{multicols}{3}
\makechar{\Alpha}
\makechar{\Beta}
\makechar{\Gamma}
\makechar{\Delta}
\makechar{\Epsilon}
\makechar{\Zeta}
\makechar{\Eta}
\makechar{\Theta}
\makechar{\Iota}
\makechar{\Kappa}
\makechar{\Lambda}
\makechar{\Mu}
\makechar{\Nu}
\makechar{\Xi}
\makechar{\Omicron}
\makechar{\Pi}
\makechar{\Rho}
\makechar{\Sigma}
\makechar{\Tau}
\makechar{\Upsilon}
\makechar{\Phi}
\makechar{\Chi}
\makechar{\Psi}
\makechar{\Omega}
\makechar{\varTheta}
\end{multicols}

\blockheader{greeklower}{Lower-Case Greek}{Times New Roman}

\begin{multicols}{3}
\makechar{\alpha}
\makechar{\beta}
\makechar{\gamma}
\makechar{\delta}
\makechar{\epsilon}
\makechar{\zeta}
\makechar{\eta}
\makechar{\theta}
\makechar{\iota}
\makechar{\kappa}
\makechar{\lambda}
\makechar{\mu}
\makechar{\nu}
\makechar{\xi}
\makechar{\omicron}
\makechar{\pi}
\makechar{\rho}
\makechar{\sigma}
\makechar{\tau}
\makechar{\upsilon}
\makechar{\phi}
\makechar{\chi}
\makechar{\psi}
\makechar{\omega}
\makechar{\varbeta}
\makechar{\varepsilon}
\makechar{\vartheta}
\makechar{\varrho}
\makechar{\varsigma}
\makechar{\varphi}
\end{multicols}

\blockheader{agreekupper}{Upper-Case Ancient Greek}{Symbola}

\begin{multicols}{3}
\makechar{\Heta}
\makechar{\Sampi}
\makechar{\Digamma}
\makechar{\Koppa}
\makechar{\Stigma}
\makechar{\Sho}
\makechar{\San}
\makechar{\varSampi}
\makechar{\varDigamma}
\makechar{\varKoppa}
\end{multicols}

\blockheader{agreeklower}{Lower-Case Ancient Greek}{Didot Bold}

\begin{multicols}{3}
\makechar{\heta}
\makechar{\sampi}
\makechar{\digamma}
\makechar{\koppa}
\makechar{\stigma}
\makechar{\sho}
\makechar{\san}
\makechar{\varsampi}
\makechar{\vardigamma}
\makechar{\varkoppa}
\end{multicols}

\blockheader{cyrillicupper}{Upper-Case Cyrillic}{EB Garamond}

\begin{multicols}{3}
\makechar{\cyrA}
\makechar{\cyrBe}
\makechar{\cyrVe}
\makechar{\cyrGhe}
\makechar{\cyrDe}
\makechar{\cyrIe}
\makechar{\cyrZhe}
\makechar{\cyrZe}
\makechar{\cyrI}
\makechar{\cyrKa}
\makechar{\cyrEl}
\makechar{\cyrEm}
\makechar{\cyrEn}
\makechar{\cyrO}
\makechar{\cyrPe}
\makechar{\cyrEr}
\makechar{\cyrEs}
\makechar{\cyrTe}
\makechar{\cyrU}
\makechar{\cyrEf}
\makechar{\cyrHa}
\makechar{\cyrTse}
\makechar{\cyrChe}
\makechar{\cyrSha}
\makechar{\cyrShcha}
\makechar{\cyrHard}
\makechar{\cyrYeru}
\makechar{\cyrSoft}
\makechar{\cyrE}
\makechar{\cyrYu}
\makechar{\cyrYa}
\makechar{\cyrvarI}
\end{multicols}

\blockheader{cyrilliclower}{Lower-Case Cyrillic}{Comic Sans MS%
  \footnote{Yes, you too can now create beautifully spaced mathematics in Comic Sans!}}

\begin{multicols}{3}
\makechar{\cyra}
\makechar{\cyrbe}
\makechar{\cyrve}
\makechar{\cyrghe}
\makechar{\cyrde}
\makechar{\cyrie}
\makechar{\cyrzhe}
\makechar{\cyrze}
\makechar{\cyri}
\makechar{\cyrka}
\makechar{\cyrel}
\makechar{\cyrem}
\makechar{\cyren}
\makechar{\cyro}
\makechar{\cyrpe}
\makechar{\cyrer}
\makechar{\cyres}
\makechar{\cyrte}
\makechar{\cyru}
\makechar{\cyref}
\makechar{\cyrha}
\makechar{\cyrtse}
\makechar{\cyrche}
\makechar{\cyrsha}
\makechar{\cyrshcha}
\makechar{\cyrhard}
\makechar{\cyryeru}
\makechar{\cyrsoft}
\makechar{\cyre}
\makechar{\cyryu}
\makechar{\cyrya}
\makechar{\cyrvari}
\end{multicols}

\blockheader{hebrew}{Hebrew}{New Peninim MT}

\begin{multicols}{3}
\makechar{\aleph}
\makechar{\beth}
\makechar{\gimel}
\makechar{\daleth}
\makechar{\he}
\makechar{\vav}
\makechar{\zayin}
\makechar{\het}
\makechar{\tet}
\makechar{\yod}
\makechar{\kaf}
\makechar{\lamed}
\makechar{\mem}
\makechar{\nun}
\makechar{\samekh}
\makechar{\ayin}
\makechar{\pe}
\makechar{\tsadi}
\makechar{\qof}
\makechar{\resh}
\makechar{\shin}
\makechar{\tav}
\makechar{\varkaf}
\makechar{\varmem}
\makechar{\varnun}
\makechar{\varpe}
\makechar{\vartsadi}
\end{multicols}

\blockheader{symbols}{Basic Math}{Arial}

\begin{multicols}{3}
\makechar{.}
\makechar{@}
\let\par\relax
\makechar{\#}\footnote{When it acts on \vrb\#, \vrb\%, and \vrb\&, \textsf{mathfont} redefines them as robust commands that expand to their usual \vrb\char\ definition in horizontal mode and a math symbol in math mode. This prevents any changes to their font outside of math mode and is how other commands such as \vrb\$ or \vrb\P\ function in both math mode and horizontal mode.}\@@par
\makechar{\$}\footnote{Technically, \textsf{mathfont} doesn't redefine \vrb\$, \vrb\P, \vrb\S, \vrb\pounds, \vrb\dag, or \vrb\ddag. The package recodes the character-command that these macros expand to when in math mode.}\@par
\makechar{\%}
\makechar{\&}
\makechar{\P}
\makechar{\S}
\makechar{\pounds}
\makechar{|}
\makechar{\neg}
\makechar{\infty}
\makechar{\partial}
\makechar{\mathbackslash}
\makechar{\degree}
\makechar{\increment}
\makechar{\hbar}
\makechar{'}
\makechar{"}
\let\par\relax
\makechar{\comma}\footnote{In addition to the comma and colon punctuation marks, the package defines \vrb\comma\ and \vrb\colon. The difference lies in the spacing. \TeX\ treats the comma and colon keystrokes as \vrb\mathpunct\ and \vrb\mathrel\ types respectively. The package codes the \vrb\comma\ and \vrb\colon\ control sequences as \vrb\mathord\ and \vrb\mathpunct\ types respectively, so both control sequence result in less space than the corresponding keystroke. I recommend using \vrb\comma\ to typeset commas in large real numbers and \vrb\colon\ to typeset colon punctuation marks, for example following a function or to indicate a subset specification.}\@par
\makechar{+}
\makechar{-}
\makechar{*}
\makechar{\times}
\makechar{/}
\makechar{\div}
\makechar{\pm}
\makechar{\bullet}
\makechar{\dag}
\makechar{\ddag}
\makechar{\cdot}
\makechar{\setminus}
\makechar{=}
\makechar{<}
\makechar{>}
\makechar{\leq}
\makechar{\geq}
\makechar{\sim}
\makechar{\approx}
\makechar{\equiv}
\makechar{\mid}
\makechar{\parallel}
\makechar{:}
\makechar{?}
\makechar{!}
\makechar{,}
\makechar{;}
\makechar{\colon}
\makechar{\mathellipsis}
\end{multicols}

\blockheader{extsymbols}{Extended Math}{Symbola}

\begin{multicols}{3}
\makechar{\wp}
\makechar{\Re}
\makechar{\Im}
\makechar{\ell}
\makechar{\forall}
\makechar{\exists}
\makechar{\emptyset}
\makechar{\nabla}
\makechar{\in}
\makechar{\ni}
\makechar{\mp}
\makechar{\angle}
\makechar{\top}
\makechar{\bot}
\makechar{\vdash}
\makechar{\dashv}
\makechar{\flat}
\makechar{\natural}
\makechar{\sharp}
\makechar{\fflat}
\makechar{\ssharp}
\let\par\relax
\makechar{\bclubsuit}\footnote{Also \vrb\clubsuit.}\@par
\makechar{\bdiamondsuit}
\makechar{\bheartsuit}
\let\par\relax
\makechar{\bspadesuit}\footnote{Also \vrb\spadesuit.}\@par
\makechar{\wclubsuit}
\let\par\relax
\makechar{\wdiamondsuit}\footnote{Also \vrb\diamondsuit.}\@@par
\makechar{\wheartsuit}\footnote{Also \vrb\heartsuit.}\@par
\makechar{\wspadesuit}
\makechar{\wedge}
\makechar{\vee}
\makechar{\cap}
\makechar{\cup}
\makechar{\sqcap}
\makechar{\sqcup}
\makechar{\amalg}
\makechar{\wr}
\makechar{\ast}
\makechar{\star}
\makechar{\diamond}
\makechar{\varcdot}
\makechar{\varsetminus}
\makechar{\oplus}
\makechar{\otimes}
\makechar{\ominus}
\makechar{\odiv}
\makechar{\oslash}
\makechar{\odot}
\makechar{\sqplus}
\makechar{\sqtimes}
\makechar{\sqminus}
\makechar{\sqdot}
\makechar{\in}
\makechar{\ni}
\makechar{\subset}
\makechar{\supset}
\makechar{\subseteq}
\makechar{\supseteq}
\makechar{\sqsubset}
\makechar{\sqsupset}
\makechar{\sqsubseteq}
\makechar{\sqsupseteq}
\makechar{\triangleleft}
\makechar{\triangleright}
\makechar{\trianglelefteq}
\makechar{\trianglerighteq}
\makechar{\propto}
\makechar{\bowtie}
\makechar{\hourglass}
\makechar{\therefore}
\makechar{\because}
\makechar{\ratio}
\makechar{\proportion}
\makechar{\ll}
\makechar{\gg}
\makechar{\lll}
\makechar{\ggg}
\makechar{\leqq}
\makechar{\geqq}
\makechar{\lapprox}
\makechar{\gapprox}
\makechar{\simeq}
\makechar{\eqsim}
\let\par\relax
\makechar{\simeqq}\footnote{Also \vrb\cong.}\@par
\makechar{\approxeq}
\makechar{\sssim}
\makechar{\seq}
\makechar{\doteq}
\makechar{\coloneq}
\makechar{\eqcolon}
\makechar{\ringeq}
\makechar{\arceq}
\makechar{\wedgeeq}
\makechar{\veeeq}
\makechar{\stareq}
\makechar{\triangleeq}
\makechar{\defeq}
\makechar{\qeq}
\makechar{\lsim}
\makechar{\gsim}
\makechar{\prec}
\makechar{\succ}
\makechar{\preceq}
\makechar{\succeq}
\makechar{\preceqq}
\makechar{\succeqq}
\makechar{\precsim}
\makechar{\succsim}
\makechar{\precapprox}
\makechar{\succapprox}
\makechar{\precprec}
\makechar{\succsucc}
\makechar{\asymp}
\makechar{\nin}
\makechar{\nni}
\makechar{\nsubset}
\makechar{\nsupset}
\makechar{\nsubseteq}
\makechar{\nsupseteq}
\makechar{\subsetneq}
\makechar{\supsetneq}
\makechar{\nsqsubseteq}
\makechar{\nsqsupseteq}
\makechar{\sqsubsetneq}
\makechar{\sqsupsetneq}
\makechar{\neq}
\makechar{\nl}
\makechar{\ng}
\makechar{\nleq}
\makechar{\ngeq}
\makechar{\lneq}
\makechar{\gneq}
\makechar{\lneqq}
\makechar{\gneqq}
\makechar{\ntriangleleft}
\makechar{\ntriangleright}
\makechar{\ntrianglelefteq}
\makechar{\ntrianglerighteq}
\makechar{\nsim}
\makechar{\napprox}
\makechar{\nsimeq}
\makechar{\nsimeqq}
\makechar{\simneqq}
\makechar{\nlsim}
\makechar{\ngsim}
\makechar{\lnsim}
\makechar{\gnsim}
\makechar{\lnapprox}
\makechar{\gnapprox}
\makechar{\nprec}
\makechar{\nsucc}
\makechar{\npreceq}
\makechar{\nsucceq}
\makechar{\precneq}
\makechar{\succneq}
\makechar{\precneqq}
\makechar{\succneqq}
\makechar{\precnsim}
\makechar{\succnsim}
\makechar{\precnapprox}
\makechar{\succnapprox}
\makechar{\nequiv}
\end{multicols}

\blockheader{delimiters}{Delimiter}{Times New Roman}

\begin{multicols}{3}
\makechar{(}
\makechar{)}
\makechar{[}
\makechar{]}
\makechar{\leftbrace}
\makechar{\rightbrace}
\end{multicols}

\blockheader{arrows}{Arrow}{STIXGeneral}

\begin{multicols}{2}
\let\par\relax
\makechar{\rightarrow}\footnote{Also \vrb\to.}\@par
\makechar{\nrightarrow}
\makechar{\Rightarrow}
\makechar{\nRightarrow}
\makechar{\Rrightarrow}
\makechar{\longrightarrow}
\makechar{\Longrightarrow}
\let\par\relax
\makechar{\rightbararrow}\footnote{Also \vrb\mapsto.}\@par
\makechar{\Rightbararrow}
\let\par\relax
\makechar{\longrightbararrow}\footnote{Also \vrb\longmapsto.}\@par
\makechar{\Longrightbararrow}
\makechar{\hookrightarrow}
\makechar{\rightdasharrow}
\makechar{\rightharpoonup}
\makechar{\rightharpoondown}
\makechar{\rightarrowtail}
\makechar{\rightoplusarrow}
\makechar{\rightwavearrow}
\makechar{\rightsquigarrow}
\makechar{\longrightsquigarrow}
\makechar{\looparrowright}
\makechar{\curvearrowright}
\makechar{\circlearrowright}
\makechar{\twoheadrightarrow}
\makechar{\rightarrowtobar}
\makechar{\rightwhitearrow}
\makechar{\rightrightarrows}
\makechar{\rightrightrightarrows}
\let\par\relax
\makechar{\leftarrow}\footnote{Also \vrb\from.}\@par
\makechar{\nleftarrow}
\makechar{\Leftarrow}
\makechar{\nLeftarrow}
\makechar{\Lleftarrow}
\makechar{\longleftarrow}
\makechar{\Longleftarrow}
\let\par\relax
\makechar{\leftbararrow}\footnote{Also \vrb\mapsfrom.}\@par
\makechar{\Leftbararrow}
\let\par\relax
\makechar{\longleftbararrow}\footnote{Also \vrb\longmapsfrom.}\@par
\makechar{\Longleftbararrow}
\makechar{\hookleftarrow}
\makechar{\leftdasharrow}
\makechar{\leftharpoonup}
\makechar{\leftharpoondown}
\makechar{\leftarrowtail}
\makechar{\leftoplusarrow}
\makechar{\leftwavearrow}
\makechar{\leftsquigarrow}
\makechar{\longleftsquigarrow}
\makechar{\looparrowleft}
\makechar{\curvearrowleft}
\makechar{\circlearrowleft}
\makechar{\twoheadleftarrow}
\makechar{\leftarrowtobar}
\makechar{\leftwhitearrow}
\makechar{\leftleftarrows}
\makechar{\leftleftleftarrows}
\makechar{\leftrightarrow}
\makechar{\Leftrightarrow}
\makechar{\nLeftrightarrow}
\makechar{\longleftrightarrow}
\makechar{\Longleftrightarrow}
\makechar{\leftrightwavearrow}
\makechar{\leftrightarrows}
\makechar{\leftrightharpoons}
\makechar{\leftrightarrowstobar}
\makechar{\rightleftarrows}
\makechar{\rightleftharpoons}
\makechar{\uparrow}
\makechar{\Uparrow}
\makechar{\Uuparrow}
\makechar{\upbararrow}
\makechar{\updasharrow}
\makechar{\upharpoonleft}
\makechar{\upharpoonright}
\makechar{\twoheaduparrow}
\makechar{\uparrowtobar}
\makechar{\upwhitearrow}
\makechar{\upwhitebararrow}
\makechar{\upuparrows}
\makechar{\downarrow}
\makechar{\Downarrow}
\makechar{\Ddownarrow}
\makechar{\downbararrow}
\makechar{\downdasharrow}
\makechar{\zigzagarrow}
\makechar{\downharpoonleft}
\makechar{\downharpoonright}
\makechar{\twoheaddownarrow}
\makechar{\downarrowtobar}
\makechar{\downwhitearrow}
\makechar{\downdownarrows}
\makechar{\updownarrow}
\makechar{\Updownarrow}
\makechar{\updownarrows}
\makechar{\downuparrows}
\makechar{\updownharpoons}
\makechar{\downupharpoons}
\makechar{\nearrow}
\makechar{\Nearrow}
\makechar{\nwarrow}
\makechar{\Nwarrow}
\makechar{\searrow}
\makechar{\Searrow}
\makechar{\swarrow}
\makechar{\Swarrow}
\makechar{\nwsearrow}
\makechar{\neswarrow}
\makechar{\lcirclearrow}
\makechar{\rcirclearrow}
\end{multicols}

\blockheader{bigops}{Big Operator}{Times New Roman}

\begin{multicols}{3}
\makechar{\sum}
\makechar{\prod}
\makechar{\intop}
\end{multicols}

\blockheader{extbigops}{Extended Big Operators}{STIXGeneral}

\begin{multicols}{3}
\makechar{\coprod}
\makechar{\bigvee}
\makechar{\bigwedge}
\makechar{\bigcup}
\makechar{\bigcap}
\makechar{\iint}
\makechar{\iiint}
\makechar{\oint}
\makechar{\oiint}
\makechar{\oiiint}
\makechar{\bigoplus}
\makechar{\bigotimes}
\makechar{\bigodot}
\makechar{\bigsqcap}
\makechar{\bigsqcup}
\end{multicols}

\blockheader{bb}{Blackboard Bold}{Symbola and Accessed with \vrb\mathbb}

\letterlikechars\mathbb
\hbox to 10em{\printchars\digits}

\blockheader{cal}{Calligraphic}{Symbola and Accessed with \vrb\mathcal}

\letterlikechars\mathcal

\blockheader{frak}{Fraktur}{Symbola and Accessed with \vrb\mathfrak}

\letterlikechars\mathfrak

\blockheader{bcal}{Bold Calligraphic}{Symbola and Accessed with \vrb\mathbcal}

\letterlikechars\mathbcal

\blockheader{bfrak}{Bold Fraktur}{Symbola and Accessed with \vrb\mathbfrak}

\letterlikechars\mathbfrak

\end{document}
\endinput
%%
%% End of file `mathfont_symbol_list.tex'.

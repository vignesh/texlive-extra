%%
%% This is file `README ',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% isodoc.dtx  (with options: `readme')
%% 
%% Copyright (C) 2006 by Wybo Dekker <wybo@dekkerdocumenten.nl>
%% 
%% This work may be distributed and/or modified under the conditions of
%% the LaTeX Project Public License, either version 1.3 of this license
%% or (at your option) any later version.  The latest version of this
%% license is in:
%% 
%%    http://www.latex-project.org/lppl.txt
%% 
%% and version 1.2 or later is part of all distributions of LaTeX version
%% 2003/12/01 or later.
%% 
|
-------:|:--------------------------------------
 isodoc:| LaTeX class used for typesetting of letters and invoices
 Author:| Wybo Dekker
 E-mail:| wybo@dekkerdocumenten.nl
License:| Released under the LaTeX Project Public License v1.3c or later
    See:| http://www.latex-project.org/lppl.txt

Short description:
The isodoc class is used for the preparation of letters and invoices.
Future versions will support the preparation of other, similar
documents. Documents are easily configured to the user's requirements
through key=value options.
At the origin of this class was Victor Eijkhout’s NTG brief class,
which implements the NEN1026 standard.
The package contains several examples, that are used in the documentation.
These can individually be compiled, which may be useful for users to
experiment with.

Full documentation:
https://bitbucket.org/wybodekker/isodoc/downloads/isodoc.pdf

Installation:
1. Copy isodoc.dtx and isodoc.ins in an empty directory
2. Run: «luatex isodoc.ins»
3. Run: «make help» for further information
\endinput
%%
%% End of file `README '.

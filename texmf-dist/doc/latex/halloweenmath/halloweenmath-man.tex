\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage{amsmath,amsfonts}
\usepackage{halloweenmath}[2019/11/01]
\usepackage{array}
\usepackage[bookmarksnumbered]{hyperref}

\title{User's manual\\for the \halloweenmath\ package}
\author{G. Mezzetti}
\date{November~1, 2019}

\hypersetup{
	pdftitle        = {User's manual for the halloweenmath package},
	pdfauthor       = {G. Mezzetti},
	pdfsubject      = {The halloweenmath LaTeX package},
	pdfkeywords     = {TeX,LaTeX,Halloween,math,math symbold},
	pdfcreationdate = {D:20191101120000},
	pdfmoddate      = {D:20191101120000}
}

\DeclareTextFontCommand{\packlass}{\normalfont\sffamily}
\DeclareTextFontCommand{\opt}     {\normalfont\ttfamily}
\newcommand*{\meta}[1]{{\normalfont \(\langle\textit{#1}\rangle\)}}
\newcommand*{\halloweenmath}{\packlass{halloweenmath}}
\newcommand*{\amsmath}      {\packlass{amsmath}}

\newcommand*{\sumxn}{x_{1}+\dots+x_{n}}

\makeatletter

\newcolumntype{H}[3]{>{\hb@xt@ #3\bgroup#1}c<{#2\egroup}}
\newcolumntype{U}[2]{H{\hss$#1}{$\hss}{#2}@{\hspace{.5em}}}
\newcolumntype{V}[1]{H{\ttfamily}{\hss}{#1}}
\newcolumntype{Q}[1]{H{}{\hss}{#1}}
\newcolumntype{O}{U{}{15\p@}}
\newcolumntype{D}{U{\displaystyle}{30\p@}}
\newcolumntype{L}{OD}
\newcolumntype{A}{U{}{40\p@}}

\newenvironment*{@symtable}[2]{\par
	\centering
	\addtolength\leftskip {-80\p@}%
	\addtolength\rightskip{-80\p@}%
	\setlength\arraycolsep{.5em}%
	\tabular{@{}*{#2}{#1}@{}}%
}{%
	\endtabular
	\par
}
\newenvironment{symtable}[1][\thr@@]{%
	\@symtable{OV{90\p@}}{#1}%
}{%
	\end@symtable
}
\newenvironment{opsymtable}[1][\tw@]{%
	\def\arraystretch{\tw@}%
	\@symtable{LV{120\p@}}{#1}%
}{%
	\end@symtable
}
\newenvironment{xasymtable}[1][\tw@]{%
	\def\arraystretch{\tw@}%
	\@symtable{AQ{200\p@}}{#1}%
}{%
	\end@symtable
}
\newcommand*\Sym[1]  {#1&\string#1}
\newcommand*\OpSym[1]{#1&#1&\string#1}

\makeatother



\begin{document}

\maketitle

\tableofcontents
\listoftables



\clearpage

\section{Package loading}
\label{S:Loading}

Load the \halloweenmath\ package as any other \LaTeXe\ package, that is, via the
usual \verb|\usepackage| declaration:
\begin{verbatim}
	\usepackage{halloweenmath}
\end{verbatim}
Note that the \halloweenmath\ package requires the \amsmath\ package, and loads
it (without specifying any option) if it is not already loaded.  If you want to
pass options to \amsmath, load it before \halloweenmath.

The \halloweenmath\ package defines no options by itself; nevertheless, it does
honor the [\opt{no}]\opt{sumlimits} options from the \amsmath\ package.



\section{Package usage}
\label{S:Usage}

The \halloweenmath\ package defines a handful of commands, all of which are
intended for use \emph{in mathematical mode}, where they yield some kind of
symbol that draws from the classic Halloween-related iconography (pumpkins,
witches, ghosts, bats, and so on).  Below, these symbols are grouped according
to their mathematical ``r\^{o}le'' (ordinary symbols, binary operators,
arrows\ldots).



\subsection{Ordinary symbols}
\label{sS:Ordinary}

Table~\ref{tab:ordinary} lists the ordinary symbols provided by the
\halloweenmath\ package.

\begin{table}[t!p]
	\centering
	\begin{symtable}\relax
		\Sym{\mathleftghost} & \Sym{\mathghost} & \Sym{\mathrightghost} \\
		\Sym{\mathleftbat} & \Sym{\mathbat} & \Sym{\mathrightbat} \\
	\end{symtable}
	\caption{Ordinary symbols}
	\label{tab:ordinary}
\end{table}



\subsection{Binary operators}
\label{sS:Binary}

Table~\ref{tab:binary} lists the binary operators available.  Note that each
binary operator has an associated ``large'' operator (see
subsection~\ref{sS:Large}).

\begin{table}[t!p]
	\centering
	\begin{symtable}[2]
		\Sym{\pumpkin} & \Sym{\skull} \\
	\end{symtable}
	\caption{Binary operators}
	\label{tab:binary}
\end{table}



\subsection{\textquotedblleft Large\textquotedblright\ operators}
\label{sS:Large}

Table~\ref{tab:large} lists the ``large'' operators.  Each of them is depicted
in two variants: the variant used for in-line math and the variant used for
displayed formulas.  In the table, besides the ``large'' operators called
\verb|\bigpumpkin|\footnote{As a homage to Linus van Pelt,
\texttt{\string\greatpumpkin} is defined as synonym of
\texttt{\string\bigpumpkin}.} and \verb|\bigskull|, which are correlated to the
binary operators \verb|\pumpkin| and \verb|\skull|, repectively, we find the
commands \verb|\mathwitch| and \verb|\reversemathwitch|: note how these two last
command have a \mbox{$*$-form} that adds a black cat on the broomstick.

\begin{table}[t!p]
	\centering
	\begin{opsymtable}\relax
		\OpSym{\mathwitch} & \OpSym{\reversemathwitch} \\
		\OpSym{\mathwitch*} & \OpSym{\reversemathwitch*} \\
		\OpSym{\bigpumpkin}\normalfont\footnotemark[\value{footnote}]
			& \OpSym{\bigskull} \\
	\end{opsymtable}
	\caption{\textquotedblleft Large\textquotedblright\ operators}
	\label{tab:large}
\end{table}

All the ``large'' operators listed in table~\ref{tab:large} honor the
[\opt{no}]\opt{sumlimits} options from the \amsmath\ package.



\subsection{\textquotedblleft Fraction-like\textquotedblright\ symbols}
\label{sS:Inner}

There are also two commands, listed on table~\ref{tab:inner}, that yield symbols
that are somewhat similar to fractions, in that they grow in size when they are
typeset in display style.\footnote{Another \TeX nical aspect of these commands
is that they yield an atom of type Inner.}  They are intended to denote 
an unspecified subformula that appears as a part of a larger one.

\begin{table}[t!p]
	\centering
	\begin{opsymtable}\relax
		\OpSym{\mathcloud} & \OpSym{\reversemathcloud} \\
	\end{opsymtable}
	\caption{\textquotedblleft Fraction-like\textquotedblright\ symbols}
	\label{tab:inner}
\end{table}



\subsection{\textquotedblleft Arrow-like\textquotedblright\ symbols}
\label{sS:Arrow}

As we'll see in subsection~\ref{sS:XArrow}, the \halloweenmath\ package provides
a series of commands whose usage parallels that of ``extensible arrows'' like
\verb|\xrightarrow| or \verb|\xleftarrow|; but the symbols that those commands
yield when used with an empty argument turn out to be too short, and it is for
this reason that the \halloweenmath\ package also offers you the four commands
you can see in table~\ref{tab:arrow}: they produce brooms, or pitchforks, having
fixed length, which is approximately the same size of a
\verb|\longrightarrow|~($\longrightarrow$).  All of these symbols are treated as
relations.

\begin{table}[t!p]
	\centering
	\begin{symtable}[2]
		\Sym{\leftbroom} & \Sym{\rightbroom} \\
		\Sym{\hmleftpitchfork} & \Sym{\hmrightpitchfork} \\
	\end{symtable}
	\caption{\textquotedblleft Arrow-like\textquotedblright\ symbols}
	\label{tab:arrow}
\end{table}



\subsection{Extensible \textquotedblleft arrow-like\textquotedblright\ symbols}
\label{sS:XArrow}

You are probably already familiar with the ``extensible arrows'' like
$\xrightarrow{abc}$ and~$\xleftarrow{abc}$; for example, you probably know that
the input
\begin{verbatim}
	\[
	    \bigoplus_{i=1}^{n} A_{i} \xrightarrow{f_{1}+\dots+f_{n}} B
	\]
\end{verbatim}
produces this result:
\[
	\bigoplus_{i=1}^{n} A_{i} \xrightarrow{f_{1}+\dots+f_{n}} B
\]
The \halloweenmath\ package features a whole assortment of extensible symbols of
this kind, which are listed in table~\ref{tab:xarrow}.  For example, you could
say
\begin{verbatim}
	\[
	    G \xrightswishingghost{h_{1}+\dots+h_{n}}
	            \bigpumpkin_{t=1}^{n} S_{t}
	\]
\end{verbatim}
to get the following in print:
\[
	G \xrightswishingghost{h_{1}+\dots+h_{n}}
			\bigpumpkin_{t=1}^{n} S_{t}
\]

\begin{table}[t!p]
	\centering
	\begin{xasymtable}\relax
		\xleftwitchonbroom{abc\dots z}
					& \verb|\xleftwitchonbroom{abc\dots z}| &
		\xrightwitchonbroom{abc\dots z}
					& \verb|\xrightwitchonbroom{abc\dots z}| \\
		\xleftwitchonbroom*{abc\dots z}
					& \verb|\xleftwitchonbroom*{abc\dots z}| &
		\xrightwitchonbroom*{abc\dots z}
					& \verb|\xrightwitchonbroom*{abc\dots z}| \\
		\xleftwitchonpitchfork{abc\dots z}
					& \verb|\xleftwitchonpitchfork{abc\dots z}| &
		\xrightwitchonpitchfork{abc\dots z}
					& \verb|\xrightwitchonpitchfork{abc\dots z}| \\
		\xleftwitchonpitchfork*{abc\dots z}
					& \verb|\xleftwitchonpitchfork*{abc\dots z}| &
		\xrightwitchonpitchfork*{abc\dots z}
					& \verb|\xrightwitchonpitchfork*{abc\dots z}| \\
		\xleftbroom{abc\dots z}
					& \verb|\xleftbroom{abc\dots z}| &
		\xrightbroom{abc\dots z}
					& \verb|\xrightbroom{abc\dots z}| \\
		\xleftpitchfork{abc\dots z}
					& \verb|\xleftpitchfork{abc\dots z}| &
		\xrightpitchfork{abc\dots z}
					& \verb|\xrightpitchfork{abc\dots z}| \\
		\xleftswishingghost{abc\dots z}
					& \verb|\xleftswishingghost{abc\dots z}| &
		\xrightswishingghost{abc\dots z}
					& \verb|\xrightswishingghost{abc\dots z}| \\
		\xleftflutteringbat{abc\dots z}
					& \verb|\xleftflutteringbat{abc\dots z}| &
		\xrightflutteringbat{abc\dots z}
					& \verb|\xrightflutteringbat{abc\dots z}| \\
	\end{xasymtable}
	\caption{Extensible \textquotedblleft arrow-like\textquotedblright\ symbols}
	\label{tab:xarrow}
\end{table}

More generally, exactly as the commands \verb|\xleftarrow| and
\verb|\xrightarrow|, on which they are modeled, all the commands listed in
table~\ref{tab:xarrow} take one optional argument, in which you can specify a
subscript, and one mandatory argument, where a---possibly empty---superscript
must be indicated.  For example,
\begin{verbatim}
	\[
	    A \xrightwitchonbroom*[abc\dots z]{f_{1}+\dots+f_{n}} B
	        \xrightwitchonbroom*{f_{1}+\dots+f_{n}} C
	        \xrightwitchonbroom*[abc\dots z]{} D
	\]  
\end{verbatim}
results in
\[
	A \xrightwitchonbroom*[abc\dots z]{f_{1}+\dots+f_{n}} B
		\xrightwitchonbroom*{f_{1}+\dots+f_{n}} C
		\xrightwitchonbroom*[abc\dots z]{} D
\]

Note that, also in this family of symbols, the commands that involve a witch all
provide a \mbox{$*$-form} that adds a cat on the broom (or pitchfork).

The commands listed above should not be confused with those presented in
subsection~\ref{sS:OUArrow}.



\subsection{Extensible \textquotedblleft over-\protect\slash
	under-arrow-like\textquotedblright\ symbols}
\label{sS:OUArrow}

The commands dealt with in subsection~\ref{sS:XArrow} typeset an extensible
``arrow-like'' symbol having some math above or below it.  But the \amsmath\
package also provides commands that act the other way around, that is, they put
an arrow over, or under, some math, as in the case of
\begin{verbatim}
	\overrightarrow{x_{1}+\dots+x_{n}}
\end{verbatim}
that yields \( \overrightarrow{x_{1}+\dots+x_{n}} \).  The \halloweenmath\ 
package provides a whole bunch of commands like this, which are listed in 
table~\ref{tab:ouarrow}, and which all share the same syntax as the 
\verb|\overrightarrow| command.

\begin{table}[t!p]
	\centering
	\begin{xasymtable}\relax
		\overleftwitchonbroom{abc\dots z}
					& \verb|\overleftwitchonbroom{abc\dots z}| &
		\overrightwitchonbroom{abc\dots z}
					& \verb|\overrightwitchonbroom{abc\dots z}| \\
		\overleftwitchonbroom*{abc\dots z}
					& \verb|\overleftwitchonbroom*{abc\dots z}| &
		\overrightwitchonbroom*{abc\dots z}
					& \verb|\overrightwitchonbroom*{abc\dots z}| \\
		\overleftwitchonpitchfork{abc\dots z}
					& \verb|\overleftwitchonpitchfork{abc\dots z}| &
		\overrightwitchonpitchfork{abc\dots z}
					& \verb|\overrightwitchonpitchfork{abc\dots z}| \\
		\overleftwitchonpitchfork*{abc\dots z}
					& \verb|\overleftwitchonpitchfork*{abc\dots z}| &
		\overrightwitchonpitchfork*{abc\dots z}
					& \verb|\overrightwitchonpitchfork*{abc\dots z}| \\
		\overleftbroom{abc\dots z}
					& \verb|\overleftbroom{abc\dots z}| &
		\overrightbroom{abc\dots z}
					& \verb|\overrightbroom{abc\dots z}| \\
		\overscriptleftbroom{abc\dots z}
					& \verb|\overscriptleftbroom{abc\dots z}| &
		\overscriptrightbroom{abc\dots z}
					& \verb|\overscriptrightbroom{abc\dots z}| \\
		\overleftpitchfork{abc\dots z}
					& \verb|\overleftpitchfork{abc\dots z}| &
		\overrightpitchfork{abc\dots z}
					& \verb|\overrightpitchfork{abc\dots z}| \\
		\overscriptleftpitchfork{abc\dots z}
					& \verb|\overscriptleftpitchfork{abc\dots z}| &
		\overscriptrightpitchfork{abc\dots z}
					& \verb|\overscriptrightpitchfork{abc\dots z}| \\
		\overleftswishingghost{abc\dots z}
					& \verb|\overleftswishingghost{abc\dots z}| &
		\overrightswishingghost{abc\dots z}
					& \verb|\overrightswishingghost{abc\dots z}| \\
		\overleftflutteringbat{abc\dots z}
					& \verb|\overleftflutteringbat{abc\dots z}| &
		\overrightflutteringbat{abc\dots z}
					& \verb|\overrightflutteringbat{abc\dots z}| \\
		\underleftwitchonbroom{abc\dots z}
					& \verb|\underleftwitchonbroom{abc\dots z}| &
		\underrightwitchonbroom{abc\dots z}
					& \verb|\underrightwitchonbroom{abc\dots z}| \\
		\underleftwitchonbroom*{abc\dots z}
					& \verb|\underleftwitchonbroom*{abc\dots z}| &
		\underrightwitchonbroom*{abc\dots z}
					& \verb|\underrightwitchonbroom*{abc\dots z}| \\
		\underleftwitchonpitchfork{abc\dots z}
					& \verb|\underleftwitchonpitchfork{abc\dots z}| &
		\underrightwitchonpitchfork{abc\dots z}
					& \verb|\underrightwitchonpitchfork{abc\dots z}| \\
		\underleftwitchonpitchfork*{abc\dots z}
					& \verb|\underleftwitchonpitchfork*{abc\dots z}| &
		\underrightwitchonpitchfork*{abc\dots z}
					& \verb|\underrightwitchonpitchfork*{abc\dots z}| \\
		\underleftbroom{abc\dots z}
					& \verb|\underleftbroom{abc\dots z}| &
		\underrightbroom{abc\dots z}
					& \verb|\underrightbroom{abc\dots z}| \\
		\underscriptleftbroom{abc\dots z}
					& \verb|\underscriptleftbroom{abc\dots z}| &
		\underscriptrightbroom{abc\dots z}
					& \verb|\underscriptrightbroom{abc\dots z}| \\
		\underleftpitchfork{abc\dots z}
					& \verb|\underleftpitchfork{abc\dots z}| &
		\underrightpitchfork{abc\dots z}
					& \verb|\underrightpitchfork{abc\dots z}| \\
		\underscriptleftpitchfork{abc\dots z}
					& \verb|\underscriptleftpitchfork{abc\dots z}| &
		\underscriptrightpitchfork{abc\dots z}
					& \verb|\underscriptrightpitchfork{abc\dots z}| \\
		\underleftswishingghost{abc\dots z}
					& \verb|\underleftswishingghost{abc\dots z}| &
		\underrightswishingghost{abc\dots z}
					& \verb|\underrightswishingghost{abc\dots z}| \\
		\underleftflutteringbat{abc\dots z}
					& \verb|\underleftflutteringbat{abc\dots z}| &
		\underrightflutteringbat{abc\dots z}
					& \verb|\underrightflutteringbat{abc\dots z}| \\
	\end{xasymtable}
	\caption{Extensible \textquotedblleft over-\protect\slash
				under-arrow-like\textquotedblright\ symbols}
	\label{tab:ouarrow}
\end{table}

Although they are not extensible, and are thus more similar to math accents, we
have chosen to include in this subsection also the commands listed in
table~\ref{tab:oubat}.  They typeset a subformula either surmounted by the bat
produced by \verb|\mathbat|, or with that symbol underneath.  Their normal
(\emph{i.e.}, unstarred) form pretends that the bat has zero width (but some
height), whereas the starred variant takes the actual width of the bat be into
account; for example, given the input
\begin{verbatim}
	\begin{align*}
	    &x+y+z && x+y+z \\
	    &x+\overbat{y}+z && x+\overbat*{y}+z
	\end{align*}
\end{verbatim}
compare the spacing you get in the two columns of the output:
\begin{align*}
	&x+y+z && x+y+z \\
	&x+\overbat{y}+z && x+\overbat*{y}+z
\end{align*}

\begin{table}[t!p]
	\centering
	\begin{xasymtable}\relax
		\overbat{xyz}  & \verb|\overbat{xyz}|  &
		\underbat{xyz} & \verb|\underbat{xyz}| \\
	\end{xasymtable}
	\caption{Over\protect\slash under bats}
	\label{tab:oubat}
\end{table}



\subsection{Script-style versions of
	\texorpdfstring{\amsmath}{amsmath}\textquoteright s
	over\protect\slash under arrows}
\label{sS:ScriptArrow}

The commands listed in table~\ref{tab:scriptarrow} all produce an output similar
to that of the corresponding \amsmath's command having the same name, but
stripped of the \texttt{script} substring, with the only difference that the
size of the arrow is smaller.  More precisely, they use for the arrow the
relative script size of the current size (that is, of the size in which their
argument is typeset).  For example, whilst \verb|\overrightarrow{x+y+z}| yields
$\overrightarrow{x+y+z}$, \verb|\overscriptrightarrow{x+y+z}| results in
$\overscriptrightarrow{x+y+z}$ (do you see the difference?), which, in the
author's humble opinion, looks \emph{much} better.

\begin{table}[t!p]
	\centering
	\begin{xasymtable}\relax
		\overscriptleftarrow{abc\dots z}
					& \verb|\overscriptleftarrow{abc\dots z}| &
		\underscriptleftarrow{abc\dots z}
					& \verb|\underscriptleftarrow{abc\dots z}| \\
		\overscriptrightarrow{abc\dots z}
					& \verb|\overscriptrightarrow{abc\dots z}| &
		\underscriptrightarrow{abc\dots z}
					& \verb|\underscriptrightarrow{abc\dots z}| \\
		\overscriptleftrightarrow{abc\dots z}
					& \verb|\overscriptleftrightarrow{abc\dots z}| &
		\underscriptleftrightarrow{abc\dots z}
					& \verb|\underscriptleftrightarrow{abc\dots z}| \\
	\end{xasymtable}
	\caption{Extensible over\protect\slash under arrows with reduced size}
	\label{tab:scriptarrow}
\end{table}



\clearpage

\section{Examples of use}
\label{S:Example}

This section illustrates the use of the commands provided by the \halloweenmath\
package: by reading the source code for this document, you can see how the
output presented below can be obtained.



\subsection{Applying black magic}

The $\mathwitch$~symbol was invented with the intent to provide a notation for
the operation of applying black magic to a formula.  Its applications range from
simple reductions sometimes made by certain undergraduate freshmen, as in
\[
	\mathwitch 2\sin\frac{x}{2} = \sin x
\]
to key steps that permit to simplify greatly the proof of an otherwise totally
impenetrable theorem, for example
\[
	\mathwitch\Bigl(
		\sup\,\{\,p\in\mathbb{N}\mid \text{$p$ and $p+2$ are both prime}\,\}
	\Bigr) = \infty
\]
Another way of denoting the same operation is to place the broom and the witch
\emph{over} the relevant subformula:
\[
	\overrightwitchonbroom{
		\sup\,\{\,p\in\mathbb{N}\mid \text{$p$ and $p+2$ are both prime}\,\}
	} = \infty
\]

Different types of magic, that you might want to apply to a given formula, can
be distinguished by adding a black cat on the broom: for example, a student
could claim that
\begin{align*}
	\mathwitch 2x\sin x &= 2\sin x^{2}\\
	\intertext{whereas, for another student,}
	\mathwitch*2x\sin x &= \sin 3x
\end{align*}



\subsection{Monoids}

Let $X$ be a non-empty set, and suppose there exists a map
\begin{equation}
	X\times X\longrightarrow X,\qquad
	(x,y)\longmapsto P(x,y)=x\pumpkin y
	\label{eq:Pdef}
\end{equation}
Suppose furthermore that this map satisfies the \textbf{associative property}
\begin{equation}
	\forall x\in X,\;\forall y\in X,\;\forall z\in X\qquad
	x\pumpkin(y\pumpkin z) = (x\pumpkin y)\pumpkin z
	\label{eq:Passoc}
\end{equation}
Then, the pair $(X,\pumpkin)$ is called a \textbf{semigroup}, and $\pumpkin$
denotes its \textbf{operation}.  If, in addition, there exists in~$X$ an
element~$\mathghost$ with the property that
\begin{equation}
	\forall x\in X\qquad
	\mathghost\pumpkin x = x = x\pumpkin\mathghost
	\label{eq:Punit}
\end{equation}
the triple $(X,\pumpkin,\mathghost)$ is called a \textbf{monoid}, and the
element~$\mathghost$ is called its \textbf{unit}.  It is immediate to prove that
the unit of a monoid is unique: indeed, if $\mathghost'$ is another element
of~$X$ having the property~\eqref{eq:Punit}, then
\[
	\mathghost' = \mathghost'\pumpkin\mathghost = \mathghost
\]
(the first equality holds because \( \mathghost'\in X \) and
$\mathghost$~satisfies~\eqref{eq:Punit}, and the second because \( \mathghost\in
X \) and $\mathghost'$~satisfies~\eqref{eq:Punit}).

Let $(X,\pumpkin,\mathghost)$ be a monoid.  Since its operation~$\pumpkin$ is
associative, we may set, for \( x,y,z\in X \),
\[
	x\pumpkin y\pumpkin z =_{\mathrm{def}}
		(x\pumpkin y)\pumpkin z = x\pumpkin (y\pumpkin z)
\]
More generally, since the order in which the operations are performed doesn't
matter, given $n$~elements \( x_{1},\dots,x_{n} \in X \), with \( n \in
\mathbb{N} \), the result of
\[
	\bigpumpkin_{i=1}^{n} x_{i} = x_{1}\pumpkin\dots\pumpkin x_{n}
\]
is unambiguously defined (it being~$\mathghost$ if \( n=0 \)).

A monoid $(X,\pumpkin,\mathghost)$ is said to be \textbf{commutative} if
\begin{equation}
	\forall x\in X,\;\forall y\in X\qquad
	x\pumpkin y = y\pumpkin x
	\label{eq:Pcomm}
\end{equation}
In this case, even the order of the \emph{operands} becomes irrelevant, so that,
for any finite (possibly empty) set~$F$, the notation \( \bigpumpkin_{i\in F}
x_{i} \) also acquires a meaning.



\subsection{Applications induced on power sets}

If $X$ is a set, we'll denote by~$\wp(X)$ the set of all subsets of~$X$, that is
\[
	\wp(X) = \{\,S:S\subseteq X\,\}
\]

Let \( f\colon A\longrightarrow B \) a function.  Starting from~$f$, we can
define two other functions \( f^{\mathrightbat} \colon \wp(A) \longrightarrow
\wp(B) \) and \( f^{\mathleftbat} \colon \wp(B) \longrightarrow \wp(A) \) in the
following way:
\begin{alignat}{2}
	&\text{for $X\subseteq A$,}\quad
		&f^{\mathrightbat}(X) &= \{\,f(x):x\in X\,\} \\
	&\text{for $Y\subseteq B$,}\quad
		&f^{\mathleftbat}(Y) &= \{\,x\in A:f(x)\in Y\,\}
\end{alignat}
In the case of functions with long names, or with long descriptions, we'll also
use a notation like \( \overrightflutteringbat{f_{1}+\dots+f_{n}} \) to mean the
same thing as \( (f_{1}+\dots+f_{n})^{\mathrightbat} \).

For example,
\begin{align*}
	\mathop{\overrightflutteringbat{\sin}}(\mathbb{R}) &= [-1,1] \\
	\mathop{\overrightflutteringbat{\sin}}\bigl([0,\pi]\bigr) &= [0,1] \\
	\mathop{\overleftflutteringbat{\arcsin}}
				\Bigl(\bigl[0,\tfrac{\pi}{2}\bigr]\Bigr) &= [0,1] \\
	\mathop{\overrightflutteringbat{\sin+\cos}}(\mathbb{R})
		&= \bigl[-\sqrt{2},\sqrt{2}\,\bigr] \\
	\mathop{\overleftflutteringbat{\log}}\bigl(\mathopen]-\infty,0]\bigr)
		&= \mathopen]0,1]
\end{align*}



\subsection{A comprehensive test}

A comparison between the ``standard'' and the ``script'' extensible over\slash
under arrows:
\begin{align*}
	\overrightarrow{f_{1}+\dots+f_{n}}
		&\neq\overscriptrightarrow{f_{1}+\dots+f_{n}}  \\
	\overleftarrow{f_{1}+\dots+f_{n}}
		&\neq\overscriptleftarrow{f_{1}+\dots+f_{n}}  \\
	\overleftrightarrow{f_{1}+\dots+f_{n}}
		&\neq\overscriptleftrightarrow{f_{1}+\dots+f_{n}}  \\
	\underrightarrow{f_{1}+\dots+f_{n}}
		&\neq\underscriptrightarrow{f_{1}+\dots+f_{n}}  \\
	\underleftarrow{f_{1}+\dots+f_{n}}
		&\neq\underscriptleftarrow{f_{1}+\dots+f_{n}}  \\
	\underleftrightarrow{f_{1}+\dots+f_{n}}
		&\neq\underscriptleftrightarrow{f_{1}+\dots+f_{n}}
\end{align*}

\bigbreak

A reduction my students are likely to make:
\[\mathwitch \frac{\sin x}{s} = x\,\mathrm{in}\]
The same reduction as an in-line formula:
\(\mathwitch \frac{\sin x}{s} = x\,\mathrm{in}\).

Now with limits:
\[
	\mathwitch_{i=1}^{n} \frac
		{\text{$i$-th magic term}}
		{\text{$2^{i}$-th wizardry}}
\]
And repeated in-line: \( \mathwitch_{i=1}^{n} x_{i}y_{i} \).

The \texttt{bold} math version is honored:\mathversion{bold}
\[
	\mathwitch*
		\genfrac{<}{>}{0pt}{}
			{\textbf{something terribly}}{\textbf{complicated}}
	= 0
\]
Compare it with \texttt{normal} math\mathversion{normal}:
\[
	\mathwitch*
		\genfrac{<}{>}{0pt}{}
			{\text{something terribly}}{\text{complicated}}
	= 0
\]
In-line math comparison:
{\boldmath $\mathwitch* f(x)$} versus $\mathwitch* f(x)$.

There is also a left-facing witch:
\[\reversemathwitch \frac{\sin x}{s} = x\,\mathrm{in}\]
And here is the in-line version:
\(\reversemathwitch \frac{\sin x}{s} = x\,\mathrm{in}\).

Test for \verb|\dots|:
\[
	\mathwitch_{i_{1}=1}^{n_{1}} \dots \mathwitch_{i_{p}=1}^{n_{p}}
	\frac
		{\text{$i_{1}$-th magic factor}}
		{\text{$2^{i_{1}}$-th wizardry}}
	\pumpkin\dots\pumpkin
	\frac
		{\text{$i_{p}$-th magic factor}}
		{\text{$2^{i_{p}}$-th wizardry}}
\]
And repeated in-line: \( \mathwitch\dots\mathwitch_{i=1}^{n} x_{i}y_{i} \).

\bigbreak

Now the pumpkins.  First the \texttt{bold} math version:\mathversion{bold}:
\[ \bigoplus_{h=1}^{m}\bigpumpkin_{k=1}^{n} P_{h,k} \]
Then the \texttt{normal} one\mathversion{normal}:
\[ \bigoplus_{h=1}^{m}\bigpumpkin_{k=1}^{n} P_{h,k} \]
In-line math comparison:
{\boldmath \( \bigpumpkin_{i=1}^{n} P_{i} \neq \bigoplus_{i=1}^{n} P_{i} \)}
versus \( \bigpumpkin_{i=1}^{n} P_{i} \neq \bigoplus_{i=1}^{n} P_{i} \).

Close test: {\boldmath $\bigoplus$}$\bigoplus$.
And against the pumpkins:
{\boldmath $\bigpumpkin$}$\bigpumpkin\bigoplus${\boldmath $\bigoplus$}.

In-line, but with \verb|\limits|:
\( \bigoplus\limits_{h=1}^{m}\bigpumpkin\limits_{k=1}^{n} P_{h,k} \).

Binary: \( x\pumpkin y \neq x\oplus y \).  And in display:
\[ a\pumpkin\frac{x\pumpkin y}{x\oplus y}\otimes b \]
Close test: {\boldmath $\oplus$}$\oplus$.
And with the pumpkins too:
{\boldmath $\pumpkin$}$\pumpkin\oplus${\boldmath $\oplus$}.

In general,
\[ \bigpumpkin_{i=1}^{n} P_{i} = P_{1}\pumpkin\dots\pumpkin P_{n} \]

\begingroup

\bfseries\boldmath

The same in bold:
\[ \bigpumpkin_{i=1}^{n} P_{i} = P_{1}\pumpkin\dots\pumpkin P_{n} \]

\endgroup

Other styles: \( \frac{x\pumpkin y}{2} \), exponent~$Z^{\pumpkin}$, 
subscript~$W_{\!x\pumpkin y}$, double script \( 2^{t_{x\pumpkin y}} \).

\bigbreak

Clouds.  A hypothetical identity:
\( \frac{\sin^{2}x + \cos^{2}x}{\cos^{2}x} = \mathcloud \).
Now the same identity set in display:
\[ \frac{\sin^{2}x + \cos^{2}x}{\cos^{2}x} = \mathcloud \]
Now in smaller size: \( \frac{\sin x+\cos x}{\mathcloud} = 1 \).

Specular clouds, \texttt{bold}\ldots\mathversion{bold}
\[ \reversemathcloud \longleftrightarrow \mathcloud \]
\ldots and in \texttt{normal} math.\mathversion{normal}
\[ \reversemathcloud \longleftrightarrow \mathcloud \]
In-line math comparison:
{\boldmath \( \reversemathcloud \leftrightarrow \mathcloud \)}
versus \( \reversemathcloud \leftrightarrow \mathcloud \).
Abutting: {\boldmath $\mathcloud$}$\mathcloud$.

\bigbreak

Ghosts: \( \mathleftghost \mathghost \mathrightghost \mathghost \mathleftghost
\mathghost \mathrightghost \).  Now with letters: \( H \mathghost H \mathghost h
\mathghost ab \mathghost f \mathghost wxy \mathghost \), and also \(
2\mathghost^{3} + 5\mathleftghost^{\!2}-3\mathrightghost_{i} =
12\mathrightghost_{j}^{4} \).  Then, what about~$x^{2\mathghost}$ and \(
z_{\!\mathrightghost+1} = z_{\!\mathrightghost}^{2} + z_{\mathghost} \)?

In subscripts:
\begin{align*}
	F_{\mathghost+2} &= F_{\mathghost+1} + F_{\mathghost} \\
	F_{\!\mathrightghost+2} &= F_{\!\mathrightghost+1} + F_{\!\mathrightghost}
\end{align*}
Another test: \( \mathghost | \mathrightghost | \mathghost | \mathleftghost |
\mathghost | \mathrightghost | \mathghost | \mathleftghost | \mathghost \).  We
should also try this: \( \mathrightghost \mathleftghost \mathrightghost
\mathleftghost \).

Let us now compare ghosts set in normal math
\( \mathrightghost \mathleftghost \mathghost \mathrightghost \mathleftghost \)
with (a few words to push the bold ghosts to the right)
{\bfseries\boldmath ghosts like these
\( \mathrightghost \mathleftghost \mathghost \mathrightghost \mathleftghost \),
which are set in bold math.}

\bigbreak

Extensible arrows:
\begin{gather*}
	A \xrightwitchonbroom[a]{\sumxn} B \xrightwitchonbroom{x+z}
		C \xrightwitchonbroom{} D  \\
	A \xrightwitchonbroom*[a]{\sumxn} B \xrightwitchonbroom*{x+z}
		C \xrightwitchonbroom*{} D  \\
	A \xleftwitchonbroom*[a]{\sumxn} B \xleftwitchonbroom*{x+z}
		C \xleftwitchonbroom*{} D  \\
	A \xleftwitchonbroom[a]{\sumxn} B \xleftwitchonbroom{x+z}
		C \xleftwitchonbroom{} D
\end{gather*}
And \( \overrightwitchonbroom*{\sumxn}=0 \) versus \(
\overrightwitchonbroom{\sumxn}=0 \); or \( \overleftwitchonbroom*{\sumxn}=0 \)
versus \( \overleftwitchonbroom{\sumxn}=0 \).

\begingroup

\bfseries \mathversion{bold}

Now repeat in bold:
\begin{gather*}
	A \xrightwitchonbroom[a]{\sumxn} B \xrightwitchonbroom{x+z}
		C \xrightwitchonbroom{} D  \\
	A \xrightwitchonbroom*[a]{\sumxn} B \xrightwitchonbroom*{x+z}
		C \xrightwitchonbroom*{} D  \\
	A \xleftwitchonbroom*[a]{\sumxn} B \xleftwitchonbroom*{x+z}
		C \xleftwitchonbroom*{} D  \\
	A \xleftwitchonbroom[a]{\sumxn} B \xleftwitchonbroom{x+z}
		C \xleftwitchonbroom{} D
\end{gather*}
And \( \overrightwitchonbroom*{\sumxn}=0 \) versus \(
\overrightwitchonbroom{\sumxn}=0 \); or \( \overleftwitchonbroom*{\sumxn}=0 \)
versus \( \overleftwitchonbroom{\sumxn}=0 \).

\endgroup

Hovering ghosts: \( \overrightswishingghost{\sumxn}=0 \).  I~wonder whether
there is enough space left for the swishing ghost; let's try again:
\( \overrightswishingghost{(\sumxn)y}=0 \)!  Yes, it looks like there is enough
room, although, of course, we cannot help the line spacing going awry.  Also try
\( \overrightswishingghost{\mathstrut} \).
\begin{gather*}
	A \xrightswishingghost[a]{\sumxn} B \xrightswishingghost{x+z} C
		\xrightswishingghost{} D  \\
	A \xleftswishingghost[a]{\sumxn} B \xleftswishingghost{x+z} C
		\xleftswishingghost{} D
\end{gather*}
Another hovering ghost: \( \overleftswishingghost{\sumxn}=0 \).
Lorem ipsum dolor sit amet consectetur adipisci elit.  Ulla rutrum, vel sivi sit
anismus oret, rubi sitiunt silvae.  Let's see how it looks like when the ghost
hovers on a taller formula, as in \(
\overrightswishingghost{H_{1}\oplus\dots\oplus H_{k}} \).  Mmm, it's suboptimal,
to say the least.\footnote{We'd better try \(
\underleftswishingghost{y_{1}+\dots+y_{n}} \), too; well, this one looks good!}

Under ``arrow-like'' symbols: \( \underleftswishingghost{\sumxn}=0 \) and \(
\underrightswishingghost{x+y+z} \).  There are \(
\underleftwitchonbroom*{\sumxn}=0 \) and \( \underrightwitchonbroom*{x+y+z} \)
as well.

Compare \( A\xrightswishingghost{\sumxn} B \) with (add a few words to push it
to the next line) {\bfseries\boldmath its bold version \(
A\xrightswishingghost{\sumxn} B \).}

\bigbreak

Bats: $\mathbat${\boldmath $\mathbat$}.  We are interested in seeing whether a
bat affixed to a letter as an exponent causes the lines of a paragraph to be
further apart than usual.  Therefore, we now try~$f^{\mathbat}$, also
{\bfseries\boldmath in bold~$f^{\mathbat}$,} then we type a few more words (just
enough to obtain another typeset line or two) in order to see what happens.  We
need to look at the transcript file, to check the outcome of the following
tracing commands.

% \begingroup
%     \showboxbreadth = 1000
%     \showboxdepth = 0 % show just the lines, not their contents
%     \showlists
% \endgroup

Asymmetric bats: $\mathleftbat${\boldmath $\mathleftbat$}, and also
$\mathrightbat${\boldmath $\mathrightbat$}.  Exponents: this is \texttt{normal}
math \( x^{\mathleftbat} \pumpkin y^{\mathrightbat} \), while
{\bfseries\boldmath this is \texttt{bold} math \( x^{\mathleftbat} \pumpkin
y^{\mathrightbat} \).} Do you note the difference?  Let's try subscripts, too:
\( f_{\mathleftbat} \pumpkin g_{\mathrightbat} \) versus {\bfseries\boldmath
bold \( f_{\mathleftbat} \pumpkin g_{\mathrightbat} \).}
Now, keep on repeating some silly text, just in order to fill up the paragraph
with a sufficient number of lines.  Now, keep on repeating some silly text, just
in order to fill up the paragraph with a sufficient number of lines.  Now, keep
on repeating some silly text, just in order to fill up the paragraph with a
sufficient number of lines.  That's enough!

Hovering bats: \( \overrightflutteringbat{\sumxn}=0 \).  I~wonder whether there
is enough space left for the swishing bat; let's try again:
\( \overrightflutteringbat{(\sumxn)y}=0 \)!  Yes, it looks like there is enough
room (with the usual remark abut line spacing).  Also try
\( \overrightflutteringbat{\mathstrut} \).
\begin{gather*}
	A \xrightflutteringbat[a]{\sumxn} B \xrightflutteringbat{x+z}
		C \xrightflutteringbat{} D  \\
	A \xleftflutteringbat[a]{\sumxn} B \xleftflutteringbat{x+z} C
		\xleftflutteringbat{} D
\end{gather*}
Another hovering bat: \( \overleftflutteringbat{\sumxn}=0 \).

Under ``arrow-like'' bats: \( \underleftflutteringbat{\sumxn}=0 \) and \(
\underrightflutteringbat{x+y+z} \).

Compare \( A\xrightflutteringbat{\sumxn} B \) with (add a few words to push it
to the next line) {\bfseries\boldmath its bold version \(
A\xrightflutteringbat{\sumxn} B \).}

Test for checking the placement of the formulas that go over or under the
fluttering bat:
\begin{gather*}
	A \xrightflutteringbat[\text{a long subscript}]{\text{a long superscript}} B
		\xrightflutteringbat[\text{a long subscript}]{|} C
		\xrightflutteringbat{|} D \xrightflutteringbat{} E  \\
	A \xleftflutteringbat[\text{a long subscript}]{\text{a long superscript}} B
		\xleftflutteringbat[\text{a long subscript}]{|} C
		\xleftflutteringbat{|} D \xleftflutteringbat{} E
\end{gather*}
I'd say it's now OK\@\ldots

\bigbreak

Extensible arrows with pitchfork:
\begin{gather*}
	A \xrightwitchonpitchfork[a]{\sumxn} B \xrightwitchonpitchfork{x+z} C
		\xrightwitchonpitchfork{} D  \\
	A \xrightwitchonpitchfork*[a]{\sumxn} B \xrightwitchonpitchfork*{x+z} C
		\xrightwitchonpitchfork*{} D  \\
	A \xleftwitchonpitchfork*[a]{\sumxn} B \xleftwitchonpitchfork*{x+z} C
		\xleftwitchonpitchfork*{} D  \\
	A \xleftwitchonpitchfork[a]{\sumxn} B \xleftwitchonpitchfork{x+z} C
		\xleftwitchonpitchfork{} D
\end{gather*}
And \( \overrightwitchonpitchfork*{\sumxn}=0 \) versus \(
\overrightwitchonpitchfork{\sumxn}=0 \); or \(
\overleftwitchonpitchfork*{\sumxn}=0 \) versus \(
\overleftwitchonpitchfork{\sumxn}=0 \).  There are \(
\underleftwitchonpitchfork*{\sumxn}=0 \) and \(
\underrightwitchonpitchfork*{x+y+z} \) as well.

\begingroup

\bfseries \mathversion{bold}

Now again, but all in boldface:
\begin{gather*}
	A \xrightwitchonpitchfork[a]{\sumxn} B \xrightwitchonpitchfork{x+z} C
		\xrightwitchonpitchfork{} D  \\
	A \xrightwitchonpitchfork*[a]{\sumxn} B \xrightwitchonpitchfork*{x+z} C
		\xrightwitchonpitchfork*{} D  \\
	A \xleftwitchonpitchfork*[a]{\sumxn} B \xleftwitchonpitchfork*{x+z} C
		\xleftwitchonpitchfork*{} D  \\
	A \xleftwitchonpitchfork[a]{\sumxn} B \xleftwitchonpitchfork{x+z} C
		\xleftwitchonpitchfork{} D
\end{gather*}
And \( \overrightwitchonpitchfork*{\sumxn}=0 \) versus \(
\overrightwitchonpitchfork{\sumxn}=0 \); or \(
\overleftwitchonpitchfork*{\sumxn}=0 \) versus \(
\overleftwitchonpitchfork{\sumxn}=0 \).  There are \(
\underleftwitchonpitchfork*{\sumxn}=0 \) and \(
\underrightwitchonpitchfork*{x+y+z} \) as well.

\endgroup

The big table of the rest:
\begin{align*}
	A &\xrightbroom{\sumxn} B &
			\overrightbroom {\sumxn} = 0 &&
			\underrightbroom{\sumxn} = 0 \\
		&&
			\overscriptrightbroom {\sumxn} = 0 &&
			\underscriptrightbroom{\sumxn} = 0 \\
	A &\xleftbroom{\sumxn} B &
			\overleftbroom {\sumxn} = 0 &&
			\underleftbroom{\sumxn} = 0 \\
		&&
			\overscriptleftbroom {\sumxn} = 0 &&
			\underscriptleftbroom{\sumxn} = 0 \\
	A &\xrightpitchfork{\sumxn} B &
			\overrightpitchfork {\sumxn} = 0 &&
			\underrightpitchfork{\sumxn} = 0 \\
		&&
			\overscriptrightpitchfork {\sumxn} = 0 &&
			\underscriptrightpitchfork{\sumxn} = 0 \\
	A &\xleftpitchfork{\sumxn} B &
			\overleftpitchfork {\sumxn} = 0 &&
			\underleftpitchfork{\sumxn} = 0 \\
		&&
			\overscriptleftpitchfork {\sumxn} = 0 &&
			\underscriptleftpitchfork{\sumxn} = 0 \\
\end{align*}

Now in bold\ldots\space No, please, seriously, just the examples for the minimal
size: in \texttt{normal} math we show \( A \xrightbroom{} B \) and \( C
\xleftpitchfork{} D \) and \( \overscriptrightbroom{} \) and \(
\overscriptleftpitchfork{} \), which we now repeat {\bfseries\boldmath in
\texttt{bold} math \( A \xrightbroom{} B \) and \( C \xleftpitchfork{} D \) and
\( \overscriptrightbroom{} \) and \( \overscriptleftpitchfork{} \).} Mmmh, the 
minimal size seems way too narrow: is it the same for the standard arrows?  
Let's see:
\begin{align*}
	A &\xrightarrow{} B & \overrightarrow{} && \overscriptrightarrow{} \\
	A &\xleftarrow {} B & \overleftarrow {} && \overscriptleftarrow {} \\
	A &\xrightbroom{} B & \overrightbroom{} && \overscriptrightbroom{} \\
	A &\xleftbroom {} B & \overleftbroom {} && \overscriptleftbroom {}
\end{align*}
Well, almost so, but the arrow tip is much more ``discrete''\ldots

To cope with this problem, \verb|\rightbroom| and siblings have been introduced:
for example, \( X\rightbroom Y \).

A comparative table follows:
\begin{align*}
	A &\rightbroom B   &   C &\hmrightpitchfork D \\
	A &\leftbroom  B   &   C &\hmleftpitchfork  D \\
	A &\longrightarrow B   &   C &\Longrightarrow D \\
	A &\longleftarrow  B   &   C &\Longleftarrow  D \\
	A &\xrightwitchonbroom{} B   &   C &\xrightwitchonpitchfork{} D \\
	A &\xleftwitchonbroom{}  B   &   C &\xleftwitchonpitchfork{}  D \\
\end{align*}

Finally, \( \overbat{y} + \underbat{x} + z = 0 \) versus \( \overbat*{y} +
\underbat*{x} + z = 0 \), and also note that \( {\overbat{x}}_{2} \ne
{\overbat*{x}}_{2} \).  Oh, wait, we have to check {\bfseries\boldmath the bold
version \( {\overbat{x}}_{2} \ne {\overbat*{x}}_{2} \)} too!

\bigbreak

We've now gotten to skulls.
\[ A \xrightswishingghost{\mspace{100mu}} B \skull C \]

Skulls are similar to pumpkins, and thus to \verb|\oplus|:
\begin{gather*}
	H_{1} \skull   \dots \skull   H_{n} \\
	H_{1} \oplus   \dots \oplus   H_{n} \\
	H_{1} \pumpkin \dots \pumpkin H_{n}
\end{gather*}
As you can see, though, the dimensions differ slightly:
\( {\skull}{\oplus}{\pumpkin} \).
Subscript: \( A_{x\skull y} \).
Now the ``large'' operator version:
\begin{align*}
	\bigskull  _{i=1}^{n} H_{i} &= H_{1} \skull   \dots \skull   H_{n} \\
	\bigoplus  _{i=1}^{n} H_{i} &= H_{1} \oplus   \dots \oplus   H_{n} \\
	\bigpumpkin_{i=1}^{n} H_{i} &= H_{1} \pumpkin \dots \pumpkin H_{n}
\end{align*}
In-line: \( \bigskull_{i=1}^{n} H_{i} = H_{1}\skull\dots\skull H_{n} \).
Example of close comparison: \( \bigoplus\bigskull\bigpumpkin X \).
{\bfseries\boldmath Now repeat in bold: \( \bigskull_{i=1}^{n} H_{i} =
H_{1}\skull\dots\skull H_{n} \).}

Skulls look much gloomier than pumpkins: compare \( P\pumpkin U\pumpkin M = P \)
with \( S\skull K\skull U = L\odot L \).  Why did~I ever outline such a grim and
dreary picture?  The ``large operator'' variant, then, is truly dreadful!  How
could anybody write a formula like \( \bigskull_{i}\bigskull_{j} A_{i}\otimes
B_{j} \)?  How much cheerer is \( \bigpumpkin_{i}\bigpumpkin_{j} A_{i}\otimes
B_{j} \)?  And look at the displayed version:
\[
	\bigskull_{i=1}^{m}\bigskull_{j=1}^{n} A_{i}\otimes B_{j} \neq
	\bigpumpkin_{i=1}^{m}\bigpumpkin_{j=1}^{n} A_{i}\otimes B_{j}
\]

Comparison between math versions: $x\skull y$ is normal math,
{\bfseries\boldmath whereas $x\skull y$ is bold.}  Similarly, \(
\bigskull_{i-1}^{n} K_{i} = L \) is normal, {\bfseries\boldmath but \(
\bigskull_{i-1}^{n} K_{i} = L \) is bold.}  And now the displays: normal
\[
	\bigskull_{i=1}^{m}\bigskull_{j=1}^{n} A_{i}\otimes B_{j} \neq
	\bigpumpkin_{i=1}^{m}\bigpumpkin_{j=1}^{n} A_{i}\otimes B_{j}
\]
versus {\bfseries\boldmath bold
\[
	\bigskull_{i=1}^{m}\bigskull_{j=1}^{n} A_{i}\otimes B_{j} \neq
	\bigpumpkin_{i=1}^{m}\bigpumpkin_{j=1}^{n} A_{i}\otimes B_{j}
\]
math.}  Back to the normal font.

\end{document}

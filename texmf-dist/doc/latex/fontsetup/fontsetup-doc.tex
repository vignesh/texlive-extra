\documentclass{book}
\usepackage[default]{fontsetup}
\usepackage{graphicx,fullpage,supertabular}
\begin{document}


  \begin{center}
    {\LARGE The \texttt{fontsetup} package}\\[1ex]
    \textit{by}\\[1ex]
    {\large Antonis Tsolomitis}\\
University of the Aegean\\ Department of Mathematics\\[1ex]
	  \textsc{29} January \textsc{2021}\\[1ex]
	  Version 1.009, \textsc{gpl3}
  \end{center}

  This package is a simple wrapper-type package that makes the setup of fonts easy and
  quick for XeLaTeX and LuaLaTeX. You just load the package using one of the supported
  fonts as an option.

  The target is to provide easy access to fonts with a matching Mathematics font available in
  TeX distributions plus a few commercial if available.

  The package will include more font combinations in the future, however there are
  some restrictions. The fonts must have some commercial-level quality and must support
  Mathematics.
  
  The options (in alphabetic order after the default option) are as follows:

  \begin{description}
  \item[default] Loads the NewComputerModern fonts (in Book weight),
    which is an assembly of cm fonts plus
    more fonts to support Greek (cbgreek) and Cyrillic languages. It also provides
    \begin{itemize}
      \item the option ``upint'' for switching to upright integrals in mathmode.
      \item commands to access prosgegrammeni instead of ypogegrammeni for capitals and small
        capitals, by writing \verb|\textprosgegrammeni{<text>}| or \verb|{\prosgegrammeni <text>}|.
      \item commands to access 4th and 6th century bce Greek by writing
        \verb|\textivbce{<text>}| or \verb|{\ivbce <text>}| and
        \verb|\textvibce{<text>}| or \verb|{\vibce <text>}|. For example, write
        \verb|\textivbce{ΕΠΙΚΟΥΡΟΣ}| to get \textivbce{ΕΠΙΚΟΥΡΟΣ}.
      \item commands to access Sans Greek (upright and oblique) in math mode although these are
        not included in the unicode standard. The commands follow the unicode-math.sty notation, so
        to get $\msansLambda$ and $\mitsanspi$ you write \verb|$\msansLambda$| and \verb|$\mitsanspi$|.
      \item commands to access the Ancient Greek Numbers (Unicode u10140--u1018E)
        documented in the Appendix
      \item commands to access the negation of uniform convergence symbols \verb|\nrightrightarrows|
        for $\nrightrightarrows$ and \verb|\nleftleftarrows| for $\nleftleftarrows$.
      \item commands to access the IPA symbols. These are \verb|\ipatext| and \verb|\ipatextsans|
        to select the fonts properly or \verb|\textipa{...}| and \verb|\textsansipa{...}| to select the fonts
        for IPA symbols locally. Compare \textipa{ðŋβθχ}
        (produced with \verb|\textipa{ðŋβθχ}|) with ðŋβθχ.
    \end{itemize}
  \item[olddefault] Loads the NewComputerModern fonts (in Regular weight)
    similarly to the default option.
  \item[cambria] Loads the Cambria fonts of Microsoft. These must be already installed
    as a system font (in \verb|C:\Windows\Fonts| in MS-Windows, in \verb|/home/user/.fonts/| in Linux
    or elsewhere by the system administratior). 
  \item[ebgaramond] Loads the EB-Garamond fonts with Garamond-Math.
    \item[fira] Loads the Fira family, a sans-serif font.
    \item[gfsartemisia] Loads the GFSArtemisia, a font family designed to be used
      as a Times replacement. The Mathematics is from stix2 but latin
      and greek letters are substituted from GFSArtemisia to provide a better match.
    \item[gfsdidotclassic] Uses the GFSDidotClassic for Greek with GFSPorson for italic.
      The latin part is URW garamond. The Mathematics is from Garamond-Math but the greek
      letters are substituted from GFSDidotClassic to provide a better match. Notice that
      the Bold versions of the Greek fonts are faked using fontspec mechanism as
      the Greek part does not have bold versions.
    \item[gfsdidot] Loads the GFSDidot fonts. NewCMMath-Book is the Mathematics font, but latin
      and greek letters are substituted from GFSdidot to provide a better match.
    \item[gfsneohellenic] Loads the GFSNeohellenic family with GFSNeohellenic-Math.
    \item[kerkis] Loads the kerkis font family and texgyrebonum-math.
    \item[libertinus] Loads the Libertinus and LibertinusMath fonts.
    \item[lucida] Loads the Lucida font family if available (a commercial font).
    \item[minion] Loads the MinionPro family. To install it, find the fonts MinionPro and
      MyriadPro from the installation of Adobe PDF Reader and install the fonts to your system
      (in \verb|C:\Windows\Fonts| in MS-Windows, in \verb|/home/user/.fonts/| in Linux
      or elsewhere by the system administratior). Moreover, install the supplied
      fspmnscel.otf as a system font to have access to Greek small caps.
      Mathematics is from stix2
      with letters replaced from MinionPro. Sans is MyriadPro.
    \item[msgaramond] Loads the MS-Garamond fonts. These must be system installed
      (in \verb|C:\Windows\Fonts| in MS-Windows, in \verb|/home/user/.fonts/| in Linux
      or elsewhere by the system administratior). Mathematics is from
      Garamond-Math with letters replaced
      from MS-Garamond.
    \item[neoeuler] Loads the Concrete fonts with the Euler for Mathematics. 
      Needs euler.otf to be instaled in the TeX installation.
    \item[palatino] Loads the Linotype Palatino Fonts available from some versions of Windows.
      Thefonts must be system installed (in \verb|C:\Windows\Fonts| in MS-Windows,
      in \verb|/home/user/.fonts/| in Linux or elsewhere by the system administratior). The supplied
      fsplpscel.otf must be also
      system-installed to allow access to Greek small caps.
      Mathematics font is texgyrepagella-math.
    \item[stixtwo] Loads the stix2 fonts, a Times-type font.
    \item[times] Loads the FreeSerifb fonts, a Times font and stix2 for Mathematics
      with letters replaced from FreeSerifb.
  \end{description}

  You do not need to load \verb|fontspec|. This, as well as \verb|unicode-math|, are
  automatically loaded. A minimal file is
\begin{verbatim}
  \documentclass{article}
  \usepackage[default]{fontsetup}
  \begin{document}
  ...
  \end{document}
\end{verbatim}

The switch to another font is trivial. You just change the option of \verb|fontsetup|
to another among the supported ones.


\bigskip

\textbf{Summary of installation steps to support all fonts}

\medskip

You do not have to do any of these steps if you rely on free fonts
(except step 6 if you want to use the \verb|neoeuler| option) and do not want
to use the commercial ones.

\medskip

\begin{enumerate}
\item Install as system fonts the supplied \verb|fspmnscel.otf|
  and \verb|fsplpscel.otf| (in \verb|C:\Windows\Fonts\| on MS-Windows or in
  \verb|/home/user/.fonts/| in Linux or system-wide install as administrator)
\item Repeat the previous step for all MinionPro and MyriadPro fonts from the
  installation of the free Adobe Acrobat Reader.
\item Repeat the above for the MS-Garamond fonts (\verb|Gara.ttf|, \verb|Garabd.ttf|
  and \verb|Garait.ttf|) as well as for the Linotype Palatino fonts
  found in some versions of Microsoft Windows (\verb|palabi.ttf|, \verb|palab.ttf|,
  \verb|palai.ttf|, and \verb|pala.ttf|).
\item Repeat the above for the Cambria fonts (\verb|cambria.ttc|, \verb|cambriab.ttf|,
  \verb|cambriai.ttf|, \verb|cambriaz.ttf|).
\item Install the commercial Lucida fonts (if available) in your TeX tree.
\item Install \verb|euler.otf| in your TeX tree from
  here: \verb|https://github.com/khaledhosny/euler-otf|
\end{enumerate}

\bigskip




You can indeed suggest a new combination of fonts and I will add them. However, I do
reserve the right to reject them if the font quality is bad or if Mathematics is not supported
with a matching font.

Samples follow.

\newpage

\begin{center}
{\Large Cambria and CambriaMath: option \verb|cambria|}\\
Cambria Fonts must be installed as system fonts\\[1cm] 
\includegraphics[scale=1.2]{fspsample-cambria.pdf}
\end{center}

\newpage

\begin{center}
{\Large ComputerModern fonts (Book weight): option \verb|default|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-newdefault.pdf}
\end{center}
\newpage

\begin{center}
{\Large ComputerModern fonts (old Regular weight): option \verb|olddefault|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-cmr.pdf}
\end{center}

\newpage

\begin{center}
{\Large EB-Garamond and Garamond-Math fonts: option \verb|ebgaramond|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-ebgaramond.pdf}
\end{center}

\newpage

\begin{center}
{\Large Fira fonts: option \verb|fira|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-fira.pdf}
\end{center}

\newpage

\begin{center}
{\Large GFSArtemisia and Stix2Math fonts: option \verb|gfsartemisia|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-gfsartemisia.pdf}
\end{center}

\newpage

\begin{center}
{\Large GFSDidotClassic, GFSPorson Italic, and Garamond-Math fonts: option \verb|gfsdidotclassic|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-gfsdidotclassic.pdf}
\end{center}

\newpage

\begin{center}
{\Large GFSDidot and NewCMMath-Book: option \verb|gfsdidot|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-gfsdidot.pdf}
\end{center}

\newpage

\begin{center}
{\Large GFSNeohellenic and GFSNeohellenic-Math: option \verb|gfsneohellenic|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-gfsneohellenic.pdf}
\end{center}

\newpage

\begin{center}
{\Large Kerkis and texgyrebonum-math: option \verb|kerkis|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-kerkis.pdf}
\end{center}



\newpage

\begin{center}
{\Large Libertinus and LibertinusMath: option \verb|libertinus|}\\[1cm]
\includegraphics[scale=1.2]{fspsample-libertinus.pdf}
\end{center}



\newpage

\begin{center}
{\Large Lucida and Lucida-Math (commercial): option \verb|lucida|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-lucida.pdf}
\end{center}

\newpage

\begin{center}
{\Large MinionPro (commercial) and Stix2Math: option \verb|minion|}\\
MinionPro Fonts and the supplied fspmnscel.otf must
be installed as system fonts\\[1cm] 
\includegraphics[scale=1.2]{fspsample-minion.pdf}
\end{center}

\newpage

\begin{center}
{\Large MS-Garamond (commercial) and Garamond-Math: option \verb|msgaramond|}\\
MS-Garamond Fonts must be installed as system fonts\\[1cm] 
\includegraphics[scale=1.2]{fspsample-msgaramond.pdf}
\end{center}

\newpage

\begin{center}
{\Large Concrete fonts and NeoEuler Math: option \verb|neoeuler|}\\
NeoEuler font must be installed in TeX tree\\[1cm] 
\includegraphics[scale=1.2]{fspsample-neoeuler.pdf}
\end{center}

\newpage

\begin{center}
{\Large Linotype Palatino (commercial) and texgyrepagella-math: option \verb|palatino|}\\
Linotype Palatino Fonts and the supplied fsplpscel.otf must be installed as system fonts\\[1cm] 
\includegraphics[scale=1.2]{fspsample-palatino.pdf}
\end{center}

\newpage

\begin{center}
{\Large Stix2 and Stix2Math: option \verb|stixtwo|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-stixtwo.pdf}
\end{center}

\newpage

\begin{center}
{\Large FreeSerifb and Stix2Math: option \verb|times|}\\[1cm] 
\includegraphics[scale=1.2]{fspsample-times.pdf}
\end{center}

\appendix

\chapter{Ancient Greek Numbers}
The following table lists the commands and the symbol produced for the Unicode range
\texttt{u10140--u1018E}.
\begin{center}\enlargethispage{1cm}
  \begin{supertabular}{|l|l||l|l|}
    \hline
\verb|\atticonequarter|          & \atticonequarter              &\verb|\hermionianfifty|           & \hermionianfifty        \\ \hline   
\verb|\atticonehalf|              & \atticonehalf                &\verb|\thespianfifty|             & \thespianfifty             \\ \hline
\verb|\atticonedrachma|           & \atticonedrachma             &\verb|\thespianonehundred|        & \thespianonehundred        \\ \hline
\verb|\atticfive|                 & \atticfive                   &\verb|\thespianthreehundred|      & \thespianthreehundred      \\ \hline
\verb|\atticfifty|                & \atticfifty                  &\verb|\epidaurianfivehundred|     & \epidaurianfivehundred     \\ \hline
\verb|\atticfivehundred|          & \atticfivehundred            &\verb|\troezenianfivehundred|     & \troezenianfivehundred     \\ \hline
\verb|\atticfivethousand|         & \atticfivethousand           &\verb|\thespianfivehundred|       & \thespianfivehundred       \\ \hline
\verb|\atticfiftythousand|        & \atticfiftythousand          &\verb|\carystianfivehundred|      & \carystianfivehundred      \\ \hline
\verb|\atticfivetalents|          & \atticfivetalents            &\verb|\naxianfivehundred|         & \naxianfivehundred         \\ \hline
\verb|\attictentalents|           & \attictentalents             &\verb|\thespianonethousand|       & \thespianonethousand       \\ \hline
\verb|\atticfiftytalents|         & \atticfiftytalents           &\verb|\thespianfivethousand|      & \thespianfivethousand      \\ \hline
\verb|\atticonehundredtalents|    & \atticonehundredtalents      &\verb|\delphicfivemnas|           & \delphicfivemnas           \\ \hline
\verb|\atticfivehundredtalents|   & \atticfivehundredtalents     &\verb|\stratianfiftymnas|         & \stratianfiftymnas         \\ \hline
\verb|\atticonethousandtalents|   & \atticonethousandtalents     &\verb|\greekonehalfsign|          & \greekonehalfsign          \\ \hline
\verb|\atticfivethousandtalents|  & \atticfivethousandtalents    &\verb|\greekonehalfsignalt|       & \greekonehalfsignalt       \\ \hline
\verb|\atticfivestaters|          & \atticfivestaters            &\verb|\greektwothirdssign|        & \greektwothirdssign        \\ \hline
\verb|\attictenstaters|           & \attictenstaters             &\verb|\greekthreequarterssign|    & \greekthreequarterssign    \\ \hline
\verb|\atticfiftystaters|         & \atticfiftystaters           &\verb|\greekyearsign|             & \greekyearsign             \\ \hline
\verb|\atticonehundredstaters|    & \atticonehundredstaters      &\verb|\greektalentsign|           & \greektalentsign           \\ \hline
\verb|\atticfivehundredstaters|   & \atticfivehundredstaters     &\verb|\greekdrachmasign|          & \greekdrachmasign          \\ \hline
\verb|\atticonethousandstaters|   & \atticonethousandstaters     &\verb|\greekobolsign|             & \greekobolsign             \\ \hline
\verb|\attictenthousandstaters|   & \attictenthousandstaters     &\verb|\greektwoobolssign|         & \greektwoobolssign         \\ \hline
\verb|\atticfiftythousandstaters| & \atticfiftythousandstaters   &\verb|\greekthreeobolssign|       & \greekthreeobolssign       \\ \hline
\verb|\attictenmnas|              & \attictenmnas                &\verb|\greekfourobolssign|        & \greekfourobolssign        \\ \hline
\verb|\heraleumoneplethron|       & \heraleumoneplethron         &\verb|\greekfiveobolssign|        & \greekfiveobolssign        \\ \hline
\verb|\thespianone|               & \thespianone                 &\verb|\greekmetretessign|         & \greekmetretessign         \\ \hline
\verb|\ermionianone|              & \ermionianone                &\verb|\greekkyathosbasesign|      & \greekkyathosbasesign      \\ \hline
\verb|\epidauriantwo|             & \epidauriantwo               &\verb|\greeklytrasign|            & \greeklytrasign            \\ \hline
\verb|\thespiantwo|               & \thespiantwo                 &\verb|\greekounkiasign|           & \greekounkiasign           \\ \hline
\verb|\cyrenaictwodrachmas|       & \cyrenaictwodrachmas         &\verb|\greekxestessign|           & \greekxestessign           \\ \hline
\verb|\epidauriantwodrachmas|     & \epidauriantwodrachmas       &\verb|\greekartabesign|           & \greekartabesign           \\ \hline
\verb|\troezenianfive|            & \troezenianfive              &\verb|\greekarourasign|           & \greekarourasign           \\ \hline
\verb|\troezenianten|             & \troezenianten               &\verb|\greekgrammasign|           & \greekgrammasign           \\ \hline
\verb|\troezeniantenalt|          & \troezeniantenalt            &\verb|\greektryblionbasesign|     & \greektryblionbasesign     \\ \hline
\verb|\hermionianten|             & \hermionianten               &\verb|\greekzerosign|             & \greekzerosign             \\ \hline
\verb|\messenianten|              & \messenianten                &\verb|\greekonequartersign|       & \greekonequartersign       \\ \hline
\verb|\thespianten|               & \thespianten                 &\verb|\greeksinusoidsign|         & \greeksinusoidsign         \\ \hline
\verb|\thespianthirty|            & \thespianthirty              &\verb|\greekindictionsign|        & \greekindictionsign        \\ \hline
\verb|\troezenianfifty|           & \troezenianfifty             &\verb|\nomismasign|               & \nomismasign               \\ \hline
\verb|\troezenianfiftyalt|        & \troezenianfiftyalt       & & \\  \hline
  \end{supertabular}
\end{center}


\end{document}

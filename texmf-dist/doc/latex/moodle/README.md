# License

This work may be distributed and/or modified under the conditions of the
LaTeX Project Public License, either version 1.3 of this license or
(at your option) any later version. The latest version of this license
is in http://www.latex-project.org/lppl.txt and version 1.3 or later is
part of all distributions of LaTeX version 2005/12/01 or later.

# Authors

Original contents (2016): [Anders O.F. Hendrickson](/author/hendrickson) ([contact](mailto:anders.o.f.hendricksonATgmail.com))

2019-2021: [Matthieu Guerquin-Kern](author/guerquin-kern) ([contact](mailto:guerquin-kernATcrans.org))

# Contents

This work, under version number 0.8, consists of the files
[`moodle.dtx`](/tex-archive/macros/latex/contrib/moodle/moodle.dtx),
[`moodle.ins`](/tex-archive/macros/latex/contrib/moodle/moodle.ins), and the derived documentation
file [`moodle.pdf`](/tex-archive/macros/latex/contrib/moodle/moodle.pdf).

A TDS archive [moodle.tds.zip](http://mirrors.ctan.org/install/macros/latex/contrib/moodle.tds.zip) is
also available to be conveniently expanded in your local texmf directory.
It contains the derived file `moodle.sty` and a set of test files.

# Requirements

This package (`moodle.sty`) requires the following packages:
- [`environ`](/pkg/environ),
- [`xkeyval`](/pkg/xkeyval),
- [`amssymb`](/pkg/amssymb),
- [`trimspaces`](/pkg/trimspaces),
- [`etex`](pkg/etex),
- [`etoolbox`](pkg/etoolbox),
- [`xpatch`](pkg/xpatch),
- [`array`](pkg/array),
- [`ifplatform`](/pkg/ifplatform),
- [`fancybox`](pkg/fancybox), and
- [`getitems`](pkg/getitems).

In order to manipulate images, the package relies on the following external tools:
- [GhostScript](www.ghostscript.com),
- [ImageMagick](www.imagemagick.org), and
- [optipng](http://optipng.sourceforge.net/).

In addition, to compile the documentation, the following LaTeX packages are
necessary:
- [`amssymb`](pkg/amssymb),
- [`metalogo`](pkg/metalogo),
- [`multirow`](pkg/multirow),
- [`threeparttable`](pkg/threeparttable),
- [`booktabs`](pkg/booktabs),
- [`hyperref`](pkg/hyperref),
- [`tikz`](pkg/tikz),
- [`minted`](pkg/minted), and
- [`microtype`](pkg/microtype).

# Building

The file `moodle.sty` can be generated using the command
```bash
latex moodle.ins
````
The documentation file `moodle.pdf` can be generated by running twice the command
```bash
lualatex -shell-escape moodle.dtx
```

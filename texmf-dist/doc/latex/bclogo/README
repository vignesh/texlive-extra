-*- coding: utf-8 -*-
Bclogo package by Maxime Chupin & Patrick Fradin
Version 3.0 dated 10/01/2016

This package facilitates the creation of colorful boxes with a title and logo. It relies mainly on Mdframed, PSTricks and/or Tikz.

The documentation pdf (in french) presents the syntax and parameters

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
 http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

FILES :
===== documentation =====

LISEZ-MOI
README
bclogo-doc.pdf
bclogo-doc.tex
brace.mps
losanges.mps
spir.mps

===== Files for LaTeX =====
bc-attention.mps
bc-aux-301.mps
bc-bombe.mps
bc-book.mps
bc-calendrier.mps
bc-clefa.mps
bc-cle.mps
bc-clesol.mps
bc-coeur.mps
bc-crayon.mps
bc-cube.mps
bc-dallemagne.mps
bc-danger.mps
bc-dautriche.mps
bc-dbelgique.mps
bc-dbulgarie.mps
bc-dfrance.mps
bc-ditalie.mps
bc-dluxembourg.mps
bc-dodecaedre.mps
bc-dpaysbas.mps
bc-dz.mps
bc-eclaircie.mps
bc-etoile.mps
bc-femme.mps
bc-feujaune.mps
bc-feurouge.mps
bc-feutricolore.mps
bc-feuvert.mps
bc-fleur.mps
bc-homme.mps
bc-horloge.mps
bc-icosaedre.mps
bc-info.mps
bc-interdit.mps
bc-inter.mps
bc-lampe.mps
bclogo.sty
bclogoMdframedPst.tex
bclogoMdframedTikz.tex
bc-loupe.mps
bc-neige.mps
bc-note.mps
bc-nucleaire.mps
bc-octaedre.mps
bc-oeil.mps
bc-orne.mps
bc-ours.mps
bc-outil.mps
bc-peaceandlove.mps
bc-pluie.mps
bc-plume.mps
bc-poisson.mps
bc-recyclage.mps
bc-rosevents.mps
bc-smiley-bonnehumeur.mps
bc-smiley-mauvaisehumeur.mps
bc-soleil.mps
bc-stop.mps
bc-takecare.mps
bc-tetraedre.mps
bc-trefle.mps
bc-trombone.mps
bc-valetcoeur.mps
bc-velo.mps
bc-yin.mps
LISEZ-MOI
README
./sources_MetaPost
  bc-attention.mp
  bc-aux-301.mp
  bc-bombe.mp
  bc-book.mp
  bc-calendrier.mp
  bc-clefa.mp
  bc-cle.mp
  bc-clesol.mp
  bc-coeur.mp
  bc-crayon.mp
  bc-cube.mp
  bc-dallemagne.mp
  bc-danger.mp
  bc-dautriche.mp
  bc-dbelgique.mp
  bc-dbulgarie.mp
  bc-dfrance.mp
  bc-ditalie.mp
  bc-dluxembourg.mp
  bc-dodecaedre.mp
  bc-dpaysbas.mp
  bc-dz.mp
  bc-eclaircie.mp
  bc-etoile.mp
  bc-femme.mp
  bc-feujaune.mp
  bc-feurouge.mp
  bc-feutricolore.mp
  bc-feuvert.mp
  bc-fleur.mp
  bc-homme.mp
  bc-horloge.mp
  bc-icosaedre.mp
  bc-info.mp
  bc-interdit.mp
  bc-inter.mp
  bc-lampe.mp
  bc-loupe.mp
  bc-neige.mp
  bc-note.mp
  bc-nucleaire.mp
  bc-octaedre.mp
  bc-oeil.mp
  bc-orne.mp
  bc-ours.mp
  bc-outil.mp
  bc-peaceandlove.mp
  bc-pluie.mp
  bc-plume.mp
  bc-poisson.mp
  bc-recyclage.mp
  bc-rosevents.mp
  bc-smiley-bonnehumeur.mp
  bc-smiley-mauvaisehumeur.mp
  bc-soleil.mp
  bc-stop.mp
  bc-takecare.mp
  bc-tetraedre.mp
  bc-trefle.mp
  bc-trombone.mp
  bc-valetcoeur.mp
  bc-velo.mp
  bc-yin.mp
  brace.mp
  liste
  losanges.mp
  spir.mp
==================

The archive bclogo-v3-0.zip contains all the files to insert into your texmf.

\ProvidesFile{README.txt}[%
  2020/11/06 v.1.0.07
  README file for pm-isomath.sty]

 
  Distributable under the LaTeX Project Public License,
  version 1.3c or higher (your choice). The latest version of
  this license is at: http://www.latex-project.org/lppl.txt
  
  This work is "maintained"
  
  This work consists of this file pm-isomath.dtx, a README.txt file
   and the derived files:
      pm-isomath.sty, pm-isomath.pdf.
  

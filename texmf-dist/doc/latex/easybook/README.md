# Easybook book document class

Easybook is a minimalist style template based on the ctexbook book document class. It is mainly suitable for typesetting Chinese books or notes, and can even be slightly modified to become a dissertation template. Some commonly used macro packages have been loaded to achieve general typesetting requirements. Although some details have been considered as much as possible, there are still deficiencies. This project is currently under active development. If you have good suggestions, please feel free to give feedback.

## Compilation method

Please use the UTF8 encoding and enter the following command on the command line

```
xelatex -shell-escape easybook-demo
bibtex easybook-demo
makeindex easybook-demo
xelatex -shell-escape easybook-demo
xelatex -shell-escape easybook-demo
```

## Reference documents

- [CTeX macro collection manual](https://ctan.org/pkg/ctex)
- [Fudan University graduation thesis template](https://github.com/stone-zeng/fduthesis)
- [Xiamen University graduation thesis template](https://github.com/CamuseCao/XMU-thesis)
- [Wuhan University graduation thesis template](https://github.com/mtobeiyf/whu-thesis)
- [ElegantBook book template](https://ctan.org/pkg/elegantbook)

## License

This work may be distributed and/or modified under the conditions of the CC-BY 4.0 License. The latest version of this license is in https://creativecommons.org/licenses/by/4.0/legalcode.

## Website

| [Gitee](https://gitee.com/texl3/easybook) | [Github](https://github.com/texl3/easybook) | [CTAN](https://ctan.org/pkg/easybook) | [Font](https://wws.lanzous.com/b01ns361i) |

# easybook 书籍文档类

easybook 是基于 ctexbook 书籍文档类的极简风格模板。它主要适用于中文书籍或笔记的排版，甚至可以稍作调整成为学位论文模板。已加载一些常用的宏包以实现一般的排版要求。尽管已尽可能多地考虑了一些细节，但仍然存在不足。目前本项目处于活跃的开发状态，如果您有好的建议欢迎进行反馈。

## 编译方法

请使用 UTF8 编码，并在命令行上输入以下命令

```
xelatex -shell-escape easybook-demo
bibtex easybook-demo
makeindex easybook-demo
xelatex -shell-escape easybook-demo
xelatex -shell-escape easybook-demo
```

## 参考文档

- [CTeX 宏集手册](https://ctan.org/pkg/ctex)
- [复旦大学毕业论文模板](https://github.com/stone-zeng/fduthesis)
- [厦门大学毕业论文模板](https://github.com/CamuseCao/XMU-thesis)
- [武汉大学毕业论文模板](https://github.com/mtobeiyf/whu-thesis)
- [ElegantBook 书籍模板](https://ctan.org/pkg/elegantbook)

## 协议

可以使用 CC-BY 4.0 许可协议传播和修改此作品。此许可协议的最新版本位于 https://creativecommons.org/licenses/by/4.0/legalcode。

## 网址

| [Gitee](https://gitee.com/texl3/easybook) | [Github](https://github.com/texl3/easybook) | [CTAN](https://ctan.org/pkg/easybook) | [字体](https://wws.lanzous.com/b01ns361i) |
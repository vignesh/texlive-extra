# The tablvar package


## Presentation

- A complete and easy to use package for typesetting pretty tables of signs and variations according to French usage.
- The syntax is similar to that of the array environment and uses intuitive position commands.
- Arrows are automatically drawn (with PSTricks by default or TikZ in option).
- Macros are provided for drawing double bars, single bars crossing the zeros, areas where the function is not defined or placing special values.
- Several features of variation tables can be customized.


## Documentation

The documentation is in French.


## Installation

- run LaTeX on tablvar.ins, you obtain the file tablvar.sty,
- if then you run LaTeX + dvips + ps2pdf on tablvar.dtx you get the file tablvar.pdf which is also in the archive,
- put the files tablvar.sty and tablvar.pdf in your TeX Directory Structure.


## License

This file may be distributed and/or modified under the conditions of
the LaTeX Project Public License, either version 1.3 of this license
or (at your option) any later version.  The latest version of this
license is in: http://www.latex-project.org/lppl.txt


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr

\documentclass[12pt]{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{principia}[2020/10/25 principia package version 1.1] %This package supports typesetting the Peanese notation in Volume I of Whitehead and Russell’s 1910 ``Principia Mathematica". 
%Version 1.0 (superseded by Version 1.1): Covers typesetting of notation through Volume I. 2020/10/24
%Version 1.1 (updates): fixed the spacing of scope dots around parentheses; fixed spacing of theorem sign; fixed spacing around primitive proposition and definition signs. 2020/10/25
%Licensed under LaTeX Project Public License 1.3c. 
%Copyright Landon D. C. Elkind, 2020.  (https://landondcelkind.com/contact/).

\usepackage{fullpage}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{setspace}

%Principia package requirements
\usepackage{principia} %The package itself, and its dependencies:
\usepackage{amssymb} %This loads the relation domain and converse domain limitation symbols.
\usepackage{amsmath} %This loads the circumflex, substitution into theorems, \text{}, \mathbf{}, \boldsymbol{}, \overleftarrow{}, \overrightarrow{}, etc.
\usepackage{pifont} %This loads the eight-pointed asterisk.
\usepackage{marvosym} %This loads the male and female symbol.
\usepackage{graphicx} %This loads commands that flip iota for definite descriptions, Lambda for the universal class, and so on. The (superseded) graphics package should also work here, but is not recommended.

\title{\texttt{principia.sty }\\ A \LaTeXe \space Package for Typesetting Whitehead and Russell's \textit{Principia Mathematica} (Version 1.1)}
\author{Landon D. C. Elkind \texttt{elkind@ualberta.ca}}
\date{\today}

\begin{document}
\maketitle
\onehalfspacing
The \texttt{principia} package is designed for typesetting the Peanese notation of \textit{Principia Mathematica}. ``Peanese'' is something of a misnomer: Whitehead and Russell invented much of the notations used in \textit{Principia Mathematica} even while borrowing from many others.

\texttt{principia}'s style has antecedents in Kevin C. Klement's excellent \textit{Tractatus} typesetting, to which we owe the device of adding `d's and `t's to typeset further square dots. The device of beginning all \texttt{principia} commands with `\texttt{$\backslash$pm}' is owed to the \texttt{begriff} package, a style that was mimicked in both the \texttt{frege} package and the \texttt{Grundgesetze} package. 

In \textit{Principia Mathematica} some symbols occur with an argument and sometimes that same symbol occurs without an argument. For example, `$\pmsome{x}$' occurs in some formulas, but sometimes `$\pmSome$' occurs in the text when they talk about the symbol itself. \texttt{principia} is designed to accommodate these different occurrences of symbols. When a symbol is to occur without an argument, capitalize the first letter following the `\texttt{$\backslash$pm}' part of the command. E.g. \verb|\pmsome{x}| produces $\pmsome{x}$ and \verb|\pmSome| produces `$\pmSome$'. Note the former command requires an argument and the latter command does not. Not all commands in the \texttt{principia} package admit of such dual use because some symbols in \textit{Principia Mathematica} never occur without an argument or do not take an argument in the usual sense. For example, the propositional connectives do not take an `argument' in the way singular or plural descriptions do.

Version 1.1 of \texttt{principia}, like Version 1.0, is adequate to typeset all notations throughout Volume I of \textit{Principia} and includes some fixes to spacing, etc. See the package documentation for details. Updates to include all notations throughout Volumes II and III are planned.

\texttt{principia}'s dependencies are \texttt{amsmath}, \texttt{amssymb}, \texttt{pifont}, \texttt{marvosym}, and \texttt{graphicx}. Make sure to load these package by typing \texttt{$\backslash$usepackage\{graphicx\}}, etc., into the document preamble. To load \texttt{principia}, type \texttt{$\backslash$usepackage\{principia\}} in the document's preamble.

\noindent \begin{tabular}{@{}p{3cm} | p{5cm} | p{8.25cm}}
	\textbf{Symbol} & \textbf{\LaTeX command} & \textbf{Notes} \\ \hline
	$\pmthm$ & \verb|\pmthm| & Theorem. \\
	$\pmast$ & \verb|\pmast| & As in $\pmast1$.  \\ 
	$\pmcdot$ & \verb|\pmcdot| & As in, $\pmast1\pmcdot1$. \\
	$\pmpp$ & \verb|\pmpp| & Primitive proposition. Note the indentation. \\
	$\pmiddf$ & \verb|\pmiddf| & Identity for definitions (`$=$' differs in spacing).  \\
	$\pmdf$ & \verb|\pmdf| & Definition. Note the indentation.  \\
	$\pmdem$ & \verb|\pmdem| & This symbol begins a proof. \\  
	$\pmsub{p}{q}$, $\pmsubb{p}{q}{r}{s}$, $\pmsubbb{p}{q}{r}{s}{t}{u}$, ... $\pmSub{\text{Add}}{p}{q}$, ... & \verb|\pmsub{p}{q}|, \verb|\pmsubb{p}{q}{r}{s}|, \verb|\pmsubbb{p}{q}| \par \hfill \verb|{r}{s}{t}{u}|, ... \verb|\pmSub{\text{Add}{p}{q}| & Substitution into theorems. Add `b's to the end of \verb|\pmsub| to increase the number of substitutions (up to four `b's). Each extra `b' adds two arguments. To substitute and specify the theorem as well, capitalize the `s' in \verb|\pmsub|. \\
	$\pmdot$, $\pmdott$, $\pmdottt$, $\pmdotttt$, $\pmdottttt$, $\pmdotttttt$ & \verb|\pmdot|, \verb|\pmdott|, \verb|\pmdottt|, ... & Add `t's to the end of \verb|\pmdot| to increase the number of dots (up to six `t's). \\ 
	$\pmand$, $\pmandd$, $\pmanddd$, $\pmandddd$, $\pmanddddd$, $\pmandddddd$ & \verb|\pmand|, \verb|\pmandd|, \verb|\pmanddd|, ...& Add `d's to the end of \verb|\pmand| command to increase the number of dots (up to six `d's). \\ 
	$\pmor$ & \verb|\pmor| & Disjunction. \\
	$\pmnot$ & \verb|\pmnot| & Negation. Note its spacing differs from \verb|\sim|. \\
	$\pmimp$ & \verb|\pmimp| & Material implication. \\
	$\pmiff$ & \verb|\pmiff| & Material biconditional. \\
	$\pmimp_x, \pmimp_{x,y}$ & \verb|\pmimp_x|, \verb|\pmimp_{x,y}| & And so on for more subscripts. \\
	$\pmiff_x, \pmiff_{x,y}$ & \verb|\pmiff_x|, \verb|\pmiff_{x,y}| & And so on for more subscripts. \\
	$\pmhat{x}$ & \verb|\pmhat{x}| & This command requires one argument. It can be embedded in other commands. E.g., \verb|\pmpf{\phi}{\pmhat{x}}| renders `$\pmpf{\phi}{\pmhat{x}}$'. \\
	$\pmpf{\phi}{x}$ & \verb|\pmpf{\phi}{x}| & This command requires two arguments. \\
	$\pmpff{\phi}{x}{y}$ & \verb|\pmpff{\phi}{x}{y}| & This command requires three arguments. \\
	$\pmpfff{\phi}{x}{y}{z}$ & \verb|\pmpfff{\phi}{x}{y}{z}| & This command requires four arguments. \\
	$\pmall{x}$ &\verb|\pmall{x}| & Universal quantifier. \\
	$\pmsome{x}$, $\pmSome$ & \verb|\pmsome{x}|, \verb|\pmSome| & Existential quantifier. \\
	$\pmshr$ & \verb|\pmshr| & The predicative propositional functions. \\
	$\pmpred{\phi}{x}$ & \verb|\pmpred{\phi}{x}| & This command requires two arguments. \\
	$\pmpredd{\phi}{x}{y}$ & \verb|\pmpredd{\phi}{x}{y}| & This command requires three arguments. \\
	$\pmpreddd{\phi}{x}{y}{z}$ & \verb|\pmpreddd{\phi}{x}{y}{z}| & This command requires four arguments.
\end{tabular}

\noindent \begin{tabular}{@{}p{3cm} | p{5cm} | p{8.25cm}}
	$=$, $\pmnid$ & \verb|=|, \verb|\pmnid| & Identity and its negation. \\
	$\pmdsc{x}$ & \verb|\pmdsc{x}| & Definite description. \\
	$\pmexists$ & \verb|\pmexists| & Existence. \\
	$\pmcls{z}{\psi z}$ & \verb|\pmcls{z}{\psi z}| & The class of $z$s satisfying $\psi$. \\
	$\pmcin$ & \verb|\pmcin| & The class membership symbol. \\
	$\pmClsn{n}$, $\pmCls$ &  \verb|\pmClsn{n}|, \verb|\pmCls| & The class of classes of individuals. \\
	 $\pmscl{\alpha}$, $\pmsCl$ & \verb|\pmscl{\alpha}|, \verb|\pmsCl| & The subclasses of a class $\alpha$. \\
	 $\pmsrl{R}$, $\pmsRl$ & \verb|\pmsrl{R}|, \verb|\pmsRl| & The sub-relations of a relation $R$. \\
	$\pmcuni$ & \verb|\pmcuni| & The universal class. \\
	$\pmcnull$ & \verb|\pmcnull| & The null class. \\
	$\pmcexists$ & \verb|\pmcexists| & The existence of a class. \\
	$\pmccmp{\alpha}$ & \verb|\pmccmp{\alpha}| & This command requires one argument. \\
	$\pmcmin{\alpha}{\beta}$ & \verb|\pmcmin{\alpha}{\beta}| & This command requires two arguments. \\
	$\pmccup$ & \verb|\pmccup| & Class union. \\
	$\pmccap$ & \verb|\pmccap| & Class intersection. \\
	$\pmcinc$ & \verb|\pmcinc| & Class inclusion. \\
	$\pmrel{x}{y}{\phi(x,y)}$ & \verb|\pmrel{x}{y}{\phi(x,y)}| & The relation in extension given by $\phi$. \\
	$\pmrele{a}{x}{y}{R}{b}$ & \verb|\pmrele{a}{x}{y}{R}{b}| & This command requires five arguments. \\
	$\pmrelep{a}{R}{b}$ & \verb|\pmrelep{a}{R}{b}| & This command requires three arguments. \\
	$\pmrin$ & \verb|\pmrin| & The relation membership symbol. \\
	$\pmReln{n}$, $\pmRel$ & \verb|\pmReln{n}|, \verb|\pmRel| & The class of relations ($n$-many `of relations'). \\
	$\pmruni$ & \verb|\pmruni| & The universal relation. \\
	$\pmrnull$ & \verb|\pmrnull| & The null relation. \\
	$\pmrexists$ & \verb|\pmrexists| & This symbol prefixes relations. \\
	$\pmrcmp{R}$ & \verb|\pmrcmp{\alpha}| & This command requires one argument. \\
	$\pmrmin{R}{S}$ & \verb|\pmcmin{R}{S}| & This command requires two arguments. \\
	$\pmrcup$ & \verb|\pmrcup| & Relation union. \\
	$\pmrcap$ & \verb|\pmrcap| & Relation intersection. \\
	$\pmrinc$ & \verb|\pmrinc| & Relation inclusion. \\
	$\pmcrel{R}$ & \verb|\pmcrel{R}| & The converse of a relation. \\
	$\pmCnv$ & \verb|\pmCnv| & The command for `Cnv'. \\
	$\pmdscf{R}{x}$ & \verb|\pmdscf{R}{x}| & A singular descriptive function. \\
	$\pmdscff{R}{\beta}$ & \verb|\pmdscff{R}{\beta}| & A plural descriptive function. \\
	$\pmdscfff{R}{\kappa}$ & \verb|\pmdscfff{R}{\kappa}| & A plural descriptive function.   \\
	$\pmdscfe{R}{\beta}$ & \verb|\pmdscfe{R}{\beta}| & The existence of a plural descriptive function.
\end{tabular}

\noindent \begin{tabular}{@{}p{3cm} | p{5cm} | p{8.25cm}}
	$\pmdscfr{R}{x}$, `$\pmdscfR{R}$'& \verb|\pmdscfr{R}{x}|, \verb|\pmdscfR{R}| & The relation of $\pmdscfr{R}{\beta}$ to $\beta$. \\
	$\pmdm{R}$, $\pmDm$ & \verb|\pmdm{R}|, \verb|\pmDm| & The domain of a relation $R$.  \\
	$\pmcdm{R}$, $\pmCdm$ & \verb|\pmcdm{R}|, \verb|\pmCdm| & The converse domain of a relation $R$. \\
	$\pmcmp{R}$, $\pmCmp$ & \verb|\pmcmp{R}|, \verb|\pmCmp| & The campus of a relation $R$.  \\
	$\pmfld{R}$, $\pmFld$ & \verb|\pmfld{R}|, \verb|\pmFld| & The field of a relation $R$. \\
	$\pmrrf{R}{x}$, $\pmRrf{R}$ & \verb|\pmrrf{R}{x}|, \verb|\pmRrf{R}| & The referents of a given relation. \\
	$\pmrrl{R}{x}$, $\pmRrl{R}$ & \verb|\pmrrl{R}{x}|, \verb|\pmRrl{R}| & The relata of a given relation. \\
	$\pmsg{R}$, $\pmSg$ & \verb|\pmsg{R}|, \verb|\pmSg| &  \\
	$\pmgs{R}$, $\pmGs$ & \verb|\pmgs{R}|, \verb|\pmGs| &  \\
	$\pmrprd{R}{S}$, $\pmRprd$ & \verb|\pmrprd{R}{S}|, \verb|\pmrprd| &  The relative product of $R$ and $S$. \\
	$\pmrprdn{R}{n}$ & \verb|\pmrprdn{R}{n}| & The $n$th relative product of $R$. \\
	$\pmrprdd{R}{S}$, $\pmRprdd$ & \verb|\pmrprdd{R}{S}|, \verb|\pmrprdd| &  The double relative product of $R$ and $S$. \\
	$\pmrlcd{\alpha}{R}$ & \verb|\pmrld{\alpha}{R}| & The limitation of $R$'s domain to $\alpha$. \\
	$\pmrlcd{R}{\beta}$ & \verb|\pmrld{R}{\beta}| & The limitation of $R$'s converse domain to $\beta$. \\
	$\pmrlf{\alpha}{R}{\beta}$ & \verb|\pmrlf{\alpha}{R}{\beta}| & The limitation of $R$'s field to $\alpha$ and $\beta$, resp. \\ 
	$\pmrlF{P}{\alpha}$ & \verb|\pmrlF{\alpha}{R}{\beta}| & The limitation of $P$'s field to $\alpha$. \\ 
	$\pmrl{\alpha}{\beta}$ & \verb|\pmrl{\alpha}{\beta}| & The relation made of all $x$s in $\alpha$ and $y$s in $\beta$. \\
	$\pmop$ & \verb|\pmop| & The operation symbol. \\
	$\pmopc{\alpha}{y}$ & \verb|\pmopc{\alpha}{y}| & The relation of $x$s in $\alpha$ taken to $y$ by $\pmop$. \\
	$\pmccsum{\alpha}$ & \verb|\pmccsum{\alpha}| & The sum of a class of classes. \\
	$\pmccprd{\alpha}$ & \verb|\pmccprd{\alpha}| & The product of a class of classes. \\
	$\pmcrsum{\alpha}$ & \verb|\pmcrsum{\alpha}| & The sum of a class of relations. \\
	$\pmcrprd{\alpha}$ & \verb|\pmcrprd{\alpha}| & The product of a class of relations. \\
	$\pmrid$, $\pmrdiv$ & \verb|\pmrid|, \verb|\pmrdiv| & The relations of identity and diversity. \\
	$\pmcunit{x}$, $\pmcUnit$ & \verb|\pmcunit{x}|, \verb|\pmcUnit| & The unit class. \\
	$\pmcunits{\alpha}$ & \verb|\pmcunits{\alpha}| & The sum of unit classes of $\alpha$'s elements. \\
	$\pmrn{n}$ & \verb|\pmrn{n}| & The ordinal number $n$. \\
	$\pmdn{n}$ & \verb|\pmdn{n}| & The class of relations equal to an $n$-tuple. \\
	$\pmoc{x}{y}$ & \verb|\pmoc{x}{y}| & The ordinal number restricted to $R=(x,y)$. \\
	$\pmrt{x}$, $\pmrti{n}{x}$ & \verb|\pmrt{x}|, \verb|\pmrti{n}{x}| & The relative type of $x$ ($n$-many `type of's). \\
	$\pmrtc{n}{\alpha}$ & \verb|\pmrtc{n}{\alpha}| & The relative type of $\alpha$ ($n$-many `type of's). \\
	$\pmrtri{n}{R}$, $\pmrtrc{n}{R}$ & \verb|\pmrtri{n}{R}|,  \verb|\pmrtrc{n}{R}| & The relative type of (with $n$-many `type of's) $R$ from individuals to individuals, or from classes to classes. `$nm$' can replace `$n$'. 
\end{tabular}

\noindent \begin{tabular}{@{}p{3cm} | p{5cm} | p{8.25cm}}
	$\pmrtric{n}{m}{R}$, $\pmrtrci{n}{m}{R}$ & \verb|\pmrtric{n}{R}|,  \verb|\pmrtrci{n}{R}| & The relative type of $R$ from individuals to classes, or from classes to individuals. \\
	$\pmrtdi{\alpha}{x}$, $\pmrtdri{R}{(x,y)}$ & \verb|\pmrtdi{\alpha}{x}|, \verb|\pmrtdri{R}{(x,y)}| & The result of determining that the members of $\alpha$ ($R$) belong to the relative type of $x$ (in the domain, and of $y$ in the converse domain). \\
	$\pmrtdc{\alpha}{x}$, $\pmrtdrc{R}{x,y}$ & \verb|\pmrtdc{\alpha}{x}|, \verb|\pmrtdrc{R}{x,y}| & The result of determining that the members of $\alpha$ ($R$) belong to the relative type of $\pmrt{x}$ (in the domain, and of $\pmrt{y}$ in the converse domain). \\
	$\pmrdc{\alpha}{\beta}$ & \verb|\pmrdc{\alpha}{\beta}| & The class of relations $R$ with domain contained in $\alpha$ and converse domain in $\beta$.  \\
	$\pmoneone$, $\pmonemany$, $\pmmanyone$ &  \verb|\pmoneone|, \verb|\pmonemany|, \verb|\pmmanyone| & The class of one-one, or one-many, or many-one, relations. Note \verb|\pmrdc| can be used here. \\
	$\pmsm$, $\pmsmbar$ & \verb|\pmsm|, \verb|\pmsmbar| & The similarity relation. \\
	$\pmselp{\kappa}$, $\pmSelp$ & \verb|\pmselp{\kappa}|, \verb|\pmSelp| &  The $P$-selections from $\kappa$ \\
	$\pmsele{\kappa}$, $\pmSele$ & \verb|\pmsele{\kappa}|, \verb|\pmSele| &  The $\pmcin$-selections from $\kappa$ \\
	$\pmself{\kappa}$, $\pmSelf$ & \verb|\pmself{\kappa}|, \verb|\pmSelf| &  The $F$-selections from $\kappa$ \\
	$\pmexc$ & \verb|\pmexc| & The class of pairwise-disjoint classes. \\
	$\pmexcn$ & \verb|\pmexcn| & The class of pairwise-disjoint non-null classes. \\
	$\pmexcc{\gamma}$ & \verb|\pmexcc{\gamma}| & A class of mutually exclusive classes in $\gamma$. \\
	$\pmselc{P}{y}$ & \verb|\pmselc{P}{y}| & The class of couples $(y, \pmdscf{P}{y})$. \\
	$\pmmultc$ & \verb|\pmmultc| & The class of multipliable classes. \\
	$\pmmultr$ & \verb|\pmmultr| & The class of multipliable relations. \\
	$\pmmultax$ & \verb|\pmmultax| & The multiplicative axiom. \\
	$\pmanc{R}$, $\pmancc{R}$ & \verb|\pmanc{R}|, \verb|\pmancc{R}| & The ancestral and its converse. \\
	$\pmrst{R}$, $\pmrts{R}$ & \verb|\pmrst{R}|, \verb|\pmrts{R}| & The powers of the ancestral and its converse. \\
	$\pmmin{P}$, $\pmmax{P}$ & \verb|\pmmin{P}|, \verb|\pmmax{P}| & The minimum and maximum under $P$. \\
	$\pmpot{R}$, $\pmpotid{R}$ & \verb|\pmpot{R}|, \verb|\pmpotid{R}| & The products (strict and not) of an ancestral. \\
	 $\pmpo{R}$ & \verb|\pmpo{R}| & The product of a class of ancestrals $R$. \\
	 $\pmB$ & \verb|\pmB| & The relation of beginning under $P$. \\
	$\pmgen{P}$ & \verb|\pmgen{P}| & The generation of $P$. \\
	$\pmefr{P}{Q}$ & \verb|\pmefr{P}{Q}| & The equi-factor relation. \\
	$\pmipr{R}{x}$ & \verb|\pmipr{R}{x}| &  The non-distinct posterity of $x$ under $R$. \\
	$\pmjpr{R}{x}$ & \verb|\pmjpr{R}{x}| &  The distinct posterity of $x$ under $R$. \\
	$\pmfr{R}{x}$ & \verb|\pmfr{R}{x}| & The ancestry and posterity of $x$ under $R$. \\
	$\pmnc{\kappa}$, $\pmNc$ & \verb|\pmnc{\kappa}|, \verb|\pmNc| & The cardinal number of $\kappa$.
\end{tabular}

\end{document}
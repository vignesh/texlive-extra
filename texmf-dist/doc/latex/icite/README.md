---
author:
- Robert Alessi
title: 'The icite package – README file'
---

Overview
========

`icite` is designed to produce from BibTeX or BibLaTeX bibliographical
databases the different indices of authors and works cited which are
called _indices locorum citatorum_. It relies on a specific
`\icite` command and can operate with either BibTeX or BibLaTeX.

License and disclamer
=====================

icite – Indices locorum citatorum

Copyright ⓒ 2019--2020 Robert Alessi

Please send error reports and suggestions for improvements to Robert
Alessi:

-   email: <alessi@robertalessi.net>

-   website: <http://www.robertalessi.net/icite>

-   comments, feature requests, bug reports:
    <https://gitlab.com/ralessi/icite/issues>

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.

This release of icite consists of the following source files:

-   `icite.dtx`

-   `icite.ins`

-   `Makefile`

License applicable to the documentation
---------------------------------------

Copyright ⓒ 2019--2020 Robert Alessi

The documentation file `icite.pdf` that is generated from the
`icite.dtx` source file is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. To view a copy of this
license, visit <http://creativecommons.org/licenses/by-sa/4.0/> or send
a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Installation
============

1.  Run `'latex icite.ins'` to produce the `icite.sty` file.

2.  To finish the installation you have to move the `icite.sty` file into
    a directory where LaTeX can find it. See the FAQ on `texfaq.org`
    at <https://texfaq.org/FAQ-inst-wlcf> for more on this.

Development, Git Repository
===========================

Browse the code
---------------

You can browse icite repository on the web:
<http://git.robertalessi.net/icite>

From this page, you can download all the releases of `icite`. For
instructions on how to install `icite`, please see above.

Comments, Feature requests, Bug Reports
---------------------------------------

<https://gitlab.com/ralessi/icite/issues>

Download the repository
-----------------------

`icite` development is facilitated by git, a distributed version
control system. You will need to install git (most GNU/Linux
distributions package it in their repositories).

Use this command to download the repository

    git clone http://git.robertalessi.net/icite

A new directory named icite will have been created, containing
`icite`.

Git hosting
-----------

Make an account on <https://gitlab.com> and navigate (while logged in)
to <https://gitlab.com/ralessi/icite>. Click *Fork* and you will
have in your account your own repository of `icite` where you will
be able to make whatever changes you like to.


css-colors is copyright 2016 by Dr Engelbert Buxbaum
<engelbert_buxbaum@web.de>

This file may be distributed and/or modified

1. under the LaTeX Project Public License and/or
2. under the GNU Public License.

css-colors.sty defines the 143 named web-safe colors for use with
D.P. Carlisle's color-package and U. Kern's xcolor (xcolor is now
loaded by css-colors, thanks to K. Konrad for this suggestion).
They may be used by authors (with \usepackage) or by package
writers (with \RequirePackage, e.g. to create Beamer color themes).

css-colors.pdf provides a list of these colors with components in
rgb (hex, word, decimal) and hsl-format. The source for this file
is the spreadsheat css-colors.xlsx

These files are provided in the hope that they may be useful, but
without any warranties.

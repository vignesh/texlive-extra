This is the README file for gastex package.

GasTeX: Graphs and Automata Simplified in TeX

GasTeX is a set of LaTeX macros which allow to draw very easily graphs,
automata, nets, diagrams, etc...  under the picture environment of LaTeX.

Author: 
-------
Paul Gastin <http://www.lsv.ens-cachan.fr/~gastin>

Copyright: 2006 by Paul Gastin
----------
This program can be redistributed and/or modified under the terms
of the LaTeX Project Public License Distributed from CTAN
archives in directory macros/latex/base/lppl.txt.

Installation:
-------------
In order to use gastex you only need two files:
- gastex.sty which contains the definition of all the LaTeX macros (this file
could be in your working folder or where other .sty files are).
- gastex.pro which contains all the postscript procedures (this file could
be in your working folder but it is best placed where the tex.pro file is).

Usage: latex+dvips(+ps2pdf)
------
- Compile the source file with latex (not pdflatex) and generate the ps file 
with dvips. One may then use ps2pdf to get a pdf file.

Remarks:
--------
- gastex mainly generates postscript code so the pictures cannot be seen with a
dvi-viewer. One has to generate a ps file (latex+dvips) and view it with a 
ps-viewer. One can also generate a pdf file (latex+dvips+ps2pdf) and view 
it with a pdf-viewer.
- gastex does not work with pdflatex since it produces postscript code
which cannot be interpreted by a pdf viewer.  Instead, one should generate
the pdf file with latex+dvips+ps2pdf.

Examples: 
---------
- Several examples are provided on the gastex web page at
http://www.lsv.ens-cachan.fr/~gastin/gastex/gastex.html
- ex-gastex.tex contains some examples to learn gastex and to test your 
installation.
- ex-beamer-gastex.tex shows how gastex may be used with beamer in order to
produce very nice animated slides.  You have to compile the file through
latex-dvips-ps2pdf (not pdflatex).

Documentation:
--------------
See http://www.lsv.ens-cachan.fr/~gastin/gastex/gastex.html
The best is to learn with the examples (see above).
Comments in the file gastex.sty provide documentation for all macros.

Enjoy!
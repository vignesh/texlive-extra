leftindex -- left indices with better spacing
--------------------------------------

The package leftindex provides commands for typesetting left indices. Unlike
any other similar package I know of, leftindex also indents the left superscript,
providing much better spacing in general.

----------------------------------------------------------------
leftindex -- left indices with better spacing
Version: 0.1beta
Maintained by Sebastian Ørsted
E-mail: sorsted@gmail.com
Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt
----------------------------------------------------------------

Copyright (C) 2020 by Sebastian Ørsted <sorsted@gmail.com>

The package is loaded via \usepackage{leftindex}

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License (LPPL), either
version 1.3c of this license or (at your option) any later
version.  The latest version of this license is in the file:

http://www.latex-project.org/lppl.txt

This work is "maintained" (as per LPPL maintenance status) by
Sebastian Ørsted.
   \documentclass[12pt,a4paper]{article}
   \usepackage{bibarts} \usepackage[utf8]{inputenc} %\usepackage[english]{babel}
        
        %% bibsort -h -k

   \bacaptionsenglish
\setlength{\footnotesep}{2ex}   %% ... as in bibarts.sty 2.0; see README.txt %%

\hyphenation{Last-Name}

  \title{The \LaTeX\hy Package \BibArts \\[.25ex] {\normalsize\slshape 
    A package to assist in making bibliographical features common in the arts}}
  \author{\textsc{Timo Baumann}}
  \date{\small Version 2.2, \copyright\,2019}

  \setcounter{secnumdepth}{0}
  %\renewcommand{\kxxemph}{\em}

     \newcommand{\pkTitle}{k\kern -0.05em Title}
     \newcommand{\kTitle}{\sort{kTitle}\protect\pkTitle}
                        
     \newcommand{\pbs}{\string\ \unskip}
     \newcommand{\bs}{\protect\pbs}

     \let\lbashortmem=\{
     \let\rbashortmem=\}
     \renewcommand{\{}{{\normalfont\lbashortmem}}
     \renewcommand{\}}{{\normalfont\rbashortmem}}

\long\def\Doppelbox#1#2{\nopagebreak\vspace{2.25ex}%
   {\fbox{\parbox{.45\textwidth}{\frenchspacing\raggedright\footnotesize\ttfamily
    \ignorespaces #1}\hfill\parbox{.45\textwidth}{\vspace{1ex}%
    \begin{minipage}{.45\textwidth}\sloppy\small
    \renewcommand{\thempfootnote}{\arabic{mpfootnote}}%
    \setcounter{mpfootnote}{\arabic{footnote}}%
    {\ignorespaces #2}%
    \setcounter{footnote}{\arabic{mpfootnote}}%
    \end{minipage}
    \vspace{1ex}}}\vspace{2.4ex}}}

\let\mempage=\thepage 
\renewcommand{\pbapageframe}[1]{\underline{#1}}
\renewcommand{\thepage}{\textit{\bapageframe{\roman{page}}}}
\pagestyle{myheadings}


\begin{document}

\maketitle \thispagestyle{empty}

{\small\tableofcontents}

\vfill\noindent\rule{\textwidth}{.1pt}

\vspace{1.75ex}
\hbox{\parbox{7.7cm}{\footnotesize\noindent
\textbf{\BibArts~2.2 (9 files, 8 dated 2019\ko/03\ko/03):} \\[.875ex]
 \begin{tabular}{ll}%
 \texttt{README.txt}   & Version history since 1.3      \\[-1.75pt]
 \texttt{bibarts.sty}  & The \LaTeX\ style file         \\[-1.75pt]
 \texttt{ba-short.pdf} & This documentation here        \\[-1.75pt]
 \texttt{ba-short.tex} & Source of \texttt{ba-short.pdf}\\[-1.75pt]
 \texttt{bibarts.pdf}  & Full documentation (German)    \\[-1.75pt]
 \texttt{bibarts.tex}  & Source of \texttt{bibarts.pdf} \\[-1.75pt]
 \texttt{bibsort.exe}  & Binary to create the lists     \\[-1.75pt]
 \texttt{bibsort.c}    & Source of \texttt{bibsort.exe} \\[-1.75pt]
 \texttt{COPYING}      & License (dated 1993/11/28)     \\
 \end{tabular}} 
\\
\hspace*{2.4mm}
\parbox{5.6cm}{\vspace{-.4ex}%
  {\footnotesize\normalfont
   \hfil\texttt{bibarts}\kern.075em(\kern-.05em at\kern-.075em)\kern.075em\texttt{gmx.de}} \\[1.4ex]
   \tiny\sffamily
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
}}


\newpage
\section{Introduction}
 Type \verb|\usepackage{bibarts}| into your \textit{file}.\texttt{tex}, and
 \verb|\bacaptionsenglish| to switch to English captions (to name pages p.,
 not S.). They are used here. \verb|\bacaptionsfrench| sets French captions;
 default is \verb|\bacaptionsgerman|.

 \vspace{.75ex}\noindent 
 Full references to literature are created with (a page number \verb|[Pg]| is optional):

 \vspace{-.4ex}
 \Doppelbox
 {
  \bs vli\{FirstName\}\{LastName\}
  \\ \ \ \{The \bs ktit\{kTitle\}, 
  \\ \ \ \ Publishing Reference\}[Pg].
 }
 {
  \vli{FirstName}{LastName}
   {The \ktit{\kTitle}, Publishing Reference}[Pg].
 }

 \noindent
 Now, \BibArts\ can also write your appendix! \texttt{bibarts.sty} writes 
 the arguments of your \verb|\vli|\hy commands into the
 \textit{file}.\texttt{aux}, and \texttt{bibsort} creates your 
 List of Literature: Type \verb|\printvli| (p.\,\pageref{appendix}), 
 \hspace{.1em}and start \LaTeX\ + \texttt{bibsort} \textit{file} + \LaTeX.

 \vspace{.75ex}\noindent
 After you have introduced literature fully, you may use a shortened reference:
 
 \vspace{-.6ex}
 \Doppelbox
 {
  \bs kli\{LastName\}\{kTitle\}[Pg].
 }
 {\vspace{1.75ex}
  \kli{LastName}{\kTitle}[Pg].
 }

 \vspace{-.3ex}\noindent
 Use \verb|\vqu| and \verb|\kqu| \hspace{.1em}in the same way
 \hspace{.1em}to cite published historical documents:

\vspace{-.25ex}
\Doppelbox
{
 \bs vqu \{Carl von\} \{Clausewitz\} \{\bs ktit\{Vom Kriege\}.
 \\ \ Hinterlassenes Werk, 3\bs fup\{rd\} 
 \\ \ ed.\bs\ Frankfurt/M. 1991\}[3].
 \\[2.6ex] 
 \bs kqu\{Clausewitz\}\{Vom Kriege\}[3].
}
{
 \vqu {Carl von} {Clausewitz} {\ktit{Vom Kriege}.
 Hinterlassenes Werk, 3\fup{rd} ed.\ Frankfurt/M. 1991}[3].
 \\[1.25ex] 
 \kqu{Clausewitz}{Vom Kriege}[3].
}

 \vspace{-.25ex}\noindent 
 Then, \verb|\printvqu| will print a List of Published Documents 
 (full references). 

 \vspace{.75ex}\noindent 
 And \verb|\printnumvkc| (p.\,\pageref{vkc}) will print an index 
 of all your shortened references (from \verb|\kli|, \verb|\kqu|, and 
 from \verb|\ktit| inside the last argument of \verb|\vli| or \verb|\vqu|).
 
 \vspace{.75ex}\vfill\noindent
 There are also \BibArts\hy commands to cite periodicals 
 and archive documents:
 
 \vspace{-.35ex}
 \Doppelbox
 {
  \bs per\{Journal\}\string_Num\string_[Pg]
  \\[1ex]
  \bs arq\{Unpublished Document\}
  \\ \ \{Archive File Sig\}\string|Vol\string|(Folio)
 }
 {
  \per{Journal}_Num_[Pg]
  \\[.5ex]        
  \arq{Unpublished Document}
   {Archive File Sig}|Vol|(Folio)
 }

 \vspace{-.25ex}\noindent 
 \verb|\printper| your Periodicals, and \verb|\printarq| the List of Archive
 Files. Spaces are forbidden before the optional arguments \verb+[Pg]+,
 \verb+(Foilo)+, \verb+|Vol|+, or \verb+_Num_+.


\newpage
\noindent
In footnotes and \texttt{minipage} footnotes, \BibArts\ is introducing 
\textsc{ibidem} automatically. That means, that \verb|\kli|, \verb|\kqu|, 
\verb|\per|, and \verb|\arq| can change to \textsc{ibidem}:

\setcounter{footnote}{0}
{\footnotesize\begin{verbatim}
 <1>  ...\footnote{ \vli{Niklas} {Luhmann} {\ktit{Soziale Systeme}. 
             Grundriß einer allgemeinen Theorie, Frankfurt/M. 1984}.}

 <2>  ...\footnote{\kli{Luhmann}{Soziale Systeme}|1|[22], and 
                   \kli{Luhmann}{Soziale Systeme}|1|[23].}
 <3>  ...\footnote{\kli{Luhmann}{Soziale Systeme}|1|[23]. Next sentence.}
 <4>     \footnote{\kli{Luhmann}{Soziale Systeme}|2|[56].}

 <5>  ...\footnote{\arq{Haber to Kultusminister, 17 December 1914} 
           {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
             Litt~A, Nr.\,108}|2|(223\f).}
                                                
 <6>     \footnote{\arq{Setsuro Tamaru to Clara Haber, 24 December 1914} 
           {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
             Litt~A, Nr.\,108}|2|(226-231).}
                                                
 <7>     \footnote{\arq{Setsuro Tamaru to Clara Haber, 24 December 1914} 
           {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
             Litt~A, Nr.\,108}|2|(226-231).}
                                                
 <8>     \footnote{\arq{Valentini to Schmidt, 13 March 1911} 
           {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
             Litt~A, Nr.\,108}|1|(47).}
                                                
   \fillarq{GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
             Litt~A, Nr.\,108} {2\,Vols.}
\end{verbatim}}

  \noindent
  When you introduce a book,\footnote{ \vli{Niklas}{Luhmann}{\ktit{Soziale Systeme}.
  Grundriß einer allgemeinen Theorie, Frankfurt/M. 1984}.}
        you are free to add a \verb+|Vol|+, or not. But if you once have setted a \verb+|Vol|+,
  you will have to repeat that, when you refer to the same book in the following
  footnote (or say \verb|\clearbamem|).
  \verb+[Pg]+ is equivalent.\footnote{\kli{Luhmann}{Soziale
  Systeme}|1|[22], and \kli{Luhmann}{Soziale Systeme}|1|[23].}
  Here are different page numbers in the foregoing
  footnote.\footnote{\kli{Luhmann}{Soziale Systeme}|1|[23]. Next sentence.}\,%
  \footnote{\kli{Luhmann}{Soziale Systeme}|2|[56].}

  \vspace{1ex}\noindent
        And here are examples about how to cite archive sources.\footnote{\arq{Haber to 
        Kultusminister, 17 December 1914}
  {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, Litt~A, Nr.\,108}|2|(223\f).}\,%
  \footnote{\arq{Setsuro Tamaru to Clara Haber, 24 December 1914}
  {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23,
  Litt~A, Nr.\,108}|2|(226-231).}\,%
  \footnote{\arq{Setsuro Tamaru to Clara Haber, 24 December 1914}
  {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23,
  Litt~A, Nr.\,108}|2|(226-231).}\,%
  \footnote{\arq{Valentini to Schmidt, 13 March 1911} 
  {GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
  Litt~A, Nr.\,108}|1|(47).} 
        \fillarq{GStAPK, HA\,1, Rep~76~Vc, Sekt~1, Tit~23, 
                   Litt~A, Nr.\,108} {2\,Vols.}
  The \verb|\fillarq| is adding to the entry in the
        arq\hy list, that ``Nr.\,108'' has 2 volumes \baref{arqlist}. 
  

\newpage
\renewcommand{\thepage}{\mempage}
\setcounter{page}{1}
\setcounter{footnote}{0}
\section{Switches}
 For writing an essay without a List of Literature, type \verb|\conferize|
 at the top of your \LaTeX\ file; then, \verb|\kli| will print a cross\hy 
 reference to the full reference:

 \Doppelbox
 {\bs conferize\ ...\bs footnote\{
  \\[.15ex] \ \ Full ref.: \ \bs vli\{Niklas\} 
  \\[.05ex] \ \ \ \{Luhmann\} \b{\{}\bs ktit\{Soziale 
  \\[.15ex] \ \ \ \ \ \ \ \ Systeme\}. Grundriß 
  \\ \ \ \ \ einer allgemeinen Theorie, 
  \\ \ \ \ \ Frankfurt/M. 1984\b{\}}[22].\}
  \\[1.5ex] ... pp.\bs footnote\{ \bs notibidemize
  \\ \ \ \ \%no ibidem in this footnote\%
  \\ \ \ Shortened ref.: \bs kli\{Luhmann\} 
  \\ \ \ \ \ \ \ \{Soziale Systeme\}[23 \bs f].\}
 }
 {\conferize ...\footnote{
        Full ref.: \vli{Niklas}{Luhmann} {\ktit{Soziale Systeme}. 
        Grundriß einer allgemeinen Theorie, Frankfurt/M. 1984}[22].} 
  \texttt{[u, v]} or \texttt{[w-x]} or \texttt{[y\bs f]} or 
          \texttt{[z\bs baplural]} are creating pp.\footnote{\notibidemize 
        %no ibidem in this footnote%
        Shortened ref.: \kli{Luhmann} {Soziale Systeme}[23 \f].}
 }

 \noindent 
 If \textit{k\fhy commands} \verb|\kli| and \verb|\kqu| are never
 used, \verb|\notannouncektit| shortened references at \textit{v\fhy commands}
 ({\footnotesize cited as ...} will not be printed at \verb|\vli| and
 \verb|\vqu|).
 
 No historian? Set \verb|\notprinthints| $-$ you will never use \verb|\vqu|
 or \verb|\kqu|, and therefore, it is unnecessary to print \ehinttovliname,
 because there is only one \textit{v-list}.


\section{The extra \texttt{*}\{\ko\textit{argument}\} of v- and k\fhy commands}
 
 To cite \textit{books from teams}, set co\hy authors in star\hy arguments; use
 x\fhy commands:

 \Doppelbox
 {
  Set names ...\bs footnote\{Two:\
  \\ \ \bs xvli\{FirstName\}\{LastName\} 
  \\ \ \ *\b{\{}\bs vauthor\{co-FirstName\}
  \\ \ \ \ \ \ \{co-LastName\}\b{\}}
  \\ \ \ \{The \bs ktit\{kTitle2\} ETC\}.\}
  \\[1.25ex]
        ... to ibidemize.\bs footnote\{
  \\ \ \bs xkli\{LastName\}
  \\ \ \ *\{\bs kauthor\{co-LastName\}\}
  \\ \ \ \{kTitle2\}[11-14].\}
 }
 {
  Set names in \texttt{\bs vauthor}~$-$\,\texttt{\bs kauthor}.\footnote{Two:
   \xvli{FirstName}{LastName} 
          *{\vauthor{co-FirstName}
                     {co-LastName}}
          {The \ktit{\kTitle2} ETC}.}
  
   That's necessary to ibidemize.\footnote{\xkli{LastName}*{\kauthor{co-LastName}}{\kTitle2}[11-14].}
 }

 \noindent 
 You may use a \texttt{*}\fhy argument also in \verb|\vli| $-$ to mask text in the
 ibidemization:

 \Doppelbox
 {
  ...\bs footnote\{An edited book is
  \\ \ \bs vli\{FirstName\}\{LastName\} 
  \\ \ \ *\{\bs onlyvoll\{\bs editor\}\}
  \\ \ \ \{The \bs ktit\{kTitleEd\} ETC\}[2].\}
  \\[1.25ex]
  \bs footnote\{ \%Without *-argument:\
  \\ \ \bs kli\{LastName\}\{kTitleEd\}[3, 6].\}
 }
 {
  \texttt{\bs editor} has no sorting 
           weight.\footnote{An edited book is
   \vli{FirstName}{LastName} 
          *{\onlyvoll{\editor}}
          {The \ktit{\kTitle Ed} ETC}[2].}
  \footnote{ %Without *-argument:\
  \kli{LastName}{\kTitle Ed}[3, 6].}
 }


 \newpage
 \noindent 
 For books with three or more authors, you have to set all `middle' authors
 in \verb|\midvauthor| (or \verb|\midkauthor|), and the `last' in
 \verb|\vauthor| (or \verb|\kauthor|):

 \vspace{-.125ex}
\Doppelbox
 {
   ...\bs footnote\{See
   \\ \ \bs xvli\{Manfred F.\}\{Boemeke\}
   \\ \ \ *\b{\{}\bs midvauthor\{Roger\}
   \\ \ \ \ \ \ \ \ \ \ \ \{Chickering\} 
   \\ \ \ \ \ \bs vauthor\{Stig\}\{Förster\}\%
         \\ \ \ \ \ \bs onlyvoll\{ \bs editors\}\b{\}}
   \\[.2ex] \ \ \b{\{}\bs ktit\{Anticipating Total War\}. 
   \\ \ \ \ The German and American 
   \\ \ \ \ Experiences 1871-{}-1914, 
   \\ \ \ \ Cambridge/U.K. 1999\b{\}}.\}
   \\[1ex]
   ... all LastNames.\bs footnote\{
   \\ \ \bs xkli\{Boemeke\}
   \\ \ \ *\b{\{}\bs midkauthor\{Chickering\} 
   \\ \ \ \ \ \bs kauthor\{Förster\}\b{\}}
   \\ \ \ \{Anticipating Total War\}[9\bs f]!\}
 }
 {
   \texttt{bibsort} is sorting \texttt{\bs vauthor\{F\}\{L\}} 
   and \texttt{\bs midvauthor\{F\}\{L\}} as \texttt{L F}.\footnote{See
   \xvli{Manfred F.}{Boemeke}*{\midvauthor{Roger}{Chickering} 
   \vauthor{Stig}{Förster}%
         \onlyvoll{ \editors}}{\ktit{Anticipating Total War}. 
   The German and American Experiences 1871--1914, Cambridge/U.K. 1999}.}

   You have to repeat all LastNames, but not the argument of 
         \texttt{\bs onlyvoll}.\footnote{\xkli{Boemeke}*{\midkauthor{Chickering} \kauthor{Förster}}
         {Anticipating Total War}[9\f]!}
 }


\vspace{-1ex}
\section{IBIDEM and inner references to literature}
 
\vspace{-.125ex}
 To cite articles in journals, use \verb|\per| inside the last argument of a
 v\fhy command:

\Doppelbox
{
...\bs footnote\{See \bs vqu \{John 
\\ \ \ Frederick Charles\} \{Fuller\} 
\\ \ \b{\{}Gold Medal (Military) 
\\ \ \ \bs ktit\{Prize Essay\} for 1919, 
\\ \ \ first published in: 
\\ \ \ \bs per\{Journal of the Royal 
\\ \ \ \ \ \ \ \ United Service 
\\ \ \ \ \ \ \ \ Institution\}\string_458 
\\ \ \ \ \ \ \ \ \ (1920)\string_[239-274]\b{\}}*[240].\}
\\[1.5ex]
...\bs footnote\{ \bs kqu \{Fuller\} 
\\ \ \ \ \ \ \ \ \ \ \ \{Prize Essay\}[241].\}
\\[1.5ex]
... here.\bs footnote\{ \bs vqu\{R.\} 
\\ \ \{Chevenix Trench\} 
\\ \ \{Gold Medal (Military) 
\\ \ \ \bs ktit\{Prize Essay\} for 1922, 
\\ \ \ in: \bs per\{Journal of the 
\\ \ \ \ \ \ \ \ Royal United Service 
\\ \ \ \ \ \ \ \ Institution\}\string_470 
\\ \ \ \ \ \ \ \ \ (1923)\string_[199-227]\}*[200].\}
}
{
You can use \texttt{*[Pg]} to cite a
certain page inside \texttt{[PgBegin-PgEnd]}.
Do not type spaces before \texttt{*[Pg]}.\footnote{See 
\vqu {John Frederick Charles} {Fuller} 
{Gold Medal (Military) \ktit{Prize Essay} for 1919, 
first published in: \per{Journal of the Royal United Service 
Institution}_458 (1920)_[239-274]}*[240].}

\BibArts\ creates an outer \textsc{ibidem} 
here.\footnote{ \kqu {Fuller} {Prize Essay}[241].}

\BibArts\ creates an inner \textsc{ibidem} 
here.\footnote{ \vqu{R.} 
{Chevenix Trench} 
{Gold Medal (Military) \ktit{Prize Essay} for 1922, 
in: \per{Journal of the Royal United Service 
Institution}_470 (1923)_[199-227]}*[200].\label{third}}
}

 \noindent 
 After the main arguments of \verb|\per|, or \verb|\vli|, etc., you are free
 to type \verb+_Num_+, or \verb+|Vol|+; both are only printing different
 separators (see in note~\ref{third}: {\footnotesize no.\,Num}).

 \noindent
 To cite articles in books, you can use \verb|\vli|\,+\,\verb|\ktit| for the book 
 \textit{inside the last v\fhy argument}. \verb|\printvli| is \textit{printing} such inner 
 references \textit{as shortened references}; the \textit{full inner reference} 
 appears as separate item (see \underline{Publ}). For further articles from 
 the same book you may use an inner \verb|\kli| for the book:

 \Doppelbox{
 ...\bs footnote\{\bs vli\{FN1\}\{LN1\} \b{\b{\{}}The 
 \\[-.2ex] \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \bs ktit\{First\}, 
 \\[.25ex] \ in: \bs vli\{iFN\}\{iLN\}\{The \bs ktit\{iT\} 
 \\[.25ex] \ \ \ \bs protect\bs underline\{Publ\}\}\b{\b{\}}}.\}
 \\[1ex] \bs footnote\{\bs kli\{LN1\}\{First\}.\}
 \\[1ex] \bs footnote\{\bs vli[m]\{FN1\}\{LN1\} \b{\{}The 
 \\[.2ex] \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \bs ktit\{Second\}, 
 \\[-.05ex] \ in: \bs kli\{iLN\}
 \\[.15ex] \ \ \ \{iT\}[PgBegin\bs hy PgEnd]\b{\}}*[Pg].\}
 }
 {
 \texttt{\bs vli[f]}...\ or \texttt{\bs kli[f]}...\ would refer to `the same female author'.%
 \footnote{\vli{FN1}{LN1}{The \ktit{First}, in: \vli{iFN}{iLN} {The \ktit{\protect\fbox{iT}} \protect\underline{Publ}}}.}
 \footnote{\kli{LN1}{First}.}
 \footnote{\vli[m]{FN1}{LN1} {The \ktit{Second}, in: \kli{iLN}{\protect\fbox{iT}}[PgBegin\hy PgEnd]}*[Pg].}
 }

\noindent 
If an inner ibidemization (as well as an inner shortened referencing in the bibliography)  
is not wanted, use \verb|\ntvauthor| instead of the inner v\fhy command:

\Doppelbox
{
 \bs footnote\{\bs vli\{Jost\}\{Dülffer\}
 \\ \ \{\bs em \bs ktit\{Einleitung\}\bs em, in: 
 \\ \ \ \bs ntvauthor\{Jost\}\{Dülffer\} 
 \\ \ \ Bereit zum Krieg\}[9].\}
 \\[1ex]
 \bs footnote\{\bs kli\{Dülffer\} 
 \\ \ \{Einleitung\}[9].\}
}
{
 This is also an example to show how to {\em emphasize} 
 inside the full title.\footnote{\vli{Jost}{Dülffer}
   {\em \ktit{Einleitung}\em, in: 
   \ntvauthor{Jost}{Dülffer} Bereit zum Krieg}[9].}
 \footnote{\kli{Dülffer} 
   {Einleitung}[9].}
}


\vfill\noindent 
If you use \verb|\printnumvli| instead of \verb|\printvli|, the page and
footnote numbers will be printed additionally (\textsc{iLN}: inner\,LastName
/ \fbox{iT}: inner\,k\kern -0.05em Title):

\vfill
{\baonecolitemdefs\printnumvlilist}


\let\memvlititlename=\evlititlename
\renewcommand{\evlititlename}{\texttt{\bs printvli} \ \memvlititlename}

\let\memvqutitlename=\evqutitlename
\renewcommand{\evqutitlename}{\texttt{\bs printvqu} \ \memvqutitlename}

\let\membibtitlename=\ebibtitlename
\renewcommand{\ebibtitlename}{\texttt{\bs printbibtitle} \ \membibtitlename}


\newpage\noindent
The command sequence \verb|\printbibtitle| \verb|\printvli| \verb|\printvqu| 
\label{appendix} will print an easy appendix. If you type \hspace{.01em}
\texttt{bibsort -k} \hspace{.02em} to DOS, you will get \hspace{.005em}
$\sim$ \hspace{.005em} in both v\fhy lists, when the name (first name and last
name) of an author is repeated:

\printbibtitle \printvli \printvqu

\vfill\noindent 
To change the size, in which a list is printed,
you may type e.\,g.\ \verb|{\small| \verb|\printvli}|. \BibArts\
provides further commands to be executed on bigger lists:
\verb|\bibsortheads| will print capital letters between two items inside the 
lists, if the initial letter changes, whereas \verb|\bibsortspaces| will
print only a bigger vertical space there instead. Both features are 
prepared by \verb|bibsort|.


\newpage
\noindent
\subsection*{\texttt{\bs printnumvkc} \ \evkctitlename}
\addcontentsline{toc}{subsection}{\texttt{\bs printnumvkc} \ \evkctitlename}
\verb|\printnumvkc| prints an index of all your shortened references in 
\texttt{twocolumn}, whereas \verb|\printnumvkclist| \label{vkc} just prints
your list without \texttt{headings} or title:\vspace{.5ex}

{\batwocolitemdefs\printnumvkclist}

\let\mempertitlename=\epertitlename
\renewcommand{\epertitlename}{\texttt{\bs printper} \ \mempertitlename}
\vspace{-.5ex}\printper

\renewcommand{\epertitlename}{\texttt{\bs printnumper} \ \mempertitlename}
\vspace{-.5ex}\printnumper

\let\memarqtitlename=\earqtitlename
\renewcommand{\earqtitlename}{\texttt{\bs printarq} \ \memarqtitlename}
\vspace{-.5ex}\printarq \balabel{arqlist}

\renewcommand{\earqtitlename}{\texttt{\bs printnumarq} \ \memarqtitlename}
\vspace{-.5ex}\printnumarq

\vspace{2ex}\noindent
If you type \verb|\arqsection{GStAPK}{Geheimes Staatsarchiv}|, 
you will get ``\textsf{GStAPK} $-$ \textbf{Geheimes Staatsarchiv}'' 
on top of all GStAPK\hy entries.


\newpage
\section{Additional features}

\BibArts\ provides an environment to send unused bibliographical information into 
the lists. This information appears on the vqu\fhy list, but is invisible here:

\vspace{-1.25ex}
{\footnotesize\begin{verbatim}
 !\begin{unused} \sethyphenation{ngerman}%  %% other hyphenation optional
   \vqu{Karl}{Marx}{Das \ktit{Kapital}, in: \midvauthor{Karl}{Marx}
   \ntvauthor{Friedrich}{Engels} Werke, \ersch|3|[1]{Berlin}{1962--1964}}
  \end{unused}!  Note, that in         %% vol.|3| and ed.[1] are optional
\end{verbatim}}

\vspace{-1.25ex}\noindent
 !\begin{unused} \sethyphenation{ngerman}%  %% other hyphenation optional
   \vqu{Karl}{Marx}{Das \ktit{Kapital}, in: \midvauthor{Karl}{Marx}
   \ntvauthor{Friedrich}{Engels} Werke, \ersch|3|[1]{Berlin}{1962--1964}}
  \end{unused}!  Note, that in         %% vol.|3| and ed.[1] are optional
\verb|{unused}|\hy environments, \textit{inner v-commands} will not send 
an own item (full reference) to the v\hy list; so, you have to repeat
them separately.

\vspace{1ex}\noindent
\BibArts\ does not only help to cite. The environment \verb|{originalquote}|
helps to quote from literature or sources. You may call all hyphenation
settings, which your \LaTeX\ possesses, in the \verb|[|\textit{optional
argument}\verb|]| of the environment:

\vspace{-.5ex}
\Doppelbox
{\vspace{.3ex}
 \bs begin\{originalquote\}[german]\%old
 \\[.4ex] \ \ \string`\string`Dies ist die erste 
 \\ \ \ Wechselwirkung und das  
 \\ \ \ erste Äu\bs hyss erste, \bs fabra
 \\ \ \ \{...\}.\string'\string'\bs footnote \b{\{}This \string`inner
 \\ \ \ \ \ Eszett\string' splits new:
 \\ \ \b{\b{\{}}\bs sethyphenation\{ngerman\}
 \\[-.4ex] \ \ Au\bs hyss er\b{\b{\}}}. \bs kqu\{Clausewitz\} 
 \\[-.4ex] \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \{Vom Kriege\}[19].\b{\}}
 \\[.3ex] \bs end\{originalquote\}
 \vspace{.2ex}
}
{\renewcommand{\originalquotetype}{\footnotesize}%
 \begin{originalquote}[german]%old
 ``Dies ist die erste
 Wechselwirkung und das
 erste Äu\hyss erste, \fabra
 {...}.''\footnote {This `inner
   Eszett' splits new:
 {\sethyphenation{ngerman}
  Au\hyss er}. \kqu{Clausewitz}
                {Vom Kriege}[19].
 \\[.5ex] \hspace*{3mm} \texttt{\% In \bs scshape, \bs hyss prints }{\scshape \hyss}\texttt{,}
 \\ \hspace*{3mm} \texttt{\% and splits }{\scshape s-s}\,\texttt{ (old AND new)!}%
 \vspace{-1.5ex}%
 }
 \end{originalquote}
}

\vspace{-.125ex}\noindent 
In quotations, you may use \verb|\abra| to print certain 
additives in small angular brackets; \verb|\fabra| affixes 
the argument to the following text (no line breaks):

\vspace{.75ex}\noindent
{\small
   \verb|       \abra{,}               => | Red\abra{,} blue and green were the \\[-.25ex]
   \verb|       \abra{.}\newsentence   => | colours\abra{.}\newsentence And \hspace{2.25em} \verb|% spacing| \\[-.25ex]
   \verb|    \abra{...}   \abra{\dots} => | there \abra{...} were \abra{\dots} others, \\[-.25ex]
   \verb|       \abra{---}             => | \abra{---} let's say \abra{---} \\[-.25ex]
   \verb|       \abra{-}   \abra{--}   => | green\abra{-}red\abra{--}painted. \\[-.25ex]
   \verb|    \fabra{`}    \fabra{'}    => | \hbox to 6em{\fabra{`}Ha\fabra{'},\hfill} \verb|% ASCIIs 96 and 39| \\[-.25ex]
   \verb|    \fabra{``}    \abra{''}   => | \fabra{``}Good!\abra{''} \\[-.25ex]
   \verb|    \fabra{"}     \abra{"}    => | \hbox to 6em{\fabra{"}Good!\abra{"}\hfill}  \verb|% or \abra{\dq}| \\[-.25ex]
   \verb|      \fabra{e}g.             => | \hbox to 6em{\fabra{e}g.\hfill} \verb|% unknown = normal|}

\vspace{1.5ex}\vfill\noindent 
\BibArts\ defines
\hspace{.1em}\verb|S\fup{te}|\hspace{.3em}\verb|=>|\hspace{.3em}S\fup{te}
(if that command for \textit{French up} is undefined), and it provides
commands to set ordinals in English, French and German:

\vspace{.875ex}\vfill\noindent
{\small
  \verb|    \eordinal{103} Assistant. => | \eordinal{103} Assistant. \\[-.2ex]
  \verb|    Le \fordinalm{1} homme.   => | Le \fordinalm{1} homme.   \\[-.25ex]
  \verb|    La \fordinalf{1} femme.   => | La \fordinalf{1} femme.   \\[-.2ex]
  \verb|    Der 1\te August.          => | Der 1\te August.} 


 \newpage\noindent
 For printing formatted abbreviations in your text, you may use 
 \verb|\abk{|\textit{xyz}\verb|}|. \textit{xyz} will only appear 
 on the List of Abbreviations, if it is resolved (defined):

 \vspace{-.25ex}
 \Doppelbox
 {
      \bs abkdef\{HMS\}\{Her Majesty\string's Ship\}
   \\[.35ex] \ \ or
   \\[.35ex] \bs defabk\{Her Majesty\string's Ship\}\{HMS\}
   \\[.35ex] \ \ and then \bs texttt\{bibsort\}
   \\[.35ex] \ \ will accept \bs abk\{HMS\}.
   \vspace{-1ex}\strut
 }
 {
      \abkdef{HMS}{Her Majesty's Ship}
   \\  or
   \\ \defabk{Her Majesty's Ship}{HMS}
   \\  and then \texttt{bibsort}
   \\  will accept \abk{HMS}.
   \vspace{-1ex}\strut
 }

\noindent 
For a correct spacing at the end of a sentence, you can repeat 
a full stop: \verb|\abk{U.\,K.}. Next |... \verb|=>| \abk{U.\,K.}. Next
... (also: \verb|\abk{e.\,g.}. Next |...).
You may use \verb|\printnumabk| (or \verb|\printabk|) to print a 
List of Abbreviations:

\vspace{-.325ex}
\printnumabklist

\noindent 
\BibArts\ provides no index with sub\hy items, but registers (geographical,
subject, and person). The commands to fill the registers have one argument;
they are invisible in your text, e.\,g.: \verb|\addtogrr{London}|,
\verb|\addtosrr{Ship}|, and \verb|\addtoprr{Churchill}|\,.
\verb|\printnumgrr|, \verb|\printnumsrr|, and \verb|\printnumprr| the
registers in your appendix. This has nothing do do with \textsc{MakeIndex}.

\vspace{1ex}\noindent 
A last feature of \BibArts\ are fill\hy commands. \verb|\fillgrr|, 
\verb|\fillsrr|, \verb|\fillprr|, \verb|\fillper|, and \verb|\fillarq| 
have two arguments. The first has to be identical with \textit{the} argument 
of a register\hy entry or \verb|\per|\hy command, or \textit{the second} 
argument of an \verb|\arq|\hy command (the archive file information). Use 
fill\hy commands for adding text componds, which you don't want to type 
in every single entry:

\vspace{-.25ex}
\Doppelbox
{\vspace{1.2ex}
 \ \bs fillprr\{Churchill\}\{1874-1965\} \\[1.5ex] 
 Churchill \bs addtoprr\{Churchill\}
 was prime minister.\bs footnote\b{\{}
 \\ \ Born \bs addtoprr\{Churchill\} 
 \\ \ Blenheim Palace.\b{\}} \\[1.325ex] 
 \bs renewcommand\{\bs xrrlistemph\}\{\bs em\} \\ 
 \bs printnumprr
 \vspace{1ex}
}
{\vspace{-1.1ex}%
 \fbox{\begin{minipage}{.95\textwidth}
    \setcounter{mpfootnote}{\arabic{footnote}}%
 \vspace{.125ex}
  \fillprr{Churchill}{1874-1965}
 Churchill \addtoprr{Churchill}
 was prime minister.\footnote
 { Born \addtoprr{Churchill} 
   Blenheim Palace.}
    \setcounter{footnote}{\arabic{mpfootnote}}%
 \end{minipage}}
 \\[1.6ex]
 \fbox{\begin{minipage}{.95\textwidth}
 \vspace{.875ex}
 \subsubsection*{\eprrtitlename}        
   \vspace{-.75ex}%
 \renewcommand{\xrrlistemph}{\em}%
 {\footnotesize \printnumprrlist}%
 \vspace{-1.375ex}%
 \end{minipage}}%
 \vspace{-1.3ex}%
}

\vspace{.5ex}\vfill\noindent 
Please use commands like \verb|\bfseries| to change
the fonts, but not \verb|\textbf|:


\vspace{1.5ex}\vfill\noindent
{\small\begin{tabular}{lll}
\textsf{Command}    & \textsf{Predefined}          & \textsf{Executed on} \\[.325ex]
\verb|\xrrlistemph| & \verb|{}|                    & entries on grr\fhy, srr\fhy, and prr\hy lists\\
\verb|\abkemph|     & \verb|{\sffamily}|           & abbreviations in your text \\
\verb|\abklistemph| & \verb|{\bfseries}|           & abbreviations on the abk\hy list \\
\verb|\kxxemph|     & \verb|{}|                    & last argument of k\hy commands \\
\verb|\peremph|     & \verb|{\normalfont\scshape}| & periodicals (\,\verb|=> {\upshape}|\,) \\
\end{tabular}}


\end{document}









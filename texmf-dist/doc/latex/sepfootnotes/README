Package:     sepfootnotes
Description: Support footnotes and endnotes from separate files

This package supports footnotes and endnotes from separate files. This is
achieved with commands \sepfootnotecontent and \sepfootnote; the former defines
the content of a note, while the latter typesets that note.

2016/07/18 v0.3c
 * Package options        Note definitions may take global scope

2014/07/22 v0.3b
 * Documentation          Minor formatting changes to page 4

2014/03/23 v0.3a
 * Package options        Error message if key is undefined or already defined
 * \{prefix}quicknote     Typeset a note without a key
 * Documentation          \print{prefix}note instead of \print{prefix}content

2013/01/17 v0.2
 * utf8                   Support keys with UTF-8 characters
 * \newfootnotes          Use the standard footnote counter
 * \newfootnotes*         Use its own footnote counter
 * \sepfootnote           Default footnote apparatus
 * \addto{prefix}notes    Add text and commands to endnotes
 * \print{prefix}note     Print the content of a note
 * \jobname.ent-{prefix}  Renamed endnote external file
 * \{prefix}noteref       Removed

2012/03/06 v0.1
 Initial version

--------------------------------------------------------------------------------
Installation:

*Copy sepfootnotes.sty to a location where LaTeX will find it.
  See http://www.tex.ac.uk/faq	

--------------------------------------------------------------------------------
Usage:

*See sepfootnotes.pdf

--------------------------------------------------------------------------------
Copyright (C) 2013-2016 Eduardo C. Lourenço de Lima

This material is subject to the LaTeX Project Public License. See
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html
for the details of that license

This work has the LPPL maintenance status "author-maintained"

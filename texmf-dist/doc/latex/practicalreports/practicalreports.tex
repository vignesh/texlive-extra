\documentclass[12pt]{article}

\usepackage[right=1in, top=1in, left=1in, bottom=1in]{geometry}

\usepackage[parfill]{parskip}

\usepackage{practicalreports}


\begin{document}

\begin{titlepage}
\vspace*{\fill}
\begin{center}
{\Huge \textbf{practicalreports} Documentation}\\[0.5cm]
{\Large Version: 2.0.3}\\[0.4cm]
{\Large Date: $7^{th}$ of October 2019}\\[0.2cm]
{\small Author: Justin Cawood}
\end{center}
\vspace*{\fill}
\end{titlepage}

\tableofcontents
\newpage

\section{Practical Reports Package}
This package provides macros for displaying scientific notation, simple title pages, tables, figures, headers footers.

\section{Importing the Package}
Import the package by typing $\backslash$usepackage\{practicalreports\} at the top of the tex file.

\section{Required Packages}
All of the below packages are required for the macros within this package. They should be installed when you install this package

\begin{itemize}
\item \textbf{array}
\item \textbf{mathtools}
\item \textbf{longtable}
\item \textbf{scrextend}
\item \textbf{float}
\item \textbf{fancyhdr}
\item \textbf{lastpage}
\item \textbf{pdfpages}
\item \textbf{pdffor}
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Exponent Macro}
This macro is much quicker and easier than the rest of the scientific notation macros in this package.

\subsection{How to use it}
To use this macro simply type "\textbf{value} $\backslash$E\{Power\} \textbf{units}". eg. 2.997 $\backslash$E\{8\} m\$s\^\ \{-1\}\$ This will output 2.997\E{8} m$s^{-1}$. The \textbf{value} and \textbf{units} are not required, so the macro can be also used like this, $\backslash$E\{8\}, this will output \E{8}.

\subsection{Useful tricks}
\textbf{\!\!\! If you are already in math mode use $\backslash$e instead\!\!\! }

If the power/exponent being use in the scientific notation is only 1 number or character, there is no need for the curly brackets \{\}. It is much quicker to simply type $\backslash$E8 than to type $\backslash$E\{8\}. They both output the same result \E8. However this wont work for powers with more than one number or character eq. $\backslash$E88 will output \E88. Instead this will have to be written as $\backslash$E\{88\}, this will output \E{88}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Setting the page to fancy}
Before any headers or footers can be used the pages need to be set to fancy. This can be done by typing $\backslash$setpagefancy just below the import statements but not inside the $\backslash$begin\{document\} $\backslash$end\{document\}. Once the page is set to fancy the page numbering will go away, this can be added back in with the macros from this package. Before the page is set to fancy is shown in \reffigure{normal_page} and after the page is set to fancy is shown in \reffigure{fancy_page}

\newfigure{Normal Page}{Normal page}{normal_page.png}{normal_page}

\newfigure{Setting the page to fancy}{Output of the macro $\backslash$setpagefancy}{fancy_page.png}{fancy_page}

\subsection{Headers}
To add a header use the macro $\backslash$header\{left header\}\{center header\}\{right header\}. An example is shown in below and in \reffigure{fancy_header}.

$\backslash$header\{left header\}\{center header\}\{right header\} this will display

\newfigure{Adding a fancy header}{Output of the macro $\backslash$header\{left header\}\{center header\}\{right header\}}{fancy_header.png}{fancy_header}

\subsection{Footers}
To add a footer use the macro $\backslash$footer\{left footer\}\{center footer\}\{right footer\}. An example is shown below and in \reffigure{fancy_footer}.

$\backslash$footer\{left footer\}\{center footer\}\{right footer\} this will display

\newfigure{Adding a fancy footer}{Output of the macro $\backslash$footer\{left footer\}\{center footer\}\{right footer\}}{fancy_footer.png}{fancy_footer}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Title Page}
To create a title page, at the beginning of the document after the $\backslash$begin\{document\} type $\backslash$newtitlepage\{Title\}\{Author\}\{Date\}
An example is shown below and the result is shown in \reffigure{new-title-page}


\newfigure{New title page}{Result of $\backslash$newtitlepage\{Title\}\{Author\}\{Date\}}{new_title_page.png}{new-title-page}

\section{Figures}
These macros can include figures and reference them in a tex file.

\subsection{Including a Figure}
To include a figure type $\backslash$newfigure\{Caption\}\{Description\}\{file\_ name.jpg\}\{reference\}.\\ \LaTeX \ accepts the file types pdf, jpg and png for figures. \reffigure{example-figure} is an example of using this macro.

\newfigure{Example Figure}{Result of using $\backslash$newfigure\{Caption\}\{Description\}\{file name\}\{reference\}}{example_figure.jpeg}{example-figure}

\subsection*{Scaling Figures}
This macro allows you to shrink figures that are two large or expand figures that are too small.
The macro is in the form

$\backslash$newsfigure\{Caption\}\{Description\}\{file\_ name.jpg\}\{reference\}\{scale factor\}

\subsubsection{Shrink Example}
\reffigure{example-shrink-figure}, shows an example of decreasing the size of a figure.

\newsfigure{Example Figure}{Result of using $\backslash$newsfigure\{Caption\}\{Description\}\{file name\}\{reference\}\{0.4\}}{example_figure.jpeg}{example-shrink-figure}{0.4}

\subsubsection{Expand Example}
\reffigure{example-expand-figure}, shows an example of increasing the size of a figure.

\newsfigure{Example Figure}{Result of using $\backslash$newsfigure\{Caption\}\{Description\}\{file name\}\{reference\}\{1.6\}}{example_figure.jpeg}{example-expand-figure}{1.6}

\subsection{Referencing a Figure}
This macro allows figures to be referenced by simply typing $\backslash$reffigure\{reference\}. If a figure is ever added or removed, this macro will take care of the numbering so the figures are always referenced correctly. An example is shown below.

$\backslash$reffigure\{example-figure\} this will output "\reffigure{example-figure}". This is the from the section of how to include a figure. Sometimes instead of displaying "\reffigure{example-figure}", instead it might display Figure ??. This can be fixed by building the tex file a second time. If this does not work then the reference inside the \{\} is wrong.


\section{Tables}
These macros can create normal tables, long tables and reference tables.
The difference be normal tables and long tables is, once a normal table becomes large than the size of an entire page the extra content will be cut off so the table fits the page. However a long table when is becomes larger than an entire page the extra content will be pushed to the next page.

\subsection{Creating a Table}
To create a normal table use the macro

$\backslash$newtable\{Caption\}
\{Column Data Positioning\}
\{Column Headings\}
\{Table Data\}\\
\{Reference\}

It is often more readable to type the macro in the form below.

$\backslash$newtable\{Caption\} \\
\{Column Data Positioning\} \\
\{Column Headings\} \\
\{Table Data\} \\
\{Reference\}

An example of a normal table is show below and the output is \reftable{Normal-Table}

$\backslash$newtable\{Normal Table Example\} \\
\{lcr\} \\
\{Left Positioned Column \& Center Positioned Column \& Right Positioned Column\} \\
\{1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3\} \\
\{Normal-Table\}

\newtable{Normal Table Example}
{lcr}
{Left Positioned Column & Center Positioned Column & Right Positioned Column} {1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3}
{Normal-Table}

The "Column Data Positioning" field is required. It accepts any of the three positioning values "l = left  c = center  r = right", each column need to have a positioning value eg. if there are two columns the possible positions are " ll, lc, lr, cl, cc, cr, rl, rc, or rr". The positioning field is required however the other fields aren't. But to display something in the table the each field will need something.

\subsection{Creating a Long Table}
The long table macro is very similar to the normal table macro
To create a long table use the macro

$\backslash$newlongtable\{Caption\}
\{Column Data Positioning\}
\{Column Headings\}
\{Table Data\}
\{Reference\}

It is often more readable to type the macro in the form below.

$\backslash$newlongtable\{Caption\} \\
\{Column Data Positioning\} \\
\{Column Headings\} \\
\{Table Data\} \\
\{Reference\}

An example of a long table is show below and the output is \reftable{Long-Table}

$\backslash$newlongtable\{Long Table Example\} \\
\{lcr\} \\
\{Left Positioned Column \& Center Positioned Column \& Right Positioned Column\} \\
\{1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3 $\backslash$ $\backslash$ \\
1 \& 2 \& 3\} \\
\{Long-Table\}

\newlongtable{Long Table Example}
{lcr}
{Left Positioned Column & Center Positioned Column & Right Positioned Column} 
{1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3 \\
1 & 2 & 3}
{Long-Table}

Clearly this table was too long to fit on the previous page so the long table macro pushes the excess to the next page. A normal table would have cut off the excess and wouldn't have displayed it.

The column positioning for a long table is the same as for a normal table.

\subsection{Referencing a Table}
To reference a table the macro $\backslash$reftable\{reference\} can be used. If a table is ever added or removed, this macro will take care of the numbering so the tables are always referenced correctly.
Below is an example of referencing tables.

$\backslash$reftable\{Normal-Table\} is the example normal table used in this manual.
This will output "\reftable{Normal-Table} is the example normal table used in this manual."

Another example is shown below

$\backslash$reftable\{Long-Table\} is the example long table used in this manual.
This will output "\reftable{Long-Table} is the example normal table used in this manual."

\section{Boxing Content}
To place a box around content simply use the macro $\backslash$boxanswer\{Content To be boxed\} this will output 

\boxanswer{Content To be boxed}

The words are joined together and displayed differently because this macro is already in math mode. So to display normal text use the $\backslash$text\{\} macro. So the previous example an be written as

$\backslash$boxanswer\{$\backslash$text\{Content To be boxed\}\}
This will display

\boxanswer{\text{Content To be boxed}}

Most of the time this will be used to box an answer to a maths question. An example of this is shown below.

$\backslash$boxanswer\{$y = mx + c \\
E = mc^2\\
E = hf$\}

This will out put

\boxanswer{y = mx + c \\
E = mc^2\\
E = hf}

\section{Other Macros}

\subsection{Shift}
The shift macro allows you to shift content left or right. $\backslash$shift\{shift amount in cm\}\{content to be shifted\}.

Tables can be shifted.

\subsection{Good Footer}

The $\backslash$goodfoot macro will do the footer for you in the form "Page x of n". The page mus be set to fancy first though using the $\backslash$setpagefancy macro.

\subsection{Join Multiple Reports}
To combine multiple pdf files together into one single pdf file use the macro $\backslash$joinpdfs\{\}. You don't need the file extension just list all the pdf files that you wish to join as follows.

$\backslash$joinpdfs\{pdf\_1, pdf\_2, pdf\_3, pdf\_4, .....\}.

This macro should be used in a new tex file because the pdf files will be joined as one file with the name of the tex file this macro is used in.

The pdf files being joined must be in the same directory as the tex file this macro is used in.

\section{Appendices}

\subsection{Appendix A - List of Figures}

\listoffigures

\subsection{Appendix B - List of Tables}

\listoftables

\end{document}
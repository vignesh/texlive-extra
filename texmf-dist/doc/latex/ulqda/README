ulqda - A LaTeX package supporting Qualitative Data Analysis
  [2009/06/12 v1.1]

Copyright (C) 2009 by Ivan Griffin (ivan.griffin@ul.ie)

This file may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either
version 1.2 of this license or (at your option) any later
version. The latest version of this license is in:

    http://www.latex-project.org/lppl.txt

and version 1.2 or later is part of all distributions of
LaTeX version 1999/12/01 or later.

ulqda is a LaTeX package for use in Qualitative Data Analysis research.
It supports the integration of Qualitative Data Analysis (QDA) research tasks,
specifically for Grounded Theory, into the LaTeX work flow. 

It assists in the analysis of textual data such as interview transcripts
and field notes by providing the LaTeX user with macros which 
are used to markup textual information - for example, in-depth 
interviews - in order to facilitate the distillation of emerging 
themes from the data in a consistent and reliably manner, and to 
support visualization of these themes.  

ulqda requires the use of pdfeTeX.  The following LaTeX packages, 
available on http://www.ctan.org/ are needed by the ulqda 
package:
 * soul.sty
 * color.sty
 * multicol.sty
 * PGF/TikZ 
 * dot2texi.sty

In addition, the following external tools are required for processing
and graph/list generation:
 * GraphViz (http://www.graphviz.org)
 * dot2tex (http://www.fauskes.net/code/dot2tex/}
 * Perl and the Digest::SHA1 module

I have tested the package on Fedora running TeX Live 2008 and on 
Windows 7 running MiKTeX.

Readme for copyedit package (copyedit.sty)

Copyedit implements copyediting support for LaTeX documents. Authors
can enjoy the freedom of using, for example, words with US or UK or
Canadian or Australian spelling in a mixed way, yet, they can choose
any one of the usage form for their entire document irrespective of
kinds of spelling they have adopted. In the same fashion, the users
can have the benefit of following features available in the package:

1. Localization --- British-American-Australian-Canadian

2. Close-up, Hyphenation, and Spaced words

3. Latin abbreviations

4. Acronyms and Abbreviations

5. Itemization, nonlocal lists and labels

6. Parenthetical and serial commas

7. Non-local tokenization in language through Abbreviations and
   pronouns.


Installation:

Create a directory $TEXMF/tex/latex/copyedit and copy copyedit.sty to
this directory.  Update the file database of your TeX system.

Documentation:

Composite documentation and code is available in copyedit.dtx. Run
TeX/LaTeX on copyedit.ins to extract copyedit.sty from copyedit.dtx.
If you [pdf]LaTeX copyedit.dtx, you will get copyedit.pdf which is the
human readable documentation.

The example directory has source and output of an example document and
the doc directory has an article written by authors on the subject.

Licence:

The package is released under LaTeX Project Public Licence.

Authors:

CV Radhakrishnan <cvr@cvr.cc>
CV Rajagopal <cvr3@cvr.cc>
SK Venkatesan <skvenkat@tnq.co.in>


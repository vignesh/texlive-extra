This is a slightly modified version of the namedplus style, which fully conforms
with the Journal of Neuroscience citation style.
Last modified 2004 April 06 by Matthias Hennig (hennig@cn.stir.ac.uk)

version = 1.00 of jneurosci.bst 2004 April 06

The following citation labels are implemented:

\citeauthoryear{author-info}{year}:
  these labels are processed by the following commands: 
\cite{key}: 
  which produces citations with both author and year, enclosed in parens. 
\shortcite{key}: 
  which produces citations with year only, enclosed in parens 
\citeauthor{key}: 
  which produces the author information only 
\citeyear{key}: 
  which produces the year information only 
\citetext{key}: 
  which produces Author (Year) 
\citenoparens{key}: 
  which produces Author, Year 
 
The output looks like that:
\cite{key}:
  (Strettoi and Masland, 1996; Kolb, 1997; Masland, 2001) and (Fisher et al., 1975)
\citetext{key}:
  for reviews, see McNaughton (1990); M�ller and Kaupp (1998); Fain et al. (2001).

The bibliography follows J Neurosci conventions, e.g.:
Koch C (1999) Biophysics of Computation: Information Processing in Single Neurons Oxford University Press.
Enroth-Cugell C, Robson JG (1966) The contrast sensitivity of retinal ganglion cells of the cat. J Physiol 187:517-552. 
 
This is a modified version of the namedplus style by:
-----------------------------------------------------
A. David Redish		adr@nsma.arizona.edu
Post-doc		http://www.cs.cmu.edu/~dredish
Neural Systems, Memory and Aging, Univ of AZ, Tucson AZ
-----------------------------------------------------

which was in turn modified by:
Oren Patashnik (PATASHNIK@SCORE.STANFORD.EDU)

and finally modified by:
Matthias H Hennig, University of Edinburgh, mhennig at inf.ed.ac.uk



| Source:  bnumexpr.dtx
| Version: v1.2e, 2019/01/08 (doc: 2019/01/08)
| Author:  Jean-Francois Burnol
| Info:    Expressions with big integers
| License: LPPL 1.3c

README: [Usage], [Installation], [License]
==========================================

Usage
-----

The package `bnumexpr` allows _expandable_ computations with big
integers and the four infix operators `+`, `-`, `*`, `/` familiar from
the `\numexpr` e-TeX parser.

Besides extending the scope to arbitrarily big numbers (and having a
more complete syntax, for example `-(1)` is legal input), it adds the
(by default) floored division operator `//`, and its associated modulo
`/:`, the power operator `^` (or equivalently `**`), and the factorial
post-fix operator `!`. The space character as well as the underscore
character `_` both may serve to optionally separate digits in long
numbers, for better readability of the input.

For example:

    \bnumeval{( 92_874_927_979^5 - 31_9792_7979^6 ) / 30!}

    The above expands (in two steps) to `-4006240736596543944035189`
    (the `/` does rounded division to match the `\numexpr` behaviour).

The expression parser is scaled-down from the `\xinttheiiexpr...\relax`
parser as provided by package xintexpr[^1]: it does not handle
hexadecimal input, boolean operators, dummy or user defined variables,
functions, etc...

By default, the package loads xintcore[^1] (release 1.3d is then
required) but it is possible via option _custom_ and macro
`\bnumexprsetup` to map the operators to macros of one's own choice. It
is the responsability of the user to load the packages providing these
custom macros.

Notice that the possibility not to use the xintcore macros might be
removed in the future: perhaps a future release will maintain during
computations a private internal representation (especially tailored
either for the xintcore macros or new ones which would be included
within `bnumexpr.sty` itself) and the constraints this implies may
render optional use of other macros impossible.

[^1]: <http://www.ctan.org/pkg/xint>

Installation
------------

Obtain `bnumexpr.dtx` (and possibly, `bnumexpr.ins` and the `README`)
from CTAN:

> <http://www.ctan.org/pkg/bnumexpr>

Both `"tex bnumexpr.ins"` and `"tex bnumexpr.dtx"` extract from
`bnumexpr.dtx` the following files:

`bnumexpr.sty`
  : this is the style file.

`README.md`
  : reconstitutes this README.

`bnumexprchanges.tex`
  : lists changes from the initial version.

`bnumexpr.tex`
  : can be used to generate the documentation:

  :  - with latex+dvipdfmx: `"latex bnumexpr.tex"` (thrice) then
      `"dvipdfmx bnumexpr.dvi"`.

  :    Ignore dvipdfmx warnings, but if the pdf file has problems with
       fonts (possibly from an old dvipdfmx), use then rather pdflatex.

  :  - with pdflatex: `"pdflatex bnumexpr.tex"` (thrice).

  : In both cases files `README.md` and `bnumexprchanges.tex` must
    be present in the same repertory.

without `bnumexpr.tex`:
  : `"pdflatex bnumexpr.dtx"` (thrice) extracts all files and
    simultaneously generates the pdf documentation.

Finishing the installation:

           bnumexpr.sty   --> TDS:tex/latex/bnumexpr/

           bnumexpr.dtx   --> TDS:source/latex/bnumexpr/
           bnumexpr.ins   --> TDS:source/latex/bnumexpr/

           bnumexpr.pdf   --> TDS:doc/latex/bnumexpr/
                 README   --> TDS:doc/latex/bnumexpr/

Files `bnumexpr.tex`, `bnumexprchanges.tex`, `README.md` may be
discarded.

License
-------

Copyright (C) 2014-2019 by Jean-Francois Burnol

| This Work may be distributed and/or modified under the
| conditions of the LaTeX Project Public License 1.3c.
| This version of this license is in

>   <http://www.latex-project.org/lppl/lppl-1-3c.txt>

| and version 1.3 or later is part of all distributions of
| LaTeX version 2005/12/01 or later.

This Work has the LPPL maintenance status "author-maintained".

The Author and Maintainer of this Work is Jean-Francois Burnol.

This Work consists of the main source file `bnumexpr.dtx`
and the derived files

    bnumexpr.sty, bnumexpr.pdf, bnumexpr.ins, bnumexpr.tex,
    bnumexprchanges.tex, README.md


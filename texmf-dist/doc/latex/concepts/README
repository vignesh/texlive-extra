LaTeX Package : concepts v0.0.5

Last Modified : 2012-12-18

Author        : Michiel Helvensteijn  (www.mhelvens.net)

This package helps to keep track of formal `concepts' for a
specific field or document. This is particularly useful for
scientific papers in e.g. physics, mathematics or computer
science. Such papers often introduce several concepts accompanied
by symbols that can represent instances of them. This package
allows you to declare such concepts once, near their first
introduction in the document, and ensures consistent use of their
symbols and names.

In the near future, this package will integrate with the
glossaries package to provide more functionality and a consistent
experience when using both packages.

===== Prerequisites =====

To use this package, you need at least the following packages
installed:

* etextools
* nth
* xspace
* xparse
* ltxkeys (2012/11/17)
* xstring

Most of these will simply exist in your LaTeX distribution.
But it's possible you may not have the right version of ltxkeys
installed, since it recently changed its interface for handling
list-values. You can find the latest version on CTAN.

In the near future, this package will drop several dependencies.

===== Installation =====

concepts.sty is provided directly in the package archive. Put
it in a place where your LaTeX distribution can find it.

(concepts.sty is not generated, but manually maintained; you
 may use docstrip to remove the documentation, but you don't
 have to; it will just work the way it is)

===== Documentation =====

concepts.pdf is provided directly in the package archive. To
generate the documentation yourself, run LaTeX on concepts.tex.

(concepts.tex does not contain the package code itself; it inputs
 concepts.sty directly to document the implementation)

===== License =====

This material is subject to the LaTeX Project Public License. See
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html 
for the details of that license.

# bpchem

    bpchem -- Sup­port for type­set­ting chem­i­cal for­mu­lae, IUPAC compound names and more
    Author:  Bjørn Pedersen
    Email:   Bjoern.Pedersen@frm2.tum.de
    Version: 1.1
    License: Released under the LaTeX Project Public License 
    See:     http://www.latex-project.org/lppl.txt

This package has been written to alleviate the task of writing publications containing
lots of chemistry. It provides methods for typesetting chemical names,
sum formulae and isotopes. It provides the possibility to break very long names
even over several lines.

This package also provides a way to automatically enumerate your chemical
compounds, allowing for one-level subgrouping.

What this package does not provide: Methods to draw chemical compounds.
Although there exist some packages,which where designed for this purpose (e.g.
xymtex, PPChTex) they are quite limited once you get to complex organic, or
metal organic compounds. I recommend using an external drawing program, possibly
in conjunction with psfrag, in these cases.


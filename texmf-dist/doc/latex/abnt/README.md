======= abnt -- A package for typesetting Brazilian academic texts ==========

This package implements rules to typeset Brazilian academic texts. 

Copyright (c) Youssef Cherem <ycherem(at)gmail.com>, 2020.

 This file is part of the abnt LaTeX2e package.
 This work may be distributed and/or modified under the conditions of
 the LaTeX Project Public License, version 1.3c of the license.
 The latest version of this license is in
 http://www.latex-project.org/lppl.txt
 and version 1.3c or later is part of all distributions of LaTeX
 version 2005/12/01 and of this work.

 This work has the LPPL maintenance status "author-maintained".
 ========================================================================



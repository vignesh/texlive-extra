diffcoeff: a package to ease the writing of a variety
of differential coefficients (derivatives) 

Andrew Parsloe (ajparsloe@gmail.com) 28 December 2020

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either 
version 1.3 of this license or (at your option) any later 
version. The latest version of this license is in
  http://www.latex-project.org/lppl.txt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This is version 3.2 of diffcoeff.sty, and associated files,
and requires the LaTeX3 bundles l3kernel and l3packages. 

Version 3.2 corrects a bug when an ordinary derivative is in  
the differentiand of a partial derivative. Possible negative
spacing before a differential is also now available.

Manifest
%%%%%%%%
README.txt            this document
diffcoeff.sty         LaTeX .sty file
diffcoeff.pdf         documentation
diffcoeff.tex         LaTeX source of documentation
diffcoeff-doc.def     definition file to be placed
                      in same directory as the .pdf
                      and .tex files
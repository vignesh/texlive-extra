﻿Delimseasy  2.0
===============

Changes to the package since Version 1.0:
   Added more delimeters (notably for binomial coefficients)
   Added delimiters for Valentine's Day
   Added a "D" version for those who like LaTeX's \left...\right construct but are tired of typing and editing those pairs.
   Revised Manual

This package contains the following files:

README2.0 - this document

delimseasyMan.pdf - the pdf version of the manual

delimseasyMan.tex - LaTeX source of the preceding; includes examples that are commented out

delimseasy.sty - the delims package

delimseasy is a package to make it simple and fast to change the size of delimiters and their blackness. Further information is in delimseasyMan.

This material is subject to the LaTeX Project Public License 1.3. See
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for the
details of that license.


Colin C. Graham
ccgraham@alum.mit.edu
2015-12-02


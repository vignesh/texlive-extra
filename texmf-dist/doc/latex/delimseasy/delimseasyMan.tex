%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Manual for the package delimseasy1.01.sty
%
% Revison history (Manual):
%  2016/02/01
%    added \thanks line for copyright notice
%    added \valentine and \diamondsgbf
%    added \L..., \R..., \BL..., \BR... as one-sided
%      commands
%   Revised \S3 to reflect addition of \llgg and 
%    \valentine commands.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{amsart}
\usepackage{verbatim}
\usepackage{delimseasy2.0}
 %
 \setcounter{tocdepth}{3}
 \title[delimseasy]{Package delimseasy}
 \author[Graham]{Colin C. Graham}
 \address{PO Box 2031\\Haines Junction YT Canada Y0B 1L0\\\texttt{
 ccgraham@alum.mit.edu}\hfill\break
 \thanks{This material is subject to the LaTeX Project Public License 1.3. See
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for the
details of that license.}}
% Encapsulated \thanks in \address so the list of delimiters appears on page 1.
%
%

\begin{document}

\begin{abstract}  Provides commands to give consistent, easy-to-remember, way to edit and  control the size  and blackness
 of delimiters: append 1-4 ``b''s to command for larger sizes; prepend ``B'' for boldface; prepend ``D'' for the
 \LaTeX\ default. These commands reduce the likelihood 
 of incomplete delimeter pairs and typically use fewer characters than the \LaTeX\ default. 
 \end{abstract}
 
\maketitle
 
\centerline{\texttt{ccgraham@alum.mit.edu}}


\centerline{Version 2.0 -- 2016 February 1}


{\smaller
\tableofcontents}

\vspace{-.75in}

\section{Installation and the basic delimiters}
Put \texttt{delimseasy.sty} in a convenient folder and
add
\texttt{$\backslash$usepackage\{delimseasy\}}
 to your  preamble. If  \LaTeX \ can't find the \texttt{sty} file,
 move it to the folder of your \texttt{tex} file 
or add a \texttt{PATH} to the \texttt{$\backslash$usepackage} parameter, so you have (for example)
\newline
\texttt{$\backslash$usepackage\{C:/user/YOU/Documents/WHEREYOUPUTIT/delimseasy\}}.

%\medskip
Here are the basic commands:
\begin{equation*}
\begin{matrix}
\text{The command}&	\quad\text{ typesets as }&\quad \text{common name}\\
\backslash\texttt{prn\{$\backslash$\#\}}  &\texttt{(\#1)}                                     &\text{round parens}\\
\backslash\texttt{sqpr\{$\backslash$\#\}} &\texttt{[\#1]}                                     &\text{square braces}\\
\backslash\texttt{crl\{$\backslash$\#\}}  &\{\texttt{\#1} \}                                  &\text{curly braces}\\
\backslash\texttt{ceil\{$\backslash$\#\}} &\backslash\texttt{lceil \#1} \backslash\texttt{rceil}&\text{ceiling}\\        
\backslash\texttt{flr\{$\backslash$\#\}}  &\backslash\texttt{lfloor \#1}\backslash\texttt{rfloor}&\text{floor}\\
\backslash\texttt{ngl\{$\backslash$\#\}}  &\backslash\texttt{langle \#1}\backslash\texttt{rangle}&\text{langle/rangle}\\
\backslash\texttt{abs\{$\backslash$\#\}}  &\backslash\texttt{vert \#1} \backslash\texttt{vert}  &\text{absolute value}\\
\backslash\texttt{nrm\{$\backslash$\#\}}  &\backslash\texttt{Vert \#1}  \backslash\texttt{Vert}    &\text{norm}\\
\backslash\texttt{stgt\{$\backslash$\#\}} &\stgt{\texttt{\#1}}& \text{lessthan greaterthan}
\end{matrix}
\end{equation*}

\subsection{Adding ``b''s to change the size; ``D''s to use \LaTeX\ default}
Add one to four ``b''s to the end of a command; each ``b'' raises size by one step. Example:
\texttt{$\backslash$prnb$\{x\widehat\ 2-1\}$} gives $\prnb{x^2-1}$.

Save typing by prepending ``D'' to the start of a 
command to use the \texttt{$\backslash$left$\dots\backslash$right}
default sizing provided by \LaTeX. 
Example: \texttt{$\backslash$Dprn$\{x\widehat\ 2-1\}$} gives
 $\Dprn{x^2-1}$ while \texttt{$\backslash$Dprn$\{x-1\}$} gives $\Dprn{x-1}$. 

\subsection{Adding ``L'' or ``R'' for one-sided delimiters}
Prepend upper or lowercase ``L''  or upper or lowercase 
``R'' to the front of the command (after the backslash) for the single 
sided (\textbf{L}eft or \textbf{R}ight).  
These commands take no parameter. Example: \texttt{$\backslash$Lprnb}
gives $\Lprnb$. 

There are no  one-sided versions of the \texttt{$\backslash$D...} commands.

\subsection{Prepending a ``B'' for  boldface}
Prepend a capital ``B'' to the front of the  command for
 boldface (poor man's bold); if
the command is already a left or right, the ``B'' must 
preceed the ``L'' or ``R''. Needs a parameter if the
non-bolded command does. Example: \texttt{$\backslash$Blprnb} gives $\Blprnb$.

This is not available for the $\backslash$\texttt{D\dots} commands.

 \subsection{Ampersands (\&) not allowed inside paired delimeters} 
 Paired delimiters cause an error if the alignment 
 ampersand \& is used between them.
 There may be other formatting characters which break 
 paired delimeters; please let me know if you
 encounter any.
 
 \textit{Workarounds}:
 \begin{enumerate}
 \item Rewrite. If you don't like the looks of
 \begin{align*}
 \int \text{A very very very very long  expression }&\le \text{ a shorter one}\\
 &\le\text{ another short one}\\
 &=\text{the final expression}
 \end{align*}
 perhaps you can break up the computations into
  smaller pieces so that you end with something like,
 `` putting (3.12)-(3.36) together we see that
 \[
 \int \text{A very very very very long  
 expression }\le \text{the final expression}.\text{''}
 \]
 
 \item Use one-sided versions.
 
 \item Use $\backslash$\texttt{phantom}. For example,
  \begin{verbatim}
 &\int \text{A very very very very long  expression }
      \le \text{ a shorter one}\\
 &\phantom{\int \text{A very }}\le\text{the final expression}
  \end{verbatim}
  \vspace{-10pt}

 gives
 \begin{align*}
 &\int \text{A very very very very long  expression }
      \le \text{ a shorter one}\\
 &\phantom{\int \text{A very }}\le\text{the final expression}
  \end{align*}
  
 \end{enumerate}
 
\section{Examples}
\begin{enumerate}


\item Here are two versions of the same displays. 
The first pair uses $\backslash$\texttt{left $\backslash$right} (implemented through $\backslash$\texttt{Dsqpr}
and $\backslash$\texttt{Dabs}); the
second $\backslash$\texttt{sqprbbb} and $\backslash$\texttt{absbbb...}.
I prefer the second pair for being using less vertical space and being more easily  resizable than the first.
 \[
 \left[ \sum_n\frac{x_n}{y_n}\right]^{1/2}
 \quad  \Dabs{\int_a^b\prnbb{\frac{x+1}{x^2+5}}^p  dx}^{1/p}
 \]
 \[
 \sqprbb{\sum_n\frac{x_n}{y_n}}^{1/2}\quad
\absbbb{\int_a^b\prnbb{\frac{x+1}{x^2+5}}^p  dx}^{1/p}
 \]
 \item Bold vs. default:
 \[
  \Bprnbb{\frac x{x^2+1}} \quad  \prnbb{\frac x{x^2+1}} \quad
  \Bcrlbb{\frac x{x^2+1}} \quad  \crlbb{\frac x{x^2+1}} 
 \] 
 \end{enumerate}
 

 The \texttt{tex} source for this PDF contains more examples, commented out.
 
 \section{Further comments}
 \subsection{An alternate package, \textit{delim}}
 The package \textit{delim} by Stefan Majewsky 
 does things this one does
 and may be preferred by many \LaTeX\ users.
 
 \subsection{Bionomial coefficients  and reversed delimiters}
  
 Binomial coefficients often display with overly large parens. Versions under your control
 are supplied.
 
 At one time one saw open intervals expressed as
 \[
 \nrp{a,b} \quad\text{ or }\quad \rpqs{c,d}.
 \]
 This package provides paired delimeters at the 5 sizes,
  with and without bold face, for those two
 situations.
 \begin{equation*}
 \begin{matrix}
\text{The command}&	\quad\text{ typesets as }&\quad \text{common name}&\quad \text{Sizes/BF version}\\
 \texttt{$\backslash$bnom$\{\#1\}\{\#2\}$}&\bnom{\#1}{\#2}&\text{binomial coefficient}&5/yes\\\\
 \texttt{$\backslash$bnomsq$\{\#1\}\{\#2\}$}&\bnomsq{\#1}{\#2}&\text{binomial coefficient}&5/yes\\\\
 \texttt{$\backslash$bnomcrl$\{\#1\}\{\#2\}$}&\bnomcrl{\#1}{\#2}&\text{binomial coefficient}&5/yes\\\\
 \texttt{$\backslash$bnomngl$\{\#1\}\{\#2\}$}&\bnomngl{\#1}{\#2}&\text{binomial coefficient}&5/yes
 \\
 \texttt{$\backslash$nrp\{$\backslash$\#1\}} &\nrp{\texttt{\#1}}& \text{reversed parens}&5/yes\\
\texttt{$\backslash$rpqs\{$\backslash$\#1\}} & \rpqs{\texttt{\#1}}&\text{reversed square braces}&5/yes
 \end{matrix}
 \end{equation*}

 
\subsection{More delimiters, including 2 for Valentine's Day}
 \begin{equation*}
 \begin{matrix}
\text{The command}&	\quad\text{ typesets as }&\quad \text{common name}&\quad \text{Sizes/BF version}
 \\
\backslash\texttt{stgt\{$\backslash$\#1\}} &\texttt{< \#1>}     & \text{lessthan greaterthan}&5/yes\\
\backslash\texttt{llgg\{$\backslash$\#1\}} &\texttt{$\ll$\#1$\gg$}&\text{muchless muchmore}&5/yes\\
\backslash\texttt{valentine\{$\backslash$\#1\}}&\valentine{\texttt{\#1}} &\text{surrounded by love}&1/no\\
\backslash\texttt{diamondsgbf\{$\backslash$\#1\}}&\diamondsgbf{\texttt{\#1}}& \text{buried in diamonds}&1/no
 \end{matrix}
 \end{equation*}

 To make your own delimiter pairs, just adapt the models in the 
 \texttt{sty} file. 
 Note that the $\backslash$\texttt{B}... versions use
 $\backslash$\texttt{pmb} (poor man's bold)
 throughout because $\backslash$\texttt{boldsymbol}
 does not work for all delimiters, while  $\backslash$\texttt{pmb}
 does. Some ``delimiters'' may not have 
 $\backslash$\texttt{big}(ger) versions so you will have to improvise, as here for
  $\ll\dots\gg$.
The larger versions of  $\ll\dots\gg$  do not look great; they merely illustrate what can be naively
 achieved.
 
 The  two Valentine delimiters  must be in Math mode. If your beloved's name has spaces, 
 you will need to protect
 the space(s) by preceding each one with
  a $\backslash$\texttt{backslash} since
 Math mode ignores unprotected spaces. The Valentines commands can be nested at least once:
 \[
\valentine{\ \diamondsgbf{\ Kim\ }\ }\qquad {\diamondsgbf{\ \valentine{Leslie}\ }}
\qquad\valentine{\valentine{\valentine{\ \valentine{ Deniz }\ }}}.
\]
 Adjusting the space in the various pairs with $\backslash{\ }$'s  may improve the look.
\subsection{Easy to remember sizers}
\texttt{delimseasy} includes 
versions of \LaTeX's 
built-ins:

\[\begin{matrix}
\text{New}&\text{\LaTeX\ built-in}\\
   &\texttt{$\backslash$big}\\
\backslash\texttt{bigb}  &\texttt{$\backslash$Big}\\
\backslash\texttt{bigbb} &\texttt{$\backslash$bigg}\\
\backslash\texttt{bigbbb}&\texttt{$\backslash$Bigg}
\end{matrix}\]
\subsection{Revision history}
2016/02/01. Added capital ``L'' and capital ``R'' option for
single sided. Removed manual's references to ``l'' and ``r'' versions though
they remain implemented for backward compatibility.

Added ``D'' versions for those who prefer \LaTeX's choice for sizing.

Added new delimiters: $\bnom{p+q}{q}$, $\llgg{\dots}$,  $\valentine{\dots}$ and $\diamondsgbf{\dots}$.

Added this subsection and information about the new delimiters.

Many minor stylistic changes to manual.
 \begin{comment}% testing, testing.........
 reverse parens
 \begin{align*}
  \nrp{
 \nrpb{
 \nrpbb{
 \nrpbbb{
 \nrpbbbb{w}}}}}
%%%%%%%%%%%%%
%  reverse bold paired square braces
 \rpqs{
 \rpqsb{
 \rpqsbb{
 \rpqsbbb{
 \rpqsbbbb{z}}}}}
%
\end{align*}
 %
 
 stgt's
 \begin{align*}
& 
\rstgt
\rstgtbb 
\rstgtbbb
\rstgtbbbb 
%%
\Brstgt
\Brstgtb
\Brstgtbb
\Brstgtbbb
\Brstgtbbbb\\
& \stgt{\stgtb{\stgtbb{\stgtbbb{\stgtbbbb{w}}}}}\\
&\Bstgt{\Bstgtb{\Bstgtbb{\Bstgtbbb{\Bstgtbbbb{w}}}}}\\
&
\lstgt
\lstgtb
\lstgtbb
\lstgtbbb
\lstgtbbbb
%%
\Blstgt
\Blstgtb
\Blstgtbb
\Blstgtbbb
\Blstgtbbbb
 \end{align*}
 
 parens, etc:
\begin{align*}
\Bprn{\Bprnb{\Bprnbb{\Bprnbb{\Bprnbbb{\Bprnbbb{}}}}}}&\quad\
\end{align*}\begin{align*}
 \Bsqpr{\Bsqprb{\Bsqprbb{\Bsqprbbb{\Bsqprbbbb{x}}}}}\\
 \end{align*}\begin{align*}
\Bcrl{\Bcrlb{\Bcrlbb{\Bcrlbbb{\Bcrlbbbb{z}}}}}
\end{align*}\begin{align*}
&\quad
\Bflr{\Bflrb{\Bflrbb{\Bflrbbb{\Bflrbbb{47}}}}}\\
\Bceil{\Bceilb{\Bceilbb{\Bceilbbb{\Bceilbbbb{2}}}}}&\quad\Bnglbbbb{\Bnglbbb{\Bnglbb{\Bnglb{\Bngl{x|y}}}}}\\
\end{align*}
\begin{align*}
\Babsbbbb{\Babsbbb{\Babsbb{\Babsb{\Babs{f}}}}}&\quad \Bnrmbbbb{\Bnrmbbb{\Bnrmbb{\Bnrmb{\Bnrm{z}}}}}\\
%\cardbbbb{\cardbbb{\cardbb{\cardb{\card{x}}}}}\\
\Blprn{\Blprnb{\Blprnbb{\Blprnbb{\Blprnbbb{\Blprnbbb{x}}}}}}&\quad \Blsqpr{\Blsqprb{\Blsqprbb{\Blsqprbbb{\Blsqprbbbb{}}}}}\\
\Blcrl{\Blcrlb{\Blcrlbb{\Blcrlbbb{\Blcrlbbbb{}}}}}&\quad
\Blflr{\Blflrb{\Blflrbb{\Blflrbbb{\Blflrbbb{}}}}}\\
\end{align*}
\begin{align*}
\Blceil{\Blceilb{\Blceilbb{\Blceilbbb{\Blceilbbbb{}}}}}
&\quad\Blnglbbbb{\Blnglbbb{\Blnglbb{\Blnglb{\Blngl{x|y}}}}}\\
\end{align*}
\begin{align*}
\Blabsbbbb{\Blabsbbb{\Blabsbb{\Blabsb{\Blabs{}}}}}
&\quad \Blnrmbbbb{\Blnrmbbb{\Blnrmbb{\Blnrmb{\Blnrm{}}}}}\\
\Brprn \Brprnb \Brprnbb \Brprnbb\Brprnbbb\Brprnbbb&\quad\ \Brsqpr{\Brsqprb{\Brsqprbb{\Brsqprbbb{\Brsqprbbbb{}}}}}\\
\Brcrl{\Brcrlb{\Brcrlbb{\Brcrlbbb{\Brcrlbbbb{}}}}}&\quad
\Brflr{\Brflrb{\Brflrbb{\Brflrbbb{\Brflrbbb{}}}}}\\
\Brceil{\Brceilb{\Brceilbb{\Brceilbbb{\Brceilbbbb{}}}}}
&\quad\Brnglbbbb{\Brnglbbb{\Brnglbb{\Brnglb{\Brngl{}}}}}\\
\Brabsbbbb{\Brabsbbb{\Brabsbb{\Brabsb{\Brabs{}}}}}
&\quad \Brnrmbbbb{\Brnrmbbb{\Brnrmbb{\Brnrmb{\Brnrm{}}}}}
\\
&\llgg{\llggb{\llggbb{X}}}%\llggbbb{\llggbbbb{X}}}}}\\
&\Bllgg{\Bllggb{\Bllggbb{\Bllggbbb{\Bllggbbbb{X}}}}}
\end{align*}
\[
 \bnom{{x+y}}{x}\quad\bnomb{x+y}{x}\quad\bnombb{x+y}{x}\quad\bnombbb{x+y}{x}\quad\bnombbbb{x+y}{x}
 \]
 \[
 \Bbnom{{x+y}}{x}\quad\Bbnomb{x+y}{x}\quad\Bbnombb{x+y}{x}\quad\Bbnombbb{x+y}{x}\quad\Bbnombbbb{x+y}{x}
 \]
 
 \[
 \bnomsq{{x+y}}{x}\quad\bnomsqb{x+y}{x}\quad\bnomsqbb{x+y}{x}\quad\bnomsqbbb{x+y}{x}\quad\bnomsqbbbb{x+y}{x}
 \]
 \[
 \Bbnomsq{{x+y}}{x}\quad\Bbnomsqb{x+y}{x}\quad\Bbnomsqbb{x+y}{x}\quad\Bbnomsqbbb{x+y}{x}\quad\Bbnomsqbbbb{x+y}{x}
 \] 
 \[
 \bnomcrl{{x+y}}{x}\quad\bnomcrlb{x+y}{x}\quad\bnomcrlbb{x+y}{x}\quad\bnomcrlbbb{x+y}{x}\quad\bnomcrlbbbb{x+y}{x}
 \]
 \[
 \Bbnomcrl{{x+y}}{x}\quad\Bbnomcrlb{x+y}{x}\quad\Bbnomcrlbb{x+y}{x}\quad\Bbnomcrlbbb{x+y}{x}\quad\Bbnomcrlbbbb{x+y}{x}
 \]
  \[
 \bnomngl{{x+y}}{x}\quad\bnomnglb{x+y}{x}\quad\bnomnglbb{x+y}{x}\quad\bnomnglbbb{x+y}{x}\quad\bnomnglbbbb{x+y}{x}
 \]
 \[
 \Bbnomngl{{x+y}}{x}\quad\Bbnomnglb{x+y}{x}\quad\Bbnomnglbb{x+y}{x}\quad\Bbnomnglbbb{x+y}{x}\quad\Bbnomnglbbbb{x+y}{x}
 \]
 \\[
 \Dprn{\int}\quad\Dsqpr{\int}\quad\Dcrl{\int}\quad\Dngl{\int}
 \]
 \[
 \Dceil{3}\quad\Dflr{pp}\quad\Dabs{\int}\quad\Dnrm{\int}\quad\Dstgt{kk}
 \]
 \end{comment} %end of testing
\end{document}  

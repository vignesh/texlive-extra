Package file:

hypdvips.sty


Documentation files:

bibdat.bib (embedded in hypdvips.pdf)
draft.pdf (attached to hypdvips.pdf as hypdvips_showdests.pdf)
hypdvips.pdf
hypdvips.tex (attached to hypdvips.pdf)
README
images/example1.eps
images/example2.eps
images/example3.eps
images/example4.eps
images/example5.eps
images/example6.eps
images/example7.eps
images/graph.eps
images/icon_draft.eps
images/ids.eps
images/logfile.eps
images/openmsg_six.eps
images/openmsg_sixinbrowser.eps
images/paperclip.eps
images/pushpin.eps
images/tag.eps

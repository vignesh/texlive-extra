DESCRIPTION:

	Qsymbols is a LaTeX 2e package defining systematic mnemonic
	abbreviations, of the form `... for math symbols and "..." for arrows.

	It accesses symbols of the amssymb and stmaryrd packages.

	Optionally, arbitrary arrows can be typeset using Xy-pic.

        See the file CATALOG for the current release details.

INSTALLATION INSTRUCTIONS:

	(1) Install qsymbol.sty in a directory searched by LaTeX.

	(2) Make sure the COPYING file is accessible to users.

DOCUMENTATION:

	Qsymbols is documented in the manual included with the package:

	  qsymbols.tex	LaTeX source (requires Xy-pic to typeset)
	  qsymbols.ps	Adobe PostScript version

AUTHOR:

	Kristoffer H�gsbro ROSE                           <krisrose@brics.dk>
	BRICS                           <URL: http://www.brics.dk/~krisrose/>
	Department of Computer Science B3.26, +45 89423193 (fax +45 89423255)
	University of Aarhus, Ny Munkegade, build. 540, 8000 �rhus C, DENMARK

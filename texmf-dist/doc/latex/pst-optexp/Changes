5.2  2014-11-26
       * Added component \asphericlens.
       * Added component \axicon.
       * Added component \optwedge.

5.1  2014-11-19
       * Added component \oapmirror.
       * Added support for filled beams with path interfaces.

5.0  2014-10-30
       * Added support for arbitrary interface shapes (experimental,
           requires the pst-intersect package and pstricks version 2.53).
       * Added component \parabolicmirror.

4.10 2014-06-04
       * Added value firstcomp to parameter beamalign.
       * Modified \optsource to handle also the beam alignment.

4.9  2014-06-03
       * Added new component \optsource.
       * Improved naming of the last component name, which can now be
           accessed also by its number besides using N.
       * Fixed bug related to extnodes together with compname

4.8  2014-02-28
       * Added component \optaom.
       * Added option beammode.
       * Added option showinterfaces.

4.7  2014-01-12
       * Added syntax <\prm{num} for accessing relative component IDs.

4.6  2013-11-27
       * Added option extnodes.
       * Added option optdipolecomp.
       * Added option opttripolecomp.

4.5  2013-07-21
       * Added option usewirestyle.
       * Added line style fade, with options fadeto, fadepoints,
           fadefunc, and fadefuncname..

4.4  2013-07-14
       * Added component \transmissiongrating.
       * Modified innerlabel to explicitly set labelalign=c
       * Fixed trailing spaces in savebeampoints (and savebeam).
       * Allow Postscript code in angle.

4.3a 2013-07-02
       * Fixed tripole alignment bugs introduced in version 4.3.
       * Again fixed the alignment of reflective \optprism.
       * Fixed improper alignment of \optprism label.

4.3  2013-06-28
       * Added option raprismalign.
       * Fixed alignment of \rightangleprism and rotation of its label.

4.2  2013-06-07
       * Added option startinsidecount.
       * Added option stopinsidecount.
       * Added option beampathcount.
       * Added option beamnodealign.
       * Added \oeBeamVecMedian.
       * Added \oeBeamCenter.
       * Modified the orientation of a reflective \optprism.
       * Fixed bug of \optprism and \rightangleprism inside an
           \env{optexp} environment.
       * Fixed bug of wrong \rightangleprism rotation in special cases.
       * Fixed buggy handling of multiple saved beams.

4.1  2013-05-21
       * Added option prismtype.

4.0  2013-04-15
       * Added electrical components.
       * Added wire connections with \drawwire.
       * Added component \optarrowcomp.
       * Added component \optbarcomp.
       * Improved \optdetector.
       * Extended \drawfiber to support component ranges.
       * Extended couplersep to support automatic calculation.
       * Fixed appearance of \optfiber.
       * Fixed extnodealign to affect also multipoles.
       * Fixed pswarning.
       * Removed old, buggy beam connection code.
       * Removed option conn.
       * Removed option connjoin.
       * Removed \newOptexpDipoleNolabel.

3.6  2013-03-20
       * Added option compoffset.
       * Added option beampathskip.
       * Added option innercompalign.
       * Fixed some bugs in the raytracing and beam code.

3.5  2013-02-22
       * Added option filterangle.
       * Fixed wrong output fiber style in \optcoupler
       * Fixed strange Postscript error which occured with some
           interpreters.

3.4  2013-02-03
       * Fixed bugs when using fillstyle for some components.
       * Extended option switchsize.
       * Modified coupler center node to be on the base line.
       * Extended error checking for \drawbeam and \drawfiber.
       * Fixed some bugs in the connection code.

3.3a 2012-09-18
       * Fixed bug which was exposed by an update of pst-node.
       * Fixed trailing spaces.

3.3  2012-08-17
       * Extended \opttripole and \optdipole.
       * Modified reference angle for label of \opttripole.
       * Added option optdipolesize to support two interfaces for
           \optdipole.
       * Added option gratingalign.
       * Added option forcebeaminside.
       * Extended option mirrorradius.
       * Improved \drawbeam and \drawwidebeam to be able to use
           ArrowInside without linestyle.
       * Fixed orientation of \oenodeBeamUp and \oenodeBeamLow.
       * Fixed a bug related to beaminside and ambiguous components.
       * Fixed some trailing spaces.
       * Added more examples.

3.2  2012-07-26
       * Added component \glanthompson.
       * Added access to beam vectors with \oeBeamVec, \oeBeamVecUp and
           \oeBeamVecLow.
       * Fixed wrong computation of node distance in \fiberbox
       * Fixed bug in \wdmsplitter for coupleralign=b and
           couplertype=none
       * Fixed bug when filling components placed with position=end/start
       * Added value absolute to parameter labelref.
       * Added examples.

3.1  2012-07-17
       * Added component \fiberbox.
       * Extended connection macros to not required curly braces around
           node parenthesis any more.
       * Modified \fibercollimator to have allowbeaminside=false by
           default.
       * Extended fiber couplers to allow using only two nodes.
       * Fixed a bug concerning node expressions with \drawfiber

3.0  2012-07-09
       * Modified beam connections with \drawbeam to support raytracing.
       * Added wide beams with \drawwidebeam.
       * Added \drawfiber for fiber connections.
       * Added optexp environment for layering of components and
           connections.
       * Added german documentation.
       * Modified naming of component nodes.
       * Modified extnode to work with more components.
       * Modified angle and rotateref to affect all components.
       * Modified endbox to affect all components.
       * Extended position by values start and end.
       * Extended abspos by values start and end.
       * Removed deprecated lens code which used lenswidth and lensheight
           for construction.
       * Added option platesize.
       * Added option phwidth.
       * Modified option caxislength.
       * Deprecated option lampscale.
       * Added style CrystalCaxis.
       * Added style CrystalLamp.
       * Added option optboxsize.
       * Extended option detsize.
       * Added style DetectorStyle.
       * Removed deprecated \detector, use \optdetector.
       * Extended option doveprismsize.
       * Added style Polarization.
       * Deprecated option pollinewidth.
       * Removed option polwidth.
       * Removed option pol.
       * Added style VariableStyle.
       * Added mirror type semitrans.
       * Renamed \optgrid to \optgrating.
       * Renamed optgridwidth to gratingwidth.
       * Renamed optgridheight to gratingheight.
       * Renamed optgriddepth to gratingdepth.
       * Renamed optgridcount to gratingcount.
       * Renamed optgridtype to gratingtype.
       * Renamed optgridlinewidth to gratinglinewidth.
       * Added option prismalign.
       * Modified faulty alignment of \rightangleprism.
       * Extended option optampsize.
       * Extended option optmzmsize.
       * Added option polcontroltype.
       * Extended option isolatorsize.
       * Added style IsolatorArrow.
       * Extended option fdlsize.
       * Added style FdlArrow.
       * Extended option fiberpolsize.
       * Added component \optcirculator.
       * Extended option couplersize.
       * Extended option couplertype.
       * Renamed option align to coupleralign.
       * Added style VariableCoupler.
       * Added style FilterStyle.
       * Extended option fibercolsize.
       * Removed deprecated option labelrelative.
       * Removed deprecated option iwidth.
       * Removed deprecated option owidth.
       * Removed deprecated option bswidth.
       * Deprecated \newOptexpDipoleNolabel, use \newOptexpDipole.
       * Deprecated option refractiveindex.
       * Deprecated option conn.
       * Extended option fiber.

2.1  2009-11-05
       * Added component \optfiberpolarizer.
       * Added option compshift.
       * Added option label.
       * Added option connjoin
       * Added options addtoBeam and newBeam.
       * Added style OptComp and related options addtoOptComp and
           newOptComp.
       * Added option bsstyle.
       * Extended \fibercollimator to use up to four reference nodes.
       * Improved thicklens to work also with plain lenses.
       * Use pst-doc class for the documentation.

2.0  2008-07-27
       * Added fiber-optical components.
       * Added component \optdiode.
       * Added component \pentaprism.
       * Added component \rightangleprism.
       * Added component \doveprism.
       * Added component \optprism.
       * Added \drawbeam.
       * Added component connections (options fiber, conn and beam).
       * Added option compname.
       * Added option extnode.
       * Renamed \detector to \optdetector.

1.2  2008-06-17
       * Modified lens design to use interface curvatures.
       * Added options lensradiusleft and lensradiusright.
       * Added option thicklens.
       * Added option lenstype.
       * Added option mirrorradius (curved mirrors).
       * Added option optgridtype (binary gratings).
       * Added \newOptexpDipole.
       * Added \newOptexpDipoleNolabel.
       * Added \newOptexpTripole.
       * Added \newOptexpFiberDipole.
       * General improvements of \TeX{} and Postscript code

1.1  2007-09-06
       * Improved labeling features.
       * Added parameter labelref.
       * Replaced labelrelative by labelref=relative.
       * Renamed \polarisation to \polarization.
       * Renamed polwidth to polsize.
       * Renamed pol to poltype.
       * Renamed bswidth to bssize.
       * Renamed iwidth to innerheight.
       * Renamed owidth to outerheight.
       * Added support for fillstyle for all components

1.0  2007-07-18
       * First CTAN version


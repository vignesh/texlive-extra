﻿chs-physics-report.sty
Written and maintained by Gary Zhang
Senior and Teacher Assistant (2018-19)



The CHS Physics Report package may be optionally used to meet all the requirements 
for physics lab reports submitted in physics courses at Carmel High School. The 
package facilitates writing reports for students unaccustomed to using LaTeX, and 
makes several aesthetic changes in the way that documents appear.

This package will not undergo extensive changes except for bug fixes, although I 
suggest that those wishing to contact me talk to me in person or send me an email
(these two are preferred), or at least leave a comment on CTAN.

The majority of this package is in the public domain under the CC0 license 
(https://creativecommons.org/publicdomain/zero/1.0/legalcode). However, a small 
portion of the code was written by Stefan Kottwitz and under a CC BY-SA 3.0 
license (https://creativecommons.org/licenses/by-sa/3.0/legalcode).

To install, you can place this package in the *same* folder as the document you 
are working on, or you your LaTeX directory (which is in 2018/texmf-dist/tex/latex 
for TeX Live and /MiKTeX 2.9/tex/latex for MikTeX). Further helpful information 
may be found at
https://artofproblemsolving.com/wiki/index.php?title= LaTeX:Packages#Making_Your_Own.
%      -*-  coding:utf-8 -*-
\documentclass[11pt,a4paper]{article}

\setlength{\parskip}{0.6ex}
\setlength{\parindent}{0mm}
\usepackage[a4paper,hmargin=2.4cm,bottom=3.2cm]{geometry}
\usepackage{url}
\usepackage{framed}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{enumitem}

\usepackage{luaotfload}
\usepackage{fontspec}
\setsansfont{Latin Modern Sans}
\setmonofont{Latin Modern Mono}
\setmainfont{Latin Modern Roman}


%% for tth: remove "noitemsep" !!


\usepackage[%
  colorlinks=true%
]{hyperref}

\title{\textsf{elpres} --- electronic presentations \\ with (pdf)\LaTeX}
\author{Volker Kiefel\thanks{volker dot kiefel at freenet dot de, 
       \url{http://vkiefel.de/elpres.html}}}
\date{v0.6\\[1.8ex]
      August 19, 2020}




\begin{document}
\maketitle
\thispagestyle{empty}

{
% \setlength{\parskip}{0ex}
  \setlength{\parskip}{0.26ex}
\tableofcontents
}

% \clearpage

\vspace{8ex}

\section{Introduction}

The \textsf{elpres} class is intended to be used for
presentations on a computer screen, a
beamer or a projector. It is derived from \LaTeX's \texttt{article} class and
may be used with \LaTeX{}, pdf\LaTeX{} (and Lua\LaTeX ).
The default ``virtual paper size''
of document pages
produced by this class: width=128mm, height=96mm corresponds to a
4:3 (width:height) aspect ratio. Other aspect ratios for widescreen monitors
may be selected
by class options. The \textsf{elpres} class requires that the \texttt{ifthen},
\texttt{fancyhdr},
\texttt{hyperref},
\texttt{graphicx},
\texttt{xcolor}
and \texttt{geometry} packages are available on the \TeX{} system: these
packages are loaded automatically by the \texttt{elpres} class.
Enhancements to \texttt{elpres} are easily made available by other packages,
these include overlay support for incremental slides (package
\texttt{overlays})  and slides
with a background from a bitmap (\texttt{wallpaper}, \texttt{eso-pic}
packages). Predefined color/layout schemes for \texttt{elpres} presentations
can be acitivated with
\verb+\usepackage+ (details can be found in
in section~\ref{sec:pres-scheme})

This manual is intended to support the user with ``recipes''.  Use of
\texttt{elpres} with its default settings should be simple, additional aspects
including overlay functions, use of colors, graphics files, ``handout
documents'' are described in section \ref{secEnhancements}. Many code snippets
have been included in this manual.
They can be used in users' presentation files.\footnote{To make copying easier,
this manual is also available as \texttt{.html} document on the
\texttt{elpres} website (\url{http://www.vkiefel.de/elpres.html})}


\section{Installation}

\label{sec:installation}


\begin{sloppypar}

If the \texttt{elpres} package has already been installed with the \TeX-system
nothing needs to be done. If an updated  version shall be installed,
\texttt{elpres.cls}, the \texttt{.sty}, \texttt{.png} and \texttt{.eps}
files should be copied
into a directory, where your \TeX-system can find it:
if an old \texttt{elpres} version of the existing \TeX\ system shall be
replaced by the current version, please copy these
into the appropriate position in the
``local'' \texttt{texmf} directory tree
(e.\,g. \verb+~/texmf-local/tex/latex/elpres+) and this manual
(\texttt{elpres-manual.pdf}) to \verb+~/texmf-local/doc/latex/elpres+.
Then, the files database should be updated by entering (in case of a \TeX~Live
installation) the command:

\end{sloppypar}

\begin{verbatim}
   mktexlsr ~/texmf-local
\end{verbatim}

or you may enter \verb+texhash ~/texmf-local+\footnote{The location
  for the ``local'', manually updated class and package files may be different,
  depending on the \TeX\ system and the OS.}.
Other \TeX-systems e.\,g.\ Mik\TeX\ have
their own package updating mechanisms. The elpres manual file should be
accessible with the command ``\mbox{\texttt{texdoc elpres}}''. If this
still calls the old version of the manual, the command ``\texttt{texdoc -l elpres}''
will prompt you to select either the old or the new version of the manual.

\section{Usage}

\label{secUsage}

The class is used with 
\begin{verbatim}
   \documentclass[options]{elpres}
\end{verbatim}

\textbf{\texttt{Elpres}-specific options}: for font selection
 \texttt{tmrfont} (Times Roman), \texttt{helvetfont}
(Helvetica), \texttt{cmfont} (Computer Modern), \texttt{sansfont} (Sans
Serif: default) and \texttt{nofonts} (no font selection, 
intended for use of \texttt{elpres} with Lua\LaTeX\
with its own font selection mechanisms) are used.
Options for different screen aspect ratios: \texttt{4x3}
(default), \texttt{16x9}, \texttt{16x10}. The option \texttt{navisymb}
adds symbols for navigation to the presentation. 
Options of the \texttt{article} class are also available for
\texttt{elpres} presentations,
e.\,g.  \texttt{10pt}, \texttt{11pt}, \texttt{12pt} for
selection of font size. 


\textbf{\texttt{Elpres}-specific commands}:
\verb+\distance+ vertically adjusts  text on a slide.
The \verb+\auvimm+\footnote{for \textbf{au}dio \textbf{vi}deo \textbf{m}edia}
command inserts a link to an external audio or video file.
Two commands: \verb+\fromlinktext+ and \verb+\totargettext+
can be used for internal links within a presentation.
With \verb+\slidetitlecolor+, the text color of slides (\texttt{psli})
can be changed.


\textbf{\texttt{Elpres}-specific environments}:
the environments \texttt{psli} and \texttt{rsli} define ``slides'' in
\texttt{elpres}; \texttt{citemize}, \texttt{cenumerate} and
\texttt{cdescription} provide vertically compressed lists.

\subsection{A minimalistic example presentation}

The following code may be used for your first ``experiments'' by adding
features described in latex sections.

\begin{verbatim}
   \documentclass[12pt,pdftex,4x3]{elpres}
   \usepackage[latin1]{inputenc}
   \usepackage[document]{ragged2e}

   % \usepackage{elpreswhitebluescheme}
   % \usepackage{elpreswhitetealscheme}
   % \usepackage{elpresbluelightgrayscheme}
   % \usepackage{elpresgrayscheme}
   % \usepackage{elpreswhiteredscheme}
   \usepackage{elpreswhitetealscheme}

   \begin{document}
   \begin{titlepage}
   \centering
   \distance{1}
   {
   \Huge \bfseries \textcolor{eptitlecolor}{Title of the presentation} \par
   }
   \vspace{1.3ex} \large
   Author\\[2ex]Institution
   \distance{2}
   \end{titlepage}

   \begin{psli}[Title of Page]
   The first page: \texttt{psli} environment

   \begin{itemize}
   \item first line in an itemized list
   \item second line in an itemized list
   \end{itemize}
   \end{psli}

   \begin{rsli}
   The second page: \texttt{rsli} environment

   \begin{enumerate}
   \item no title
   \item not centered vertically

   \end{enumerate}

   \end{rsli}
   \end{document}
\end{verbatim}

You may copy the code examples from this manual more easily from the
\texttt{.html} version of this manual available at
\url{http://vkiefel.de/elpres/elpres-manual.html}.


\subsection{Essential elements of an \texttt{elpres} presentation}

The \textbf{title page} slide can be created with
the \texttt{titlepage}  environment (or \texttt{rsli}, see below), \LaTeX's 
\verb+\maketitle+ command is not available.
\textbf{Slides} may be created with the
\texttt{psli}-environment\footnote{\texttt{psli}: \textbf{pl}ain
  \textbf{sl}ide}, you may add
the title of the slide with the optional parameter. 
The contents of the slide are centered vertically.

Another environment generating a \textbf{slide} is
\texttt{rsli}\footnote{\texttt{rsli}: \textbf{ra}w \textbf{sl}ide}:
slides are written 
without title,
contents are not vertically centered. 

The \verb+\distance{number}+ command allows to introduce vertical space into
slides constructed with the \texttt{rsli} and \texttt{titlepage} environments.
You should use pairs of \verb+\distance{}+ commands with numbers indicating
the relative height of empty space, see the titlepage in the example above.

The use of footnotes on slides is often problematic, if they cannot be
avoided, the \texttt{footmisc} package is recommended: the \texttt{perpage}
option resets numbering for each new slide. For a presentation, the
\texttt{symbol} option allows to use symbols instead of numbers. 
After inserting a new footnote, numbers or symbols are correctly
inserted only after a second run of \LaTeX.

\subsection{Alternatives to \TeX's justified paragraphs}

By default, \LaTeX\ produces justified paragraphs with lines of equal length,
this may often not be appropriate for the usually very short lines of text
in presentations. The \LaTeX\ \verb+\raggedright+ command has its own
deficiencies: by inhibiting hypenation in texts with rather short lines,
the right margin will often look too
ragged. A solution is to use the \verb+\RaggedRight+ command of the
\texttt{ragged2e} package. 


\subsection{Vertically compressed lists}

As the spaces between lines may be too great with the \texttt{itemize}
environment,
the \texttt{elpres} package provides a ``vertically compressed''
\texttt{citemize}-environment:

\begin{verbatim}
   \begin{citemize}
     \item  one
     \item  two
   \end{citemize}
\end{verbatim}

Similarly, a \texttt{cenumerate} and a \texttt{cdescription} environment may
be used.

% neu Dezember 2017
Another solution for the customization of \texttt{itemize}
environments is given by the \texttt{enumitem} package. Therefore

\begin{verbatim}
   \usepackage{enumitem}
\end{verbatim}

should be added to the preamble, and  a comma-sperated list of parameters
parameters can be added in the format:

\begin{verbatim}
   \begin{itemize}[parameter-list] 
      ... 
   \end{itemize} 
\end{verbatim}

The ``vertically compressed'' list can then be obtained with

\begin{verbatim}
   \begin{itemize}[nosep]
     \item  one
     \item  two
   \end{itemize}
\end{verbatim}

Similarly, the \texttt{enumitem} package is also able to modify the
\texttt{enumerate} and description \texttt{environments}.



\subsection{Slide layout: changes in slide title, footer, page numbers}

\label{sec:footerlayout}

By default, the \textbf{text color of the titles} of
\texttt{psli}-slides is black,
it may be changed by redefining the ``value'' of the \verb+\slidetitlecolor{}+
command like

\begin{verbatim}
   \slidetitlecolor{blue}
\end{verbatim}

in the preamble. Of course, you may also use the named colors of the
\texttt{xcolor} package (see section \ref{sec:use-colors}).

By default the \textbf{page number} appears at the right bottom of the slide
(in the ``footer'' in the terminology of the \texttt{fancyhdr} package)
this position can be addressed by the \verb+\rfoot{}+ command: the
\verb+\rfoot{}+, \verb+\cfoot{}+ and \verb+\lfoot{}+ commands can be used
by the author of a presentaton.

\begin{sloppypar}
However, access to the header fields defined by \texttt{fancyhdr}
is blocked\footnote{If you enter
  \texttt{\textbackslash rhead}, \texttt{\textbackslash chead}
  or \texttt{\textbackslash lhead}, \texttt{elpres} will issue
  an error message. Use of the header fields will generate unwanted effects
  on page layout due to \texttt{fancyhdr}.}. 
If you wish to change the position of the page number in the footer,
you can overwrite the default page number with an ``empty'' \verb+\rfoot{}+
command and put it to the center or the left margin of the footer
(\verb+\cfoot{\footnotesize \thepage}+ or 
\verb+\lfoot{\footnotesize \thepage}+). These ``footer fields''
are also suitable to enter a \textbf{logo} visible on all pages
(in form of a graphics file using
\verb+\includegraphics[]{}+) or text with the name of the speaker's
institution.
\end{sloppypar}

The user may also use predefined \textbf{presentation schemes} with defined
colors and layout elements (e.\,g. symbols used in list envoronments), details
are described in section \ref{sec:pres-scheme}.



\section{Enhancements to \texttt{elpres}}

\label{secEnhancements}

\subsection{Use of named colors of \texttt{xcolor}}
\label{sec:use-colors}

The \texttt{elpres} class automatically loads the \texttt{xcolor} package.
Color related commands may therefore be used with names defined by
\texttt{colorx}. They are however only accessible in groups
(\texttt{dvipsnames}, \texttt{svgnames}, \texttt{x11names}). As an example,
the color \texttt{Indigo} is available in \texttt{svgnames}. If you wish
to use it you will have to enter \texttt{svgnames} as \texttt{elpres}
class option:

\begin{verbatim}
   \documentclass[11pt,16x9,svgnames]{elpres}
\end{verbatim}

This option  is then automatically ``handed over'' to the
\texttt{xcolor} package.
The complete lists of named colors are found in the \texttt{xcolor} manual.


\subsection{Include graphics files}

Graphics files/pictures can be included with the
\texttt{includegraphics}-command of the \texttt{graphicx}-package. Please be
aware that the dimensions of the pages are \mbox{128mm x 96mm} and
therefore included graphics are scaled appropriately. A safe way to generate a
page with a picture could be (with \texttt{pict.png} as the name of the
graphics file): 

\label{secUsageGraph}

\begin{verbatim}
   \usepackage[pdftex]{graphicx} % (in preamble) 
   ...
   \begin{rsli}
     \centering
     \distance{1}
     \includegraphics[width=0.9\textwidth,%
                      height=0.9\textheight,%
                      keepaspectratio=true]{pict.png}
     \distance{1}
   \end{rsli}
\end{verbatim}


The \verb+\includegraphics[]{}+ comannd requires to select the correct device
driver related option (e.\,g. \texttt{pdftex} or \texttt{dvips})
(documentclass).


\label{secInclGraphicsFiles}


\subsection{Arrange text and pictures in two (or more) columns}

\label{secTwoColumns}

Text and graphics may be arranged in two or more columns with
\texttt{minipage} environments:

\begin{verbatim}
   \begin{minipage}[b][0.8\textheight][t]{0.5\textwidth}
     \colorbox{white}{%
       \includegraphics[width=0.9\textwidth]{graphics-file.png}}
   \end{minipage}
   \begin{minipage}[b][0.8\textheight][t]{0.48\textwidth}
   \footnotesize
   \begin{citemize}
      \item ...
      \item ...
      ...
   \end{citemize}
   \end{minipage}
\end{verbatim}

Details on the minipage environment may be found in the \LaTeX{}
documentation.


\subsection{Incremental slides (overlays)}

\label{secOverlay}

If the contents of slides are to be made visible step
by step this can be achieved by a series of output PDF or (PS) files (carrying
the same page number) usually called \emph{overlays}. It may also be of
interest to change a highlighting color in a series of overlays. This is most
easily done by using the excellent \texttt{overlays} package written by
Andreas Nolda. 

To generate a series of four overlays sequentially showing four lines of a
list:

\begin{enumerate}
  \item load the \texttt{overlays} package in the preamble
  \item put a \texttt{psli} or \texttt{rsli} slide environment into an
  \texttt{overlays} (or \texttt{fragileoverlays}) environment 
  \item enter the number of overlays as the first parameter to the 
  \texttt{overlays} environment  
  \item enter text contents with the \texttt{visible} command with the range
    of overlays showing this text content
\end{enumerate}

A simple example:

\begin{verbatim}
   % to be added in preamble
   \usepackage{overlays}
   ...
   \begin{overlays}{4}
   \begin{psli}[Title of slide]
   \begin{itemize}
     \visible{1-4}{\item first item of list} 
     \visible{2-4}{\item second list item}
     \visible{3-4}{\item 3rd list item}
     \visible{4}{\item final list item}
   \end{itemize}
   \end{psli}
   \end{overlays}
   ... 
\end{verbatim}

The following example uses the \texttt{alert} command to highlight lines
sequentially:

\begin{verbatim}
   \begin{overlays}{4}
   \begin{psli}[Title of slide]
   \begin{itemize}
     \alert{1}{\item first item of list} 
     \alert{2}{\item second list item}
     \alert{3}{\item 3rd list item}
     \alert{4}{\item final list item}
   \end{itemize}
   \end{psli}
   \end{overlays}
\end{verbatim}

The last example shows short text fragments which are shown sequentially using
the \texttt{only} command:

\begin{verbatim}
   \begin{overlays}{4}
     \begin{rsli}
       \only{1}{a short text, which will be replaced \ldots } 
       \only{2}{\dots by a second \ldots}
       \only{3}{\ldots and a third \ldots}
       \only{4}{\ldots and a final text.}
     \end{rsli}
   \end{overlays}
\end{verbatim}


The ``hidden'' text contents are written by \texttt{overlays} in the same
color as the background, default is white. If you use a different background
color, you have to change the color of the hidden text as well by assigning the
background color to the color name \texttt{background} (understood by the
\texttt{overlays} package).  In the following example you define a light
yellow as background:

\begin{verbatim}
   % (in the preamble)
   \definecolor{myyellow}{rgb}{0.96,0.98,0.72} % define color
   \definecolor{background}{named}{myyellow}   % color assigned to 
                                               % hidden text
   \pagecolor{myyellow}                        % color of slide background
\end{verbatim}



If you use one of the \emph{presentation schemes} described in section
\ref{sec:pres-scheme}, the necessary adjustments
for the background color will be made automatically.
For more details on \texttt{overlays}, see the documentation of the package.


\subsection{Navigation symbols}
\label{sec:NavigationSymbols}

With the \texttt{navisymb} option of \texttt{elpres} a panel with navigation
symbols appears
in the right lower corner of the presentation:\\


{ \setlength{\parindent}{2em}
  \indent
  \textbf{\texttt{$\ll$ $<$ $>$ $\gg$ $\leftarrow$ $\rightarrow$ [n]}}
}\\

These commands ($\ll$: jump to the first page,
$<$: go to the previous page, $>$: go to the next page,
$\gg$: jump to the last page, $\leftarrow$:
go back in history, $\rightarrow$: go forward in history,
\texttt{[n]}: prompt for a
page number)
work with Adobe Acrobat Reader (Windows), and (with the exception of
\texttt{$\leftarrow$}, \texttt{$\rightarrow$} and \verb+[n]+)
with \texttt{evince} (Linux).
Some of these functions also work in presentation-mode
of the ``internal'' PDF-viewer in recent versions of the
\texttt{Firefox}-browser (Linux, Windows).


\subsection{Run multimedia content from a presentation}
\label{sec:MultimediaContent}


The command \verb+\auvimm{text}{media-file}+ inserts \texttt{text} with a
link (``poster''), which allows to start the default application
for an audio or video media file
(\texttt{media-file}).  This worked flawlessly on the computers of the author of
this manual with Adobe Acrobat Reader (Windows) and with \texttt{evince} and
\texttt{Impress!ve}\footnote{Can be started from the terminal:
  \texttt{impressive}} (Linux).\footnote{The \texttt{\textbackslash auvimm}
  command uses the \texttt{\textbackslash href} command of the
  \texttt{hyperref} package} An example for using this command:

\begin{verbatim}
   \auvimm{[sound-file]}{./audiofile.mp3}
\end{verbatim}

This produces the (text-based) ``button'' or ``poster'' \texttt{[sound-file]}.
You may also insert an image file as a poster with a graphics file using
\texttt{\textbackslash inlcudegraphics}:

\begin{verbatim}
   \auvimm{\includegraphics{/path/to/poster.png}}{./audiofile.mp3}
\end{verbatim}

If you hit the poster with the mouse pointer it will call the default
application for audio files. If this does not work, it may be necessary to
write as ``launch'' command:

\begin{verbatim}
   \auvimm{[video-file]}{run:./videofile.mp4}
\end{verbatim}

This approach requires that you keep the presentation file
together with the multimedia files in the same folder, \textbf{also at
  the time of
  the presentation}. On an unknown computer system (where you have to show
your presentation) you should test this aspect of your presentation. Sometimes
a call to an external program might be blocked due security reasons in Acrobat
Reader.

\begin{leftbar}
This \texttt{\textbackslash auvimm}-command should be considered experimental.
Its behavior depends on the pdf-viewer and the configuration of the OS.
\end{leftbar}



\subsection{Prepare a ``handout'' from a presentation}

\label{secHandout}

In advance of a lecture it is often expected that you
prepare a ``handout'' from your presentation with more than one
pages on a printed page. Therefore
you have several options, two of them are described
here.

\subsubsection{\LaTeX\ article document with a series of single-page pdf-files included}

One solution to this problem is (1) to generate a series of single-page
pdf-files and (2) to create a pdf\LaTeX\ document with the single
pages included.

\textbf{(1)} can be done with \texttt{gs} (the Ghostscript program):

\begin{verbatim}
   gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite  -sOutputFile=page_%03d.pdf in.pdf
\end{verbatim}

with \texttt{in.pdf} as the initial presentation.  The syntax of \texttt{gs}
is described in detail on the ghostscript
website\footnote{\url{https://www.ghostscript.com/doc/current/Use.htm}}.
On Windows systems the name of the ghostscript command may be
\texttt{gswin32c.exe} or  \texttt{gswin64c.exe}.
An alternative to \texttt{gs} is the \texttt{pdftk} tool:

\begin{verbatim}
    pdftk in.pdf  burst output  page_%03d.pdf
\end{verbatim}

The syntax of \texttt{pdftk} is explained in the documentation
(\texttt{pdftk~--help}).

\textbf{(2)} an example for a \LaTeX\ document which can serve as handout:

\begin{verbatim}
   \documentclass[12pt]{article}
   \usepackage[pdftex]{graphicx}
   \usepackage[latin1]{inputenc}
   \usepackage[a4paper,hmargin=2.4cm,top=24mm,bottom=28mm]{geometry}
   \newcommand{\PictScaleFact}{0.45}

   \begin{document}
   \centering
   \fbox{\includegraphics[width=\PictScaleFact\textwidth]{page_001.pdf}}
   \hspace{5mm}
   \fbox{\includegraphics[width=\PictScaleFact\textwidth]{page_002.pdf}}
   \\[2ex]
   \fbox{\includegraphics[width=\PictScaleFact\textwidth]{page_003.pdf}}
   \hspace{5mm}
   % ...
   \fbox{\includegraphics[width=\PictScaleFact\textwidth]{page_008.pdf}}
   % ...
   \end{document}
\end{verbatim}

\subsubsection{Convert presentation pdf-file directly into handout file with \texttt{pdfjam}}

With the following command:

\begin{verbatim}
   pdfjam --nup 2x4 --frame true --scale 0.9 -o new.pdf in.pdf '1-4,6-22'   
\end{verbatim}

\begin{sloppypar}
\texttt{pdfjam}\footnote{which regerettably is only available on
Linux or other Unix-like systems}
creates a ``handout'' PDF document (\texttt{new.pdf}) fom
\texttt{in.pdf} with the slides nr. 1-4,6-22
arranged in  two columns and four rows.
With the additional option ``\texttt{--frame~true}'',
\texttt{pdfjam} draws a box
around each slide. More details can be found in the \texttt{pdfjam}
man page and the project
website\footnote{https://github.com/DavidFirth/pdfjam/blob/master/README.md}. 
\end{sloppypar}



\subsection{Create presentations with hypertext elements}

\label{secHypertext}

You may use two commands \verb+\fromlinktext{}{}+ and \verb+\totargettext{}{}+
for ``hypertext features''.  As an example: as you normally will not insert
\verb+\section{}+-like commands and therefore do not generate a ``table of
contents''-like page or a menu in \texttt{elpres}-presentations, you are able
define links with:

\begin{verbatim}
   \totargettext{text}{link-label}
\end{verbatim}
% \hypertarget{target-name}{text}


which can be addressed by

\begin{verbatim}
   \fromlinktext{text}{link-label}
\end{verbatim}

% \hyperlink{target-name}{text}

If you use these commands to generate a \textbf{menu page} (as a hyperlinked
table of contents) with items pointing to
specific slides: 
\texttt{link-label} acts as label, \texttt{text} in the
\verb+\fromlinktext+ command is converted to a link (e.\,g. on a menu-page).
If you click on this text ``button'', you jump to
the \texttt{text} labelled with
\verb+\totargettext+; \texttt{link-label} has to be identical in a
\verb+\fromlinktext+ and \verb+\totargettext+ pair,
\texttt{text} in the link
and the target, of course need not to be identical.

The default for the link border color is red, it may be changed with
\verb+\hypersetup{}+, a command from the \texttt{hyperref}
package\footnote{both, \texttt{\textbackslash totargettext} and
\texttt{\textbackslash fromlinktext} are
are redefinitions of commands from the \texttt{hyperref} package}.
The option for the color of the frame around the link is
\texttt{linkbordercolor},
colors must be defined whith three figures \texttt{[0..1]} according to the RGB
color model:

\begin{verbatim}
   \hypersetup{linkbordercolor={0.6 0.6 0.6}}
\end{verbatim}

This defines a light gray color for the link border, more details on
\verb+\hypersetup{}+ are
described in the documentation of the \texttt{hyperref} package.

\subsection{Fill background of a presentation with bitmaps}

\label{secBackgroundWallpaper}

\subsubsection{\texttt{Wallpaper} package}

To create a slide background with a graphical wallpaper 
background using bitmap files you may use the \texttt{wallpaper}
package\footnote{written by Michael H.F. Wilkinson and available on CTAN}. 
Load the \texttt{wallpaper} package with

\begin{verbatim}
   \usepackage{wallpaper}
\end{verbatim}

\begin{sloppypar}
in the preamble. In order to generate a background based on bitmap
file \texttt{background.png}, enter 
\end{sloppypar}

\begin{verbatim}
   \CenterWallPaper{1}{background.png}
\end{verbatim}

before the contents of the presentation\footnote{i.\,e. following
\texttt{\textbackslash begin\{document\}}}. 
This works best with bitmaps with an appropriate aspect ratio, in the case of
an 4x3 screen format a bitmap picture of 640x480 pixel would fit perfectly. 
Moreover bitmap files may be
used as tiles as described in the \texttt{wallpaper} documentation like

\begin{verbatim}
   \TileSquareWallPaper{4}{background.png}
\end{verbatim}

More details on this topic may be found in the \texttt{wallpaper}
documentation.

\subsubsection{\texttt{Eso-pic} package}
\label{secBackgroundEsopic}

Another package which allows you to paint the background with a picture is
\texttt{eso-pic}\footnote{written by Rolf Niepraschk and available on CTAN}:

\begin{verbatim}
   \usepackage{eso-pic}
   
   ...

   \AddToShipoutPicture{
   \includegraphics[height=\paperheight]{background.png}
   }
\end{verbatim}

\verb+\AddToShipoutPicture{}+ puts the picture on every page, 
\verb+\AddToShipoutPicture*{}+ puts it on to the current page,
\verb+\ClearShipoutPicture+ clears the background beginning with the current
page.
Details of \texttt{eso-pic}'s commands can be found in the documentation.


\subsection{Presentation schemes}

\label{sec:pres-scheme}

\texttt{Elpres} provides predefined \textbf{presentation schemes}:
color and layout schemes which are applied to a presentation with
\verb+usepackage{}+. Their naming follows the convention
\mbox{\texttt{elpres...scheme}}, an
example is \texttt{elpreswhiteredscheme}.\footnote{the name of the
  corresponding style or package file is \texttt{elpreswhiteredscheme.sty}}
These \texttt{elpres}-schemes also support incremental slides with the
\texttt{overlays} package. 
Moreover, these schemes define color names
which may be used by the author of a presentation:\footnote{Color names for use
by the presentation author start with \texttt{ep...}}




\begin{description}[noitemsep]
\item[\texttt{eptextcolor}] ``normal text color'' (default values for
  documents without
  presentation scheme included in \texttt{elpres.cls}: black)
\item[\texttt{ephighlightcolor}] color, which may be used for highlighting
  text (default:
  dark red)
\item[\texttt{eptitlecolor}] color of the title of slides, may be used to
  highlight the
  title in the title slide (default: dark red)
\end{description}

Presentation schemes available in the current version of \texttt{elpres}:

\begin{sloppypar}

\begin{description}[noitemsep]

% \item[\texttt{elpresplainscheme}] ..

\item[\texttt{elpresbluelightgrayscheme}] dark blue background of slides;
  \texttt{eptextcolor}: light gray;
  \texttt{eptitlecolor}: turquoise;
  \texttt{ephighlightcolor}: yellow;
  symbols in \emph{itemize} environment: pale blue bullets;
  highlighted elements in \emph{enumerate} and \emph{description}
  environments: pale blue
  
\item [\texttt{elpresgrayscheme}] gray background of slides;
  \texttt{eptextcolor}: dark gray;
  \texttt{eptitlecolor}:
  dark green; \texttt{ephighlightcolor}: dark red;
  symbols in \emph{itemize} environment: gray bullets; highlighted elements
  in \emph{enumerate} and \emph{description} environments: dark green

\item [\texttt{elpreswhitebluescheme}] white background of slides;
  \texttt{eptextcolor}: blue-black;
  \texttt{eptitlecolor}: blue;
  \texttt{ephighlightcolor}: dark red;
  symbols in \emph{itemize} environment: blue bullets; highlighted elements
  in \emph{enumerate} and \emph{description} environments: blue

\item [\texttt{elpreswhiteredscheme}] white background of slides;
  \texttt{eptextcolor}: red/brown-black;
  \texttt{eptitlecolor}: dark red;
  \texttt{ephighlightcolor}: dark blue;
  symbols in \emph{itemize} environment: red bullets;
  highlighted elements in \emph{enumerate} and \emph{description}
  environments: red

\item [\texttt{elpreswhitetealscheme}] white background of slides;
  \texttt{eptextcolor}: black-teal;
  \texttt{eptitlecolor}: teal;
  \texttt{ephighlightcolor}: dark red;
  symbols in \emph{itemize} environment: blue bullets; highlighted elements
  in \emph{enumerate} and \emph{description} environments: dark teal
  
\end{description}
\end{sloppypar}

In all of these schemes, the colors of borders for internal and external
links have been adjusted.


% Die Farben in LibreOffice / rgb (266) / rgb (1)
% ----------------------------------------------
% Tango himmelblau 1 / 114 159 207 /
% Rot 5 / 153 0 102 /
% Grau 6 / 128 128 128 /

\section{Recipes for use of \texttt{elpres}}
\label{sec:elpres-recipes}

\subsection{Colored text boxes}
\label{sec:colored-text-boxes}

Colored text boxes often help the presentation author to highlight small text
fragments and to make slides more ``readable'' and more attractive.

\subsubsection{Text boxes with \texttt{\textbackslash colorbox}, \texttt{\textbackslash fcolorbox}}
\label{sec:simple-text-boxes}


A simple method for colored text boxes uses the \texttt{minipage} environment
and the \texttt{colorbox} or \texttt{fcolorbox}
commands (\texttt{xcolor} package):

\begin{verbatim}
   \begin{psli}[Slide with a textbox]
   \begin{center}
     \colorbox{eptitlecolor}{\begin{minipage}{0.85\textwidth}
        \centering \color{white} \vspace{2ex}
        {\Large
        Text centered in a colorbox
        } \par
       \vspace{2ex}
     \end{minipage}}
   \end{center}
   \end{psli}
\end{verbatim}


\subsubsection{Text boxes with the \texttt{tcolorbox} package}
\label{sec:tcolorbox-boxes}

The \texttt{tcolorbox} package provides great support for colored boxes. A
minimal example:

\begin{verbatim}
   \begin{psli}[Slide with a textbox]
    \begin{tcolorbox}[colframe=eptitlecolor!95!black]
     \textcolor{eptitlecolor}{This is a textbox generated with the
       \texttt{tcolorbox} package}
     \tcblower This the lower part of the tcolorbox
    \end{tcolorbox}  
   \end{psli}
\end{verbatim}

An example with a titled colored textbox:

\begin{verbatim}
   \begin{psli}[Slide with a textbox]
    \begin{tcolorbox}[colback=eptitlecolor!10!white,%
       colframe=eptitlecolor!95!black,%
       title=Heading of a textbox]
       This is another \textbf{tcolorbox}.
     \tcblower
        Here, the lower part of the box.
      \end{tcolorbox}
   \end{psli}
   
\end{verbatim}

The manual of the \texttt{tcolorbox} provides perfect instuction for the use
of this package.

\subsection{Inactivate \texttt{overlays} commands}
\label{sec:inact-overl-comm}

\begin{sloppypar}
If a pesentation has been prepared with overlay slides (using the
\texttt{overlays}) package, the \LaTeX{} source code will contain
\verb+\visible+, \verb+\only+ and \verb+\alert+ commands and
\texttt{overlays} and \texttt{fragileoverlays} environments.

Sometimes, it is desirable to generate a version of such a presentation
without overlays effects, for example for preparation of a printed handout
(see section \ref{secHandout}).
The effect of these overlay-specific instuctions can be
inactivated with this series of commands:
\end{sloppypar} 


\begin{verbatim}
   \renewenvironment{overlays}[1]{}{}%
   \renewenvironment{fragileoverlays}[1]{}{}%
   \renewcommand{\visible}[2]{#2}%
   \renewcommand{\only}[2]{#2}%
   \renewcommand{\alert}[2]{#2}%
\end{verbatim}

which should be copied or entered in the preamble following
\verb+\usepackage{overlays}+. 


\subsection{Convert an \texttt{elpres}-presentation (PDF) into an Impress (LibreOffice) or Powerpoint presentation}
\label{sec:convert-into-libreoff}

Sometimes it is unclear, if organizers of a (scientific) meeting allow
presentations with pdf-files but insist on files in Powerpoint format. This is
an unpleasant situation, but sometimes it is better to be prepared.

There is no elegant way for a reliable solution.
First, you may use one of the free or commercial online conversion
tools (intended to convert \texttt{.pdf} into
\texttt{.ppt} or \texttt{.pptx}-files). Results are often not satisfactory.

An alternative, more reliable way preferred
by the author of this manual (although a little awkward) is:


\begin{enumerate}
\item convert the pdf-file of the presentation into a series of bitmap
  graphics files, (e.\,g. in \texttt{.png} format)
\item import these image \texttt{.png} files into pages of an empty Impress
  (LibreOffice or OpenOffice) presentation and save the presentation in
  \texttt{.ppt} or \texttt{.pptx}-Format
\end{enumerate}

\textbf{Details on step 1 --- generate \texttt{.png} images:}

Please copy the following command (ignoring the line-break) into a single line
at the terminal command prompt or copy them into one line of a short shell
script (Windows: a \texttt{.bat} file) and ``run'' or execute the shell
script:

\begin{verbatim}
   gs -dNOPAUSE -dBATCH -sDEVICE=png256 -r300
          -sOutputFile=page_%03d.png input.pdf
\end{verbatim}

\begin{sloppypar}
  Please do not forget to adjust the name of the input file
  \texttt{input.pdf}.  This command generates a series of \texttt{.png}-files
  \texttt{page\_001.png}, \texttt{page\_002.png} ... \texttt{page\_0nn.png}.
  On Windows systems the name of the ghostscript command is
  \texttt{gswin32c} or \texttt{gswin64c}.
\end{sloppypar}

If you wish a better resolution of the \texttt{.png} change \texttt{-r300} to
\texttt{-r600} or even \texttt{-r1200}:

\begin{verbatim}
   gs -dNOPAUSE -dBATCH -sDEVICE=png256 -r600 
           -dTextAlphaBits=4 -sOutputFile=page_%03d.png input.pdf
\end{verbatim}

The additional option  \texttt{-dTextAlphaBits=4} improves font
antialiasing.\footnote{Details can be found in the docume3ntation of gs
  \url{https://www.ghostscript.com/doc/current/Use.htm}}

\textbf{Details on step 2 --- Import \texttt{.png} images:}

Open an empty presentation with LibreOffice Impress, adjust the appropriate
slide format (e.\,g. 4:3), import the first \texttt{.png} file with
\emph{Insert~/ Image}, then adjust the size of the included image with
\emph{Format~/ Object~and~Shape~/ Position~and~Size (F4)} in the
\emph{Position~and~Size} dialog box please enter: \emph{Postion~X}:
\texttt{0.0cm}, \emph{Postion~Y}: \texttt{0.0cm}, \emph{Width}:
\texttt{28.0cm}, activate the checkbox \emph{Keep~ratio}.\footnote{Menu
  structure of LibreOffice v6.3}. The presentation can now be saved
in one of the Microsoft Powerpoint formats \texttt{.ppt} or \texttt{.pptx}.

\section{License}

This class is distributed under the \textsl{\LaTeX{} Project Public License}
(LPPL) which may be downloaded from
\url{http://www.latex-project.org/lppl.txt}. No warranty is provided for this
work (as stated in the LPPL).

\section{Version history}

\textbf{v0.1} (19.6.2004):  initial version.
\textbf{v0.2} (1.9.2004): page numbers now changed to footnotesize, left and
right margins slightly changed, `cenumerate' and `cdescription' environments
added.
\textbf{v0.2a} (19.9.2004): Section ``License'' added to the documentation.
\textbf{v0.2b} (17.10.2004): Documentation completed: description of the
\verb+\distance{}+ command included.
\textbf{v0.2c} (28.11.2004): Documentation completed (section
\ref{secHypertext} 
added).
\textbf{v0.2d} (25.12.2004): Documentation completed (section
\ref{secBackgroundWallpaper} added).
\textbf{v0.2e} (15.04.2005): Documentation completed (sections
\ref{secBackgroundEsopic} and \ref{secHandout} added).
\textbf{v0.3} (12.08.2005): new  (class) options for font selection:
\texttt{tmrfont} (Times Roman), \texttt{helvetfont} (Helvetica),
\texttt{cmfont} (Computer Modern), \texttt{sansfont} (Sans Serif: default).
Documentation updated, sections \ref{secInclGraphicsFiles} and
\ref{secTwoColumns} added.
\textbf{v0.4} (20.01.2018): New class options for different screen aspect
ratios \texttt{4x3}, \texttt{16x9}, \texttt{16x10}; ``compressed'' list
environments modified; documentation completed:
packages for use with \texttt{elpres}: \texttt{enumitem} (alternative list
environments), \texttt{overlays} (overlay support: incremental slides);
section \ref{secHandout} was completely rewritten.
\textbf{v0.4a} (24.01.2018): Documentation completed.
\textbf{v0.5} (12.07.2020): New class options: \texttt{nofonts}
(no font selection) and \texttt{navisymb} (inserts a panel of
symbols for navigation), new commands: \verb+auvimm+ (generates
a link to external audio, video files), \verb+\fromlinktext+ and
\verb+\totargettext+ (create links within a presentation).
Documentation has been partly rewritten.
\textbf{v0.6} (19.08.2020):
Access to the \texttt{fancyhdr} ``header fields'' (\verb+\lhead{}+,
\verb+\chead{}+ and \verb+\rhead{}+) is now explicitly blocked.
New command \verb+\slidetitlecolor{}+ for the text color of titles in
\texttt{psli}-slides.
New: style files for presentation schemes  added.
Documentation has been significantly
completed: sections \ref{sec:footerlayout},
\ref{sec:use-colors}, \ref{sec:pres-scheme} and \ref{sec:elpres-recipes} added,
section \ref{sec:installation} on installation rewritten.


\end{document}



ReadableCV v3

This class, ReadableCV, is a re - working of how I wrote my own CV using other software. The intention is that it is a highly readable CV which is easy for a company to find the information which shows you as the best person for the job.

The class file has plenty of comments describing it and the accompanying .tex file, whilst serving as an example of how to use the class, also contains plenty of comments.

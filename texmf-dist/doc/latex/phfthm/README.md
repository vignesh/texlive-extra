# The phfthm package

Goodies for theorems and proofs.

The phfthm package provides enhanced theorem and proof environments, based on
the amsthm original versions. It allows for hooks to be placed, adds some
default goodies and is highly customizable. In particular, it can connect
theorems to proofs, automatically producing text such as "See proof on page XYZ"
and "Proof of Theorem 4: ..."


# Documentation

Run 'make sty' to generate the style file and 'make pdf' to generate the package
documentation. Run 'make' or 'make help' for more info.

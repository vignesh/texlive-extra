%%
%% This is file `README.txt',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% euclideangeometry.dtx  (with options: `readme')
%% 
%%   Copyright (C)  2020 Claudio Beccari all rights reserved.
%%   License information appended
%% 
File README.txt for package euclideangeometry
        [2020-04-15 v.0.1.8 Extension package for curve2e]

The package bundle euclideangeometry is composed of the following files

euclideangeometry.dtx
euclideangeometry.pdf
euclideangeometry-man.tex
euclideangeometry-man.pdf
README.txt

euclideangeometry.dtx is the documented TeX source file of package
euclideangeometry.sty; you get euclideangeometry.sty,
euclideangeometry.pdf by running pdflatex on euclideangeometry.dtx.

README.txt, this file, contains general information.

euclideangeometry-man.tex and euclideangeometry-man.pdf are
the source file and the readable document containing the end
user manual.
In other words euclideangeometry.pdf is oriented towards the
developers and euclideangeometry-man.pdf to the end users.

Claudio Beccari

claudio dot beccari at gmail dot com
%% 
%% Distributable under the LaTeX Project Public License,
%% version 1.3c or higher (your choice). The latest version of
%% this license is at: http://www.latex-project.org/lppl.txt
%% 
%% This work is "maintained"
%% 
%% This work consists of files:
%% 1) euclideangeometry.dtx, and the derived files euclideangeometry.sty
%%                           and euclideangeometry.pdf,
%% 2) euclideangeometry-man.tex and the derived file euclideangeometry-man.pdf,
%% 3) the auxiliary derived file README.txt
%% 
%%
%% End of file `README.txt'.

The gcite Package
==================

Copyright 2007--2009 Matthew Tylee Atkinson, Iain Phillips
Distributed under the LPPL 1.3 (http://www.latex-project.org/lppl.txt)

Abstract
---------

This package allows you to make citations in the German style, which
is considered by many to be particularly reader-friendly. The citation
provides a small amount of bibliographic information in a footnote on
the page where each citation is made.  It combines a desire to eliminate
unnecessary page-turning with the look-up efficiency afforded by numeric
citations.

Installation
-------------

If you wish to install the latest version of gcite locally...

1. mkdir -pv ~/texmf/tex/latex/gcite/
2. cp gcite.dtx gcite.ins ~/texmf/tex/latex/gcite/
3. cd ~/texmf/tex/latex/gcite/
4. latex gcite.ins
5. Compile the manual...
    a. pdflatex gcite.dtx
    b. bibtex gcite
    c. pdflatex gcite.dtx
    d. pdflatex gcite.dtx
6. Read the manual ;-).

Please contact us if you have any problems or suggestions.


== END ==

%% Documentation for jumplines.sty
%%
%% 
%%
%% -------------------------------------------------------------------------------------------
%% Copyright (c) 2015 by Dr. Christian Hupfer <christian dot hupfer at yahoo dot de>
%% -------------------------------------------------------------------------------------------
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% This work consists of all files listed in README
%%

\documentclass[12pt,english]{ltxdoc}
\usepackage{scrlayer-scrpage}
\usepackage[lmargin=3cm,rmargin=3cm,a4paper]{geometry}
\usepackage{savesym}%
\usepackage{bbding}%
\savesymbol{Cross}%

\usepackage{blindtext}%
\usepackage{enumitem}%
\usepackage{datetime}%
\usepackage{imakeidx}%
\usepackage[babel,style=english]{csquotes}
\newcommand{\myquotes}[1]{\enquote{#1}}%

\usepackage{array}%

\usepackage[languages={spanish,french,ngerman,english}]{jumplines}

\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}

\tcbuselibrary{documentation}
\tcbset{color command={blue}}


\newcommand{\tcolorboxdoclink}{http://mirrors.ctan.org/macros/latex/contrib/tcolorbox/tcolorbox.pdf}
\newcommand{\packagename}[1]{\fbox{\textcolor{blue}{\textbf{\ding{41}~#1}}}\index{Package!#1}}%
\newcommand{\classname}[1]{\fbox{\textcolor{brown}{\textbf{\Writinghand~#1}}}\index{Class!#1}}%

\newcommand{\handrightnote}{\tcbdocmarginnote{\ding{43}}}

\newtcolorbox{docCommandArgs}[1]{colbacktitle={blue},coltitle={white},title={Description of arguments of command \cs{#1}}}


\def\packageversion{0.2}%

\makeindex

\newcommand{\PackageDocName}{jumplines.sty}%


\hypersetup{breaklinks=true}

\begin{document}
\selectlanguage{english}
\mmddyyyydate


\setlength{\parindent}{0pt}

\thispagestyle{empty}%

\begin{center}
\begin{tcolorbox}[boxrule=1mm,arc=4mm,colback=yellow!30!white,width=0.8\textwidth]
\large \bfseries%
\begin{center}%
\begin{tabular}{C{0.7\textwidth}}%
\textsc{\PackageDocName} \tabularnewline
\tabularnewline
Providing teaser headline articles continued later on \tabularnewline
\tabularnewline
Version \packageversion \tabularnewline
\tabularnewline
\today \tabularnewline
\tabularnewline
\addtocounter{footnote}{2}
Author: Christian Hupfer\(^\mathrm{\fnsymbol{footnote}}\)% }{\makeatletter christian.hupfer@yahoo.de}
\tabularnewline
\end{tabular}
\end{center}
\end{tcolorbox}
\makeatletter
\renewcommand{\thefootnote}{\fnsymbol{footnote}}%
\footnotetext{christian.hupfer@yahoo.de}%
\makeatother
\end{center}

\clearpage
\tableofcontents




\pagestyle{scrheadings}%
\setheadsepline{2pt}[\color{blue}]

\setcounter{footnote}{0}


\section{Introduction}

The aim of this package is to provide support for writing teaser articles which are common in newspapers, starting for example on the frontpage and are continued on another page, with some reference on which page this continuation happens.

\JumplineArticle[ArticleHeadline={Something to know},TeaserHeight=0.5in]{%
This is the first teaser, which is broken after some dummy text,\textcolor{blue}{\blindtext}\textcolor{red}{\blindtext}}%

\JumplineArticle[TeaserHeight=1.5in,ArticleHeadline={And another useless article}]{%
This is the second teaser headline, which is broken after some dummy text also\blindtext[1]}%

\ShipoutArticleTeasers%



\ShipoutArticleHangingArticles%


\section{Package options}%

As of version \packageversion~ the package has only the \oarg{languages} option so far. It is used to set the language of the articles. See section \ref{sec::language_support} for more information about the support for foreign languages.



\section{Requirements and incompatibilities}%

\subsection{Required packages}

\begin{itemize}
\item \packagename{etoolbox}%
\item \packagename{xkeyval}%
\item \packagename{xparse}%
\item \packagename{tcolorbox}%
\item \packagename{luacolor}%
\item \packagename{hyperref}%
\end{itemize}

Additionally, the features of \packagename{bookmark} package are used for more convenient bookmark generation if this package is loaded, otherwise it is restricted to the bookmarking capatibilities of \packagename{hyperref}.

\subsection{Incompatibilities}

This package does \textbf{not} work with floating contents \textbf{inside} of the articles, however, floats outside of such articles are possible. Although it's possible to use \packagename{wrapfig} and its \docAuxEnvironment{wrapfigure} environment, it's advisable to adapt the teaser box height manually since the wrapping works but is continued later on too. 

\section{Documentation of Macros}
\begin{docCommand}{JumplineArticle}{\oarg{options}\marg{article content}}

This provides the main command for typesetting a jumpline article. All of the article content is specified in the (2nd) mandatory argument.

\begin{docCommandArgs}{JumplineArticle}%

  \begin{enumerate}[label={\textcolor{blue}{\#\arabic*}}]
  \item \oarg{options}: 

    \begin{docKey}{TeaserHeight}{=\meta{length value}}{2in}
      Sets the cut off value for the height of the teaser -- this is not the height of the shown teaser itself, since this involves the teaser header box and some vertical spacings above and below the heading and the teaser content.

    If the height of the article is smaller than the \refKey{TeaserHeight} value the full article is displayed then and not broken into two pieces. 
    \end{docKey}

    \begin{docKey}{ArticleHeadline}{=\meta{arbitray text}}{no value}
      This sets an article headline after the article number. Should be enclosed in a \brackets{}-pair
    \end{docKey}

    \begin{docKey}{TeaserHeaderOptions}{=\meta{options for the teaser header}}{breakable,leftlower=0pt,rightlower=0pt,boxrule=0pt,left=0pt,right=0pt, arc=0pt,auto outer arc,colbacktitle=black,coltitle=white,toptitle=2mm,bottomtitle=2mm}
      This sets additional options for the display of the teaser header. All options are directly passed to the underlying \cs{tcolorbox}, see the \href{\tcolorboxdoclink}{\packagename{tcolorbox}} documentation on those options.  
   \end{docKey}
   
    \begin{docKey}{ContinuedArticleHeaderOptions}{=\meta{options for the continued article header}}{breakable,leftlower=0pt,rightlower=0pt,boxrule=0pt,left=0pt,right=0pt, arc=0pt,auto outer arc,colbacktitle=black,coltitle=white,toptitle=2mm,bottomtitle=2mm}
      This sets additional options for the display of the continued article header. All options are directly passed to the underlying \cs{tcolorbox}, see the \packagename{tcolorbox} documentation for this. This option has the same meaning to the continued article as \refKey{TeaserHeaderOptions} to the teaser of the same article. 
   \end{docKey}

    \begin{docKey}{ContinuedOnTopskip}{=\meta{length}}
      This key value holds the vertical spacing between the splitted teaser and the line \textit{Continued on}.
    \end{docKey}

    \begin{docKey}{ContinuedOnBottomskip}{=\meta{length}}
      This key value holds the vertical spacing between the line \textit{Continued on} and the next possible teaser header.
    \end{docKey}


    \begin{docKey}{ArticleAuthor}{=\meta{}}{initially unset}
      This sets a (list of) the author name(s) -- it will be displayed at the bottom of the teaser only, introduced by \myquotes{\textit{By}}. 
   \end{docKey}


 \item \marg{article content}
   This contains arbitrary content, as long no floating objects are involved. 
\end{enumerate}
\end{docCommandArgs}

\end{docCommand}%


\begin{docCommand}{ShipoutArticleTeasers}{}
This places all teasers consecutively without interruption. If this is placed after \refCom{ShipoutArticleHangingArticles}, the teasers are placed after the lower parts of the articles however.
\end{docCommand}

\begin{docCommand}{ShipoutArticleHangingArticles}{}
  This places all continued articles consecutively without interruption from this position. This macro should be placed \textbf{after} \refCom{ShipoutArticleHangingArticles}.
\end{docCommand}


\begin{docCommand}{listofarticle}{}
  This introduces a table of the articles with reference to starting page of the teaser.
\end{docCommand}

\begin{docCommand}{listofcontarticle}{}
  This introduces a table of the continued articles with reference to starting page of the continued article.
\end{docCommand}

\begin{docCommand}{listofarticlesname}{}
  This command holds the name which is displayed as heading of the list of articles. Default value: \textbf{\listofarticlesname}
\end{docCommand}

\begin{docCommand}{listofcontinuedarticlesname}{}
  This command holds the name which is displayed as heading of the list of continued articles. Default value: \textbf{\listofcontinuedarticlesname}
\end{docCommand}

\subsection{Counters}

The internal counter \docCounter{article} is used internal for counting issues of the articles -- it can't be changed as of version \packageversion.


\clearpage

\section{Language support}\label{sec::language_support}

The package uses \packagename{babel} in order to provide support for foreign languages. Default language is English, as of version \packageversion~ ngerman (New German) is the only other language which is used inside the package and command translations are given. 






\section{To-Do list}

\begin{itemize}
\item More options for fine control of the behaviour of teasers and continued articles
\item Better height management of the articles/placement on pages
\item Better option handling
\item Improve support for languages other than English or German 
\item Bookmarks
\item Better behaviour in multicol-environment
\item Improve documentation

\end{itemize}

If you 

\begin{itemize}
  \item find bugs
  \item errors in the documentation
  \item have suggestions 
  \item have feature requests
  \item want provide or improve translations of the language specific items 
\end{itemize}

don't hesitate and contact me via \makeatletter christian.hupfer@yahoo.de\makeatother

\clearpage

\section{Acknowledgments}

I would like to express my gratitudes to the developpers of fine \LaTeX{} packages and of course
to the users at tex.stackexchange.com, especially to

\begin{itemize}
  \item Enrico Gregorio
  \item Joseph Wright
  \item David Carlisle
  \item Paulo Roberto Massa Cereda
  \item Werner Doe ;-)
\end{itemize}

for their invaluable help on many questions on macros.

\vspace{2\baselineskip}
A special gratitude goes to Prof. Dr. Dr. Thomas Sturm for providing the wonderful \packagename{tcolorbox} package which was used to
write this documentation.

\clearpage


\clearpage

%%%% Index of commands

\printindex

\end{document}
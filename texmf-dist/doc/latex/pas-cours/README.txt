************* README file for pas-cours **********************
********************* ENGLISH *****************************

This package uses TikZ to built mathematics lessons.

Licence
-------

This program can be redistributed and/or modified under the terms
of the LaTeX Project Public License Distributed from CTAN
archives in directory macros/latex/base/lppl.txt. 

Documentation
-------------

The documentation of the package is in the ZIP file or under the 
directory /doc/ in french language.
If you don't find it, go to the URL http://get-software.net/macros/latex/contrib/pas-cours/doc/pas-cours.pdf

Thanks to use pas-cours.sty.

************* Fichier README pour pas-cours **********************
********************** FRANCAIS *******************************

Cette extension utilise TikZ pour écrire des cours de mathématiques.

Licence
-------

L'extension peut être redistribuée et/ou modifiée sous les termes 
de la licence LaTeX Project Public (voir macros/latex/base/lppl.txt).

Documentation
-------------

La documentation de l'extension se trouve dans le fichier ZIP ou
dans dans le répertoire /doc/.
Si vous ne trouvez pas cette documentation, allez sur la page http://get-software.net/macros/latex/contrib/pas-cours/doc/pas-cours.pdf

Merci d'utiliser pas-cours.sty.

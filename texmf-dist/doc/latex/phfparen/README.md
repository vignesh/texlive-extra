# The phfparen package

Parenthetic math expressions made simpler and less redundant.

Provides a more condensed and flexible syntax for parenthesis-delimited
expressions in math mode, which allows switching brace sizes easily for
instance. For example, the syntax " `\big( a + b ) " can be used to replace
"\bigl( a + b \bigr)".


# Documentation

Run 'make sty' to generate the style file and 'make pdf' to generate the package
documentation. Run 'make' or 'make help' for more info.


# skills v1.0.0 (2020-10-21)

Author: Pierre-Amiel Giraud

## License

This material is subject to the LaTeX Project Public License, either version 1.3 of this license or (at your option) any later version. The latest version of this license is in http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all distributions of LaTeX version 2003/12/01 or later.

- This work has the LPPL maintenance status "maintained".
- The Current Maintainer of this work is Pierre-Amiel Giraud.
- This work consists of the files `skills.sty` (the package itself) and `skillsdoc.tex` (its documentation).

Copyright 2020 Pierre-Amiel Giraud

## Description

This package attempts to make it easy for even a LaTeX novice to prepare proficiency tests, especially in combination with the `exam` document class. Thus, almost all command names are very similar.

After defining skills in the preamble or in an external file, they are declared using labels, and can optionally be set as global skills. A skills table is generated to summarize the evaluated competencies and to allow for writing down the resulting proficiency level.

The user's guide `skillsdoc.tex` attempts to explain all of the possibilities in a readable way, with many examples.
    % LaTeX Package: runcode  v1.0 2020/10/04
    %
    % Copyright (C) 2020 by Haim Bar and HaiYing Wang
    %
    % This file may be distributed and/or modified under the
    % conditions of the LaTeX Project Public License, either
    % version 1.3c of this license or (at your option) any later
    % version.  The latest version of this license is in:
    %
    %    http://www.latex-project.org/lppl.txt
    %
    % and version 1.3c or later is part of all distributions of
    % LaTeX version 2005/12/01 or later.

\documentclass{ltxdoc}
\usepackage[hyphens]{url}
%\usepackage{runcode}
\ProvidesPackage{runcode}[2020/10/04 runcode v1.0]
\begin{document}
    \title{The \textsf{runcode} package\thanks{This document  corresponds to \textsf{runcode}~v1.0, dated~2020/10/04.}}
    \author{Haim Bar and HaiYing Wang \\ \texttt{haim.bar@uconn.edu}, \texttt{haiying.wang@uconn.edu}}
    \maketitle

\abstract{\textsf{runcode} is a \LaTeX package that executes programming source codes (including all command line tools) from \LaTeX, and embeds the results in the resulting pdf file. Many programming languages can be easily used and any command-line executable can be invoked when preparing the pdf file from a tex file.

It is recommended to use this package in the server-mode together with the Python talk2stat package. Currently, the server-mode supports Julia, MatLab, Python, and R. More languages will be added.

For more details and usage examples, refer to the package's github repository, at
\url{https://github.com/Ossifragus/runcode}.
}

\section{Installation}
The package on CTAN can be installed automatically by your \TeX\space software (e.g., MikTeX Update Wizard). You can also simply put the runcode.sty file in the \LaTeX\space project folder. To use the package you have to enable the `shell-escape' option when compiling a \LaTeX\space document.
	
The server mode requires the \textit{talk2stat} Python package. To install it from the command line, use:
\verb|pip3 install talk2stat|\\
The \textit{talk2stat} source is available from \url{https://pypi.org/project/talk2stat/}.
Note that Python version 3.8.* and up is required.

\section{Usage}
\subsection{Load the package}
\verb|\usepackage[options]{runcode}|\\
\\
Available options are: 
\begin{itemize}
\item  \texttt{julia}: start a \textit{talk2stat} server* for Julia [\url{https://julialang.org/}].
\item  \texttt{matlab}: start a \textit{talk2stat} server* for MatLab [\url{https://www.mathworks.com/products/matlab.html}].
\item  \texttt{R}: start a \textit{talk2stat} server* for R [\url{https://www.r-project.org/}].
\item  \texttt{run}: run source code, and store results in cache files.
\item  \texttt{cache}: use cached results.
\item  \texttt{stopserver}: stop the \textit{talk2stat} server(s) when the pdf compilation is done.
\item  \texttt{nominted}: use the \textit{fvextra} package [\url{https://ctan.org/pkg/fvextra}] instead of the \textit{minted}  package [\url{https://ctan.org/pkg/minted}] to show code (\textit{fvextra} does not require Python's pygments package [\url{https://pygments.org/}], but it does not provide syntax highlights).
\end{itemize}
* Requires the Python package \textit{talk2stat} to be installed.

\subsection{Basic commands}
\verb|\runExtCode{Arg1}{Arg2}{Arg3}[Arg4]| runs an external code. The arguments are:
\begin{itemize}
\item \texttt{Arg1} is the executable program.
\item \texttt{Arg2} is the source file name.
\item \texttt{Arg3} is the output file name (with an empty value, the counter  `codeOutput` is used).
\item \texttt{Arg4} controls whether to run the code. \texttt{Arg4} is optional with three possible values: if skipped or with empty value, the value of the global Boolean variable \texttt{runcode} as determined by the \texttt{run} option when loading the package, is used; if the value is set to `run`, the code will be executed; if set to `cache` (or anything else), use cached results (see more about the cache below).
\end{itemize}

\bigskip
\noindent \verb|\showCode{Arg1}{Arg2}[Arg3][Arg4]| shows the source code, using minted for a pretty layout or fvextra (if pygments is not installed). 
\begin{itemize}
\item \texttt{Arg1} is the programming language.
\item \texttt{Arg2} is the source file name.
\item \texttt{Arg3} is the first line to show (optional with a default value 1).
\item \texttt{Arg4} is the last line to show (optional with a default value of the last line).
\end{itemize}

\bigskip
\noindent \verb|\includeOutput{Arg1}[Arg2]| is used to embed the output from executed code.
\begin{itemize}
\item \texttt{Arg1} is the output file name, and it needs to have the same value as that of \texttt{Arg3} in \verb|\runExtCode|. If an empty value is given to \texttt{Arg1}, the counter `codeOutput` is used. 
\item \texttt{Arg2} is optional and it controls the type of output with a default value `vbox` 
\begin{itemize}
\item \texttt{vbox} (or skipped) = verbatim in a box.
\item \texttt{tex} = pure latex.
\item \texttt{inline} = embed result in text. 
\end{itemize}
\end{itemize}

\bigskip
\noindent \verb|\inln{Arg1}{Arg2}[Arg3]| is designed for simple calculations; it runs one command (or a short batch) and displays the output within the text. 
\begin{itemize}
\item \texttt{Arg1} is the executable program or programming language. 
\item \texttt{Arg2} is the source code. 
\item \texttt{Arg3} is the output type.
\begin{itemize}
\item \texttt{inline} (or skipped or with empty value) = embed result in text.
\item \texttt{vbox} = verbatim in a box.
\end{itemize}
\end{itemize}

\subsection{Language specific shortcuts}
\verb|\runJulia[Arg1]{Arg2}{Arg3}[Arg4]| runs an external Julia code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses \textit{talk2stat}'s Julia server by default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the basic command \verb|\runExtCode|. 
\end{itemize}

\noindent \verb|\inlnJulia[Arg1]{Arg2}[Arg3]| runs Julia source code (\texttt{Arg2}) and displays the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the Julia server by default.
\item \texttt{Arg2} is the Julia source code to run. If the Julia source code is wrapped between \verb|```|  on both sides (as in the markdown grammar), then it will be implemented directly; otherwise the code will be written to a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command \verb|\inln|.
\end{itemize}

\bigskip
\noindent \verb|\runMatLab[Arg1]{Arg2}{Arg3}[Arg4]| runs an external MatLab code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses \textit{talk2stat}'s MatLab server by default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the basic command \verb|\runExtCode|. 
\end{itemize}

\noindent 
\verb|\inlnMatLab[Arg1]{Arg2}[Arg3]| runs MatLab source code (\texttt{Arg2}) and displays the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the MatLab server by default.
\item \texttt{Arg2} is the MatLab source code to run. If the MatLab source code is wrapped between \verb|```| on both sides (as in the markdown grammar), then it will be implemented directly; otherwise the code will be written to a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command \verb|\inln|.
\end{itemize}


\bigskip
\noindent \verb|\runR[Arg1]{Arg2}{Arg3}[Arg4]| runs an external R code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses \textit{talk2stat]}'s R server by default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the basic command \verb|\runExtCode|. 
\end{itemize}

\noindent \verb|\inlnR[Arg1]{Arg2}[Arg3]| runs R source code (\texttt{Arg2}) and displays the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the R server by default.
\item \texttt{Arg2} is the R source code to run. If the R  source code is wrapped between \verb|```| on both sides (as in the markdown grammar), then it will be implemented directly; otherwise the code will be written to a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command  \verb|\inln|.
\end{itemize}


%
\section{Contributing}
We welcome your contributions to this package by opening issues on GitHub and/or making a pull request. We also appreciate more example documents written using \textsf{runcode}.

\end{document}

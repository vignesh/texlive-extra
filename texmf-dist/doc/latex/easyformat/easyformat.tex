% !TeX spellcheck = en_GB

\documentclass[11pt, cm-default]{l3doc}
% 11pt and keep on using the Computer Modern.
% We want to maintain the original look of
% our documentation so away with the coloured
% links and references as wel ↓↓↓
\hypersetup{hidelinks}

\usepackage[british]{babel}
\usepackage{microtype}
\usepackage{easyformat}

\def\labelitemi{--}

\title{\pkg{easyformat}\\[.5em]
		\large _the_ manual for the 2017/06/03 version (v1.4.0)}

\author{Evert Provoost}

\date{}

\begin{document}

	\maketitle
	
	\begin{abstract}
		\pkg{easyformat} is a package that allows the use of~|_| to begin/end _italics_ or __boldface__ and |^| to begin/end ^smallcaps^. As an alternative to the standard \LaTeX\ |\textit{italic}|, |\textbf{bold}| and |\textsc{smallcaps}|.
	\end{abstract}

	\tableofcontents
	\newpage
	
	%% NOT NEEDED.
	% Insert a blank page.
	%\null % The page has to contain 'something'.
	%\thispagestyle{empty} % Hide the pagenumber.
	%\addtocounter{page}{-1} % So the next doesn't turn 3.
	%\newpage
	
	\section{Introduction}
	Usually when you want to make something _italic_ or __bold__ in \LaTeX\ you insert |\textit{italic}| or |\textbf{bold}| respectively.
	
	However when writing a book or another text where this formatting is common, this quickly becomes annoying to type.

	\begin{quote}
	__Quick disclaimer:__\\
	I'm not saying that the \LaTeX\ way is bad. I just want to explain that it's not ideal under all circumstances.
	\end{quote}
	
	Simple markuplanguages (eg. Markdown) have a more elegant solution.	
	If you, for example, wanted to write:
	\begin{quote}
		__Stop!__ You _have_ to wait!
	\end{quote}

	In Markdown, you'd type:
	\begin{quote}
		|**Stop!** You *have* to wait!|
	\end{quote}

	As you can see it's a matter of _wanting_ italic or bold formatting, adding~*'s where needed and done; no (long) macros to type.
	
	With the help of \pkg{easyformat} we can do something similar, here we get the same result with:
	\begin{quote}
		|__Stop!__ You _have_ to wait!|
	\end{quote}
	
	(Why underscores? See: \ref{Why?!})\\
	
	And since it is good practice to add abbreviations using ^smallcaps^, \pkg{easyformat} gives you |^smallcaps^|.
	
	So:
	\begin{quote}
		|^nasa^ and ^esa^ are probably the best known space agencies.|
	\end{quote}

	Results in:
	\begin{quote}
		^nasa^ and ^esa^ are probably the best known space agencies.
	\end{quote}
	
	\section{Usage, syntax and examples}
	Like usual you import the package with: |\usepackage{easyformat}|

	\pkg{easyformat} makes |_| an active character, however it was already `special' before so you still have to type~|\_| if you want to insert an underscore.
	
	The same can be said of |^|, however since this one is difficult to add in ordinary \LaTeX, the \pkg{easyformat} package gives you |\cir| to get \cir.
	
	\newpage

	As already said, \pkg{easyformat} uses a Markdown-like syntax, so:
	
	\begin{quote}
		|_italic,_ __bold,__ ___bolditalic___ and ^smallcaps!^|
	\end{quote}

	Gives:
	\begin{quote}
		_italic,_ __bold,__ ___bolditalic___ and ^smallcaps!^
	\end{quote}

	However we can also do more complex stuff, eg.:
	\begin{quote}
		|_Lorem __ipsum_ totalem__ ^da^ __givea _zin_ doram__|\\
		|_zet_tim, liefkan.|
	\end{quote}
	
	Becomes:
	\begin{quote}
		_Lorem __ipsum_ totalem__ ^da^ __givea _zin_ doram__ _zet_tim, liefkan.
	\end{quote}

	Those with a good eye might have noticed that \pkg{easyformat} adds italics correction when needed. Just like |\textit{zet}tim| would have.
	
	\section{Troubleshooting}
	\pkg{easyformat} tends to conflict with the loading of other packages, therefore you should probably load \pkg{easyformat} last.
	
	\begin{quotation}
		``For some or other reason I get some weird formatting\dots''
	\end{quotation}

	It could be a bug---in which case I'd love to hear from you (see \ref{contact})---however, you probably forgot to close your formatting somewhere and because of how \pkg{easyformat} works you can get weird results.

	%__Explanation:__ when you start eg. _italics_ with |_|, \pkg{easyformat} remembers the shape (the same is true for ^smallcaps^, when you start __boldface__ it remembers the series) of the font. When you get out of _italics_, it resets the shape (respectively the series) to what it was before the starting the formatting.


	%% This is solved in version 2017/04/09 v1.1.0
	%\subsection{Underscore in subtitles\label{undtitles}}
	%If you ever try something similar to |\section{__Vectors:__ $\vec{F}_g$}|: \TeX\ will shout at you. Why? I don't really know. However I do know how to fix this. If we want to get the previous example we would use:
	
	%\begin{quote}
	%	|\setundsub|\\
	%	% Yeah there's a glitch were the first is indented slightly more
	%	% unless there is something before the next lines......
	%	\hspace*{0pt}|\section{$\textbf{Vectors:} \vec{F}_g$}|\\
	%	\hspace*{0pt}|\setundact|
	%\end{quote}
	
	%This first resets the |_| character so it can only be used as the start of subscripts; then adds the subtitle and afterwards re-initiates |_| for __bold__ and _italic_. (Obviously the \pkg{easyformat}-syntax does not work _in_ the subtitle, so you'll have to use the \LaTeX-commands.)
	
	\newpage
	
	\section{Macros}
	\subsection{Special characters}
	\begin{function}{\cir}
		\begin{syntax}
			\cs{cir}
		\end{syntax}
		Insert a circumflex (\cir) character.
	\end{function}

	%%\subsection{Fonts}
	% Removed in v1.4.0
	%\begin{function}{\nrfamily}
	%	\begin{syntax}
	%		\cs{nrfamily}
	%	\end{syntax}
	%	Reverts the font family to the default.
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\nrshape}
	%	\begin{syntax}
	%		\cs{nrshape}
	%	\end{syntax}
	%	Reverts the font shape to the default.
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\nrseries}
	%	\begin{syntax}
	%		\cs{nrseries}
	%	\end{syntax}
	%	Reverts the font series to the default.
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\setffamily}
	%	\begin{syntax}
	%		\cs{setffamily} \Arg{font family}
	%	\end{syntax}
	%	Quickly change the font family.
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\setfshape}
	%	\begin{syntax}
	%		\cs{setfshape} \Arg{font shape}
	%	\end{syntax}
	%	Quickly change the font shape.
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\setfseries}
	%	\begin{syntax}
	%		\cs{setfseries} \Arg{font series}
	%	\end{syntax}
	%	Quickly change the font series.
	%\end{function}
	
	\subsection{\pkg{easyformat}-syntax}
	\begin{function}{\enableeasyformat}
		\begin{syntax}
			\cs{enableeasyformat}
		\end{syntax}
		Enables the \pkg{easyformat}-syntax.
	\end{function}
	
	\begin{function}{\disableeasyformat}
		\begin{syntax}
			\cs{disableeasyformat}
		\end{syntax}
		Disables the \pkg{easyformat}-syntax.
	\end{function}
	
	% Removed in v1.4.0
	%\begin{function}{\setciract}
	%	\begin{syntax}
	%		\cs{setciract}
	%	\end{syntax}
	%	Sets the catcode of |^| to 13 (active).
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\setcirsup}
	%	\begin{syntax}
	%		\cs{setcirsup}
	%	\end{syntax}
	%	Sets the catcode of |^| to 7 (superscript).
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\setundact}
	%	\begin{syntax}
	%		\cs{setundact}
	%	\end{syntax}
	%	Sets the catcode of |_| to 13 (active).
	%\end{function}

	% Removed in v1.4.0
	%\begin{function}{\setundsub}
	%	\begin{syntax}
	%		\cs{setundsub}
	%	\end{syntax}
	%	Sets the catcode of |_| to 8 (subscript).
	%\end{function}

	\section{Technical details}
	\subsection{Why \texttt{\_} and not *?\label{Why?!}}
	To make * work we would have to make it an active character, however this would break things like |\section*{Art}|. The~|_| is already protected by \TeX\ because of it's meaning in mathmode. However, it has little to no use in textmode. This is why we can use~|_| for this purpose and not~*.
	
	\subsection{Does this break mathmode?}
	No it doesn't. |_| and |^| keep behaving like before in mathmode, their meaning only changes in textmode. (We first check whether we are in mathmode or not and then behave accordingly.)

	\section{Contact\label{contact}}
	If you want to make suggestions or have any questions whose answer could be included in a future version of this document, you can email to this address: \url{mailto:evert.provoost@gmail.com}
	\newpage
	
	\section{Changelog}
	We only include important changes from v1.0.0 onwards, since earlier versions barely worked.

	\subsection*{2017/06/03 v1.4.0}
	Improved code readability.
	We now fully use \pkg{expl3}, which eliminates most of the possible future issues with \LaTeX3.
	Fixed a kerning issue with |\cir| in mathmode.
	Removed: |\setundact|, |\setundsub|, |\setciract|, |\setcirsup|, |\nrfamily|, |\nrshape|, |\nrseries|, |\setffamily|, |\setfshape| and |\setfseries| as these do not add any value to the package.

	\subsection*{2017/05/28 v1.3.0}
	Removed forced re-enabling of the \pkg{easyformat}-syntax at the start of the document.
	Rewritten to use \pkg{expl3}, this makes the code shorter and a future move to \LaTeX3 easier.
	Fixed an error that could occur with |\cir|.
	\pkg{easyformat} now also behaves correctly when a fontpackage is loaded.

	\subsection*{2017/04/17 v1.2.0}
	Improved handling of mixed styles.
	Added |^smallcaps^| for ^smallcaps^.

	\subsection*{2017/04/09 v1.1.0}
	Made usage of syntax in |\section{}|, and similar macros, possible. Changed the behaviour so it works more like |\emph{}| (this also simplifies the code).

	\subsection*{2017/04/07 v1.0.0}
	First stable version.

	% Insert a friendly message :)
	\vspace*{\fill}\noindent
	Made with $\heartsuit$ in Berlaar, Belgium.
	\clearpage

\end{document}

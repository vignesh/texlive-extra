 Copyright 2016 Simon M. Laube

 This work may be distributed and/or modified under the
 conditions of the LaTeX Project Public License, either version 1.3
 of this license or (at your option) any later version.
 The latest version of this license is in
   http://www.latex-project.org/lppl.txt
 and version 1.3 or later is part of all distributions of LaTeX
 version 2005/12/01 or later.

 This work has the LPPL maintenance status `author maintained'.

 The Current Maintainer of this work is S. M. Laube

 The shdoc package provides a simple, though fancy, float
 environment that can be used to document operations or command
 executions of a terminal session on a shell.

 The current version of the package is v2.1b.

 To create the package (*.sty) from source open a command line prompt,
 change to the package directory and type

     tex shdoc.ins

 Afterwards you can generate the package documentation by typing

     (pdf)latex shdoc.dtx

 Be aware, that the shdoc package is using Unix/Linux specific shell
 commands within the documentation. Thus, you may not be able to build
 the documentation on your system. For security reasons, make sure that
 you do not execute unknown shell scripts -- that are distributed with this
 or other packages -- blindly. Further, you will need to enable `write18'
 to build the documentation successfully.



// ce: combine
/**
 * HelloWorld.java
// be: the class
public class HelloWorld
{
// ee: the class
    // be: specific constructor
    public HelloWorld()
    {
        System.out.println("Object HelloWorld created");
    } // end of specific constructor "HelloWorld()"
    // ee: specific constructor

    // be: main method
    public static final void main(final String[] args)
    {
        HelloWorld h = new HelloWorld();
    } // end of method "main(String[] args)"
    // ee: main method
// be: the class
} // end of class "HelloWorld"
// ee: the class

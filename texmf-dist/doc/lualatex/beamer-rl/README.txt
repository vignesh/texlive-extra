The beamer-rl class
v1.4a

This class provides patchs of some beamer templates and commands 
for right to left presentation, the class require babel with lualatex engine  
If you want to report any bugs or typos and corrections in the
documentation  then
please use the issue tracker:

  <https://github.com/seloumi/beamer-rl/issues>

Current version release date: 2020/08/21

___________________
Salim Bou

Copyright (c) 2019-2020
It may be distributed and/or modified under the LaTeX Project Public License,
version 1.3c or higher (your choice). The latest version of
this license is at: http://www.latex-project.org/lppl.txt

This work is author-maintained (as per LPPL maintenance status)
by Salim Bou.
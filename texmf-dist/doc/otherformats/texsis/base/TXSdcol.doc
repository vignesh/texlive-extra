%% file: TXSdcol.doc  - Double Columns - TeXsis version 2.18
%% @(#) $Id: TXSdcol.doc,v 18.2 2000/05/17 00:19:46 myers Exp $
%======================================================================*
% (C) Copyright 1989, 1992, 1993 by Eric Myers and Frank E. Paige
% This file is a part of TeXsis.  Distribution and/or modifications
% are allowed under the terms of the LaTeX Project Public License (LPPL).
% See the file COPYING or ftp://ftp.texsis.org/texsis/LPPL
%======================================================================*
\ifx\undefined\bs \texsis\input TXSdocM.doc\input Manual.aux\draft\fi


\section{Double Column Output           \label{sect.dcol}}

   Typesetting text in more than one column on a single page is more
involved than typesetting text in a single page-wide column, but this
extra effort can be rewarded with a document which looks much more
professional and which can be easier to read (provided that the
resolution of your printer is up to it).  Many publishers of
``\idx{camera-ready}'' conference proceedings require double
column\index{double columns} \index{multiple columns|see{double
columns}} typesetting, and the {\sl \idx{Physical Review}} and other APS
and AIP journals are typeset in double columns.  In order to make it
easy for authors to prepare manuscripts either for such camera-ready
conference proceedings or for submission to {\sl Physical Review}, we
have included some powerful double column typesetting macros in \TeXsis.
These macros can automatically balance columns which do not fill an
entire page, and there are also provisions for equations which must be
extended across both columns because they are too wide for a single
column.  Figures and tables (or other such ``floating'' insertions) can
be put either in a single column or across both columns.  The same is
true for footnotes.  By changing a single instruction in the manuscript
file it is also possible to take a document which has been prepared for
double columns and to print it in single column mode.  The converse is
not true --- typesetting text in double columns with automatic column
balancing is a nontrivial task.  While the macros generally behave as
intended, more attention to the layout of the document is required, and
even then the macros will sometimes fail without some extra guidance.
It is not possible to divide three lines into two balanced columns!  In
such cases you must rearrange text or the placement of figures and
tables, or explicitly select column or page breaks elsewhere in the
document, in order to give \TeXsis\ some extra guidance on how best to
format the page.

%\bigbreak

  In order to use the double column formatting macros you must first
specify the size of the columns with the command
\CS{SetDoubleColumns}\arg{c-width}.  The width and height of the full
page are taken from the current settings of \cs{hsize} and \cs{vsize},
while the width of each column is \meta{c-width}.  This re-defines
\TeX's \cs{output} routine, so it should be called shortly after you say
\cs{texsis}, before any output is actually produced, and it should only
be called if double column output is to be produced. 

  \CS{SetDoubleColumns} only sets the column dimensions and
prepares the \cs{output} routine for double column output at some point
in the future --- it does not actually put you in double column mode.
Text will still be set in a single page-wide column until you say
\CS{doublecolumns}.  After you say \CS{doublecolumns} text will be set
in double columns until you say \CS{enddoublecolumns}.  This will cause
the text collected so far to be put into two ``balanced'' columns of the
same height.  You can, if you desire, return to double column mode at
any later time by saying \CS{doublecolumns} again, and you can switch
back and forth as much as you like.  However, keep in mind that
frequently switching between single and double columns will make it very
difficult for \TeXsis\ to choose good points to break columns and pages,
and you may get an error message warning you that a column cannot be
balanced.

%\bigbreak
%\pagecheck{3cm}

A simple example of the use of these double column commands is the
following: 
\TeXexample
\SetDoubleColumns{0.47\hsize}
\doublecolumns\tenpoint
{\bf BISMUTH} (Ger.\ {\it Weisse Masse}, white mass; later
{\it Wismuth} and {\it Bisemutum}, Bi; at.~wt. 308.981;
at.~no. 83; m.p.  271.3$^\circ$C; b.p. 1560 $\pm$ 5$^\circ$C;
sp.~gr. 9.747 (20$^\circ$); valence 3 or~5.  In early times
bismuth was confused with tin and lead.  Claude Geoffroy the 
Younger showed it to be distinct from lead . . .
     . . . . . . . 
. . .   High purity bismuth metal costs about \$4/lb.
\enddoublecolumns
|endTeXexample
\noindent
Here the column size is chosen to be slightly less than half of the
\cs{hsize} (0.47 times the \cs{hsize}) to allow some space (called the
``gutter'') between the two columns.  The result is the
following:\reference{CRC}
\booktitle{Handbook of Chemistry and Physics}, 56th Edition,
(CRC Press, Cleveland, Ohio, 1974), pg. B-9
\endreference
\medbreak

% set up double column mode:
\SetDoubleColumns{0.47\hsize}

\doublecolumns\tenpoint\baselineskip=12pt plus 1pt \relax
{\bf BISMUTH} (Ger.\ {\it Weisse Masse}, white mass; later {\it Wismuth}
and {\it Bisemutum}, Bi; at.~wt. 308.981; at.~no. 83; m.p.
271.3$^\circ$C; b.p. 1560 $\pm$ 5$^\circ$C; sp.~gr. 9.747 (20$^\circ$);
valence 3 or~5.  In early times bismuth was confused with tin and lead.
Claude Geoffroy\index{Geoffroy, Claude} the Younger showed it to be
distinct from lead in 1753.  It is a white, crystalline, brittle metal
with a pinkish tinge.  It occurs native.  The most important ores are
{\it bismuthinite} or {\it bismuth glance} (Bi$_2$S$_3$) and {\it
bismite} (Bi$_2$O$_3$).  Peru, Japan, Mexico, Bolivia, and Canada are
major \idx{bismuth} producers.  Much of the bismuth produced in the U.S. is
obtained as a by-product in refining lead, copper, tin, silver, and
gold ores.  Bismuth is the most diamagnetic of all metals, and the
thermal conductivity is lower than any metal, except mercury.  It has a
high electrical resistance, and has the highest \idx{Hall effect} of any
metal (i.e., greatest increase in electrical resistance when placed in
a magnetic field). ``Bismanol'' is a permanent magnet of high coercive
force, made of MnBi, by the U.S. Naval Ordnance Laboratory.  Bismuth
expands 3.32\% on solidification.  This property makes bismuth alloys
particularly suited to the making of sharp castings of objects subject
to damage by high temperatures.  With other metals, such as tin,
\idx{cadmium}, etc., bismuth forms low-melting alloys which are extensively
used for safety devices used in \idx{fire detection} and extinguishing
systems. Bismuth is used in producing malleable irons and is finding use
as a catalyst for making acrylic fibers.  When bismuth is heated in air
it burns with a blue flame forming yellow fumes of the oxide.  The metal
is also used as a \idx{thermocouple} material (has highest negativity
known), and has found application as a carrier for U$^{235}$ or
U$^{238}$ fuel in \idx{atomic reactors}.  Its soluble salts are
characterized by forming insoluble basic salts on the addition of water,
a property sometimes used in detection work.  Bismuth oxychloride is
used extensively in cosmetics.  Bismuth subnitrate and subcarbonate are
used in medicine.  High purity bismuth metal costs about \$4/lb.
\enddoublecolumns

\nobreak
\medskip

  The \cs{IEEE}, \cs{PhysRev}, and \cs{NorthHollandTwo} document formats
described in \Sect{fmts} also use these double column macros.

\bigbreak

  Any document prepared for double column output using the macros just
described can also be printed in the simpler single column mode very
easily, simply by leaving out the \CS{SetDoubleColumns}.  Without the
initialization provided by \CS{SetDoubleColumns} the \cs{doublecolumns}
and \cs{enddoublecolumns} commands do nothing (they are, in fact, the
same as saying \cs{relax}).   However, if you restore the
\CS{SetDoubleColumns} command, or invoke one of the specialized document
styles which use double column output (like \cs{PhysRev}) they will come
back to life and perform as required.  

\bigbreak

   In double column mode displayed equations that fit within a
single column are typed within the usual |$$|\ttdots|$$|.  If an equation
is too long then it may have to be printed across both columns.  This is
easily done by saying \CS{longequation} before the |$$| which begins the
equation and \CS{endlongequation} after the |$$| which ends the
equation, like so:\index{double columns!long equations}%
\TeXexample
|smalltt\longequation
$$ 
\psi_{nlm}(r,\theta,\phi) 
        = [({2Z \over n a_0})^3 {(n-l-1)! \over 2n(n+l)!} ]^{1 \over 2}
           \, (\rho)^l \, e^{-\rho / 2} \, \,
        L_{n-l-1}^{2l+1} (\rho)  \,\,  Y_l^m(\theta,\phi)
$$
\endlongequation
|endTeXexample
\noindent 
The command \CS{longequation} invokes \cs{enddoublecolumns} to end
double column mode and balance the partial double columns, and it will
also draw a ruled line across the bottom of the left column.  After the
equation has been completed \CS{endlongequation} invokes
\cs{doublecolumns} to go back to double column mode, and it also draws a
rule across the top of the right column. 

   The purpose of these ruled lines is to guide the reader's eye from the
incomplete left column to the right column above the equation, and to
the top of the incomplete right column from the left column below the
long equation.  The default style of these rules is the same as is used
by {\sl Physical Review}.   The rules are drawn by macros called
\CS{leftcolrule} and \CS{rightcolrule}, and these can be changed either
by you or by a style file.  For
example, if you don't want these rules you can turn them off simply
by redefining these macros to be \cs{relax}:
\TeXexample
        \def\leftcolrule{\relax}
        \def\rightcolrule{\relax}
|endTeXexample
\noindent
As with the other double column macros, \CS{longequation} and
\CS{endlongequation} do nothing if they are invoked when double column
mode has not been initialized with \CS{SetDoubleColumns}, to make it
easy to print the document in single column mode.

   Keep in mind that when dealing with long equations which span both
columns, switching frequently between single and double columns will
almost certainly cause trouble in balancing the double columns.  Also
note that each separate piece of double-column material forms its own
group, so any definitions made inside that group will be forgotten
outside it. To avoid this problem put all definitions at the beginning of the
paper, before double column mode is begun.  Alternatively, make your
definitions global by using \cs{gdef} rather than \cs{def}.

\bigbreak

   Figures, tables, or other floating insertions can be inserted
within a single column using the usual insertion commands of \TeX\ and
\TeXsis:\index{double columns!insertions} \cs{topinsert}, \cs{midinsert},
\cs{pageinsert}, \cs{bottominsert}, or \cs{heavyinsert}, all of which
are terminated as usual by \cs{endinsert}.  A \cs{midinsert} or
\cs{heavyinsert} will appear in the column where it is called for,
unless it won't fit, in which case it migrates to the next {\sl
column} (rather than all the way to the next page).  You can force an
insertion into a particular column by placing {\sl within} the
insertion either \CS{forceleft} or \CS{forceright}.  Since insertions
which have been forced into the left or right columns are handled
separately, an insertion which fails to fit on a page is moved to the
same column of the next page and not just to the next column. This means
that if there are several figures per page, then the figures can
easily appear in the wrong order. Also, a \cs{topinsert} which follows
a wide equation on a page will be placed after the equation, not at
the top of the page. Such problems can easily be corrected by moving
the insertion to a better place in the manuscript file.


Besides putting insertions in one column or the other, it is also
sometimes desirable to be able to put an insertion across the full width
of the page.  These ``wide'' insertions can be made with ``wide''
versions of some of the the insertions already mentioned:
\CS{widetopinsert}, \CS{widepageinsert} and \CS{widebottominsert}
(there is no such thing as |\widemidinsert| or |\wideheavyinsert|).  These
wide insertions behave just like their more conventional counterparts 
when in single column mode.

       The standard \TeXsis\ macros for table and figure insertions
described in \Sect{tbls} use \cs{topinsert} and cousins, and so make
insertions within a single column in double column format.
There are also macros to make two-column wide tables and figures:
\index{double columns!figures and tables}%
\description{Xwidetable}\clump
%
\itm{\CS{widetable}\arg{label\/}}
Insert a two-column wide table, with the tag \meta{label\/}, at the top of the
current page.
%
\itm{\CS{widefigure}\arg{label\/}}
Insert a two-column wide figure, with the tag \meta{label\/}, at the top of the
current page.
%
\itm{\CS{widetopfigure}\arg{label\/}}
A synonym for |\widefigure|.
%
\itm{\CS{widefulltable}\arg{label\/}}
Insert a two-column wide, full-page table, with the tag \meta{label\/}.
%
\itm{\CS{widefullfigure}\arg{label\/}}
Insert a two-column wide, full-page figure, with the tag \meta{label\/}.
%
\enddescription
\noindent 
These also behave correctly in single column document formats.

\bigbreak
%================

Here is a summary of the commands which can be used to set-up and
control double column mode:
\description{Xdoublecolumns\quad}\clump
%
\itm{\CS{SetDoubleColumns}\arg{c-width}}
Set up the double column format, using the current \cs{hsize} and
\cs{vsize} for the total size of the page  and \meta{c-width} for the
column width.
%
\itm{\CS{doublecolumns}}
Begin double column mode.
%
\itm{\CS{newcolumn}}
Force a \idx{column break}, much like \cs{newpage} forces a page break.
%
\itm{\CS{enddoublecolumns}}
End double column mode, making balanced double columns,
%
\itm{\CS{longequation}}
Begins a ``long'' equation which cannot fit within one column
and therefore must stretch across both columns.  This balances the
columns, putting a \CS{leftcolrule} below the left column, and then
enters single column mode.
%
\itm{\CS{endlongequation}}
Ends a long equation, returning to double column mode and
putting a \CS{rightcolrule} above the right column of the text following.
%
\enddescription

%>>> EOF TXSdcol.doc <<<

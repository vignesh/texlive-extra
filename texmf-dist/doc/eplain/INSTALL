Installation
************

Your TeX installation should already contain a version of Eplain
(`eplain.tex') in its main `texmf' tree (usually under
`/usr/share/texmf/tex/eplain/' on Unix systems).  To install a newer
version of Eplain, put the new `eplain.tex' (included in Eplain
distributions) in the `tex/eplain/' subdirectory of your local `texmf'
tree.  The newer version you install in the local tree should override
the older one in the main tree.

   The location of the local `texmf' tree obviously depends on your
operating system and TeX installation.  On Unix systems the usual
location is `/usr/local/share/texmf/'.  If you don't have write
permissions for `/usr/local/share/texmf/', many installations read the
`texmf' tree in the user's home directory; `eplain.tex' then should go
under `~/texmf/tex/eplain/'.  For more information about TeX directory
structure, please see
`http://www.tex.ac.uk/cgi-bin/texfaq2html?label=tds'.

   If you prefer to install `eplain.tex' in a non-standard place, set
an environment variable (`TEXINPUTS' for the Web2C port of TeX to Unix)
to tell TeX how to find it.

   If you want, you can also create a format (`.fmt') file for Eplain,
which will eliminate the time spent reading the macro source file with
`\input'.  You do this by issuing a sequence of Unix commands something
like this:

     prompt$ touch eplain.aux
     prompt$ initex
     This is TeX, ...
     **&plain eplain
     (eplain.tex)
     *\dump
     ... MESSAGES ...

You must make sure that `eplain.aux' exists _before_ you run `initex';
otherwise, warning messages about undefined labels will never be issued.

   You then have to install the resulting `eplain.fmt' in your local
`texmf' tree or set an environment variable to tell TeX how to find it.
For the Web2C port of TeX to Unix, format files are usually installed
under `/usr/local/share/texmf/web2c/' or `/var/lib/texmf/web2c/'; the
environment variable is `TEXFORMATS'.


<!--
Filename:       README.md
Author:         Shiro Takeda
e-mail          <shiro.takeda@gmail.com>
First-written:  <2017-07-30>
-->

econ.bst
==============================

`econ.bst` is a BibTeX style file for economics. It provids the following
features:

* The author-year type citation (you need `natbib.sty` as well).
* Reference style used in economics papers (`econ.bst` is not a BibTeX style for a specific journal).
* Highly customizable.  You can easily customize reference style as you wish.
* You can use "certified random order" proposed by [Ray ⓡ Robson (2018), AER](http://dx.doi.org/10.1257/aer.20161492).


## Explanation of files

| File                                   | Explanation                                |
|:---------------------------------------|:-------------------------------------------|
| `econ.bst`                             | This is the main bst file.                 |
| `econ-example.tex`                     | This file explains how to use econ.bst.    |
| [`econ-example.pdf`](econ-example.pdf) | A PDF file created fom `econ-example.tex`. |
| `econ-example.bib`                     | This is a bibliography database file.      |
| [`customization`](customization)       | This folder contains customized bst files. |
| [`CHANGES.md`](CHANGES.md)             | Changelog file.                            |
| `README.md`                            | This file.                                 |

| bst file                | Explanation                                               |
|:------------------------|:----------------------------------------------------------|
| `econ-a.bst`            | A simple style.                                           |
| `econ-b.bst`            | A style with much decoration.                             |
| `econ-no-sort.bst`      | This style lists entries in citation order.               |
| `econ-abbr.bst`         | This style uses abbreviated journal name.                 |
| `econ-aea.bst`          | The style for AEA journals such as AER, JEL, AEJ and JEP. |
| `econ-econometrica.bst` | The style for Econometrica.                               |
| `econ-jpe.bst`          | The style for JPE (Journal of Poilitical Economy).        |
| `econ-jet.bst`          | The style for JET (Journal of Economic Theoriy).          |
| `econ-jie.bst`          | The style for JIE (Journal of International Economics).   |
| `econ-old.bst`          | The style of the old econ.bst.                            |

<!-- リンクの作成方法 -->
<!-- [リンクテキスト](URLを記入) -->

<!--
--------------------
Local Variables:
mode: markdown
fill-column: 80
coding: utf-8-dos
End:
-->



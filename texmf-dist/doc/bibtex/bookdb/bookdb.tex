% bookdb.tex  User manual for the bookdb.bst package
\begin{filecontents}{bookdb.bib}

@misc{DALY,
  author = {Patrick W. Daly},
  title = {Customizing Bibliograhic Style Files},
  note = "\url{http://mirror.ctan.org/macros/latex/contrib/custom-bib/makebst.pdf}",
  month = aug,
  year = 2003
}

@misc{NATBIB,
  author = {Patrick W. Daly},
  title = {Natural Sciences Citations and References},
  note = "\url{http://mirror.ctan.org/macros/latex/contrib/natbib/natbib.pdf}",
  month = sep,
  year = 2010
}

@misc{LEHMAN,
  author = {Philipp Lehman},
  title = {The biblatex package},
  note = "\url{http://mirror.ctan.org/macros/latex/contrib/biblatex/doc/biblatex.pdf}",
  month = jul,
  year = 2009
}

@misc{MARKEY,
  author = {Nicolas Markey},
  title = {Tame the BeaST},
  note = "\url{http://mirror.ctan.org/info/bibtex/tamethebeast/ttb_en.pdf}",
  month = oct,
  year = 2009
}

@misc{PATASHNIK,
  author = {Oren Patashnik},
  title = {Designing BibTeX Styles},
  note = "\url{http://mirror.ctan.org/biblio/bibtex/base/btxhak.pdf}",
  month = feb,
  year = 1988
}

\end{filecontents}

\documentclass{article}
\fontencoding{T1}
\usepackage{booktabs}
%\usepackage{natbib}

\bibliographystyle{plain}
\usepackage{url}
\usepackage{alltt}
\usepackage{lmodern}
\usepackage{comment}

\addtolength{\topmargin}{-1in}
\addtolength{\textheight}{2in}
\addtolength{\oddsidemargin}{-0.75in}
\addtolength{\textwidth}{1.5in}
\addtolength{\evensidemargin}{-0.75in}

\newenvironment{ttdesc}[1]
  {\begin{list}{}%
    {\renewcommand{\makelabel}[1]{\texttt{##1:}\hfil}%
     \settowidth{\labelwidth}{\makelabel{#1}}%
     \setlength{\leftmargin}{\labelwidth}%
     \addtolength{\leftmargin}{\labelsep}}}%
  {\end{list}}

\newcommand{\BibTeX}{\texttt{BibTeX}}
\newcommand{\file}[1]{\texttt{#1}}
\newcommand{\field}[1]{\texttt{#1}}
\newcommand{\pack}[1]{\texttt{#1}}

\title{\texttt{bookdb.bst} --- A Personal Book Catalogue}
\author{Peter Wilson}
\date{4 June 2015}

\begin{document}
\maketitle

\section{Introduction}

For many years I have been collecting books. They are located in five rooms, as well as 
two bookcases in my printing and binding workshop. But recently I found that I was buying
books that I already had. I decided that the best way to save money was to catalogue all 
the books that I owned and where they were kept. 

I searched on the web for free database programs that would be appropriate. There were only 
a few that would run under Linux, which is the operating system I am most comfortable with.
Of those, some I could not install, and the one that I could I couldn't get to work
for me.

I contacted several booksellers that I
dealt with and asked them what they used for catalogueing their stock. They all replied
but most used proprietry and expensive software that included things like preparing 
invoices that were
irrelevant as far as I was concerned. The two responses that grabbed my attention were 
`use a card index' (but I needed space for books, not card indexes) and `try Bibtex', 
which immediately appealed as I had used it for many years; why I hadn't thought of it myself
I'll never know.

\section{Usage}

The standard \BibTeX\ enries did not meet my needs so I looked at 
\pack{biblatex}~\cite{LEHMAN}
but its entries also didn't match my requirements so I decided to tweak \BibTeX.
To that end I used Patrick Daly's \texttt{makebst} program~\cite{DALY} 
for generating a \file{*.bst}
file that went someway towards meeting my needs. This required some hand-coded additions
later; I read the articles by Oren Patashnik~\cite{PATASHNIK}, the creator of \BibTeX, 
and Nicolas Markey~\cite{MARKEY} which helped me on my way. 
In the end I had a \BibTeX\ file called \file{bookdb.bst} that included all the
regular entries but a greatly expanded \texttt{book} entry, as follows:

\vspace{0.5\baselineskip}
\noindent \begin{tabular}{lp{0.9\textwidth}} \toprule
\texttt{book} & A book with a publisher \\
 & \textit{Required:} \texttt{author} or {editor}, \texttt{title, publisher, year} \\
 & \textit{Optional:} \texttt{volume} or \texttt{number, series, address, edition,
   month, note, collator, foreword, preface, introduction, volumes, pages, illustrations, 
   binding, size, condition, copy, location, category, value} \\ \bottomrule
\end{tabular}
\vspace{0.2\baselineskip}

And also I created a new entry called \texttt{heading}, as follows:

\vspace{0.5\baselineskip}
\noindent \begin{tabular}{lp{0.87\textwidth}} \toprule
\texttt{heading} & A heading in the bibliography \\
 & \textit{Required:} \texttt{key} \\
 & \textit{Optional:} \texttt{note} \\ \bottomrule
\end{tabular}
\vspace{0.5\baselineskip}

The additional fields in the \texttt{book} entry are:
\begin{ttdesc}{condition}
\item[binding] Information about the book's binding. Output as: \\
  Binding: `binding.'
\item[category] The general theme of the book. Output as: \\
  Category: `\textbf{category.}'
\item[collator] The name(s) of those who collated the book contents. Output as: \\
  `collator' (collator(s))
\item[condition] The book's condition. Output as: \\
  Condition: `condition.'
\item[copy] For a limited edition, the particular copy. Output as: \\
  Copy: `copy.'
\item[foreword] The name(s) of the author(s) of the Foreword, if not written by the
  author(s) of the main text. Output as: \\
  foreword by `foreword'
\item[illustrations] \mbox{} \\ Information about the number and kind of any illustrations and
                              possibly who created them. Output as: \\
  Illustrations: `illustrations.'
\item[introduction] \mbox{} \\ The name(s) of the author(s) of the book's Introduction, if not
  written by the author(s) of the main text. Output as: \\
  introduction by `introduction'
\item[location] Where the book is located. Output as: \\
  Location: `location.'
\item[pages] The total number of pages. Output as: \\
  `pages' pp.,
\item[preface] The name(s) of the author(s) of the Preface, if not written by the
  author(s) of the main text. Output as: \\
  preface by `preface'
\item[size] The book's physical dimensions. Output as: \\
  Size: `size.'
\item[value] The book's value. Output as: \\
  Value: `\textbf{value}.'
\item[volumes] The number of volumes. Output as: \\
  Volumes: `volumes.'
\end{ttdesc}

I use the \texttt{heading} entry for putting a heading or division marker into
a bibliography. The \field{key} is required so that the \texttt{heading} is sorted
into the correct position in the bibliography (normally sorting is based on the author 
or editor). The contents of the \field{note}
form the printed heading. For instance if you wanted a heading before each alphabetical
group of authors you could do something like:
\begin{verbatim}
@heading{A1,
  key = {A1},
  note = {\ahead{AAAAAAAA...}}
}
@heading{B1,
  key ={B1},
  note = {\ahead{BBBBBBBB...}}
}
etc
\end{verbatim}
where \verb!\ahead! might be defined as:
\begin{verbatim}
\providecommand{\ahead}[1]{%
  \textbf{\large #1}}
\end{verbatim}

To help clarify matters Figure~\ref{fig:data} shows a possible entry in a 
\file{*.bib} file. The output after processing in a document using 
\file{bookdb.bst} is illustrated in Figure~\ref{fig:result}.

%\noindent
\begin{figure}
\centering
\begin{minipage}{0.75\columnwidth}
\rule{0.75\columnwidth}{0.4pt}
\begin{verbatim}
@book{ABOOK,
  author = {A. N. Author and A. Nother},
  title = {A Book Entry},
  editor = {Smith and Jones},
  collator = {Jane and Tim},
  translator = {Jo and Mary},
  foreword = {Alpha},
  preface = {Zoe},
  introduction = {Bloggs and Friend},
  volume = 7,
  publisher = {Herries Press},
  year = 2020,
  pages = {xii + 278 + vi},
  edition = {Third},
  isbn = {0-201-36299-8},
  volumes = 9,
  illustrations = {11 wood engravings},
  binding = {full red leather},
  size = {11 by 17 inches},
  note = {This is a note},
  condition = {Hot off the press},
  copy = {23 of 125},
  location = {my study},
  category = {private press},
  value = {\$270}
}
\end{verbatim}
\rule{0.75\columnwidth}{0.4pt}
\end{minipage}
\caption{An example entry for \texttt{bookdb} processing} \label{fig:data}
\end{figure} 

\begin{figure}
\rule{\textwidth}{0.4pt} \par
\hangindent=1.5em\hangafter=1
\noindent A. N. Author and A. Nother. \textit{A Book Entry}, Smith and Jones (eds.),
  Jane and Tim (collators), Jo and Mary (translators), foreword by Alpha,
  preface by Zoe, introduction by
  Bloggs and Friend, volume 7 (Herries Press, 2020), xii + 278 + vi pp.,
  third edition. ISBN 0-201-36299-8. Volumes: 9. Illustrations: 11 wood 
  engravings. Binding: full red leather.
  Size: 11 by 17 inches. This is a note. Condition: Hot off the press.
  Copy: 23 of 125. Location: my study. Category: \textbf{private press}.
  Value: \textbf{\$270}. \par
\noindent\rule{\textwidth}{0.4pt}
\caption{The example's output} \label{fig:result}
\end{figure}

\clearpage

As an example this is a file that I use for printing a catalogue of my books,
where the book details are in file \file{mybooks.bib}. Note that using
\pack{bookdb} requires the use of the \pack{natbib} package ~\cite{NATBIB}.
\begin{verbatim}
% books.tex  a catalogue of my books
\documentclass[11pt,a4paper]{memoir}
\usepackage[T1]{fontenc}
\usepackage{natbib}
\pagestyle{empty}
\begin{document}
\nocite{*}
\bibliographystyle{bookdb}
\bibliography{mybooks}
\end{document}
\end{verbatim}


\begin{comment}
\section{Implementation}

As I said earlier I had to extend the \file{bookdb.bst} file produced by
the \texttt{makebst} program. I didn't really know how it all worked but after
much trial and many errors I got something that on the whole met my needs. My
basic process was to copy elements of the original \file{bst}, change some names, 
and see what was produced.

First of all I added the new \texttt{book} fields to the \file{bookdb.bst} \texttt{ENTRY}
command as:
\begin{verbatim}
ENTRY
  { ...
    binding
    category
    collator
    condition
    copy
    illustrations
    introduction
    location
    pages
    size
    translator
    value
    volumes
  }
\end{verbatim}

The next thing was to add the new entries in the correct order to the function that output 
the \texttt{book} bibliography entries, together with how they should be formatted. This
was the final result after much try it, \BibTeX\ it, change it, and repeat 
(the original code is in 
a typewriter font and my additions are in an italic font).

\begin{alltt}
{\ttfamily
FUNCTION \{book\}
\{ output.bibitem
  author empty$ 
    \{ format.editors "author and editor" 
                       output.check
      editor format.key output
      add.blank
    \}
    \{ format.authors output.nonnull
       crossref missing$
        \{ "author and editor" editor 
                       either.or.check \}
        'skip$
       if$
    \}
  if$
  new.block
  format.btitle "title" output.check }{\itshape
  format.editors output
  format.collator output
  format.translator output
  format.introduction output }{\ttfamily
  crossref missing$
   \{ format.bvolume output
     new.block
     format.number.series output
     new.sentence
     format.publisher.address output
   \}
   \{ 
    new.block
    format.book.crossref output.nonnull
   \}
  if$
  format.book.pages output
  format.edition output
  format.isbn output }{\itshape
  format.volumes output
  format.illustrations output
  format.binding output
  format.size output }{\ttfamily
  new.block
  format.note output }{\itshape
  format.condition output
  format.copy output
  format.location output
  format.category output
  format.value output }{\ttfamily
  fin.entry
\} } 
\end{alltt}

Effectively `all' I had left to do was to specify the formatting of my new fields.
I used three basic forms:
\begin{enumerate}
\item Some introductory text, like `introduction by' or `Illustrations:', followed by
      the field data.
\item Like the first form but with the field data in a bold font.
\item Name(s) forming the field data followed by what their contribution was 
      in parentheses.
\end{enumerate}

As an example of the first form here is the code for \field{binding}:

\begin{verbatim}
FUNCTION {format.binding}
{ binding "binding" bibinfo.check
  duplicate$ empty$ 'skip$
    {
      new.block
      "Binding: " swap$ *
    }
  if$
}
\end{verbatim}

Life was a little more complicated for the second form. This is the code for the
\field{value} field which requires two functions, the first dealing with the
bolding and the second with the output.

\begin{verbatim}
FUNCTION {boldval}
{ duplicate$ empty$
  { pop$ "" }
  { "Value: \textbf{" swap$ * "}" * }
 if$
}
FUNCTION {format.value}
{ value "value" bibinfo.check
  duplicate$ empty$ 'skip$
    {
      new.block
      boldval
    }
  if$
}
\end{verbatim}
 
The third form required several functions, as in the code for \field{collator}, where if there
is a single collator this is output as `Name (collator)' but if the are multiple collators
the output is `Name1 and Name2 ... (collators)'. 

\begin{verbatim}
FUNCTION {bbl.collator}
{ "collator" }
FUNCTION {bbl.collators}
{ "collators" }
FUNCTION {get.bbl.collator}
{ collator num.names$ #1 >
    'bbl.collators 'bbl.collator
  if$
}
FUNCTION {format.collator}
{ collator "collator" format.names 
    duplicate$ empty$ 'skip$
  { " " *
    get.bbl.collator
  "(" swap$ * ")" *
  }
  if$
}
\end{verbatim}

\end{comment}

%\clearpage

\section{My book database}

The \file{*.bib} for my book catalogue looks somewhat like this:
\begin{verbatim}
%%% mybooks.bib 2015/04/22

%%% for formating headings
@preamble{ "\providecommand{\ahead}[1]{%
 \textbf{\large #1}}" }

%%% publishers
@string{CUP = 
  "Cambridge University Press"}
% etc
%%% categories
@string{science = 
  "science, mathematics, computers"}
% etc

@heading{A1,
  key = {A},
  note = {\ahead{AAAAAAAAAAA....}
}

@book{A1KEY,
author = {First A. Author},
% etc
}

@book{A2KEY,
author = {Second A. Author},
% etc
}

% etc
\end{verbatim}

I used the \BibTeX\ \verb!@preamble! command to provide a definition of the \verb!\ahead!
macro. This, if required, can be overridden by an existing definition in the 
document used to print the bibliography.

I added various \verb!@string! commands to provide shorthands for many of the
fields in the \file{.bib} file, such as publisher, location, category, that
would have the same value. This meant that I could have a shortened field entry 
that looked like:
\begin{verbatim}
publisher = CUP,
\end{verbatim}
instead of:
\begin{verbatim}
publisher = {Cambridge University Press},
\end{verbatim}

\section{Infelicities}


    \BibTeX\ uses a stack-based language which I find hard to understand.
%Many years ago I wrote an interpreter for a stack-based language whose name
%I have forgotten but even so I was unable to use the language itself. I think
%that it is a little like crosswords. I like doing `cryptic' crosswords but
%I find that with some setters I can follow their clues easily but with
%others I haven't a clue.

    My basic approach was to take an existing \file{*.bst} file, try and see what it did,
then copy and modify what seemed relevant to my needs.

    I did have a couple of infelicities that I did not manage to resolve.

    The first was that no matter what I tried I could not stop the \texttt{heading}
from outputting its \field{key}, so it should be made as short and unobtrusive as possible.

    The second was that I couldn't stop the warning message issued by \BibTeX\ if both
an \field{author} and \field{editor} were supplied although the output was printed
including both.

    In spite of these, if you are a collector then you may want to consider tweaking a 
\file{*.bst} file to meet your particular needs. 


\bibliography{bookdb}

\end{document}












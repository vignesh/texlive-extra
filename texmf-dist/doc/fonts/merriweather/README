This is the README for the merriweather package, version 2020-09-12.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX support for
the Merriweather and MerriweatherSans families of fonts, designed by
Eben Sorkin.

Merriweather features a very large x height, slightly condensed
letterforms, a mild diagonal stress, sturdy serifs and open forms.
The Sans family closely harmonizes with the weights and styles of
the serif family. There are four weights and italics for each.

To install this package on a TDS-compliant TeX system download the
file "tex-archive"/install/fonts/merriweather.tds.zip, where the
preferred URL for "tex-archive" is http://mirror.ctan.org. Unzip
the archive at the root of an appropriate texmf tree, likely a
personal or local tree. If necessary, update the file-name database
(e.g., texhash). Update the font-map files by enabling the Map file
merriweather.map.

To use, add

\usepackage{merriweather}

to the preamble of your document. This will activate Merriweather as the
main (serifed) text font and MerriweatherSans as the sans font. 

To activate Merriweather without MerriweatherSans, use

\usepackage[rm]{merriweather}

Similarly, to activate MerriweatherSans without Merriweather use

\usepackage[sf]{merriweather} 

To use MerriweatherSans as the main text font, use

\usepackage[sfdefault]{merriweather}

This re-defines \familydefault, not \rmdefault. 

Options light, rmlight, sflight, black, rmblack, and sfblack choose
the light or heavy weights instead of the regular and bold weights,
respectively, for both or single families.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or who wish
to avoid fontspec may use the type1 option.

Options scaled=<number> or scale=<number> may be used to scale the
MerriweatherSans fonts; the serifed variants are not affected.

Commands \merriweather and \merriweathersans select the Merriweather
and MerriweatherSans families, respectively. Commands \merriweatherlight,
\merriweatherblack, \merriweathersanslight, and \merriweathersansblack
allow for localized use of light or black variants.

Options rmlining, sflining, rmoldstyle, sfoldstyle,
rmtabular, sftabular, rmproportional, sfproportional select
the number styles for the serif or sans-serif families. The
defaults are proportional, oldstyle.

Font encodings supported are OT1, T1, LY1 and TS1.

The original fonts are available at
https://github.com/SorkinType and are licensed under the
SIL Open Font License, (version 1.1); the text may be found
in the doc directory. The type1 versions were created using
cfftot1 and re-named in compliance with the Reserved
Font Name provision. The support files were created using
autoinst and are licensed under the terms of the LaTeX
Project Public License. The maintainer of this package is
Bob Tennent (rdt at cs.queensu.ca)

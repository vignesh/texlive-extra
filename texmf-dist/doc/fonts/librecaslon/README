This is the README for the librecaslon package, version
2020-07-31.

This package provides the Libre Caslon Text family of fonts,
designed by Pablo Impallari.  An artificially generated
BoldItalic variant has been added.

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/librecaslon.tds.zip, where the
preferred URL for "tex-archive" is http://mirror.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). Update the font-map
files by enabling the Map file LibreCaslon.map.

To use, add

\usepackage{librecaslon}

to the preamble of your document. This will activate Libre
Caslon Text as the main (serifed) text font. 

Options scaled=<number> or scale=<number> may be used to
scale the fonts.

The default figure style is proportional lining but options
osf (or oldstyle) and tf (or tabular) allow for use of
oldstyle and lining tabular figures, respectively.

Macro \librecaslon selects the Libre Caslon Text family.
Macros \librecaslonLF, \librecaslonOsF and \librecaslonTLF
allow for local use of lining, oldstyle or tabular figures,
respectively. \textsu{...} and \textin{...} can be used for
superior (superscript) and inferior (subscript) figures,
respectively. Command \useosf switches the default figure
style to old-style figures; this is primarily for use after
calling a math package with lining figures as the default.

The fonts (version 1.002) were obtained from

https://github.com/impallari/Libre-Caslon-Text

and are licensed under the SIL Open Font License (version
1.1); the text may be found in the doc directory. The
BoldItalic variant and the type1 fonts were generated from
them by fontforge or cfftot1. The rest of the package
is licensed under the terms of the LaTeX Project Public
License. The maintainer of this package is Bob Tennent (rdt
at cs.queensu.ca)

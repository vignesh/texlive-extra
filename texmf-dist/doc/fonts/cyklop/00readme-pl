% --- iso8859-2 ---
Font: Cyklop
Autor: Janusz Marian Nowacki, Grudzi�dz
Wersja: 0.915
Data: grudzie� 2008
Licencja:
 % Copyright 2008 Janusz M. Nowacki
 % This work is released under the GUST Font License
 % This work has the LPPL maintenance status "author-maintained".
 % This work consists of the files listed in the MANIFEST.txt file.

W latach dwudziestych XX wieku, w pracowni projektowej warszawskiej
,,Odlewni Czcionek J. Id�kowski i S-ka'' opracowano kr�j pisma
drukarskiego o nazwie Cyklop. Dwuelementowe, bezszeryfowe czcionki
charakteryzuj� si� bardzo du�ym kontrastem. Stemy pionowe s� znacznie
grubsze od stem�w poziomych. Wewn�trzne oczka liter maj� w wi�kszo�ci
kszta�t wyd�u�onego prostok�ta. Dzi�ki temu znaki maj� niepowtarzalny
kszta�t.

Cyklop, w postaci czcionek o�owianych, produkowany by� w odmianie
kursywnej w rozmiarach 8-48 pt. U�ywany by� bardzo intensywnie w
tytulariach gazetowych i drukach akcydensowych. Zecerzy si�gali po
niego w okresie miedzywojennym, w okresie okupacji w prasie
podziemnej. Stosowany by� a� do poczatk�w druku offsetowego i sk�adu
komputerowego. Obecnie trudno ju� go znale�� w formie metalowych
czcionek.

Fonty wygenerowa�em korzystaj�c z pakietu Metatype1. Uzupe�ni�em je o
kompletny zestaw akcentowanych liter oraz znaki, kt�rych
brak w oryginalnym zestawie, uwzgl�dniaj�cy wsp�czesne alfabety
�aci�skie (w tym wietnamski).

Wygenerowa�em r�wnie� odmian� prost�, co okaza�o si� zadaniem
bardziej skomplikowanym, ni� si� wcze�niej wydawa�o. W ksi��ce Filipa
Trzaski, Podstawy techniki wydawniczej (Warszawa 1967, Wydawn. CRZZ),
pokazano 11 liter prostego Cyklopa. Jednak nawet F. Trzaska nie
pami�ta sk�d si� te przyk�adowe litery wzi�y.

Fonty udost�pniono w formatach Type 1 oraz OpenType. Dla wykorzystania
w systemie TeX przygotowano odpowiednie pliki przekodowa�: T1 (ec),
T5 (wietnamski), OT4, QX, texnansi oraz niestandardowych (IL2 dla font�w
czeskich), jak te� wparcie w postaci odpowiednich makr i plik�w
definiuj�cych fonty dla LaTeX-a.

Historia:
Wersja 0.8 zosta�a zaprezentowana podczas konferencji Grupy
U�ytkownik�w Systemu TeX w Polsce -- BachoTeX-2007, w maju 2007 r.

W wersji 0.9 uwzgl�dniono zg�oszone uwagi i propozycje.

W wersji 0.91 poprawiono pliki metryczne .tfm oraz pliki .fd dla LaTeX-a.

W wersji 0.0915 poprawiono jedynie komentarze w plikach .otf

http://jmn.pl/cyklop.html
e-mail: janusz@jmn.pl

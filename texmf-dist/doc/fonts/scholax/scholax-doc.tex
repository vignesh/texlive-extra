% !TEX TS-program = pdflatexmk
% Template file for TeXShop by Michael Sharpe, LPPL
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
\pdfcompresslevel=0
\pdfgentounicode=1
\input glyphtounicode.tex
\usepackage[a-1b]{pdfx}
\InputIfFileExists{glyphtounicode-cmr.tex}{}{}
\InputIfFileExists{glyphtounicode-ntx.tex}{}{}
\pdfmapfile{=ScholaX.map}
\usepackage{trace}
\usepackage{fonttable}
%\usepackage{amssymb}% don't use with newtxmath
%SetFonts
% scholax+newtxmath
\usepackage[theoremfont,foresolidus=-.1em,aftsolidus=-.11em]{scholax} % use sb in place of bold
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata
\usepackage{amsmath,amsthm} 
\usepackage[scaled=1.075,ncf]{newtxmath}
% option vvarbb gives you stix blackboard bold
%\useosf % use oldstyle figures except in math
%SetFonts 
\newtheoremstyle{oldplain}
  {\topsep}   % ABOVESPACE
  {\topsep}   % BELOWSPACE
  {\itshape}  % BODYFONT
  {}       % INDENT (empty value is the same as 0pt)
  {\bfseries} % HEADFONT
  {.}         % HEADPUNC
  {5pt plus 1pt minus 1pt} % HEADSPACE
  {}          % CUSTOM-HEAD-SPEC
\theoremstyle{oldplain}
\newtheorem{oldthm}{Theorem}[section]

%\newtheoremstyle{plain}
%  {\medskipamount}   % ABOVESPACE
%  {\medskipamount}   % BELOWSPACE
%  {\thfamily}  % BODYFONT
%  {}       % INDENT (empty value is the same as 0pt)
%  {\bfseries} % HEADFONT
%  {.}         % HEADPUNCT
%  {5pt plus 1pt minus 1pt} % HEADSPACE
%  {}          % CUSTOM-HEAD-SPEC
\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]  
\usepackage{trace}
\title{ScholaX}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date
\makeatletter
%\renewcommand*{\textnu}[1]{\zcs@raisefrac \hbox{\textsu{#1}}}
%\newcommand*{\textde}[1]{\zcs@raisefrac \hbox{\textin{#1}}}
%\renewcommand*{\textfrac}[2]{%
%    \hbox{\textsu{#1}\kern \zcs@foresolidus \textfractionsolidus\kern \zcs@aftsolidus \textin{#2}}}
\makeatother
\begin{document}
\maketitle
%\show\thdefault
\section*{In short}

{\tt ScholaX} has a text part and a math support part that is called with an option to {\tt newtxmath}. The text component has many of the options and macros that are part of the {\tt newtxtext} package, but with Times fonts replaced by {\tt TeXGyreScholaX} fonts, the latter being an an extension of the {\tt TeXGyreSchola} fonts which in turn extended the URW rendering of \emph{New Century Schoolbook} that was donated as open source to the Ghostscript project. The font is based on early nineteenth century ``Scotch'' type, whose dominant features are low contrast as compared to, say, Times and Baskerville, and a relatively tall x-height. Its revivals gained popularity because of studies indicating its readability by children leading to its adoption in many texts for that market. Now that I am approaching the age where second childhood is not uncommon, the font is looking more interesting.

Slanted and Bold Slanted styles are provided in addition to the basic Regular, Bold, Italic and Bold Italic.


\section*{Options and Macros}
{\tt scaled, scale:}\\
Rescales every font in the package. E.g.,
\begin{verbatim}
\usepackage[scaled=1.15]{scholax}
\end{verbatim}
scales everything up by 15\%.

{\tt proportional (p)}, {\tt tabular}, {\tt lining}, {\tt osf}:\\
These affect the figure style used in text. The default is {\tt tabular}, {\tt lining}, which is appropriate for math usage, and will be used in math mode no matter how you set the text figure option. E.g., {\tt p,osf} sets the text figure style to {\tt proportional}, {\tt oldstyle}, like \textosf{12345}, but in math mode, figures will always be tabular, lining. 

The option {\tt osf} has the same effect as the macro \verb|\useosf|, which must occur in the preamble, but after loading {\tt scholax}. A similar macro \verb|\useproportional| makes proportional figures the default outside math mode.

{\tt scosf:}\\
This option makes {\tt osf} the default figure style in small cap text. 

{\tt theoremfont:}\\
This option makes use of a new text font family, {\tt thfamily}, amounting to Italic with upright punctuation glyphs. 
(This differs from the behavior in earlier packages, which involved redefining  the meaning of \verb|\slshape| and substituting it for \verb|\itshape| in the definition of the {\tt plain} theorem style.)\\
{\thfamily This is how the \verb|\thfamily|  looks: it is Italic but has upright punctuation symbols :;!? etc. The figures are not changed, but should in many cases be tabular lining figures, accessed using, e.g.,\/} \verb|\texttlf|.

There are two ways to make use of \verb|\thfamily| for body text in theorem-like text.
\begin{itemize}
\item
\textbf{(for {\tt amsthm} and {\tt theorem} only)}---add the option {\tt theoremfont} to {\tt scholax}, which will patch the \verb|\th@plain| command which stores the definition of the plain theorem format, replacing \verb|\itshape| by \verb|\thfamily|. Then, in your document preamble, make the usual theorem specifications, like
\begin{verbatim}
\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
etc
\end{verbatim}
\item
\textbf{(for {\tt ntheorem} only)} The {\tt theoremfont} option has no effect but will cause no harm. 
%\begin{verbatim}
%\newtheoremstyle{plain}
%  {\medskipamount}   % ABOVESPACE
%  {\medskipamount}   % BELOWSPACE
%  {\thfamily}  % BODYFONT <<<<<<<<<NOTE THE CHANGE FROM \itshape
%  {}       % INDENT (empty value is the same as 0pt)
%  {\bfseries} % HEADFONT
%  {.}         % HEADPUNCT
%  {5pt plus 1pt minus 1pt} % HEADSPACE
%  {}          % CUSTOM-HEAD-SPEC
%\end{verbatim}
Simply replace the theorem specifications described above for the {\tt plain} theorem style with
\begin{verbatim}
\theoremstyle{plain}
\theorembodyfont{\thfamily}
\newtheorem{thm}{Theorem}[section]
etc
\end{verbatim}
\end{itemize}

After doing this, you should get output like  this:

\begin{thm}
This is Theorem Italic: text numbers are upright---12345; punctuation is in many cases upright (also, parens (), braces \{\} and brackets []). What about question marks and exclamations? Also upright! [These fit better with math mode punctuation and figures, like: for all $x\in[0,1]$, let $f(x)\coloneq \exp(\alpha x)$.]
\end{thm}
Compare this to traditional {\tt plain} theorem style of the same text:
\begin{oldthm}
This is NOT Theorem Italic: text numbers are NOT upright---12345; punctuation is in many cases NOT upright (also, parens (), braces \{\} and brackets []). What about question marks and exclamations? Also NOT upright! [These DON'T fit better with math mode punctuation and figures, like: for all $x\in[0,1]$, let $f(x)\coloneq \exp(\alpha x)$.]
\end{oldthm}



{\tt sups:}\\
This makes superior figures to be used in footnote markers except in minipages. This option is prevented when using the KOMA classes, with which it may conflict. The option has no effect if you define a non-default handling of footnote markers in your preamble before loading {\tt scholax}.


\verb|\textosf, \texttosf, \textlf, \texttlf, \textsu, \textin|:\\
These macros provide ways to use other figures styles, no matter what defaults may have been set. They denote respectively proportional oldstyle, tabular oldstyle, lining, tabular lining, superiors, inferiors which render as follows.

\verb|\textosf{0123456789}| \textosf{0123456789}\\
\verb|\texttosf{0123456789}| \texttosf{0123456789}\\
\verb|\textlf{0123456789}| \textlf{0123456789}\\
\verb|\texttlf{0123456789}| \texttlf{0123456789}\\
\verb|\textsu{0123456789abc+-=()}| \textsu{0123456789abc+-=()}\\
\verb|\textin{0123456789}| \textin{0123456789}\\
%\verb|\textnu{0123456789}| \textnu{0123456789}\\
%\verb|\textde{0123456789}| \textde{0123456789}\\
%\verb|\textsu{1}\textnu{1}; \textin{1}\textde{1}| \textsu{1}\textnu{1}; \textin{1}\textde{1} \verb|Note the differences in size and baseline|
%\verb|\textfrac{17}{32}| \textfrac{17}{32} Constructs fractions using \verb|\textnu| and \verb|\textde|.
\verb|\textsu{1}X\textin{1}| renders as \textsu{1}X\textin{1} \\
\verb|2\textfrac{19}{32}| renders as 2\textfrac{19}{32} with default settings. Note also \textfrac{1}{2}, \textfrac{1}{4}, \textfrac{3}{4}, \textfrac{7}{8}, \textfrac{3}{16}, the first three of which are hard-coded in the font. There are clear deficiencies in the \verb|\textfrac| macro used in this package, and for a better one, one must use the approach now used in {\tt newtxtext}, which is much more involved.

The \verb|\textfrac| constructs fractions using \verb|\textsu| and \verb|\textin|, raising the result to align with the text baseline. The behavior is quite configurable, there being three parameters available to control the kerns before and after the fraction solidus, and the amount to raise the resulting box. The three parameters are passed as options to {\tt scholax}, named
\begin{verbatim}
foresolidus % default value -.1em
aftsolidus  % default value -.11em
raisefrac   % default value .111em
\end{verbatim}
(The values should always be in {\tt em} units in order to behave correctly with respect to scaling.)
If you were to change the default behavior with the option
\begin{verbatim}
raisefrac=0em
\end{verbatim}
you would get a fraction with the denominator's baseline  at the baseline of the \verb|\textin| figures, namely {\tt -.111em}. Those who wish the fractional part to be vertically centered with respect to lining figures should specify
\begin{verbatim}
raisefrac=.052em
\end{verbatim}


\section*{Spacing issues}
The  inter-word spacing in {\tt scholax} may be controlled by the three parameters {\tt spacing, stretch, shrink} which reset {\tt fontdimens} 2, 3 and 4 of the text font. The default values are {\tt .278em, .139em, .093em}. Option {\tt looser} increases them somewhat, and option {\tt loosest} increases them even more.

If you want full control, the options {\tt spacing, stretch, shrink} allow you to modify one or more of the above fontdimens. For example,
\begin{verbatim}
\usepackage[stretch=.18em,shrink=.11em]{scholax}
\end{verbatim}

\section*{Using \texttt{newtxmath} as accompanying math font}
The options {\tt nc} and {\tt ncf} to {\tt newtxmath} both entail using math italic letters drawn from {\tt scholax}. Option {\tt nc} gets Greek letters from {\tt newtxmath}, and option {\tt ncf} gets them from (an adaptation of) {\tt fourier} Greek. The latter are I think a better match to the {\tt scholax} style, having lower contrast than those from {\tt newtxmath}. Note that {\tt scholax} and {\tt newtxmath} are not at the same scale. You have to either scale down {\tt scholax} by {\tt 0.93} or scale up {\tt newtxmath} by {\tt 1.075}, or some intermediate combination. See the example preamble below.



\section*{Example preamble with math} 
\textbf{Caution:} If you have an up-to-date distribution (TeXLive or MiKTeX), then there is an important change to note in the AMS classes, making the option {\tt noamsfonts} viable. If you plan to use the {\tt newtxmath} package with an AMS class, be sure to enable this option, as it will save you two of your precious sixteen math families. (The equivalent symbols are already available in {\tt newtxmath} at a weight appropriate to fonts heavier than Computer Modern.)

\textsc{Example preamble:}
\begin{verbatim}
\usepackage[11pt,noamsfonts]{amsart}
\usepackage[p,osf]{scholax} 
% T1 and textcomp are loaded by package. Change that here, if you want
% load sans and typewriter packages here, if needed
\usepackage{amsmath,amsthm}% must be loaded before newtxmath
% amssymb should not be loaded
\usepackage[scaled=1.075,ncf,vvarbb]{newtxmath}% need to scale up math package
% vvarbb selects the STIX version of blackboard bold. See below.
\end{verbatim}

\section*{Usage with {\tt babel}}
You should normally load {\tt babel} before loading {\tt scholax} in order for {\tt babel} to function as expected.   For example:
\begin{verbatim}
\usepackage[greek.polutonico,english]{babel}
% the next line makes text figures proportional, oldstyle, while math uses lining figures
\usepackage[scale=.93,theoremfont,p,osf]{scholax}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl]{inconsolata}
\usepackage{amsmath,amsthm}
\usepackage[ncf,vvarbb]{newtxmath} 
% option ncf loads math italics from scholax, Greek from fourier 
% option vvarbb gives you stix blackboard bold
%\linespread{1.05} %needed only if scholax loaded at full scale
\end{verbatim}


\section*{Choices for blackboard bold}
Unless you specify in the options a choice (one of {\tt varbb}, {\tt vvarbb} for your blackboard bold alphabet, \verb|\mathbb| will use the default blackboard bold alphabet: $\mathbb{A\dots Z, k}$ (the only lowercase letter available is $\mathbb{k}$), which I find unappealing because the uneven separations between boundaries makes the gray level likewise uneven. There are  two macros which provide access to other alphabets: 
\begin{itemize}
\item
\verb|\vmathbb| gives you $\vmathbb{A\dots Z k 1\imath\jmath}$ ($\vmathbb{k}$  is sole lowercase, $\vmathbb{1\imath\jmath}$ borrowed from \verb|\vvmathbb|);
\item \verb|\vvmathbb| gives you $\vvmathbb{A\dots z12\imath\jmath}$ (full lowercase, digits, \verb|\imath,\jmath|).
\end{itemize}
For further details, consult the {\tt newtx} documentation.

\section*{Textcomp glyphs}

\fonttable{TeXGyreScholaX-Regular-tlf-ts1}

\section*{PDF/A Compliance}
This remains a work in progress---currently, this package does not generate pdfs that comply with the PDF/A-1b standard even when using the standard incantation in the preamble:
\begin{verbatim}
\pdfcompresslevel=0
\pdfgentounicode=1
\input glyphtounicode.tex
\usepackage[a-1b]{pdfx}
\InputIfFileExists{glyphtounicode-cmr.tex}{}{}
\InputIfFileExists{glyphtounicode-ntx.tex}{}{}
\end{verbatim}

%This pdf document was generated using pdflatex with the following lines in the preamble:
%\begin{verbatim}
%\pdfcompresslevel=0
%\pdfgentounicode=1
%\input glyphtounicode.tex
%\usepackage[a-1b]{pdfx}
%\InputIfFileExists{glyphtounicode-cmr.tex}{}{}
%\InputIfFileExists{glyphtounicode-ntx.tex}{}{}
%\end{verbatim}
%
%It was tested for compliance with that standard using three different methods, with varying results, as is common.
%\begin{itemize}
%\item
%\textsf{Adobe Acrobat Pro DC} says it does comply with PDF/A-1b.
%\item The test site \url{https://www.pdf-online.com/osa/validate.aspx} affirms that also.
%\item The free test package \textsf{veraPDF} says that it does not, and shows a number of issues about widths of glyphs and non-existence of some used glyphs. I found this report unconvincing, as when I merely removed the hints from the type 1 fonts, it gave me the same errors, but relating to a different set of glyphs. A message from its developers said that the type 1 fonts would fail the conformance test if their disassembled code contained a subroutine call before the width specification {\tt hsbw}, and that is common for type 1 fonts constructed using {\tt cfftot1} from the well known \textsf{LCDF Type Tools}. When I changed to using \textsf{FontForge} to construct the type 1 fonts, \textsf{veraPDF} gave a different set of problem glyphs, not overlapping the previous set. Finally, I removed all hints and received yet another set of problem glyphs.
%\end{itemize}

\newpage
\section*{Some math samples}
The following snippets mostly originated with the \TeX Book\ and were  adapted for \LaTeX\ from  Karl Berry's torture test for plain tex math fonts.

$x + y - z$, \quad $x + y * z$, \quad $z * y / z$, \quad 
$(x+y)(x-y) = x^2 - y^2$, 

$x \times y \cdot z = [x\, y\, z]$, \quad $x\circ y \bullet z$, \quad
$x\cup y \cap z$, \quad $x\sqcup y \sqcap z$, \quad

$x \vee y \wedge z$, \quad $x\pm y\mp z$, \quad
$x=y/z$, \quad $x:=y$, \quad $x\le y \ne z$, \quad $x \sim y \simeq z$
$x \equiv y \nequiv z$, \quad $x\subset y \subseteq z$

$\sin2\theta=2\sin\theta\cos\theta$, \quad
$\hbox{O}(n\log n\log n)$, \quad
$\Pr(X>x)=\exp(-x/\mu)$,

$\bigl(x\in A(n)\bigm|x\in B(n)\bigr)$, \quad
$\bigcup_n X_n\bigm\|\bigcap_n Y_n$

% page 178

In text matrices $\binom{1\,1}{0\,1}$ and $\bigl(\genfrac{}{}{0pt}{}{a}{1}\genfrac{}{}{0pt}{}{b}{m}\genfrac{}{}{0pt}{}{c}{n}\bigr)$

% page 142

$$a_0+\frac1{\displaystyle a_1 +
{\strut \frac1{\displaystyle a_2 +
{\strut \frac1{\displaystyle a_3 +
{\strut \frac1{\displaystyle a_4}}}}}}}$$

% page 143

$$\binom{p}{2}x^2y^{p-2} - \frac1{1 - x}\frac{1}{1 - x^2}
=
\frac{a+1}{b}\bigg/\frac{c+1}{d}.$$

%% page 145

$$\sqrt{1+\sqrt{1+\sqrt{1+\sqrt{1+\sqrt{1+x}}}}}$$

%% page 147

$$\left(\frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2}\right)
\bigl|\varphi(x+iy)\bigr|^2=0$$

%% page 149

% $$\pi(n)=\sum_{m=2}^n\left\lfloor\biggl(\sum_{k=1}^{m-1}\bigl
% \lfloor(m/k)\big/\lceil m/k\rceil\bigr\rfloor\biggr)^{-1}\right\rfloor.$$

$$\pi(n)=\sum_{m=2}^n\left\lfloor\Biggl(\sum_{k=1}^{m-1}\bigl
\lfloor(m/k)\big/\lceil m/k\rceil\bigr\rfloor\Biggr)^{-1}\right\rfloor.$$

% page 168

$$\int_0^\infty \frac{t - i b}{t^2 + b^2}e^{iat}\,dt=e^{ab}E_1(ab), \quad
a,b > 0.$$

% page 176

$$\mathbf{A} \coloneq \begin{pmatrix}x-\lambda&1&0\\
0&x-\lambda&1\\
0&0&x-\lambda\end{pmatrix}.$$

$$\left\lgroup\begin{matrix}a&b&c\\ d&e&f\\\end{matrix}\right\rgroup
\left\lgroup\begin{matrix}u&x\cr v&y\cr w&z\end{matrix}\right\rgroup$$

% page 177

$$\mathbf{A} = \begin{pmatrix}a_{11}&a_{12}&\ldots&a_{1n}\\
a_{21}&a_{22}&\ldots&a_{2n}\\
\vdots&\vdots&\ddots&\vdots\\
a_{m1}&a_{m2}&\ldots&a_{mn}\end{pmatrix}$$

$$\mathbf{M}=\bordermatrix{&C&I&C'\cr
C&1&0&0\cr I&b&1-b&0\cr C'&0&a&1-a}$$

%% page 186

$$\sum_{n=0}^\infty a_nz^n\qquad\hbox{converges if}\qquad
|z|<\Bigl(\limsup_{n\to\infty}\root n\of{|a_n|}\,\Bigr)^{-1}.$$

$$\frac{f(x+\Delta x)-f(x)}{\Delta x}\to f'(x)
\qquad \hbox{as $\Delta x\to0$.}$$

$$\|u_i\|=1,\qquad u_i\cdot u_j=0\quad\hbox{if $i\ne j$.}$$

%% page 191

$$\it\hbox{The confluent image of}\quad
\begin{Bmatrix}\hbox{an arc}\hfill\\\hbox{a circle}\hfill\\
\hbox{a fan}\hfill\\\end{Bmatrix}
\quad\hbox{is}\quad
\begin{Bmatrix}\hbox{an arc}\hfill\\
\hbox{an arc or a circle}\hfill\\
\hbox{a fan or an arc}\hfill\end{Bmatrix}.$$

%% page 191

\begin{align*}
T(n)\le T(2^{\lceil\lg n\rceil})
&\le c(3^{\lceil\lg n\rceil}-2^{\lceil\lg n\rceil})\\
&<3c\cdot3^{\lg n}\\
&=3c\,n^{\lg3}.
\end{align*}

%\begin{align*}
%\left\{%
%\begin{gathered}\alpha&=f(z)\\ \beta&=f(z^2)\\ \gamma&=f(z^3)
%\end{gathered}
%\right\}
%\qquad
%\left\{%
%\begin{gathered}
%x&=\alpha^2-\beta\\ y&=2\gamma
%\end{gathered}
%\right\}%
%\end{align*}

%$$\left\{
%\begin{align}
%\alpha&=f(z)\cr \beta&=f(z^2)\cr \gamma&=f(z^3)\\
%%\end{align}
%\right\}
%\qquad
%\left\{
%%\begin{align}
%x&=\alpha^2-\beta\cr y&=2\gamma\\
%\end{align}
%\right\}.$$
%%% page 192

\begin{align*}
\begin{aligned}
(x+y)(x-y)&=x^2-xy+yx-y^2\\
&=x^2-y^2\\
(x+y)^2&=x^2+2xy+y^2.
\end{aligned}
\end{align*}

%% page 192

\begin{align*}
\begin{aligned}
\biggl(\int_{-\infty}^\infty e^{-x^2}\,dx\biggr)^2
&=\int_{-\infty}^\infty\int_{-\infty}^\infty e^{-(x^2+y^2)}\,dx\,dy\\
&=\int_0^{2\pi}\int_0^\infty e^{-r^2}\,dr\,d\theta\\
&=\int_0^{2\pi}\biggl(e^{-\frac{r^2}{2}}
\biggl|_{r=0}^{r=\infty}\,\biggr)\,d\theta\\
&=\pi.
\end{aligned}
\end{align*}


%% page 197

$$\prod_{k\ge0}\frac{1}{(1-q^kz)}=
\sum_{n\ge0}z^n\bigg/\!\!\prod_{1\le k\le n}(1-q^k).$$

$$\sum_{\substack{\scriptstyle 0< i\le m\\\scriptstyle0<j\le n}}p(i,j) \,\ne
%
% $$\sum_{i=1}^p \sum_{j=1}^q \sum_{k=1}^r a_{ij} b_{jk} c_{ki}$$
%
\sum_{i=1}^p \sum_{j=1}^q \sum_{k=1}^r a_{ij} b_{jk} c_{ki} \,\ne
%
\sum_{\substack{\scriptstyle 1\le i\le p \\ \scriptstyle 1\le j\le q\\
\scriptstyle 1\le k\le r}} a_{ij} b_{jk} c_{ki}$$

$$\max_{1\le n\le m}\log_2P_n \quad \hbox{and} \quad
\lim_{x\to0}\frac{\sin x}{x}=1$$

$$p_1(n)=\lim_{m\to\infty}\sum_{\nu=0}^\infty\bigl(1-\cos^{2m}(\nu!^n\pi/n)\bigr)$$


 \end{document}  
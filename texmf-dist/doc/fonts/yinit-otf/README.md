yinit-otf
=========

This package is a conversion of the yinit font into OTF.

Original METAFONT files for yinit are in the [yinit](https://www.ctan.org/pkg/yinit) package. The conversion has been made by the excellent [mf2pt1](https://www.ctan.org/pkg/mf2pt1) with only tiny bugfixes afterwards.

To use this font in LaTeX:

```
\usepackage{fontspec}
\setmainfont{Yinit}
```

The copyright is (C) Yannis Haralambous, 1994, Public Domain.
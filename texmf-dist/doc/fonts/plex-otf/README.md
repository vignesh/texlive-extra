# README #
Package plex-otf supports the free otf-fonts from
the IBM Plex project which is available from GitHub
or already part of your system (Windows/Linux/...)

This package supports only XeLaTeX or LuaLateX,
for pdfLaTeX use the package plex-mono, plex-sans
and/or plex-serif.

IBM Plex has no math symbols. You have to use one 
the existing math fonts if you need it.


%% This file is distributed under the terms of the LaTeX Project Public
%% License from CTAN archives in directory  macros/latex/base/lppl.txt.
%% Either version 1.3 or, at your option, any later version.

hvoss@tug.org
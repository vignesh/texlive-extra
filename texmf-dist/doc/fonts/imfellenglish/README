This is the README for the imfellEnglish package, version (2015-10-04).

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the IM Fell English family of fonts, revived by
Igino Marini:

http://iginomarini.com/fell/the-revival-fonts/

Only upright and italic variants in medium weight and small
caps are available. 

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/imfellenglish.tds.zip, where the
preferred URL for "tex-archive" is http://mirror.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). Update the font-map
files by enabling the Map file imfellEnglish.map.

To use, add

\usepackage{imfellEnglish}

to the preamble of your document. 

Available shapes include:

it              italic
sc              small caps

Slanted variants are not supported; the italic variants will
be automatically substituted. Font encodings supported are
OT1, T1, LY1 and TS1.

Options scaled=<number> or scale=<number> may be used to
adjust fontsizes. The type1 option may be used by xelatex
or lualatex users who prefer to use type1 fonts or to avoid
fontspec.

The original fonts are available at

https://www.google.com/fonts/ 

and are licensed under the SIL Open Font License, (version
1.1); the text may be found in the doc directory. 

The OpenType versions were created using fontforge. The
support files were created using autoinst and otftotfm
(version 2.104) and are licensed under the terms of the
LaTeX Project Public License. The maintainer of this package
is Bob Tennent (rdt at cs.queensu.ca)

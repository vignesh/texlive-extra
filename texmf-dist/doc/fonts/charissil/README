This is the README for the charissil package, version 5.0,
release 2020-07-22.

This package provides the CharisSIL family of TrueType fonts
adapted by SIL International from Bitstream Charter. Because
of licensing restrictions, type1 versions are not available
and documents must be processed using xelatex or lualatex.

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/charissil.tds.zip where the
preferred URL for "tex-archive" is http://mirrors.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). 

To use, add

\usepackage{CharisSIL}

to the preamble of your document. This will activate
CharisSIL as the main (serifed) text font. Regular, Italic,
Bold and BoldItalic styles are available. The only figure
style is tabular-lining.

Options scaled=<number> or scale=<number> may be used to
scale the fonts.

The fonts are licensed under the SIL Open Font License,
version 1.1; the text may be found in the doc directory.
The support files are licensed under the terms of the LaTeX
Project Public License. The maintainer of this package is
Bob Tennent (rdt at cs.queensu.ca).

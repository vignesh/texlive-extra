kpfonts-otf package
===================

## Description

`kpfonts-otf’ is meant as a replacement, for LuaLaTeX and XeLaTeX users,
of Christophe Caignaert’s `kpfonts’ package. Christophe’s Type1 fonts have
been converted to OpenType using fontforge and Adobe's AFDKO bundle.
The package is still experimental, bug reports and suggestions are welcome.

## Contents

* the `font/` directory holds 21 OpenType fonts (16 for text and 5 for math);
* the `tex/`  directory holds the style file kpfonts-otf.sty and
  fontspec configuration files;
* the `doc/`  directory holds documentation in English and French and
  a table of available Math symbols comparing kpfonts-otf to LatinModern,
  STIXTwo, Erewhon, TeXGyreTermes, Garamond and Libertinus.

## Installation

This package is meant to be installed automatically by TeXLive, MikTeX, etc.
Otherwise, `kpfonts-otf’ can be installed under TEXMFHOME or TEXMFLOCAL, f.i.
* alls fonts (fonts/*.otf files) in directory
  `texmf-local/fonts/opentype/public/kpfonts-otf/`
* style and fontspec files (tex/kpfonts-otf.sty and tex/*.fontspec) in directory
  `texmf-local/tex/latex/kpfonts-otf/`
* documentation (from doc/ directory) in
  `texmf-local/doc/fonts/public/kpfonts-otf/`

Don't forget to rebuild the file database (mktexlsr or so) if you install
under TEXMFLOCAL.

Finally, you may want to make the system font database aware of the
`kpfonts-otf’ fonts (fontconfig under Linux).

## License

* The fonts included in `kpfonts-otf’ are licensed under the
SIL Open Font License, Version 1.1.
This license is available with a FAQ at: http://scripts.sil.org/OFL
* The other files are distributed under the terms of the LaTeX Project
Public License from CTAN archives in directory macros/latex/base/lppl.txt.
Either version 1.3 or, at your option, any later version.

## Changes
* First release version: 0.30 (experimental, expect changes!).

* v. 0.31:
  Fixed inconsistent widths/sidebearings for six glyphs:
  =  ≠  <  ≤  >  ≥ (all five Math fonts affected, original Type1 too).

* v. 0.32:
   1) Text fonts:
   kpfonts-otf.sty corrected: the "light" option didn't work properly.
   kffonts-otf should now cover all glyphs in TS1 encoding (textcomp.sty).
   The height of all diacritics has been reviewed and corrected
   New combining diacritics added : U+0310, U+0323, U+0325, U+0327, U+0328.
   New glyphs added: U+0110 (Dcroat), U+0111 (dcroat), U+0123 (gcircumflex),
   U+0126 (Hbar),  U+0127 (hbar), U+0129 (itilde), U+012B (imacron),
   U+012D (ibreve), U+0135 (jcircumflex), U+0166 (Tbar),  U+0167 (tbar)
   and their counterparts in Petite Caps and Small Caps.
   Corrected Petite Caps and Small Caps variant for U+00F0 (eth),
   U+00FE (thorn), U+0111 (dcroat)  and U+014B (eng).
   Optional ft and tt ligatures added, see feature "Ligatures=Required".
   Variants for ligatures fi ffi fl ffl added, see "Alternate=1".
   (faked) slanted fonts added to match kpfonts T1 version.

   2) Math fonts:
   Corrected mismatch between mitl (U+1d459) and ell (U+2113).
   Slanted versions for \shortparallel and \nshortparallel
   and for \gtreqless, \lesseqgtr, \gtreqqless, \lesseqqgtr added.
   Stretchy accents \wideoverbar, \widebreve, \widecheck added.
   Reduced boldness for superscripts and supersuperscripts.
   New option "tight" to reduce horizontal spaces in math mode
   (same settings as \pkg{fourier} and \pkg{fourier-otf}).

* v. 0.33:
   Roman Text fonts:
   Added 54 glyphs in Latin-A range (mostly complete now).
   Math fonts:
   Fixed inconsistencies in superscripts and supersuperscripts metrics.

* v. 0.34:
   Massive glyph cleaning: many spurious control points deleted in glyphs
   for both Math and Text fonts.
   Math fonts:
     metrics (width, italic correction) reviewed;
     accents ovhook (U+0309), candra (U+0310), vertoverlay (U+202D) added.
   Text fonts:
     kernings before and after quoteright improved;
     German capital Eszet (U+1E9E) added, in Petite and Small caps too,
     feature "StylisticSet=2" added to get SS instead of capital Eszet;
     diacritics corrected in KpRoman-LightItalic (some were upright);
     breaking change: feature "Alternate=1" changed to "StylisticSet=1"
     as it didn't work with XeTeX.

---
Copyright 2020-2021  Daniel Flipo
E-mail: daniel (dot) flipo (at) free (dot) fr

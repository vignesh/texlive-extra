\documentclass[paper=a4]{scrartcl}
% Packages
\usepackage{graphics, array}
\usepackage[svgnames]{xcolor}
\usepackage[hypcap=false]{caption}
\usepackage{shortvrb}
\usepackage{kpfonts-otf}
\setmonofont{KpMono}[Color=DarkBlue]
%Math demos
\setmathfont{KpMath-Regular.otf}[version=full, CharacterVariant=3,
                                 StylisticSet={1,2,3,4,5,6,7}]
\setmathfont{KpMath-Regular.otf}[version=base, CharacterVariant=3]
\setmathfont{KpMath-Light.otf}[version=light, CharacterVariant=3]
\setmathfont{KpMath-Bold.otf}[version=bold, Style=leqslant, CharacterVariant=3]
\setmathfont{KpMath-Sans.otf}[version=sans, Style=leqslant, CharacterVariant=3]

\usepackage[british,french]{babel} % french layout (lists, …)
\usepackage[english]{varioref}
\usepackage{realscripts}
\usepackage{microtype}
\usepackage{hyperref}
\hypersetup{pdfcreator={LuaTeX},
            pdfauthor={Daniel FLIPO},
            unicode,
            colorlinks,
            urlcolor=DarkBlue,
            linkcolor=Blue,
            }
%
\MakeShortVerb{\|}

\newcommand*{\pkg}[1]{\texttt{#1}}
\newcommand*{\opt}[1]{\texttt{#1}}
\newcommand*{\optit}[1]{\textit{\color{DarkBlue}#1}}
\newcommand*{\file}[1]{\texttt{#1}}
\newcommand*{\style}[1]{\textsf{\textbf{#1}}}
\newcommand*{\cmd}[1]{\texttt{\textbackslash #1}}
\newcommand*{\family}[1]{\textsb{#1}}
\newcommand*{\famvar}[1]{\textit{#1}}

\title{Kp-fonts: OpenType version}
\author{\href{mailto:daniel.flipo@free.fr}{Daniel \textsc{Flipo}}}
\newcommand*{\version}{0.34}

\begin{document}
\selectlanguage{british}
\maketitle
\thispagestyle{empty}

\begin{abstract}
  This bundle provides OpenType versions of Type1 Kp-fonts designed by
  Christophe Caignaert. See \file{Kpfonts-Doc-English.pdf} for the  full
  documentation of the original Type1 fonts.

  It is usable only with LuaTeX or XeTeX engines; it consists of sixteen Text
  OpenType fonts, a Roman family \family{KpRoman} in eight shapes and weights
  ---\famvar{Regular}, \famvar{Italic}, \famvar{Bold},
  \famvar{Bold\-Italic}, \famvar{Light}, \famvar{Light\-Italic},
  \famvar{Semi\-bold}, \famvar{Semibold\-Italic}---, a Sans-Serif family
  \family{KpSans} and a TypeWriter family \family{KpMono}, each of them
  in four shapes and weights---\famvar{Regular}, \famvar{Italic},
  \famvar{Bold} and \famvar{Bold\-Italic}--- and five Math OpenType fonts
  \family{KpMath-Regular}, \family{KpMath-Bold}, \family{KpMath-Light},
  \family{KpMath-Semibold} and \family{KpMath-Sans}.

  \family{KpRoman} and \family{KpSans} families have small caps available in
  two sizes (SmallCaps and PetitesCaps), upper and lowercase digits
  ({\addfontfeatures{Numbers=Lowercase} 0123456789}), ancient ligatures
  {\addfontfeatures{Ligatures=Rare} ct, st} and
  {\addfontfeatures{Style=Swash}Q} a long-tailed capital Q.
  Superior and inferior digits and letters have been added to the OpenType
  \family{KpRoman} and \family{KpSans} fonts for footnotes’ calls and
  abbreviations 1\textsuperscript{st}, 2\textsuperscript{nd}…

  Latin and Greek letters are available in Upright and Italic shapes, in Bold
  and Regular weights, for all Math fonts:
  $\symup{a}$ $\symup{\beta}$ $\symup{C}$ $\symup{\Delta}$,
  $\symit{a}$ $\symit{\beta}$ $\symit{C}$ $\symit{\Delta}$,
  $\symbfup{a}$ $\symbfup{\beta}$ $\symbfup{C}$ $\symbfup{\Delta}$,
  $\symbfit{a}$ $\symbfit{\beta}$ $\symbfit{C}$ $\symbfit{\Delta}$.

  Blackboard Bold capitals are available in two shapes, Serif and Sans:
  |\mathbb{ABC}| prints either
  \mathversion{full}$\mathbb{ABC}$ (option |[Style=bbsans]|) or
  \mathversion{base} $\mathbb{ABC}$ (default).
  Commands  |\mathcal{ABC}| and |\mathscr{ABC}| print either
  $\mathscr{ABC}$ (default) or
  \mathversion{full}$\mathscr{ABC}$ with option |[Style=mathcal]|,
  \mathversion{base}.

  File \file{unimath-kpfonts.pdf} shows the full list of Unicode Math symbols
  provided by Kp-fonts, compared with other common Math fonts.
  More symbols, specific to Kp-fonts, are listed in
  section~\ref{ssec:symb-spec-kp}.

  A style file \file{kpfont-otf.sty} is provided to load Kp-fonts easily.
  It is derived from \file{kpfont.sty} but options differ.

  Please beware that the current version (\version) is \emph{experimental}.

  All fonts are covered by OFL licence, style file and documentation
  are under LPPL-1.3 licence.
\end{abstract}

\newpage
\section{Loading \pkg{kpfonts-otf.sty}}

For users of the original \pkg{kpfonts.sty} package, the easiest way to
try the OpenType version is to load \pkg{kpfonts-otf.sty}:\\[.5\baselineskip]
|\usepackage[ |\optit{options}| ]{kpfonts-otf}|\\[.5\baselineskip]
this loads \pkg{unicode-math} (and \pkg{fontspec}) and defines
\family{KpRoman} (\family{Regular} or \family{Light} depending on options),
\family{KpSans} and \family{KpMono} as Text fonts, \family{KpMath}
(\family{Regular} or \family{Light} depending on options) as Math fonts.

\pkg{kpfonts-otf.sty} also defines all symbols available in \pkg{latexsym} and
\pkg{amssymb} under the same names%
\footnote{Unicode names often differ from AMS names.}
and a bunch of Kp-fonts specifics symbols.

\subsection{Global options for both Text and Maths}

\begin{description}
\item[light:] switches to \famvar{light} fonts, metrics are unchanged;
  \famvar{light} fonts might not look perfect on screen but they print fine.

  Please compare \famvar{regular} (left) and \famvar{light} fonts (right):

\newcommand*{\test}{Normal or light? Just a matter of taste}
\medskip

\begin{minipage}{.45\textwidth}\test. $E=mc^2$\end{minipage}\hfill
\begin{minipage}{.45\textwidth}\Light \test. \mathversion{light}$E=mc^2$
\end{minipage}

\medskip\mathversion{base}
\begin{minipage}{.45\textwidth}\itshape \test\end{minipage}\hfill
\begin{minipage}{.45\textwidth}\LightItalic \test\end{minipage}

\medskip
\begin{minipage}{.45\textwidth}\bfseries \test\end{minipage}\hfill
\begin{minipage}{.45\textwidth}\Semibold \test\end{minipage}

\medskip
\begin{minipage}{.45\textwidth}\bfseries\itshape \test\end{minipage}\hfill
\begin{minipage}{.45\textwidth}\SemiboldItalic \test\end{minipage}

\medskip

\item[nomath:] load neither \pkg{unicode-math} nor \family{KpMath} fonts;
  useful for documents without Maths, or to choose other Math fonts.
\item[notext:] do not change any Text font, use the defaults.
\item[nosf:]   do not change Sans-Serif Text fonts, use the defaults.
\item[nott:]   do not change Typewriter Text fonts, use the defaults.
\item[onlyrm:] equivalent to the last two combined.
\end{description}

\subsection{Options for Text fonts \emph{only}}

\begin{description}
\item[lighttext:] switches to \famvar{light} Text fonts.

\item[oldstylenums:] provides lowercase digits as a default.

  To get uppercase digits locally: |{\addfontfeature{Numbers=Lining} 123}|.

  Examples, upright, italic, bold and bolditalic:
  \begin{itemize}\addfontfeatures{Numbers=Lowercase}
	\item 0123456789 !
	\item \textit{0123456789 !}
	\item \textbf{0123456789 !}
	\item \textbf{\textit{0123456789 !}}
  \end{itemize}

\item[oldstyle:] provides lowercase digits as a default, long-tailed Q
  ({\addfontfeatures{Style=Swash}Quebec}) and (for Roman and Sans-Serif
  fonts only) old style ligatures
  {\addfontfeatures{Ligatures=Rare}« ct » et « st »}.

  Examples:
  \begin{itemize}\addfontfeatures{Style=Swash,Ligatures=Rare}
  \newcommand*{\test}{Quest for an attractive font!}
	\item \test
	\item \textit{\test}
	\item \textbf{\test}
	\item \textsc{\test}
	\item \textbf{\textit{\test}}
	\item \textsc{\textbf{\test}}
  \end{itemize}

\item[veryoldstyle:] \label{veryoldstyle} same as option \opt{oldstyle} but
  the round `s’  is replaced by the long one
  {\addfontfeatures{RawFeature=+hist;+hlig}`s\hspace{1sp}’},
  unless it ends a word (then it remains `s’)%
  \footnote{The OpenType \texttt{calt} feature is used to catch `s’ ending
    a word.}
  and ancient ligatures {\addfontfeatures{RawFeature=+hist;+hlig} si, sl, st}
  are activated.
  Coding \texttt{s=} prints a round `s’ anytime; in most cases this coding is
  not necessary with LuaTeX%
  \footnote{TeX’s \tild{} char (nobreakspace) fails to be recognised as ending
    a word: \texttt{boys\tild{}band} prints
    {\addfontfeatures{RawFeature=+hist;+hlig}boys~band}.},
  f.i. |\textit{some of Highlands’ mysterious castles…}|
  will print
  {\addfontfeatures{RawFeature=+hist;+hlig;+calt}%
    \textit{some of Highlands’ mysterious castles…}} which is correct;
  with XeTeX an \texttt{=} sign must be added at the end of
  \texttt{mysterious=}  to get a round `s’ there.

\item[largesmallcaps:] prints larger \textlsc{Small Caps} than the default
  ones (\textsc{Petites Caps}).

\item[altfligs :] prints alternative shapes for ligatures
  {\addfontfeatures{StylisticSet=1}fi, fl, ffi, ffl}
  instead of fi, fl, ffi, ffl.

\item[germandbls :] |\SS| prints {\addfontfeatures{StylisticSet=2}\SS} instead
  of {\char"1E9E} (capital \textit{Eszett}), ditto for small/petite caps.

\end{description}

\subsection{Options for Math fonts \emph{only}}

\begin{description}

\item[lightmath:] uses \famvar{light} Math fonts.

\item[bbsans:] command |\mathbb| prints Sans-Serif Blackboard Bold capitals
  with Serif fonts too: compare
  \mathversion{full}
  $\mathbb{C}$, $\mathbb{K}$, $\mathbb{N}$, $\mathbb{Q}$, $\mathbb{R}$,
  $\mathbb{Z}$, with
  \mathversion{base}
  $\mathbb{C}$, $\mathbb{K}$, $\mathbb{N}$, $\mathbb{Q}$, $\mathbb{R}$,
  $\mathbb{Z}$ (default).

\item[mathcal:] forces commands |\mathcal| and |\mathscr| to print
  \mathversion{full}$\mathcal{A,B,C,D}$  % $\mathscr{ABCD}$
  instead of \mathversion{base} $\mathcal{A,B,C,D}$ (default).

\item[frenchstyle:] Latin uppercase letters and all Greek letters are
  printed upright, only lowercase latin letters are printed in italics;
  this follows the French typographic usage.

\item[oldstylenumsmath:] prints lowercase digits in Maths (default is
  uppercase).

\item[narrowiints:] prints condensed repeated integrals :

  \mathversion{full}$\iiint$ et $\displaystyle\iiint$
  \quad instead of \quad
  \mathversion{base}$\iiint$ et $\displaystyle\iiint$ (default).

\enlargethispage*{\baselineskip}
\item[partialup:] the |\partial| symbol is printed upright $\symup{\partial}$
  instead of $\partial$.

\item[fancyReIm:] commands |\Re| et |\Im| print $\RE$ and $\IM$ respectively
  instead of $\Re$ et $\Im$.

\item[tight :] horizontal spaces tightened in math mode (same settings as
  \pkg{fourier-otf}).

\item[noDcommand:] do not define |\D| to avoid incompatibilities with other
  packages.

\end{description}

\section{Another way to load Kp-fonts}
\label{sec:options-standard}

Loading Kp-fonts through \file{kpfonts-otf.sty} offers only a limited choice
of options; the standard commands \cmd{setmainfont}, \cmd{setsansfont},
\cmd{setmonofont}, \cmd{setmathfont} offer much more flexibility.

On the other hand, \file{kpfonts-otf.sty} defines a lot of useful commands
to access AMS and specific Kp-fonts symbols. Loading \pkg{kpfonts-otf} with
the \opt{symbols} option enables to get all these commands defined without
loading any font:
\begin{verbatim}
\usepackage[symbols]{kpfonts-otf}
\end{verbatim}
Please note that \pkg{unicode-math}%
\footnote{A carefull reading of both manuals \file{unicode-math.pdf} and
  \file{fonspec.pdf} (available in all TeX distributions) is required in
  order to take full advantage of these packages.}
(and \pkg{fontspec}) \emph{are loaded} by this procedure, no need to do it
again, unless specific options are required, then \pkg{unicode-math} has to be
loaded \emph{before} \pkg{kpfonts-otf}, f.i.:
\begin{verbatim}
\usepackage[math-style=ISO,bold-style=upright]{unicode-math}
\usepackage[symbols]{kpfonts-otf}
\end{verbatim}
Then, it is up to the user to load Kp-fonts with whatever option he/she likes
using commands\\[.5\baselineskip]
|\set...font{|\optit{font}|}[|\optit{options}|]|.

For documents requiring no Math fonts, loading \pkg{fontspec} and using
the |\set...font| commands is enough.

\subsection{Options for Text fonts}

Here are the options available for Text Kp-fonts:
\begin{description}
\item[Numbers=Lowercase] to get lowercase digits
  {\addfontfeature{Numbers=Lowercase} 1,2,3} instead of 1,2,3;
  the defaut is\\   \style{Numbers=Lining}.

\item[SmallCapsFeatures = \{Letters=SmallCaps\}] the |\textsc{}| command
  will print larger \textlsc{Small Caps} than the default
  \textpsc{Petites Caps}.\\
  The default setting is \style{SmallCapsFeatures = \{Letters=PetiteCaps\}}.

\item[Ligatures=TeX]  (default) |'|\quad|!`|\quad|?`|\quad|--|\quad |---|\quad
  print respectively\quad ’ \quad !` \quad ?`\quad -- \quad ---.

\item[Ligatures=Common] (default) automatic ligatures  ff ffi ffl fi fl.

\item[StylisticSet=1] provides an alternative for glyphs
  {\addfontfeature{StylisticSet=1} ffi ffl fi fl} (ff is unchanged).

\item[Ligatures=Required :] adds {\addfontfeature{Ligatures=Required} ft et tt}
  ligatures.

\item[Ligatures=Rare] adds {\addfontfeature{Ligatures=Rare} ct et st} ligatures.

\item[Style=Swash] to get the long-tailed capital
  Q: {\addfontfeature{Style=Swash}Queen, also in small caps (both sizes):
    \textpsc{Queen} and \textlsc{Queen}}.

\item[Style=Historic] replaces any instance of `s’ by the long variant
  {\addfontfeature{Style=Historic}s}. It is still possible to get a round
  `s’ coding it as `|s=|’.  As the long variant is never used at words’ ends
  the feature \opt{calc} is also activated: it tries to catch end of words,
  see \opt{veryoldstyle} p.~\pageref{veryoldstyle} for more details.

\item[Ligatures=Historic] switches specific ligatures for the long
  {\addfontfeature{Style=Historic}s}:
   {\addfontfeature{Style=Historic,Ligatures=Historic} si, sl, st}.

\item[StylisticSet=2 :] |\SS| prints {\addfontfeatures{StylisticSet=2}\SS}
  instead of {\char"1E9E} (capital \textit{Eszett}), ditto for small/petite caps.

\end{description}

Options may be are chosen for each font, say:
\begin{verbatim}
\setmonofont{KpMono}[Numbers=Lowercase,Style=Historic]
\end{verbatim}
but can also be shared by different typefaces:
\begin{verbatim}
\defaultfontfeatures+[KpRoman,KpSans,KpMono]{Numbers=Lowercase}
\defaultfontfeatures+[KpRoman,KpSans]{%
  Ligatures = Rare,
  Style = Swash,
  SmallCapsFeatures = {Letters=SmallCaps},
  }
\setmainfont{KpRoman}
\setsansfont{KpSans}
\setmonofont{KpMono}
\end{verbatim}
Notes : 1. |\setmonofont{KpMono}|, |\setsansfont{KpSans}|,
|\setmainfont{KpRoman}| rely on files \file{KpMono.fontspec},
\file{KpSans.fontspec} and \file{KpRoman.fontspec} installed by Kpfonts.

2. Note the |+| ending |\defaultfontfeatures+| : options are \emph{added},
not overwriting any other (including those of \file{fontspec.cfg}).

3. Options can be gathered: |Ligatures={Rare,Historic}| (with braces)
is equivalent to |Ligatures=Rare| and |Ligatures=Historic|.

4. These options can also be switched on and off \emph{locally}
using |\addfontfeatures| inside a group, f.i.\ to print lowercase digits
{\addfontfeatures{Numbers=Lowercase}1234576890} with a font loaded with option
|Numbers=Lining|:
\begin{verbatim}
{\addfontfeatures{Numbers=Lowercase}1234576890}
\end{verbatim}
Actually, a shortcup is available in this case: |\oldstylenums{1234576890}|.

\subsection{Options for Math fonts}

The following options can be passed either to \pkg{unicode-math}%
\footnote{See the manual \file{unicode-math.pdf}.}
or to |\setmathfont{}|:
\begin{description}
\item[math-style =] \opt{ISO}, \opt{TeX} (défaut), \opt{french}, \opt{upright};
\item[bold-style =] \opt{ISO}, \opt{TeX} (défaut), \opt{upright};
\item[partial =] \opt{upright} (default italic);
\item[nabla =]  \opt{italic}  (default upright);
\end{description}

Seven `Style Variants’ are available with Kp-fonts, here are the first three:
\begin{description}
\item[Style=mathcal] (\texttt{+ss01}) commands |\mathcal{}| and |\mathscr{}|
  print \mathversion{full}$\mathcal{ABC}$ instead of
  \mathversion{base}$\mathcal{ABC}$ (default);

\item[Style=bbsans] (\texttt{+ss02}) |\mathbb{}| prints Sans-Serif
  Blackboard bold capitals \mathversion{full}$\mathbb{ABC}$ for Serif Math fonts
  \file{KpMath-Regular} and \file{KpMath-light} instead of
  \mathversion{base}$\mathbb{ABC}$ ;

\item[Style=narrowiints] (\texttt{+ss03}) provides condensed repeated integrals:
  \mathversion{full}$\iiint$ instead of
  \mathversion{base}$\iiint$ (default).
\end{description}

\vspace{\baselineskip}
The next four tables present the other Style Variants available:
\par\noindent
\begin{minipage}{\linewidth}
  \begin{minipage}[t]{0.45\linewidth}\centering
    \captionof{table}{Style=leqslant (\texttt{+ss04})}\label{ss04}
    \begin{tabular}{@{}lcc@{}}
      \hline
      Command           & Default        & Variant \\
      \hline
      \cmd{leq}         & $\leq$         & $\leqslant$ \\
      \cmd{geq}         & $\geq$         & $\geqslant$ \\
      \cmd{nleq}        & $\nleq$        & $\nleqslant$ \\
      \cmd{ngeq}        & $\ngeq$        & $\ngeqslant$ \\
      \cmd{leqq}        & $\leqq$        & $\leqqslant$ \\
      \cmd{geqq}        & $\geqq$        & $\geqqslant$ \\
      \cmd{eqless}      & $\eqless$      & $\eqslantless$ \\
      \cmd{eqgtr}       & $\eqgtr$       & $\eqslantgtr$ \\
      \cmd{lesseqgtr}   & $\lesseqgtr$   & $\lesseqslantgtr$ \\
      \cmd{gtreqless}   & $\gtreqless$   & $\gtreqslantless$ \\
      \cmd{lesseqqgtr}  & $\lesseqqgtr$  & $\lesseqqslantgtr$ \\
      \cmd{gtreqqless}  & $\gtreqqless$  & $\gtreqqslantless$ \\
      \hline
    \end{tabular}
  \end{minipage}\hspace{\fill}
  \begin{minipage}[t]{0.5\linewidth}\centering
    \captionof{table}{Style=smaller (\texttt{+ss05})}\label{ss05}
    \begin{tabular}{@{}lcc@{}}
      \hline
      Command                & Default        & Variant \\
      \hline
      \cmd{mid}              & $\mid$              & $\shortmid$ \\
      \cmd{nmid}             & $\nmid$             & $\nshortmid$ \\
      \cmd{parallel}         & $\parallel$         & $\shortparallel$ \\
      \cmd{nparallel}        & $\nparallel$        & $\nshortparallel$ \\
      \cmd{parallelslant}    & $\parallelslant$    & $\shortparallelslant$ \\
      \cmd{nparallelslant}   & $\nparallelslant$   & $\nshortparallelslant$ \\
      \hline
    \end{tabular}
  \end{minipage}\\[\baselineskip]
  \begin{minipage}[t]{0.45\linewidth}\centering
    \captionof{table}{Style=subsetneq (\texttt{+ss06})}\label{ss06}
    \begin{tabular}{@{}lcc@{}}
      \hline
      Command           & Default         & Variant \\
      \hline
      \cmd{subsetneq}   & $\subsetneq$    & $\varsubsetneq$ \\
      \cmd{supsetneq}   & $\supsetneq$    & $\varsupsetneq$ \\
      \cmd{subsetneqq}  & $\subsetneqq$   & $\varsubsetneqq$ \\
      \cmd{supsetneqq}  & $\supsetneqq$   & $\varsupsetneqq$ \\
      \hline
    \end{tabular}
  \end{minipage}\hspace{\fill}
  \begin{minipage}[t]{0.5\linewidth}\centering
    \captionof{table}{Style=parallelslant (\texttt{+ss07})}\label{ss07}
    \begin{tabular}{@{}lcc@{}}
      \hline
      Command               & Default            & Variant \\
      \hline
      \cmd{parallel}        & $\parallel$        & $\parallelslant$ \\
      \cmd{nparallel}       & $\nparallel$       & $\nparallelslant$ \\
      \cmd{shortparallel}   & $\shortparallel$   & $\shortparallelslant$ \\
      \cmd{nshortparallel}  & $\nshortparallel$  & $\nshortparallelslant$ \\
      \hline
    \end{tabular}
  \end{minipage}
\end{minipage}

\vspace{\baselineskip}
Example: switching styles 4 (leqslant) and 6 (subsetneq) can be achieved
coding either\\
|\setmathfont{KpMath-Regular.otf}[StylisticSet={4,6}]|\quad or\\
|\setmathfont{KpMath-Regular.otf}[Style={leqslant,subsetneq}]|\\
but this second syntax is available only if \pkg{kpfonts-otf.sty} has been
loaded (eventually with the \opt{symbols} option).

\vspace{\baselineskip}
Table~\vref{cv} shows the available `Glyphs Variants’:

\begin{table}[ht]
  \centering
  \caption{Glyphs Variants}
  \label{cv}
  \begin{tabular}{@{}>{\ttfamily}lccl@{}}
    \hline
           & Default       & Variant          & Command\\
    \hline
      cv00 & $\Re\quad\Im$ & $\RE\quad\IM$    & \cmd{Re}\quad\cmd{Im}\\
      cv01 & $\hslash$     & $\mithbar$       & \cmd{hslash} or \cmd{hbar} \\
      cv02 & $\emptyset$   & $\varemptyset$   & \cmd{emptyset} \\
      cv03 & $\mitepsilon$ & $\mitvarepsilon$ & \cmd{epsilon} \\
      cv04 & $\mitkappa$   & $\mitvarkappa$   & \cmd{kappa} \\
      cv05 & $\mitpi$      & $\mitvarpi$      & \cmd{pi} \\
      cv06 & $\mitphi$     & $\mitvarphi$     & \cmd{phi} \\
      cv07 & $\mitrho$     & $\mitvarrho$     & \cmd{rho} \\
      cv08 & $\mitsigma$   & $\mitvarsigma$   & \cmd{sigma} \\
      cv09 & $\mittheta$   & $\mitvartheta$   & \cmd{theta} \\
      cv10 & $\mitTheta$   & $\mitvarTheta$   & \cmd{Theta}\\
    \hline
  \end{tabular}
\end{table}

\vspace{\baselineskip}
Example: with |\setmathfont{KpMath-Regular.otf}[CharacterVariant={3,6}]|\\
commands |\epsilon| and |\phi| print $\mitvarepsilon$
and $\mitvarphi$ instead of $\mitepsilon$ et $\mitphi$.
The same is true of course for all shapes and and weights (upright, bold,
bolditalic, sans-derif, etc.): f.i.\ with \opt{math-syle=french}, |\epsilon|
and |\phi| print $\mupvarepsilon$ and $\mupvarphi$.

Note about \cmd{hbar}: \pkg{unicode-math} defines \cmd{hbar} as
\cmd{hslash} (U+210F) while \pkg{amsmath} provides two different glyphs
(italic h with horizontal or diagonal stroke).\\
\pkg{kpfonts-otf} now follows \pkg{unicode-math}; the italic h with horizontal
stroke can be printed using \cmd{hslash} or \cmd{hbar} together with character
variant \texttt{cv01} or with \cmd{mithbar} (replacement for AMS’ command
\cmd{hbar}).

\section{Kp-fonts specific commands}

\subsection{Integrals}

Kp-fonts offers variants for integral symbols suitable for indefinite
integrals, they are coded as |\varint|, |\variint|, |\variiint|, |\variiiint|
and |\varidotsint|.  Compare $\int f(t)\,dt$ and $\varint f(t)\,dt$ and also

\[\int f(t)\,dt \qquad\text{and}\qquad\varint f(t)\,dt\]

|\D{...}| prints an upright `d’ and improves kernings around the differential
element:
|\displaystyle\varint f(t)\D{t}| donne	$\displaystyle\varint f(t)\D{t}$.


\subsection{Specific Math symbols}
\label{ssec:symb-spec-kp}

The next tables present symbols unvailable as Unicode characters, they
are coded in Kp-fonts’ private zone.

\begin{center}\begin{tabular}{r>{$}c<{$}|r>{$}c<{$}}
\verb=\mmapsto=&\mmapsto&\verb=\longmmapsto=&\longmmapsto\\
\verb=\mmapsfrom=&\mmapsfrom&\verb=\longmmapsfrom=&\longmmapsfrom\\
\verb=\Mmapsto=&\Mmapsto&\verb=\Longmmapsto=&\Longmmapsto\\
\verb=\Mmapsfrom=&\Mmapsfrom&\verb=\Longmmapsfrom=&\Longmmapsfrom\\
\verb=\leftrightdasharrow=&\leftrightdasharrow&\verb=\leadsto=&\leadsto
\end{tabular}\end{center}

\begin{center}\begin{tabular}{r>{$}c<{$}|r>{$}c<{$}}
\verb=\boxright=&\boxright&\verb=\boxleft=&\boxleft\\
\verb=\circleright=&\circleright&\verb=\circleleft=&\circleleft\\
\verb=\Diamondright=&\Diamondright&\verb=\Diamondleft=&\Diamondleft\\
\verb=\boxdotright=&\boxdotright&\verb=\boxdotleft=&\boxdotleft\\
\verb=\circledotright=&\circledotright&\verb=\circledotleft=&\circledotleft\\
\verb=\Diamonddotright=&\Diamonddotright&\verb=\Diamonddotleft=&\Diamonddotleft
\end{tabular}\end{center}

\begin{center}\begin{tabular}{r>{$}c<{$}|r>{$}c<{$}}
\verb=\boxRight=&\boxRight&\verb=\boxLeft=&\boxLeft\\
\verb=\boxdotRight=&\boxdotRight&\verb=\boxdotLeft=&\boxdotLeft\\
\verb=\DiamondRight=&\DiamondRight&\verb=\DiamondLeft=&\DiamondLeft\\
\verb=\DiamonddotRight=&\DiamonddotRight&\verb=\DiamonddotLeft=&\DiamonddotLeft
\end{tabular}\end{center}

\begin{center}\begin{tabular}{r>{$}c<{$}|r>{$}c<{$}}
\verb=\multimapdot=&\multimapdot&\verb=\multimapdotinv=&\multimapdotinv\\
\verb=\multimapdotboth=&\multimapdotboth\\
\verb=\multimapbothvert=&\multimapbothvert&\verb=\multimapdotbothvert=&\multimapdotbothvert\\
\verb=\multimapdotbothAvert=&\multimapdotbothAvert&\verb=\multimapdotbothBvert=&\multimapdotbothBvert
\end{tabular}\end{center}

\begin{center}\begin{tabular}{r>{$}c<{$}|r>{$}c<{$}|r>{$}c<{$}}
\verb=\capplus=&\capplus&\verb=\sqcupplus=&\sqcupplus&\verb=\sqcapplus=&\sqcapplus\\
\verb=\parallelslant=&\parallelslant&\verb=\colonsim=&\colonsim&\verb=\colonapprox=&\colonapprox\\
\verb=\parallelbackslant=&\parallelbackslant&\verb=\Colonsim=&\Colonsim&\verb=\Colonapprox=&\Colonapprox\\
\verb=\eqqColon=&\eqqColon&\verb=\Colondash=&\Colondash&\verb=\dashColon=&\dashColon\\
\end{tabular}\end{center}

\begin{center}\begin{tabular}{r>{$}c<{$}|r>{$}c<{$}|r>{$}c<{$}}
\verb=\strictif=&\strictif&\verb=\strictfi=&\strictfi&\verb=\strictiff=&\strictiff\\
\verb=\circledvee=&\circledvee&\verb=\circledwedge=&\circledwedge&\verb=\circledbar=&\circledbar\\
\verb=\openJoin=&\openJoin&\verb=\opentimes=&\opentimes&\verb=\VvDash=&\VvDash\\
\verb=\lambdaslash=&\lambdaslash&\verb=\lambdabar=&\lambdabar&\verb=\Wr=&\Wr
\end{tabular}\end{center}

\renewcommand{\arraystretch}{2}

\begin{center}
\begin{tabular}{r>{$}c<{$}c|r>{$}c<{$}c}

\verb+\idotsint+ &\idotsint& $\displaystyle\idotsint$\\
\verb+\ointclockwise+ &\ointclockwise&$\displaystyle\ointclockwise$
&\verb+\varointctrclockwise+&\varointctrclockwise& $\displaystyle\varointctrclockwise$\\
\verb+\oiintclockwise+ &\oiintclockwise&$\displaystyle\oiintclockwise$&\verb+\oiintctrclockwise+&\oiintctrclockwise&$\displaystyle\oiintctrclockwise$\\
\verb+\varoiintclockwise+&\varoiintclockwise& $\displaystyle\varoiintclockwise$&\verb+\varoiintctrclockwise+&\varoiintctrclockwise&$\displaystyle\varoiintctrclockwise$\\
\verb+\oiiintclockwise+ &\oiiintclockwise&$\displaystyle\oiiintclockwise$&\verb+\oiiintctrclockwise+ &\oiiintctrclockwise&$\displaystyle\oiiintctrclockwise$\\
\verb+\varoiiintclockwise+ &\varoiiintclockwise& $\displaystyle\varoiiintclockwise$&\verb+\varoiiintctrclockwise+&\varoiiintctrclockwise&$\displaystyle\varoiiintctrclockwise$\\
\verb+\sqiint+&\mbox{$\sqiint$}&$\displaystyle\sqiint$&\verb+\sqiiint+&\mbox{$\sqiiint$}&$\displaystyle\sqiiint$
\end{tabular}
\end{center}

The full list of Unicode symbols available with Kp-fonts is shown in file
\file{unimath-kpfonts.pdf}.

\newpage
\subsection{Wide accents}

\begin{itemize}
\item \cmd{widehat} and \cmd{widetilde}
\[\widehat{x}\; \widehat{xx} \;\widehat{xxx} \;\widehat{xxxx}\;
  \widehat{xxxxx} \;\widehat{xxxxxx} \;\widetilde{x}\; \widetilde{xx}\;
  \widetilde{xxx} \;\widetilde{xxxx} \;\widetilde{xxxxx}\;
  \widetilde{xxxxxx}\]

\item \cmd{overline} and \cmd{underline}
  \[\overline{x}\quad \overline{xy}\quad \overline{xyz}\quad
    \overline{A\cup B}\quad \overline{A\cup (B\cap C)\cup D}\quad
    \underline{m+n+p}\]

\item \cmd{wideoverbar}, \cmd{widecheck}  et \cmd{widebreve}
  \[\wideoverbar{x}\quad \wideoverbar{xy}\quad \wideoverbar{xyz}\quad
    \widecheck{x}\quad \widecheck{xxxx}\quad \widecheck{xxxxxx}\quad
    \widebreve{x}\quad \widebreve{xxxx}\quad \widebreve{xxxxxx}
  \]

\item \cmd{overparen} and \cmd{underparen}
  \[\overparen{x}\quad \overparen{xy}\quad \overparen{xyz}\quad
    \mathring{\overparen{A\cup B}}\quad
    \overparen{A\cup (B\cap C)\cup D}^{\smwhtcircle}\quad
    \overparen{x+y}^{2}\quad \overparen{a+b+...+z}^{26}\]

\[\underparen{x}\quad \underparen{xz} \quad \underparen{xyz}
  \quad \underparen{x+z}_{2}\quad \underparen{a+b+...+z}_{26}\]

\item \cmd{overbrace} and \cmd{underbrace}
  \[\overbrace{a}\quad \overbrace{ab}\quad \overbrace{abc}\quad
  \overbrace{abcd}\quad \overbrace{abcde}\quad
  \overbrace{a+b+c}^{3}\quad \overbrace{ a+b+. . . +z }^{26}\]

\[\underbrace{a}\quad\underbrace{ab}\quad\underbrace{abc}\quad
  \underbrace{abcd}\quad \underbrace{abcde}\quad
  \underbrace{a+b+c}_{3}  \quad \underbrace{ a+b+...+z }_{26}\]

\item \cmd{overrightarrow} and \cmd{overleftarrow}
  \[\overrightarrow{v}\quad \overrightarrow{M}\quad \overrightarrow{vv}
  \quad \overrightarrow{AB}\quad \overrightarrow{ABC}
  \quad \overrightarrow{ABCD} \quad \overrightarrow{ABCDEFGH}.
\]

\[\overleftarrow{v}\quad \overleftarrow{M}\quad \overleftarrow{vv}
  \quad \overleftarrow{AB}\quad \overleftarrow{ABC}
  \quad \overleftarrow{ABCD} \quad \overleftarrow{ABCDEFGH}\]

\item Enfin \cmd{widearc} and \cmd{widearcarrow} (ou \cmd{overrightarc})
\[\widearc{AMB}\quad \widearcarrow{AMB}\]
\end{itemize}

\subsection{Math Versions}

Different versions of the \family{KpMath} fonts may be defined in the
document’s preamble:\\
|\setmathfont{KpMath-Regular.otf}[version=base|, \optit{options} |]|\\
|\setmathfont{KpMath-Bold.otf}[version=bold|, \optit{options} |]|\\
|\setmathfont{KpMath-Semibold.otf}[version=semibold|, \optit{options} |]|\\
|\setmathfont{KpMath-Sans.otf}[version=sans|, \optit{options} |]|\\
|\setmathfont{KpMath-Light.otf}[version=light|, \optit{options} |]|\\
then, it is easy to switch from one version to another one with
|\mathversion{|\optit{name}|}|.


\newpage
Example\footnote{Option \opt{CharacterVariant=3} changes
$\epsilon$ into $\varepsilon$.} :
\begin{verbatim}
\setmathfont{KpMath-Regular.otf}[Style=leqslant, CharacterVariant=3]
\setmathfont{KpMath-Bold.otf}[version=bold,
                              Style=leqslant, CharacterVariant=3]
\setmathfont{KpMath-Sans.otf}[version=sans,
                              Style=leqslant, CharacterVariant=3]
\end{verbatim}

Here is the same equation in three versions, normal, bold and Sans-Serif:
\mathversion{base}

\[ \mathbb{E}_i(N_i) = \sum_{n\ge1} P_i(N_i\ge n)
                     = \frac{\epsilon_i}{1-\epsilon_i}<+\infty \]

|\mathversion{bold}| \mathversion{bold}
\[ \mathbb{E}_i(N_i) = \sum_{n\ge1} P_i(N_i\ge n)
                     = \frac{\epsilon_i}{1-\epsilon_i}<+\infty \]

|\mathversion{sans}| \mathversion{sans}
\[ \mathbb{E}_i(N_i) = \sum_{n\ge1} P_i(N_i\ge n)
                     = \frac{\epsilon_i}{1-\epsilon_i}<+\infty \]
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-master: t
%%% coding: utf-8
%%% End:

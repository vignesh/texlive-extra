% !TEX TS-program = pdflatexmk
% Template file for TeXShop by Michael Sharpe, LPPL
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
%\pdfmapfile{+SticksTooText.map}
\pdfmapfile{=newtx.map}
\usepackage[a-1b]{pdfx}
%\usepackage{amssymb}% don't use with newtxmath
%SetFonts
% stickstoo+newtxmath
\usepackage{stickstootext} % use sb in place of bold
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata
\usepackage{amsmath,amsthm}
\usepackage[stix2]{newtxmath}
% option vvarbb gives you stix blackboard bold
\useosf % use oldstyle figures except in math
%SetFonts
\title{SticksToo}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date

\begin{document}
\maketitle
\section{In short}
{\tt SticksToo} is a repackaging for \LaTeX\ of the text part of {\tt STIX2} that was released in early May 2018. The style file {\tt stix2.sty} does offer support for text processing at a rather basic level, but does not provide much access to the riches of the {\tt STIX2} Text fonts. With 
{\tt SticksToo} there are most of the options and macros that are part of the {\tt newtxtext} package, but with Times fonts replaced by {\tt StixTwoText} fonts. As of the initial release of {\tt STIX2}, the math fonts have some problem which make them unsuited to high quality mathematical typesetting, and so I'm offering an update of {\tt newtxmath} with option {\tt stix2} that makes use of the Roman and Greek letters from {\tt STIX2} (the two year old beta version, not the one now available on CTAN)
together with math symbols from {\tt newtx}. In my opinion, the {\tt STIX2} fonts are a great improvement over Times based fonts---more readable because of the increased x-height, fewer spacing issues with overly sloped Italic shapes, and  handsome new Greek glyphs.

The symbols in {\tt newtx} are thinner than those in {\tt STIX2}. The usual unit used in PostScript based fonts is one thousandth of the {em} value specified in the font, and that is almost always 10{\tt bp}. So, think of 100 units as 1{\tt bp}. In terms of these units, the line width is 56 units in {\tt newtxmath} versus 66 for {\tt STIX2} and the appearance is therefore somewhat lighter. The math axis with {\tt newtxmath} is 2 units less than with {\tt STIX2}, an amount that few would notice. There is no mixing of 
math symbols from {\tt STIX2}  and {\tt newtx}  where the difference in math axis, however small, might be observable.
\section{Options and Macros}
{\tt scaled, scale:}\\
Rescales every font in the package. E.g.,
\begin{verbatim}
\usepackage[scaled=1.15}{stickstootext}
\end{verbatim}
scales everything up by 15\%.

{\tt proportional (p)}, {\tt tabular}, {\tt lining}, {\tt osf}:\\
These affect the figure style used in text. The default is {\tt tabular}, {\tt lining}, which is appropriate for math usage, and will be used in math mode no matter how you set the text figure option. E.g., {\tt p,osf} sets the text figure style to {\tt proportional}, {\tt oldstyle}, like \textosf{12345}. 

{\tt scosf:}\\
This option makes {\tt osf} the default figure style in small cap text. 

{\tt theoremfont:}\\
This option redefines the meaning of \verb|\textsl| and substitutes it for \verb|\textit| in the definition of the {\tt plain} theorem style. \textsl{This is how the Theorem font looks: it is Italic but has upright punctuation symbols :;!? etc. The figures are not changed, but should in many cases be tabular lining figures, accessed using, e.g.,\/} \verb|\texttlf|.

{\tt sups:}\\
This makes superior figures to be used in footnote markers except in minipages. This option is prevented when used the KOMA classes, with which it may conflict. The option has no effect if you define a non-default handling of footnote markers in your preamble before loading {\tt stickstootext}.

For other options, run
\begin{verbatim}
texdoc newtx
\end{verbatim}
and look at its discussion of the newtx text options.

\verb|\textosf, \texttosf, \textlf, \texttlf, \textsu, \textin, \textnu, \textde|:
These macros provide ways to use other figures styles, no matter what defaults may have been set. They denote respectively proportional oldstyle, tabular oldstyle, lining, tabular lining, superiors, inferiors, numerators and denominators, which render as follows.

\verb|\textosf{0123456789}| \textosf{0123456789}\\
\verb|\texttosf{0123456789}| \texttosf{0123456789}\\
\verb|\textlf{0123456789}| \textlf{0123456789}\\
\verb|\texttlf{0123456789}| \texttlf{0123456789}\\
\verb|\textsu{0123456789abc+-=()}| \textsu{0123456789abc+-=()}\\
\verb|\textin{0123456789}| \textin{0123456789}\\
\verb|\textnu{0123456789}| \textnu{0123456789}\\
\verb|\textde{0123456789}| \textde{0123456789}\\
\verb|\textsu{1}\textnu{1}; \textin{1}\textde{1}| \textsu{1}\textnu{1}; \textin{1}\textde{1} \verb|Note the differences in size and baseline|
\verb|\textfrac{17}{32}| \textfrac{17}{32} Constructs fractions using \verb|\textnu| and \verb|\textde|.
\section{Example preamble with math}
\textbf{Caution:} If you have an up-to-date distribution (TeXLive or MiKTeX), then there is an important change to note in the AMS classes, making the option {\tt noamsfonts} viable. If you plan to use the {\tt newtxmath} package with an AMS class, be sure to enable this option, as it will save you two of your precious sixteen math families. (The equivalent symbols are already in {\tt newtxmath} at a weight appropriate to Times and STIX2.)

\textsc{Example preamble:}
\begin{verbatim}
\usepackage[11pt,noamsfonts]{amsart}
\usepackage[p,osf]{stickstootext} % loads Helvetica as SF, newtxtt as TT
% T1 and textcomp are loaded by package. Change that here, if you want
% load replacements for Helvetica and newtxtt here, if desired
\usepackage{amsthm}% must be loaded before newtxmath
% amsmath and amssymb should not be loaded
\usepackage[stix,vvarbb]{newtxmath}
% vvarbb selects the STIX version of blackboard bold. See below.
\end{verbatim}
\section{Choices for blackboard bold}
Unless you specify in the options a choice (one of {\tt varbb}, {\tt vvarbb} for your blackboard bold alphabet, \verb|\mathbb| will use the default blackboard bold alphabet: $\mathbb{A\dots Z}$ (upper-case only), which I find unappealing because the uneven separations between boundaries makes the gray level likewise uneven. There are  two macros which provide access to other alphabets: 
\begin{itemize}
\item
\verb|\vmathbb| gives you $\vmathbb{A\dots z\imath\jmath}$;
\item \verb|\vvmathbb| gives you $\vvmathbb{A\dots z\imath\jmath}$.\end{itemize}
These work similarly to those in the original {\tt newtxmath}, but have some advantages.
\begin{itemize}
\item
both have full uppercase and lowercase, plus \verb|\imath| (dotlessi) and \verb|\jmath| (dotlessj);
\item both work correctly on strings with more than one token, correctly rendering digits, A..z, \verb|\imath| and \verb|\jmath| and passing through all other control sequences. \\
For example, \verb|\vmathbb{A\imath z\dots\jmath\alpha| renders as $\vmathbb{A\imath z\dots\jmath\alpha}$, the control sequences \verb|\dots| and \verb|\alpha| having passed through to LaTeX for processing. The macro \verb|\vvmathbb| works similarly and renders as STIX blackboard bold.
\end{itemize}

Option {\tt varbb} causes \verb|\mathbb| to be let equal to \verb|\varmathbb|, effectively {\tt DSSerif}, and option {\tt vvarbb} causes \verb|\mathbb| to be let equal to \verb|\vvmathbb|,  effectively STIX blackboard bold.

\section{PDF/A Compliance}
This pdf document was generated with 

was tested for compliance with {\tt PDFA-1b} a preamble containing the line
\begin{verbatim}
\usepackage[a-1b]{pdfx}
\end{verbatim}
It was tested for compliance with that standard using three different methods, with varying results, as is common.
\begin{itemize}
\item
\textsf{Adobe Acrobat Pro DC} says it does comply with PDF/A-1b.
\item The test site \url{https://www.pdf-online.com/osa/validate.aspx} affirms that also.
\item The free test package \textsf{veraPDF} says that it does not, and shows a number of issues about widths of glyphs and non-existence of some used glyphs. I found this report unconvincing, as when I merely removed the hints from the type 1 fonts, it gave me the same errors, but relating to a different set of glyphs. A message from its developers said that the type 1 fonts would fail the conformance test if their disassembled code contained a subroutine call before the width specification {\tt hsbw}, and that is common for type 1 fonts constructed using {\tt cfftot1} from the well known \textsf{LCDF Type Tools}. When I changed to using \textsf{FontForge} to construct the type 1 fonts, \textsf{veraPDF} gave a different set of problem glyphs, not overlapping the previous set. Finally, I removed all hints and received yet another set of problem glyphs.
\end{itemize}•


\section{Some math samples}
The following snippets mostly originated with the \TeX Book\ and were  adapted for \LaTeX\ from  Karl Berry's torture test for plain tex math fonts.

$x + y - z$, \quad $x + y * z$, \quad $z * y / z$, \quad 
$(x+y)(x-y) = x^2 - y^2$, 

$x \times y \cdot z = [x\, y\, z]$, \quad $x\circ y \bullet z$, \quad
$x\cup y \cap z$, \quad $x\sqcup y \sqcap z$, \quad

$x \vee y \wedge z$, \quad $x\pm y\mp z$, \quad
$x=y/z$, \quad $x:=y$, \quad $x\le y \ne z$, \quad $x \sim y \simeq z$
$x \equiv y \nequiv z$, \quad $x\subset y \subseteq z$

$\sin2\theta=2\sin\theta\cos\theta$, \quad
$\hbox{O}(n\log n\log n)$, \quad
$\Pr(X>x)=\exp(-x/\mu)$,

$\bigl(x\in A(n)\bigm|x\in B(n)\bigr)$, \quad
$\bigcup_n X_n\bigm\|\bigcap_n Y_n$

% page 178

In text matrices $\binom{1\,1}{0\,1}$ and $\bigl(\genfrac{}{}{0pt}{}{a}{1}\genfrac{}{}{0pt}{}{b}{m}\genfrac{}{}{0pt}{}{c}{n}\bigr)$

% page 142

$$a_0+\frac1{\displaystyle a_1 +
{\strut \frac1{\displaystyle a_2 +
{\strut \frac1{\displaystyle a_3 +
{\strut \frac1{\displaystyle a_4}}}}}}}$$

% page 143

$$\binom{p}{2}x^2y^{p-2} - \frac1{1 - x}\frac{1}{1 - x^2}
=
\frac{a+1}{b}\bigg/\frac{c+1}{d}.$$

%% page 145

$$\sqrt{1+\sqrt{1+\sqrt{1+\sqrt{1+\sqrt{1+x}}}}}$$

%% page 147

$$\left(\frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2}\right)
\bigl|\varphi(x+iy)\bigr|^2=0$$

%% page 149

% $$\pi(n)=\sum_{m=2}^n\left\lfloor\biggl(\sum_{k=1}^{m-1}\bigl
% \lfloor(m/k)\big/\lceil m/k\rceil\bigr\rfloor\biggr)^{-1}\right\rfloor.$$

$$\pi(n)=\sum_{m=2}^n\left\lfloor\Biggl(\sum_{k=1}^{m-1}\bigl
\lfloor(m/k)\big/\lceil m/k\rceil\bigr\rfloor\Biggr)^{-1}\right\rfloor.$$

% page 168

$$\int_0^\infty \frac{t - i b}{t^2 + b^2}e^{iat}\,dt=e^{ab}E_1(ab), \quad
a,b > 0.$$

% page 176

$$\mathbf{A} \coloneq \begin{pmatrix}x-\lambda&1&0\\
0&x-\lambda&1\\
0&0&x-\lambda\end{pmatrix}.$$

$$\left\lgroup\begin{matrix}a&b&c\\ d&e&f\\\end{matrix}\right\rgroup
\left\lgroup\begin{matrix}u&x\cr v&y\cr w&z\end{matrix}\right\rgroup$$

% page 177

$$\mathbf{A} = \begin{pmatrix}a_{11}&a_{12}&\ldots&a_{1n}\\
a_{21}&a_{22}&\ldots&a_{2n}\\
\vdots&\vdots&\ddots&\vdots\\
a_{m1}&a_{m2}&\ldots&a_{mn}\end{pmatrix}$$

$$\mathbf{M}=\bordermatrix{&C&I&C'\cr
C&1&0&0\cr I&b&1-b&0\cr C'&0&a&1-a}$$

%% page 186

$$\sum_{n=0}^\infty a_nz^n\qquad\hbox{converges if}\qquad
|z|<\Bigl(\limsup_{n\to\infty}\root n\of{|a_n|}\,\Bigr)^{-1}.$$

$$\frac{f(x+\Delta x)-f(x)}{\Delta x}\to f'(x)
\qquad \hbox{as $\Delta x\to0$.}$$

$$\|u_i\|=1,\qquad u_i\cdot u_j=0\quad\hbox{if $i\ne j$.}$$

%% page 191

$$\it\hbox{The confluent image of}\quad
\begin{Bmatrix}\hbox{an arc}\hfill\\\hbox{a circle}\hfill\\
\hbox{a fan}\hfill\\\end{Bmatrix}
\quad\hbox{is}\quad
\begin{Bmatrix}\hbox{an arc}\hfill\\
\hbox{an arc or a circle}\hfill\\
\hbox{a fan or an arc}\hfill\end{Bmatrix}.$$

%% page 191

\begin{align*}
T(n)\le T(2^{\lceil\lg n\rceil})
&\le c(3^{\lceil\lg n\rceil}-2^{\lceil\lg n\rceil})\\
&<3c\cdot3^{\lg n}\\
&=3c\,n^{\lg3}.
\end{align*}

%\begin{align*}
%\left\{%
%\begin{gathered}\alpha&=f(z)\\ \beta&=f(z^2)\\ \gamma&=f(z^3)
%\end{gathered}
%\right\}
%\qquad
%\left\{%
%\begin{gathered}
%x&=\alpha^2-\beta\\ y&=2\gamma
%\end{gathered}
%\right\}%
%\end{align*}

%$$\left\{
%\begin{align}
%\alpha&=f(z)\cr \beta&=f(z^2)\cr \gamma&=f(z^3)\\
%%\end{align}
%\right\}
%\qquad
%\left\{
%%\begin{align}
%x&=\alpha^2-\beta\cr y&=2\gamma\\
%\end{align}
%\right\}.$$
%%% page 192

\begin{align*}
\begin{aligned}
(x+y)(x-y)&=x^2-xy+yx-y^2\\
&=x^2-y^2\\
(x+y)^2&=x^2+2xy+y^2.
\end{aligned}
\end{align*}

%% page 192

\begin{align*}
\begin{aligned}
\biggl(\int_{-\infty}^\infty e^{-x^2}\,dx\biggr)^2
&=\int_{-\infty}^\infty\int_{-\infty}^\infty e^{-(x^2+y^2)}\,dx\,dy\\
&=\int_0^{2\pi}\int_0^\infty e^{-r^2}\,dr\,d\theta\\
&=\int_0^{2\pi}\biggl(e^{-\frac{r^2}{2}}
\biggl|_{r=0}^{r=\infty}\,\biggr)\,d\theta\\
&=\pi.
\end{aligned}
\end{align*}


%% page 197

$$\prod_{k\ge0}\frac{1}{(1-q^kz)}=
\sum_{n\ge0}z^n\bigg/\!\!\prod_{1\le k\le n}(1-q^k).$$

$$\sum_{\substack{\scriptstyle 0< i\le m\\\scriptstyle0<j\le n}}p(i,j) \,\ne
%
% $$\sum_{i=1}^p \sum_{j=1}^q \sum_{k=1}^r a_{ij} b_{jk} c_{ki}$$
%
\sum_{i=1}^p \sum_{j=1}^q \sum_{k=1}^r a_{ij} b_{jk} c_{ki} \,\ne
%
\sum_{\substack{\scriptstyle 1\le i\le p \\ \scriptstyle 1\le j\le q\\
\scriptstyle 1\le k\le r}} a_{ij} b_{jk} c_{ki}$$

$$\max_{1\le n\le m}\log_2P_n \quad \hbox{and} \quad
\lim_{x\to0}\frac{\sin x}{x}=1$$

$$p_1(n)=\lim_{m\to\infty}\sum_{\nu=0}^\infty\bigl(1-\cos^{2m}(\nu!^n\pi/n)\bigr)$$


\section{Caution}
This work should be considered experimental, being based essentially on a beta version of {\tt STIX2}. This package may change substantially as that family changes. Moreover, some features of this package and  {\tt newtxmath} with {\tt stix2} option may change in a way that is incompatible with previous versions. It is also possible that it will not follow the evolution of {\tt STIX2}.
 \end{document}  
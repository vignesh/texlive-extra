# Anonymous Pro fonts for LaTeX #

This work provides the necessary files to use the monowidth Anonymous
Pro fonts with LaTeX.  Anonymous Pro is a set of four truetype fonts
provided by
[Mark Simonson](http://www.marksimonson.com/fonts/view/anonymous-pro)
under the Open Font License [(OFL)](http://scripts.sil.org/OFL),
version 1.1.  The truetype fonts are copyright (c) 2009 Mark Simonson.

The LaTeX package is released under the LaTeX Project Public License
[(LPPL)](http://www.latex-project.org/lppl.txt) v1.3c or later,
copyright (c) 2013-2019 Arash Esbati.

	v2.2, 2019/07/07
	* Add the `scaled' key

	v2.1, 2014/03/22
	* Remap `visiblespace' to Anonymous Pro glyph `openbox' (uni2423)
	  in `anonymouspro-fixlatin.mtx' which looks less heavy
	* Minor documentation updates

	v2.0, 2013/12/29
	* Reorganize the fontinst driver file so that all glyphs are
	  available from the fonts
	* Updated documentation and added a font table

	v1.1, 2013/12/12
	* Regenerate .afm and .pfb from .ttf with FontForge (was
	  ttftopt1).  Now the fonts pass `t1lint' without warnings

	v1.0, 2013/12/06
	* Initial version

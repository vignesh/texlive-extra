cmathbb font
----------

The font was designed by Conden Chao with Adobe Illustrator 2018 and FontLab 7 on September 9-15, 2020. It contains all the digits, and Latin letters uppercase and lowercase.

Published under the LaTeX Project Public License 1.3c. See 
https://www.ctan.org/license/lppl1.3c
for the details of that license.

cmathbb pcakge
----------

 The package was created by Saravanan Murugaiah on September 10-17, 2020. 

(La)TeX support by Conden Chao also released under the LaTeX Project Public License 1.3c. See 
https://www.ctan.org/license/lppl1.3c
for the details of that license.

For usage, see cmathbb.pdf

Current version
---------------

1.0, 2020-09-17


Contact Details
---------------

Conden Chao
zhaoxy00@126.com

Saravanan Murugaiah
saravanan.murugaiah@gmail.com


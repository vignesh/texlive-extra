\documentclass[a4paper,12pt]{scrartcl}

\usepackage{amsmath, array, varioref}
\usepackage[british]{babel}
\usepackage{fourier-otf}
\setsansfont{Cabin}[Scale=MatchLowercase]
\setmonofont{Inconsolatazi4}[Scale=MatchLowercase,
                             HyphenChar=None,StylisticSet={2,3}]
\usepackage{microtype}

\newcommand*{\FGUT}{Fourier-GUT\textit{enberg}}
\newcommand*{\FOTF}{Erewhon-Math}
\newcommand*{\pkg}[1]{\texttt{#1}}
\newcommand*{\file}[1]{\texttt{#1}}
\newcommand*{\opt}[1]{\texttt{#1}}
\newcommand*{\cmd}[1]{\texttt{\textbackslash #1}}
\newcommand*{\showtchar}[1]{\cmd{#1}~\csname #1\endcsname}
\newcommand*{\showmchar}[1]{\cmd{#1}~$(\csname #1\endcsname)$}
\newcommand*{\showmchardollar}[1]{\texttt{\$\cmd{#1}\$}~$(\csname #1\endcsname)$}

\renewcommand{\labelitemi}{\lefthand}

\title{\decofourleft\,\FOTF\,\decofourright}
\author{Daniel Flipo \\ \texttt{daniel.flipo@free.fr}}

\newcommand*{\version}{0.45}

\begin{document}
\maketitle

\section{What is \FOTF{}?}

\FOTF{} is an Utopia based Opentype mathematical font.
The mathematical symbols and Greek letters are borrowed or derived from
Michel Bovani’s \FGUT, Latin letters and digits are borrowed from
Michael Shape’s Erewhon font.

It requires LuaTeX or XeTeX as engine and the \pkg{unicode-math} package%
\footnote{Please read the documentation \file{unicode-math.pdf}.}.

It is meant to be used with Utopia based Opentype text fonts like {Erewhon}.
For \FGUT{} users who want to switch to LuaLaTeX or XeLaTeX, the
file \file{fourier-otf.sty} can be used as a replacement of \file{fourier.sty}.

Please note that the current version (\version) is \emph{experimental,
do expect metrics and glyphs to change} until version 1.0 is reached.
Comments, suggestions and bug reports are welcome!

\section{Usage}

\subsection{Calling \cmd{setmathfont}}

A basic call for \FOTF{} would be:
\begin{verbatim}
\usepackage{unicode-math}
\setmathfont{Erewhon-Math.otf} % Call by file name or
\setmathfont{Erewhon Math}     % Call by font name
\end{verbatim}
this loads \FOTF{} as math font with the default options, see
subsections~\vref{ssection-um}, \vref{ssection-cv} and~\vref{ssection-ss}
for customisation.

Please note that the three sets of text fonts have to be chosen separately,
f.i.:
\begin{verbatim}
\setmainfont{Erewhon}                              % rm
\setsansfont{Cabin}[Scale=MatchLowercase]          % sf
\setmonofont{Inconsolatazi4}[Scale=MatchLowercase] % tt
\end{verbatim}
otherwise you would get Latin Modern for text fonts.

\subsection{Calling \pkg{fourier-otf.sty}}

As an alternative to load \FOTF{} --\emph{this is the recommended way}--
you can type:\\[.5\baselineskip]
\verb+\usepackage[ +\textit{options}
\footnote{Possible \textit{options} are \opt{loose} or any of the options
  described below for \cmd{setmathfont}.}%
\verb+ ]{fourier-otf}+\\[.5\baselineskip]
it also loads \pkg{unicode-math} with the default options and and sets
\file{Fourier Math} as Math font but does a bit more:
\begin{enumerate}
\item it checks at \verb+\begin{document}+ if packages \pkg{amssymb} or
  \pkg{latexsym} are loaded and issues warnings in case they are;
\item it provides aliases for glyphs named differently in Unicode, so that
  \pkg{latexsymb} or AMS names are also available;
\item it loads \pkg{fourier-orns.sty}, providing many text ornaments;
\item it defines specific Math characters like \showmchar{Bbbbackslash},
  \showmchar{varemptyset}, \linebreak[4]
  \showmchar{parallelslant},
  \showmchar{shortparallelslant}, etc.;
\item it reduces spacing in math mode: \cmd{thinmuskip}, \cmd{medmuskip}
  and \cmd{thickmuskip} are reduced as in \file{fourier.sty}.
%  \verb+\thinmuskip=2mu+,\\
%  \verb+\medmuskip=2.5mu plus 1mu minus 2.5mu+,\\
%  \verb+\thickmuskip=3.5mu plus 2.5mu+.\\
  The option \opt{loose} disables these settings.
\end{enumerate}


\section{What is provided?}

\FOTF{} provides all glyphs supplied by \FGUT{} plus all glyphs available in
the \pkg{amssymb} and \pkg{latexsym} packages and many more.  Therefore, these
two packages \emph{should not} be loaded as they might override \FOTF{} glyphs.

Sans-serif, typewriter and fraktur styles are borrowed from Latin Modern fonts.
See in section~\vref{ssec-math-alphabets} how to choose
from other Math fonts for these styles.

A full list of available glyphs is shown in file \file{unimath-erewhon.pdf}.

\subsection{Upright or slanted?}
\label{ssection-um}

Package \pkg{unicode-math} follows \TeX{} conventions for Latin and Greek
letters: in math mode, the default option (\opt{math-style=TeX}) prints
Latin letters $a$…$z$ $A$…$Z$ and lowercase greek letters $\alpha$…$\omega$
slanted (italic) while uppercase greek letters $\Alpha \Beta \Gamma$…$\Omega$
are printed upright.
This can be changed by option \opt{math-style} as shown in
table~\vref{math-style}.

\begin{table}[ht]
  \centering
  \caption{Effects of the \opt{math-style} package option.}
  \label{math-style}
  \begin{tabular}{@{}>{\ttfamily}lcc@{}}
    \hline
      \rmfamily Package option & Latin & Greek \\
    \hline
      math-style=ISO & $(a,z,B,X)$ & $\symit{(\alpha,\beta,\Gamma,\Xi)}$ \\
      math-style=TeX & $(a,z,B,X)$ & $(\symit\alpha,\symit\beta,\symup\Gamma,\symup\Xi)$ \\
      math-style=french & $(a,z,\symup B,\symup X)$ & $(\symup\alpha,\symup\beta,\symup\Gamma,\symup\Xi)$ \\
      math-style=upright & $(\symup a,\symup z,\symup B,\symup X)$ & $(\symup\alpha,\symup\beta,\symup\Gamma,\symup\Xi)$ \\
    \hline
  \end{tabular}
\end{table}

Bold letters are printed upright except lowercase Greek letters
which are slanted (the default option is \opt{bold-style=TeX}). This can be
changed by option \opt{bold-style} as shown in table~\vref{bold-style}.

\begin{table}[ht]
  \centering
  \caption{Effects of the \opt{bold-style} package option.}
  \label{bold-style}
  \begin{tabular}{@{}>{\ttfamily}lcc@{}}
    \hline
      \rmfamily Package option & Latin & Greek \\
    \hline
      bold-style=ISO & $(\symbfit a, \symbfit z, \symbfit B, \symbfit X)$ & $(\symbfit\alpha, \symbfit\beta, \symbfit\Gamma, \symbfit\Xi)$ \\
      bold-style=TeX & $(\symbfup a,\symbfup z,\symbfup B,\symbfup X)$ & $(\symbfit\alpha, \symbfit\beta,\symbfup \Gamma,\symbfup \Xi)$ \\
      bold-style=upright & $(\symbfup a,\symbfup z,\symbfup B,\symbfup X)$ & $(\symbfup \alpha,\symbfup \beta,\symbfup \Gamma,\symbfup \Xi)$ \\
    \hline
  \end{tabular}
\end{table}

Other possible customisation: $\nabla$ is printed upright and $\partial$ is
printed slanted by default, but \opt{nabla=italic} and
\opt{partial=upright} can change this.

All these options are offered by the \pkg{unicode-math} package but they can
be added to the \cmd{setmathfont} call%
\footnote{IMHO it is easier to add \emph{all options} to the \cmd{setmathfont}
  command.}, for example:

\verb+\setmathfont{Erewhon Math}[math-style=french,partial=upright]+\\
will print for the code
\begin{verbatim}
\[ \frac{\partial f}{\partial x} = \alpha \symbf{V} + a\nabla\Gamma
                                 + \symbf{\beta}\symbf{M} \]
\end{verbatim}
\setmathfont{Erewhon Math}[math-style=french,partial=upright]
\[\frac{\partial f}{\partial x} = \alpha \symbf{V} + a\nabla\Gamma +
              \symbf{\beta}\symbf{M} \]
while the default settings would print
\setmathfont{Erewhon Math}[math-style=TeX,partial=italic]
\[\frac{\partial f}{\partial x} = \alpha \symbf{V} + a\nabla\Gamma +
              \symbf{\beta}\symbf{M} \]

If your text editor is able to handle greek letters or math symbols, they can
be entered in the code instead control sequences (i.e.
$\symup{α}$, $\symup{β}$, $\symup{Γ}$,… for \cmd{alpha}, \cmd{beta},
\cmd{Gamma},…).

\subsection{Character variants}
\label{ssection-cv}

\FOTF{} provides eleven ``Character Variants’’ options to choose between
different glyphs for Greek characters and some others, see table~\vref{cv}
for the full list.

For instance, to get \cmd{epsilon} and \cmd{phi} typeset as $\varepsilon$
and $\varphi$ instead of $\epsilon$ and $\phi$, you can add option
\verb+CharacterVariant={3,6}+ to the \cmd{setmathfont} call:
\begin{verbatim}
\setmathfont{Erewhon Math}[CharacterVariant={3,6}]
\end{verbatim}

This  works for all shapes and weights of these characters:
\verb+$\symit{\epsilon}$+,\\
\verb+$\symup{\epsilon}$+, \verb+$\symbf{\epsilon}$+,
\verb+$\symbfit{\epsilon}$+ are output as
\setmathfont{Erewhon Math}[CharacterVariant={3,6}]
$\symit{\epsilon}$, $\symup{\epsilon}$, $\symbf{\epsilon}$, $\symbfit{\epsilon}$
instead of
\setmathfont{Erewhon Math}
$\symit{\epsilon}, \symup{\epsilon}, \symbf{\epsilon}, \symbfit{\epsilon}$.

The same is true for \verb+\phi+ of course. Please note that curly braces are
mandatory whenever more than one ``Character Variant’’ is selected.

\begin{table}[ht]
  \centering
  \caption{Character variants.}
  \label{cv}
  \begin{tabular}{@{}>{\ttfamily}lccl@{}}
    \hline
           & Default       & Variant          & Name\\
    \hline
      cv00 & $0$           & $\mupvarzero$    &  0 \\
      cv01 & $\hslash$     & $\mithbar$       & \cmd{hslash} \\
      cv02 & $\emptyset$   & $\varemptyset$   & \cmd{emptyset} \\
      cv03 & $\mitepsilon$ & $\mitvarepsilon$ & \cmd{epsilon} \\
      cv04 & $\mitkappa$   & $\mitvarkappa$   & \cmd{kappa} \\
      cv05 & $\mitpi$      & $\mitvarpi$      & \cmd{pi} \\
      cv06 & $\mitphi$     & $\mitvarphi$     & \cmd{phi} \\
      cv07 & $\mitrho$     & $\mitvarrho$     & \cmd{rho} \\
      cv08 & $\mitsigma$   & $\mitvarsigma$   & \cmd{sigma} \\
      cv09 & $\mittheta$   & $\mitvartheta$   & \cmd{theta} \\
      cv10 & $\mitTheta$   & $\mitvarTheta$   & \cmd{Theta} \\
    \hline
  \end{tabular}
\end{table}

Note about \cmd{hbar} (v\,0.43): \pkg{unicode-math} defines \cmd{hbar} as
\cmd{hslash} (U+210F) while \pkg{amsmath} provides two different glyphs
(italic h with horizontal or diagonal stroke).\\
\pkg{kpfonts-otf} now follows \pkg{unicode-math}; the italic h with horizontal
stroke can be printed using \cmd{hslash} or \cmd{hbar} together with character
variant \texttt{cv01} or with \cmd{mithbar} (replacement for AMS’ command
\cmd{hbar}).


\subsection{Stylistic sets}
\label{ssection-ss}

\FOTF{} provides four ``Stylistic Sets’’ options to choose between different
glyphs for families of mathematical symbols.

\verb+StylisticSet=4+, alias%
\footnote{These \texttt{Style} aliases are provided by \file{fourier-otf.sty}.}
\verb+Style=leqslant+, converts (large) inequalites into their slanted
variants as shown by table~\vref{ss04}.
\begin{table}[ht]
  \centering
  \caption{Stylistic Set 4 -- \texttt{leqslant} (slanted inequalities)}
  \label{ss04}
  \begin{tabular}{@{}lcc@{}}
    \hline
      Command           & Default         & Variant \\
    \hline
      \cmd{leq}         & $\leq$         & $\leqslant$ \\
      \cmd{geq}         & $\geq$         & $\geqslant$ \\
      \cmd{nleq}        & $\nleq$        & $\nleqslant$ \\
      \cmd{ngeq}        & $\ngeq$        & $\ngeqslant$ \\
      \cmd{leqq}        & $\leqq$        & $\leqqslant$ \\
      \cmd{geqq}        & $\geqq$        & $\geqqslant$ \\
      \cmd{eqless}      & $\eqless$      & $\eqslantless$ \\
      \cmd{eqgtr}       & $\eqgtr$       & $\eqslantgtr$ \\
      \cmd{lesseqgtr}   & $\lesseqgtr$   & $\lesseqslantgtr$ \\
      \cmd{gtreqless}   & $\gtreqless$   & $\gtreqslantless$ \\
      \cmd{lesseqqgtr}  & $\lesseqqgtr$  & $\lesseqqslantgtr$ \\
      \cmd{gtreqqless}  & $\gtreqqless$  & $\gtreqqslantless$ \\
   \hline
  \end{tabular}
\end{table}

\verb+StylisticSet=5+, alias \verb+Style=smaller+, converts some symbols into
their smaller variants as shown by table~\vref{ss05}.
\begin{table}[ht]
  \centering
  \caption{Stylistic Set 5 -- \texttt{smaller} (smaller variants)}
  \label{ss05}
  \begin{tabular}{@{}lcc@{}}
    \hline
      Command                & Default             & Variant \\
    \hline
      \cmd{in}               & $\in$               & $\smallin$ \\
      \cmd{ni}               & $\ni$               & $\smallni$ \\
      \cmd{mid}              & $\mid$              & $\shortmid$ \\
      \cmd{nmid}             & $\nmid$             & $\nshortmid$ \\
      \cmd{parallel}         & $\parallel$         & $\shortparallel$ \\
      \cmd{nparallel}        & $\nparallel$        & $\nshortparallel$ \\
      \cmd{parallelslant}    & $\parallel$         & $\shortparallelslant$ \\
      \cmd{nparallelslant}   & $\nparallelslant$   & $\nshortparallelslant$ \\
   \hline
  \end{tabular}
\end{table}

\verb+StylisticSet=6+, alias \verb+Style=subsetneq+, converts some inclusion
symbols as shown by table~\vref{ss06}.
\begin{table}[ht]
  \centering
  \caption{Stylistic Set 6 -- \texttt{subsetneq} (inclusion variants)}
  \label{ss06}
  \begin{tabular}{@{}lcc@{}}
    \hline
      Command           & Default         & Variant \\
    \hline
      \cmd{subsetneq}   & $\subsetneq$    & $\varsubsetneq$ \\
      \cmd{supsetneq}   & $\supsetneq$    & $\varsupsetneq$ \\
      \cmd{subsetneqq}  & $\subsetneqq$   & $\varsubsetneqq$ \\
      \cmd{supsetneqq}  & $\supsetneqq$   & $\varsupsetneqq$ \\
   \hline
  \end{tabular}
\end{table}

\verb+StylisticSet=7+, alias \verb+Style=parallelslant+, converts
``parallel’’ symbols into their slanted variants as shown by table~\vref{ss07}.
\begin{table}[ht]
  \centering
  \caption{Stylistic Set 7 -- \texttt{parallelslant} (slanted variants)}
  \label{ss07}
  \begin{tabular}{@{}lcc@{}}
    \hline
      Command               & Default            & Variant \\
    \hline
      \cmd{parallel}        & $\parallel$        & $\parallelslant$ \\
      \cmd{nparallel}       & $\nparallel$       & $\nparallelslant$ \\
      \cmd{shortparallel}   & $\shortparallel$   & $\shortparallelslant$ \\
      \cmd{nshortparallel}  & $\nshortparallel$  & $\nshortparallelslant$ \\
   \hline
  \end{tabular}
\end{table}

To enable Stylistic Sets 4, 6 and 7 for \FOTF{}, you should enter
\begin{verbatim}
\setmathfont{Erewhon Math}[StylisticSet={4,6,7}]  or
\usepackage[Style={leqslant,subsetneq,parallelslant}]{fourier-otf}
\end{verbatim}
then, \verb+\[x\leq y \quad A \subsetneq B\quad D \parallel D' \]+
will print as
\setmathfont{Erewhon Math}[StylisticSet={4,6,7}]
\[x\leq y \quad A \subsetneq B\quad D \parallel D' \]
instead of
\setmathfont{Erewhon Math}
\[x\leq y \quad A \subsetneq B\quad D \parallel D' \]

\clearpage
\subsection{Standard \LaTeX{} math commands}
\label{ssec-math-commands}

All standard \LaTeX{} math commands, all \pkg{amssymb} commands and all
\pkg{latexsym} commands are supported by \FOTF{}, for some of them loading
\pkg{fourier-otf.sty} is required.

Various wide acccents are also supported:
\begin{itemize}
\item \cmd{widehat} and \cmd{widetilde}
\[\widehat{x}\; \widehat{xx} \;\widehat{xxx} \;\widehat{xxxx}\;
  \widehat{xxxxx} \;\widehat{xxxxxx} \;\widetilde{x}\; \widetilde{xx}\;
  \widetilde{xxx} \;\widetilde{xxxx} \;\widetilde{xxxxx}\;
  \widetilde{xxxxxx}\]

\item \cmd{overline} and \cmd{underline}
  \[\overline{x}\quad \overline{xy}\quad \overline{xyz}\quad
    \overline{A\cup B}\quad \overline{A\cup (B\cap C)\cup D}\quad
    \underline{m+n+p}\]

\item \cmd{wideoverbar}, \cmd{widecheck}  and \cmd{widebreve}
  \[\wideoverbar{x}\quad \wideoverbar{xy}\quad \wideoverbar{xyz}\quad
    \widecheck{x}\quad \widecheck{xxxx}\quad \widecheck{xxxxxx}\quad
    \widebreve{x}\quad \widebreve{xxxx}\quad \widebreve{xxxxxx}
  \]

\item \cmd{overparen} and \cmd{underparen}
  \[\overparen{x}\quad \overparen{xy}\quad \overparen{xyz}\quad
    \mathring{\overparen{A\cup B}}\quad
    \overparen{A\cup (B\cap C)\cup D}^{\smwhtcircle}\quad
    \overparen{x+y}^{2}\quad \overparen{a+b+...+z}^{26}\]

\[\underparen{x}\quad \underparen{xz} \quad \underparen{xyz}
  \quad \underparen{x+z}_{2}\quad \underparen{a+b+...+z}_{26}\]

\item \cmd{overbrace} and \cmd{underbrace}
  \[\overbrace{a}\quad \overbrace{ab}\quad \overbrace{abc}\quad
  \overbrace{abcd}\quad \overbrace{abcde}\quad
  \overbrace{a+b+c}^{3}\quad \overbrace{ a+b+. . . +z }^{26}\]

\[\underbrace{a}\quad\underbrace{ab}\quad\underbrace{abc}\quad
  \underbrace{abcd}\quad \underbrace{abcde}\quad
  \underbrace{a+b+c}_{3}  \quad \underbrace{ a+b+...+z }_{26}\]

\item \cmd{overbracket} and \cmd{underbracket}
  \[\overbracket{a}\quad \overbracket{ab}\quad \overbracket{abc}\quad
  \overbracket{abcd}\quad \overbracket{abcde}\quad
  \overbracket{a+b+c}^{3}\quad \overbracket{ a+b+. . . +z }^{26}\]

\[\underbracket{a}\quad\underbracket{ab}\quad\underbracket{abc}\quad
  \underbracket{abcd}\quad \underbracket{abcde}\quad
  \underbracket{a+b+c}_{3}  \quad \underbracket{ a+b+...+z }_{26}\]

\item \cmd{overrightarrow} and \cmd{overleftarrow}
  \[\overrightarrow{v}\quad \overrightarrow{M}\quad \overrightarrow{vv}
  \quad \overrightarrow{AB}\quad \overrightarrow{ABC}
  \quad \overrightarrow{ABCD} \quad \overrightarrow{ABCDEFGH}.
\]

\[\overleftarrow{v}\quad \overleftarrow{M}\quad \overleftarrow{vv}
  \quad \overleftarrow{AB}\quad \overleftarrow{ABC}
  \quad \overleftarrow{ABCD} \quad \overleftarrow{ABCDEFGH}\]

\item Finally \cmd{widearc} and \cmd{overrightarc} (loading
  \pkg{fourier-otf.sty} is required)
\[\widearc{AMB}\quad \overrightarc{AMB}\]
\end{itemize}

\subsection{Mathematical alphabets}
\label{ssec-math-alphabets}

\begin{itemize}
\item  All Latin and Greek characters are available in italic, upright, bold
  and bold italic via the \verb+\symit{}+, \verb+\symup{}+, \verb+\symbf{}+
  and \verb+\symbfit{}+ commands.

\item Calligraphic alphabet (\cmd{symscr} or \cmd{symcal} or
  \cmd{mathcal} command), uppercase only:

  $\symscr{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$

\item Blackboard-bold alphabet (\cmd{symbb} or \cmd{mathbb} command),
  uppercase, lowercase and digits:

  $\symbb{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$

  $\symbb{abcdefghijklmnopqrstuvwxyz\quad 0123456789}$

\item Fraktur alphabet is borrowed from Latin Modern,

  $\symfrak{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$

  but this can overwritten, i.e.
\begin{verbatim}
\setmathfont{STIX2Math.otf}[range=frak,Scale=MatchUppercase]
$\symfrak{ABCDEFGHIJKL...XYZ abcdefghijkl...xyz}$
\end{verbatim}
\setmathfont{STIX2Math.otf}[range=frak,Scale=MatchUppercase]
$\symfrak{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$

\item Sans serif alphabet is borrowed from Latin Modern,

$\symsfup{ABCDEFGHIJKLM abcdefghijk}\quad\symsfit{NOPQRSTUVWXYZ mnopqrstuvwxyz}$

  but it can be borrowed from another Math font, i.e.
\begin{verbatim}
\setmathfont{STIX2Math.otf}[range={sfup,sfit},
                            Scale=MatchUppercase]
$\symsfup{ABCD...klm}\quad\symsfit{NOPQ...xyz}$
\end{verbatim}
\setmathfont{STIX2Math.otf}[range={sfup,sfit},Scale=MatchUppercase]
$\symsfup{ABCDEFGHIJKLM abcdefghijklm}\quad
    \symsfit{NOPQRSTUVWXYZ nopqrstuvwxyz}$

\item Typewriter alphabet  is borrowed from Latin Modern,

  $\symtt{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$

but it can be borrowed from  another Math font, i.e.
\begin{verbatim}
\setmathfont{STIX2Math.otf}[range=tt,Scale=MatchUppercase]
$\symtt{ABCDE...XYZ abcde...xyz}$
\end{verbatim}
\setmathfont{STIX2Math.otf}[range=tt,Scale=MatchUppercase]
$\symtt{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$
\end{itemize}

\subsection{Missing symbols}

\FOTF{} does not aim at being as complete as \file{STIX2Math} or
\file{Cambria}, the current glyph coverage compares with TeXGyre Math fonts.
In case some symbols do not show up in the output file, you see warnings
in the \file{.log} file, for instance:

\setmathfont{STIX2Math.otf}[range={"2964}]
\texttt{Missing character: There is no }$⥤$%
\texttt{ (U+2964) in font ErewhonMath}

Borrowing them from a more complete font, say \file{STIX2Math},
is a possible workaround:
\verb+\setmathfont{STIX2Math.otf}[range={"2964},Scale=1.02]+\\
scaling is possible, multiple character ranges are separated with commas:\\
\verb+\setmathfont{STIX2Math.otf}[range={"294A-"2951,"2964,"2ABB-"2ABE}]+

\subsection{Fourier ornaments}

All logos and ornaments provided by \FGUT{} (\pkg{fourier-orns.sty}) are
available with \FOTF{} when loaded by \verb+\usepackage{fourier-otf}+.

\pkg{fourier-orns.sty} as of v2.0 automatically fetches its glyphs in
a specific OpenType font with LuaTeX or XeTeX engines and from
a Type\,1 font otherwise (pdfTeX).

\begin{raggedright}
\begin{itemize}

\item \showtchar{textpertenthousand}, \textit{\textpertenthousand},
  \textbf{\textpertenthousand},  \textbf{\textit{\textpertenthousand}},

\item A variant of the euro symbol: \showtchar{eurologo}, \textit{\eurologo},
  \textbf{\eurologo},  \textbf{\textit{\eurologo}},

\item A ``starred'' bullet: \showtchar{starredbullet},

\item Decos and logos: \showtchar{warning}, \showtchar{noway},
    \showtchar{textxswup}, \showtchar{textxswdown}, \showtchar{bomb},
  \showtchar{decoone}, \showtchar{decotwo}, \showtchar{decothreeleft},
    \showtchar{decothreeright},
  \showtchar{decofourleft}, \showtchar{decofourright}, \showtchar{decosix},
  \showtchar{floweroneleft}, \showtchar{floweroneright}, \showtchar{lefthand},
    \showtchar{righthand}.

\item Smileys: \showtchar{grimace}, \showtchar{textthing}.

\item Leaves: \showtchar{aldineleft}, \showtchar{aldineright},
   \showtchar{aldine}, \showtchar{aldinesmall}, %\\
  \showtchar{leafleft}, \showtchar{leafright}, \showtchar{leafNE},
  \showtchar{leafNW}, \showtchar{leafSE}, \showtchar{leafSW}.

\item Pilcrows: \showtchar{oldpilcrowone}, \showtchar{oldpilcrowtwo},
  \showtchar{oldpilcrowthree}, %\\
  \showtchar{oldpilcrowfour},
  \showtchar{oldpilcrowfive} aaaa, %\\
  \showtchar{oldpilcrowsix} aaaaaaaaaaa.
\end{itemize}
\end{raggedright}

Finally, some symbols are also provided in math mode, with other names:
\begin{itemize}
\item \showmchardollar{forbidden}, \showmchardollar{beware},
      \showmchardollar{boom},
\item \showmchardollar{thething} is a \emph{QED symbol}
  for a false proof. Of course, you don't need it!
\item \showmchardollar{xswordsup} and \showmchardollar{xswordsdown} may be used
  as tags for a debatted statement, or for anything else. $\xswordsdown$
\end{itemize}

\section{Acknowledgements}

All glyphs in \FOTF{} are borrowed or derived either from Erewhon fonts or
\FGUT{} package.  Many thanks to Michael Sharpe and Michel Bovani for
providing these.

I am grateful to George Williams and his co-workers for providing and
maintaining FontForge and to Ulrik Vieth for his illuminating paper published
in TUGboat~2009 Volume~30 about Open Type Math.

Thanks to Oliver Natt for providing valuable feedback!

\bigskip
\begin{center}\Huge
\decotwo
\end{center}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-master: t
%%% coding: utf-8
%%% End:

% !TEX TS-program = pdflatexmk
% Template file for TeXShop by Michael Sharpe, LPPL
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}
\pdfmapfile{+DSSerif.map}
\usepackage{graphicx}
%\usepackage{amssymb}% don't use with newtxmath
%SetFonts
% libertine+newtxmath
\usepackage[sb]{libertine} % use sb in place of bold
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata
\usepackage{amsmath,amsthm}
\usepackage[libertine,bigdelims,vvarbb]{newtxmath}
% option vvarbb gives you stix blackboard bold
\useosf % use oldstyle figures except in math
\usepackage{dsserif}
\usepackage{bm}
%SetFonts
\title{The DSSerif Package}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date

\begin{document}
\maketitle
DSSerif is short for Double Struck Serif, and, while based on the Courier  clone of URW++ (version 2), though much distorted, its double striking and weights are very much in the style of the STIX double struck fonts. The main difference between the two is that STIX is sans serif, while DSSerif is not. The only package option is {\tt scaled}, which may be used to scale the size, like
\begin{verbatim}
\usepackage[scaled=1.03]{dsserif}
\end{verbatim}


The available characters are:\\
In regular weight:
\[\mathbb{0}\;\mathbb{1}\;\mathbb{2}\;\mathbb{3}\;\mathbb{4}\;\mathbb{5}\;\mathbb{6}\;\mathbb{7}\;\mathbb{8}\;\mathbb{9}\]
\[\imathbb\;\jmathbb \quad\text{(math dotlessi, dotlessj.)}\]
\[{\mathbb{A}}\;{\mathbb{B}}\;{\mathbb{C}}\;{\mathbb{D}}\;{\mathbb{E}}\;{\mathbb{F}}\;{\mathbb{G}}\;{\mathbb{H}}\;{\mathbb{I}}\;{\mathbb{J}}\;{\mathbb{K}}\;{\mathbb{L}}\;{\mathbb{M}}\]
\[{\mathbb{N}}\;{\mathbb{O}}\;{\mathbb{P}}\;{\mathbb{Q}}\;{\mathbb{R}}\;{\mathbb{S}}\;{\mathbb{T}}\;{\mathbb{U}}\;{\mathbb{V}}\;{\mathbb{W}}\;{\mathbb{X}}\;{\mathbb{Y}}\;{\mathbb{Z}}\]
\[{\mathbb{a}}\;{\mathbb{b}}\;{\mathbb{c}}\;{\mathbb{d}}\;{\mathbb{e}}\;{\mathbb{f}}\;{\mathbb{g}}\;{\mathbb{h}}\;{\mathbb{i}}\;{\mathbb{j}}\;{\mathbb{k}}\;{\mathbb{l}}\;{\mathbb{m}}\]
\[{\mathbb{n}}\;{\mathbb{o}}\;{\mathbb{p}}\;{\mathbb{q}}\;{\mathbb{r}}\;{\mathbb{s}}\;{\mathbb{t}}\;{\mathbb{u}}\;{\mathbb{v}}\;{\mathbb{w}}\;{\mathbb{x}}\;{\mathbb{y}}\;{\mathbb{z}}\]
In bold:
{\boldmath
\[\mathbb{0}\;\mathbb{1}\;\mathbb{2}\;\mathbb{3}\;\mathbb{4}\;\mathbb{5}\;\mathbb{6}\;\mathbb{7}\;\mathbb{8}\;\mathbb{9}\]
\[\imathbb\;\jmathbb\]
\[{\mathbb{A}}\;{\mathbb{B}}\;{\mathbb{C}}\;{\mathbb{D}}\;{\mathbb{E}}\;{\mathbb{F}}\;{\mathbb{G}}\;{\mathbb{H}}\;{\mathbb{I}}\;{\mathbb{J}}\;{\mathbb{K}}\;{\mathbb{L}}\;{\mathbb{M}}\]
\[{\mathbb{N}}\;{\mathbb{O}}\;{\mathbb{P}}\;{\mathbb{Q}}\;{\mathbb{R}}\;{\mathbb{S}}\;{\mathbb{T}}\;{\mathbb{U}}\;{\mathbb{V}}\;{\mathbb{W}}\;{\mathbb{X}}\;{\mathbb{Y}}\;{\mathbb{Z}}\]
\[{\mathbb{a}}\;{\mathbb{b}}\;{\mathbb{c}}\;{\mathbb{d}}\;{\mathbb{e}}\;{\mathbb{f}}\;{\mathbb{g}}\;{\mathbb{h}}\;{\mathbb{i}}\;{\mathbb{j}}\;{\mathbb{k}}\;{\mathbb{l}}\;{\mathbb{m}}\]
\[{\mathbb{n}}\;{\mathbb{o}}\;{\mathbb{p}}\;{\mathbb{q}}\;{\mathbb{r}}\;{\mathbb{s}}\;{\mathbb{t}}\;{\mathbb{u}}\;{\mathbb{v}}\;{\mathbb{w}}\;{\mathbb{x}}\;{\mathbb{y}}\;{\mathbb{z}}\]}
If you load the {\tt dsserif} package using
\begin{verbatim}
\usepackage{dsserif}
\end{verbatim}
then
most of these are accessed in the usual ways using \verb|\mathbb|. E.g., \verb|\mathbb{0}|, \verb|\mathbb{A}| and \verb|\mathbb{z}| produce $\mathbb{0}$, $\mathbb{A}$ and $\mathbb{z}$ unless \verb|\boldmath| was previously specified, and {\boldmath $\mathbb{0}$, $\mathbb{A}$ and $\mathbb{z}$} otherwise. The dotless {\tt i} and {\tt j} are a bit different, and require the special macros \verb|\imathbb|, \verb|\jmathbb|. If you load the package {\tt bm}, then the macro \verb|\bm{}| will in all cases give you the bold version. E.g., \verb|\bm{\imathbb}| gives $\bm{\imathbb}$, as expected. Finally, the macro \verb|\mathbbb| may be used without \verb|\boldmath| or \verb|\bm| to render a bold symbol, e.g., \verb|\mathbbb{A}| gives $\mathbbb{A}$.

I like to use $\imathbb$ and $\jmathbb$ (or their bold versions) for unit vectors in the $x$ and $y$ directions, though this is not ISO compliant, and prefer the output to what I would get from the corresponding STIX symbols, where there can be problems distinguishing  unserifed glyphs.

The {\tt DSSerif} glyphs may also be accessed using {\tt mathalfa}:
\begin{verbatim}
\usepackage[bb=dsserif]{mathalfa}
\end{verbatim}
(added afer loading other math fonts) will redefine \verb|\mathbb| and \verb|\mathbbb| to point to the {\tt DSSerif} versions. Use of either {\tt dsserif} or {\tt mathalfa} will entail using at least one of your precious math groups. You may find it sufficient to simply use the symbols as text. E.g., 
\begin{verbatim}
$x\in \text{{\usefont{U}{DSSerif}{m}{n}C}}^n$
\end{verbatim}
renders as $x\in \text{{\usefont{U}{DSSerif}{m}{n}C}}^n$ without using an additional math group.

If using {\tt newtxmath}, version 1.55 or higher, with the {\tt stix2} option, you will find the {\tt DSSerif} alphabet built in, and it will not be necessary to load it with further commands. See the newtx documentation for further details.
 
\end{document}  
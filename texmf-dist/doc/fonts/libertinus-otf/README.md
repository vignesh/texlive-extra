# README #
Package libertinus-otf supports the free fonts from
ctan.org/fonts/libertinus  and defines the missing ones 
via several font feature settings.

% This file is distributed under the terms of the LaTeX Project Public
% License from CTAN archives in directory  macros/latex/base/lppl.txt.
% Either version 1.3 or, at your option, any later version.
%
%

% Copyright 2020 Herbert Voss hvoss@tug.org


%% $Id: spark-otf-doc.tex 1047 2019-04-12 17:22:07Z herbert $
%
\listfiles
\documentclass[fontsize=11pt,DIV=14,parskip=half-]{scrartcl}
\usepackage{fontspec}
\usepackage{graphicx}
\usepackage{multido,showexpl}
\usepackage[%usefilenames,
            TT={Scale=0.88,FakeStretch=0.9},
            SS={Scale=0.9},
            RM={Scale=0.9},
            DefaultFeatures={Ligatures=TeX}]{lucida-otf}  % support opentype math fonts
\usepackage[usefilenames=false]{spark-otf}  % support opentype spark fonts
\makeatletter
\let\SparkVersion\spark@version
\makeatother
\newsavebox\ZBox \newsavebox\SBox

\usepackage{biblatex}
\addbibresource{\jobname.bib}

\usepackage{dtk-logos} % for Wikipedia W

\pagestyle{headings}

\usepackage[colorlinks,hyperfootnotes=false]{hyperref}
% define \code for url-like breaking of typewriter fragments.
\ifx\nolinkurl\undefined \let\code\url \else \let\code\nolinkurl \fi

% Define \cs to prepend a backslash, and be unbreakable:
\DeclareRobustCommand\cs[1]{\mbox{\texttt{\char`\\#1}}}


\title{Support for the Spark OpenType fonts \\--\\ v.~\SparkVersion}
\author{Herbert Voß}
\begin{document}
\maketitle
\tableofcontents

\section{Introduction}

A sparkline is a very small line chart, typically drawn without axes or coordinates. 
It presents the general shape of the variation (typically over time) in some measurement, 
such as temperature or stock market price, in a simple and highly condensed way. 
Sparklines are small enough to be embedded in text, or several sparklines may be 
grouped together as elements of a small multiple. Whereas the typical chart is 
designed to show as much data as possible, and is set off from the flow of text, 
sparklines are intended to be succinct, memorable, and located where they are discussed.~\cite{wikipedia}


\section{The fonts}

The fonts are available from \url{https://github.com/aftertheflood/spark} and should be saved
either in \path{Library/fonts/} (MAC OSX), \path{c:\Windows\Fonts} (Windows) or
\path{/usr/local/share/fonts} (Linux)  or any other location where
the fonts will be found by the system.

\begin{verbatim}
Sparks-Bar-Extranarrow.otf
Sparks-Bar-Extrawide.otf
Sparks-Bar-Medium.otf
Sparks-Bar-Narrow.otf
Sparks-Bar-Wide.otf
Sparks-Dot-Extralarge.otf
Sparks-Dot-Extrasmall.otf
Sparks-Dot-Large.otf
Sparks-Dot-Medium.otf
Sparks-Dot-Small.otf
Sparks-Dotline-Extrathick.otf
Sparks-Dotline-Extrathin.otf
Sparks-Dotline-Medium.otf
Sparks-Dotline-Thick.otf
Sparks-Dotline-Thin.otf
\end{verbatim}
%-rw-r--r-- 1 voss voss 24708 Sep 15 11:20 Spark - Bar - Medium.otf
%-rw-r--r-- 1 voss voss 24696 Sep 15 11:20 Spark - Bar - Narrow.otf
%-rw-r--r-- 1 voss voss 24680 Sep 15 11:20 Spark - Bar - Thin.otf
%-rw-r--r-- 1 voss voss 22140 Sep 15 11:20 Spark - Dot-line - Medium.otf
%-rw-r--r-- 1 voss voss 24616 Sep 15 11:20 Spark - Dot - Medium.otf
%-rw-r--r-- 1 voss voss 24580 Sep 15 11:20 Spark - Dot - Small.otf


The package defines the following font macros (using symbol names):

\small
\begin{verbatim}
\newfontface\sparkBarMedium{Spark-Bar-Medium}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkBarNarrow{Spark-Bar-Narrow}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkBarExtranarrow{Spark-Bar-Extranarrow}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkBarWide{Spark-Bar-Wide}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkBarExtrawide{Spark-Bar-Extrawide}[RawFeature=+calt,\spark@DefaultFeatures]
%
\newfontface\sparkDotLineMedium{Spark-Dotline-Medium}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotLineThick{Spark-Dotline-Thick}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotLineExtrathick{Spark-Dotline-Extrathick}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotLineThin{Spark-Dotline-Thin}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotLineExtrathin{Spark-Dotline-Extrathin}[RawFeature=+calt,\spark@DefaultFeatures]
%
\newfontface\sparkDotMedium{Spark-Dot-Medium}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotSmall{Spark-Dot-Small}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotExtralarge{Spark-Dot-Extralarge}[RawFeature=+calt,\spark@DefaultFeatures]
\newfontface\sparkDotExtrasmall{Spark-Dot-Extrasmall}[RawFeature=+calt,\spark@DefaultFeatures]
\end{verbatim}

\normalsize

\section{The macros}

\begin{verbatim}
\sparkBar[<Type>][<No>]{values}[<No>]
\sparkDot[<Type>][<No>]{values}[<No>]
\sparkDotline[<Type>][<No>]{values}[<No>]
\sparkBar*[<Type>][<No>]{values}[<No>]
\sparkDot*[<Type>][<No>]{values}[<No>]
\sparkDotline*[<Type>][<No>]{values}[<No>]
\sparkBar[<Type>]{!value,values,...,!value}
\sparkDot[<Type>]{!value,values,...,!value}
\sparkDotline[<Type>]{!value,values,...,!value}
\sparkBar*[<Type>]{!value,values,...,!value}
\sparkDot*[<Type>]{!value,values,...,!value}
\sparkDotline*[<Type>]{!value,values,...,!value}
\end{verbatim}

If \texttt{[<Type>]} is missing, \texttt{Medium} is assumed. The type is mandatory if you use
the first \texttt{[<No>]} argument! The star versions are only valid for Lua\LaTeX\ where the values
can be of any interval. They will be changed to the allowed interval: 

\begin{itemize}
\item for \cs{sparkDotline}: $[0,9]$
\item for all other: $[0,100]$
\end{itemize}


Instead of using the optional arguments for printing the first and/or last value you
can use the !-notation, then the first and/or last listed value are printed. There is no difference
in using the optional arguments; it saves only some keystrokes if the printed values are
the same as in the list.

\section{Text examples}

\subsection{Bars}

\typeout{Bar-Medium}%

\subsubsection{Bar-Medium}

\begin{LTXexample}[pos=t]
Text \sparkBar{14,95,68,9,19,41,91,1,81,97,79,45,96,76,17,65,8,92} Text\\
Text \sparkBar[Medium]{14,95,68,9,19,41,91,1,81,97,79,45,96,76,17,65,8,92} Text\\
Text \sparkBar[Medium][14]{14,95,68,9,19,41,91,1,81,97,79,45,96,76,17,65,8,92}[92] Text\\
Text \sparkBar{!14,95,68,9,19,41,91,1,81,97,79,45,96,76,17,65,8,!92} Text
\end{LTXexample}


\subsubsection{Bar-Narrow}

\begin{LTXexample}[pos=t]
Text \sparkBar[Narrow]{19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,8} Text\\
Text \sparkBar[Narrow][19]{19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,!8} Text\\
Text \sparkBar[Narrow]{!19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,!8} Text
\end{LTXexample}


\subsubsection{Bar-Extranarrow}

\begin{LTXexample}[pos=t]
Text \sparkBar[Extranarrow]{19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,8} Text\\
Text \sparkBar[Extranarrow][19]{19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,!8} Text\\
Text \sparkBar[Extranarrow]{!19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,!8} Text
\end{LTXexample}


\subsubsection{Bar-Wide}

\begin{LTXexample}[pos=t]
Text \sparkBar[Wide]{13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22} Text\\
Text \sparkBar[Wide][13]{13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22}[22] Text\\
Text \sparkBar[Wide]{!13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22} Text\\
Text \sparkBar[Wide]{!13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,!22} Text
\end{LTXexample}


\subsubsection{Bar-Extrawide}

\begin{LTXexample}[pos=t]
Text \sparkBar[Extrawide]{13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22} Text\\
Text \sparkBar[Extrawide][13]{13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22}[22] Text\\
Text \sparkBar[Extrawide]{!13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22} Text\\
Text \sparkBar[Extrawide]{!13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,!22} Text
\end{LTXexample}



\subsection{Dots}

\subsubsection{Dot-Medium -- the default}

\begin{LTXexample}[pos=t]
Text \sparkDot{54,39,26,65,29,58,36,99,16,56,76,69,71,77,7,40,79,1} Text\\
Text \sparkDot[Medium][54]{54,39,26,65,29,58,36,99,16,56,76,69,71,77,7,40,79,1}[1] Text\\
Text \sparkDot{!54,39,26,65,29,58,36,99,16,56,76,69,71,77,7,40,79,!1} Text
\end{LTXexample}


\subsubsection{Dot-Small}


\begin{LTXexample}[pos=t]
Text \sparkDot[Small]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDot[Small][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDot[Small]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}

\subsubsection{Dot-Extrasmall}

\begin{LTXexample}[pos=t]
Text \sparkDot[Extrasmall]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDot[Extrasmall][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDot[Extrasmall]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}


\subsubsection{Dot-Extralarge}

\begin{LTXexample}[pos=t]
Text \sparkDot[Extralarge]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDot[Extralarge][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDot[Extralarge]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}




\subsection{Dotline}

\subsubsection{Dotline-Thin}

\begin{LTXexample}[pos=t]
Text \sparkDotline[Thin]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDotline[Thin][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDotline[Thin]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}

\subsubsection{Dotline-Extrathin}

\begin{LTXexample}[pos=t]
Text \sparkDotline[Extrathin]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDotline[Extrathin][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDotline[Extrathin]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}



\subsubsection{Dotline-Medium -- the default}

\begin{LTXexample}[pos=t]
Text \sparkDotline{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDotline[Medium][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDotline{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}

\subsubsection{Dotline-Thick}

\begin{LTXexample}[pos=t]
Text \sparkDotline[Thick]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDotline[Thick][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDotline[Thick]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}

\subsubsection{Dotline-Extrathick}

\begin{LTXexample}[pos=t]
Text \sparkDotline[Extrathick]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDotline[Extrathick][1]{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}[76] Text\\
Text \sparkDotline[Extrathick]{!1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,!76} Text
\end{LTXexample}


\section{Examples for Lua\protect\LaTeX}
The current version of \texttt{spark-otf} supports only star versions for Lua\LaTeX.


\subsection{Bars}

\begin{LTXexample}[pos=t]
Text \sparkBar*{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkBar*{9,4,2,1,6,7,3,8,3,7,1,4,9,2,8,5,1,8} Text\\
Text \sparkBar*[Narrow]{111,179,188,146,154,177,191,124,170,122,127,129,140,133,131,195,126,176} Text\\
\end{LTXexample}


\subsection{Dots}
\begin{LTXexample}[pos=t]
Text \sparkDotline*{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76} Text\\
Text \sparkDotline{9,4,2,1,6,7,3,8,3,7,1,4,9,2,8,5,1,8} Text\\
Text \sparkDotline+{9,4,2,1,6,7,3,8,3,7,1,4,9,2,8,5,1,8} Text\\
Text \sparkDot*[Medium][19]{19,114,12,111,16,17,113,118,113,67,61,74,39,52,38,55,111,88}[88] Text\\
Text \sparkDotline*[Medium][111]{111,179,188,146,154,177,191,124,170,122,127,129,140,133,131,195,126,176}[176] Text
\end{LTXexample}



\section{Using color}
It is possible to mark single or some values with a specific color, which must
be given in binary notation, e.g. \verb|ff0000| for red (R-G-B). The macro \verb|\setSparkColor|
\emph{should} include the (last) comma after the value:

\begin{LTXexample}[pos=t]
Text \sparkBar{14,95,68,9,19,41,\setSparkColor{ff0000}{91,}1,81,97,79,45,96,76,17,65,8,92} Text
     \sparkBar{!14,95,68,9,19,41,\setSparkColor{ff0000}{91,1,81,97,79,}45,96,76,17,65,8,!92} Text

\bigskip
\noindent
Text \sparkDot{14,95,68,9,19,41,\setSparkColor{ff0000}{91,}1,81,97,79,45,96,76,17,65,8,92} Text
     \sparkDot{!14,95,68,9,19,41,\setSparkColor{ff0000}{91,1,81,97,79,}45,96,76,17,65,8,!92} Text
\end{LTXexample}

Coloring does \emph{not} work for the star version of the macro and \emph{not} for
\verb|\sparkDotline|.


\section{The charsets}

\subsection{Spark-BarMedium}

%\includegraphics{images/Spark-BarMedium}

{\sparkBarMedium 
\multido{\iA=32+50}{20000}{\sbox\ZBox{%
  \multido{\iB=\iA+1}{50}{%
    \sbox\SBox{\symbol{\iB}}%
    \ifdim\wd\SBox>0pt\usebox\SBox\kern0.45pt\fi}}%
  \ifdim\wd\ZBox>0pt \sbox\ZBox{\makebox[4em][r]{\iA: }%
    \multido{\iB=\iA+1}{50}{%
      \sbox\SBox{\symbol{\iB}}%
        \ifdim\wd\SBox>0pt\usebox\SBox\else\kern0.45em\fi\kern0.25pt}}%
    \usebox\ZBox\\\fi}
}


%\string{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,1\string}



%-* !"\#\$\%'()*+,-./0123456789.;<=>@CDEGHILNOSTU[\textbackslash]\_ht\{\_\}°¢

\subsection*{Spark-BarNarrow}

{\sparkBarNarrow 
\multido{\iA=32+50}{20000}{\sbox\ZBox{%
  \multido{\iB=\iA+1}{50}{%
    \sbox\SBox{\symbol{\iB}}%
    \ifdim\wd\SBox>0pt\usebox\SBox\kern0.35pt\fi}}%
  \ifdim\wd\ZBox>0pt \sbox\ZBox{\makebox[4em][r]{\iA: }%
    \multido{\iB=\iA+1}{50}{%
      \sbox\SBox{\symbol{\iB}}%
        \ifdim\wd\SBox>0pt\usebox\SBox\else\kern0.35em\fi\kern0.25pt}}%
    \usebox\ZBox\\\fi}
}


\iffalse

\subsection*{Spark-BarThin}

{\sparkBarThin
\multido{\iA=32+50}{20000}{\sbox\ZBox{%
  \multido{\iB=\iA+1}{50}{%
    \sbox\SBox{\symbol{\iB}}%
    \ifdim\wd\SBox>0pt\usebox\SBox\kern0.1pt\fi}}%
  \ifdim\wd\ZBox>0pt \sbox\ZBox{\makebox[4em][r]{\iA: }%
    \multido{\iB=\iA+1}{50}{%
      \sbox\SBox{\symbol{\iB}}%
        \ifdim\wd\SBox>0pt\usebox\SBox\else\kern0.1em\fi\kern0.25pt}}%
    \usebox\ZBox\\\fi}
}


\subsection*{Spark-DotMedium}

{\sparkDotMedium
\multido{\iA=32+50}{20000}{\sbox\ZBox{%
  \multido{\iB=\iA+1}{50}{%
    \sbox\SBox{\symbol{\iB}}%
    \ifdim\wd\SBox>0pt\usebox\SBox\kern0.35pt\fi}}%
  \ifdim\wd\ZBox>0pt \sbox\ZBox{\makebox[4em][r]{\iA: }%
    \multido{\iB=\iA+1}{50}{%
      \sbox\SBox{\symbol{\iB}}%
        \ifdim\wd\SBox>0pt\usebox\SBox\else\kern0.35em\fi\kern0.25pt}}%
    \usebox\ZBox\\\fi}
}


\subsection*{Spark-DotSmall}

{\sparkDotSmall
\multido{\iA=32+50}{20000}{\sbox\ZBox{%
  \multido{\iB=\iA+1}{50}{%
    \sbox\SBox{\symbol{\iB}}%
    \ifdim\wd\SBox>0pt\usebox\SBox\kern0.2pt\fi}}%
  \ifdim\wd\ZBox>0pt \sbox\ZBox{\makebox[4em][r]{\iA: }%
    \multido{\iB=\iA+1}{50}{%
      \sbox\SBox{\symbol{\iB}}%
        \ifdim\wd\SBox>0pt\usebox\SBox\else\kern0.2em\fi\kern0.25pt}}%
    \usebox\ZBox\\\fi}
}


\subsection*{Spark-DotLine}

{\sparkDotLine
\multido{\iA=32+50}{20000}{\sbox\ZBox{%
  \multido{\iB=\iA+1}{50}{%
    \sbox\SBox{\symbol{\iB}}%
    \ifdim\wd\SBox>0pt\usebox\SBox\kern0.4pt\fi}}%
  \ifdim\wd\ZBox>0pt \sbox\ZBox{\makebox[4em][r]{\iA: }%
    \multido{\iB=\iA+1}{50}{%
      \sbox\SBox{\symbol{\iB}}%
        \ifdim\wd\SBox>0pt\usebox\SBox\else\kern0.4em\fi\kern0.25pt}}%
    \usebox\ZBox\\\fi}
}


\fi


\nocite{*}
\printbibliography


\end{document}


<p class=spark-bar-medium>{14,95,68,9,19,41,91,1,81,97,79,45,96,76,17,65,8,92}</p>

<p class=spark-bar-narrow>{19,32,93,4,95,46,13,23,50,86,94,68,58,41,89,57,74,8}</p>

<p class=spark-bar-thin>{13,15,59,73,42,1,41,51,4,97,35,55,37,24,89,21,30,22}</p>

<p class=spark-dot-medium>{54,39,26,65,29,58,36,99,16,56,76,69,71,77,7,40,79,1}</p>

<p class=spark-dot-small>{1,79,88,46,54,77,91,24,70,22,27,29,40,33,31,95,26,76}</p>

<p class=spark-line-medium>{9,4,2,1,6,7,3,8,3,7,1,4,9,2,8,5,1,8}</p>
\NewDocumentCommand\spark{omo}{{\sparklinesbarmedium
		\IfValueT{#1}{#1}\string{#2\string}\IfValueT{#3}{#3}}}

# README #
Package spark-otf supports the free fonts from
"After the Flood"  which are available from the 
website https://aftertheflood.com/projects/sparks/
or from https://github.com/aftertheflood/spark

Following font files are supported:

- Sparks-Bar-???.otf
- Sparks-Dotline-???.otf
- Sparks-Dot-???.otf



%% This file is distributed under the terms of the LaTeX Project Public
%% License from CTAN archives in directory  macros/latex/base/lppl.txt.
%% Either version 1.3 or, at your option, any later version.

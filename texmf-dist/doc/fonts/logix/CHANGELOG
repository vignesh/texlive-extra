2021-01-17 v1.08

   1. The \OpnTurn and \ClsTurn symbols have been renamed to \OpenForce and \ClsForce,
      respectively, to match existing usage.

2020-09-18 v1.07

   1. The normal sans serif script was moved from the Latin-1 area to the Private Use Area
      removing the last overlap between Logix symbols and Unicode symbols.

   2. The remaining glpyhs in the Latin-1 area were not useful and were removed.

   3. Modified glyphs of the highly regarded BitStream Vera mono font have been incorporated.
      The modifications were make to make the glyphs more consistent with the STIX2 Math
      font. Thus the Logix font can be directly used as a monospace font.

   4. Some older applications may not display correctly when using the Logix. Therefore,
      the monospace glpyhs are also entirely contained in a new Logix Mono font which
      does not contain any other symbols, making it a smaller, lightweight font.

2020-05-24 v1.06

   1. Four symbols were added

         \SMapTo
         \MapTo
         \LMapTo
         \XMapTo

2020-05-23 v1.05

   1. Font loading was tweaked to accomodate a change in the luaotfload package
      which broke stretchy delimiters in LuaLaTeX.

   2. Three symbols were renamed.

         \Ng           --> \Ngt
         \DottedCircle --> \DottedCircl
         \Dts          --> \CDots

   3. Several existing symbols were exported:

         \TFNone
         \NTrue
         \TFBoth
         \NFalse
         \Bot
         \Top
         \VDots

   4. Two symbols were added

         \TripleQuote
         \LDots

   5. A pair of stretchy delimiters were added

         \OpnTrpBar
         \ClsTrpBar

   6. A bold calligraphic script was added.

   7. Since many (most) Unicode fonts do not provide a systematic set of the basic
      geometric shapes in different sizes, 126 new geometric symbols are provided.
      Nine sizes (each) for circles, squares, diamonds, right triangle, left
      triangle, up triangle and down triangle for both white and black variants
      were added. Unlike the existing geometric symbols which are vertically
      centered at 0.25 em for use in logic, these are vertically centered at
      0.50 em for general use, with side bearings of 0.1 em.  The smallest shapes
      have basic dimension of 0.2 em, and each larger shape increases in size by
      0.1 em, up to 1.0 em. These symbols are name:

         \WhiteCircleA ... \WhiteCircleI
         \BlackCircleA ... \BlackCircleI
         \WhiteSquareA ... \WhiteSquareI
         \BlackSquareA ... \BlackSquareI
         \WhiteDiamondA ... \WhiteDiamondI
         \BlackDiamondA ... \BlackDiamondI
         \WhiteRightTriangleA ... \WhiteRightTriangleI
         \BlackRightTriangleA ... \BlackRightTriangleI
         \WhiteLeftTriangleA ... \WhiteLeftTriangleI
         \BlackLeftTriangleA ... \BlackLeftTriangleI
         \WhiteUpTriangleA ... \WhiteUpTriangleI
         \BlackUpTriangleA ... \BlackUpTriangleI
         \WhiteDownTriangleA ... \WhiteDownTriangleI
         \BlackDownTriangleA ... \BlackDownTriangleI

   8. The environments for logic proofs were slightly tweaked for improved readability.

2020-05-01 v1.04

   1. Two new Knot symbols were added, \KntHDASH and \KntVDASH.

   2. A few symbols were renamed to avoid conflict with standard Unicode names.

         \Comma   --> \Coma
         \Number  --> \Numbr
         \Percent --> \Percnt
         \Plus    --> \Pls

2020-03-07 v1.03

   1. Some macros were tweaked which were not expanding correctly in some circumstances.

   2. The "bold-style=ISO" option is now used for unicode-math. This causes \symbf to
      use italic bold, \mathbf remains non-italic. See the unicode-math package
      documentation for more information.

   3. LaTeX2e version 2020-02-02 patch level 5 is now required. Some updates prior to
      patch level 5 were causing errors.

2020-01-04 v1.02

   1. Version number corrected in logix.sty.

   2. Two new stretchy delimiters added.

   3. \DelimS variants added for automatic stretchy delimiter \Delim.

   4. Corresponding updates to README file, documentation and the logix.sty file.

   5. Changes made to match changes in other packages.

2019-07-07 v1.01

   1. Minor additions and corrections to README file, documentation and
      logix.sty comments.

   2. Corrected widths and macro names for several \Knt symbols.

   3. Added \KntlgX macros for changing the height of \Knt symbols.

   4. Added a dozen half width or height symbols to facilitate drawing knot bridges.

   5. Additional examples added to the documentation for using the \Knt symbols.

2019-07-01 v1.00

   Initial release.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

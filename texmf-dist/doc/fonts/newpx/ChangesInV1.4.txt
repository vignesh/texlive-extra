Files changed in Newpx as of version 1.4 (2019/09/09)

MATH

NewPXMI.{pfb,afm,tfm} : math italics, LPPL licensed (new)
NewPXBMI.{pfb,afm,tfm} : bold math italics, LPPL licensed (new)
NewPXMI_gnu.{pfb,afm,tfm} : math italics, GNU licensed (new)
NewPXBMI_gnu.{pfb,afm,tfm} : bold math italics, GNU licensed (new)
stsscr.{pfb,afm,tfm} : old STIX script alphabets OFL (new)
txmiaSTbb.{pfb,afm,tfm} : old STIX BBB alphabet OFL (new)
txbmiaSTbb.{pfb,afm,tfm} : old STIX bold BBB alphabet OFL (new)
px[b]miaX.{pfb,afm,tfm} : extended and modified px[b]mia
px[b]sys.{pfb,afm,tfm} : modified px[b]sys
zpl[b]mi.{tfm,vf} : letters
zpl[b[sy.{tfm,vf} : symbols
zpl[b]mia.{tfm,vf} : lettersA

TEXT

The npxsups*.tfm were completely revised so as to pass PDF/A-1b validation.
npxsups-Regular.{pfb,afm} LPPL (New)
npxsups-Italic.{pfb,afm} LPPL (New)
npxsups-Bold.{pfb,afm} LPPL (New)
npxsups-BoldItalic.{pfb,afm} LPPL (New)
npxsups_OT1.enc (New)
npxsups_T1.enc (New)
npxsups_LY1.enc (New)

The tfm files were generated using

afm2tfm npxsups-Regular -a -T npxsups_OT1.enc -v npxsups-Regular-ot1 npxsups-Regular-ot1
afm2tfm npxsups-Regular -a -T npxsups_T1.enc -v npxsups-Regular-t1 npxsups-Regular-t1
afm2tfm npxsups-Regular -a -T npxsups_LY1.enc -v npxsups-Regular-ly1 npxsups-Regular-ly1
afm2tfm npxsups-Italic -a -T npxsups_OT1.enc -v npxsups-Italic-ot1 npxsups-Italic-ot1
afm2tfm npxsups-Italic -a -T npxsups_T1.enc -v npxsups-Italic-t1 npxsups-Italic-t1
afm2tfm npxsups-Italic -a -T npxsups_LY1.enc -v npxsups-Italic-ly1 npxsups-Italic-ly1
afm2tfm npxsups-Bold -a -T npxsups_OT1.enc -v npxsups-Bold-ot1 npxsups-Bold-ot1
afm2tfm npxsups-Bold -a -T npxsups_T1.enc -v npxsups-Bold-t1 npxsups-Bold-t1
afm2tfm npxsups-Bold -a -T npxsups_LY1.enc -v npxsups-Bold-ly1 npxsups-Bold-ly1
afm2tfm npxsups-BoldItalic -a -T npxsups_OT1.enc -v npxsups-BoldItalic-ot1 npxsups-BoldItalic-ot1
afm2tfm npxsups-BoldItalic -a -T npxsups_T1.enc -v npxsups-BoldItalic-t1 npxsups-BoldItalic-t1
afm2tfm npxsups-BoldItalic -a -T npxsups_LY1.enc -v npxsups-BoldItalic-ly1 npxsups-BoldItalic-ly1
#
for f in npxsups*.vpl; do /bin/cp -fp $f ${f%.*}.pl; done
for f in npxsups*.pl; do pltotf $f ${f%.*}.tfm; done

MAP

newpx.map was revised to relect the changes above.
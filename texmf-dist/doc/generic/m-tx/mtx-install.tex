\documentclass[11pt]{article}
\usepackage[textwidth=6.5in,textheight=8.5in]{geometry}
\usepackage[osf]{mathpazo}
\PassOptionsToPackage{urlcolor=black,colorlinks}{hyperref}
\RequirePackage{hyperref}
\usepackage{xcolor}
\newcommand{\myurl}[1]{\textcolor{blue}{\underline{\textcolor{black}{\url{#1}}}}}
\newcommand{\mtxVersion}{0.63}
\begin{document}
\title{Installation of the CTAN M-Tx Distribution}
\author{Bob Tennent\\
\small\url{rdt@cs.queensu.ca}}
\date{\today}
\maketitle 
\thispagestyle{empty}

\section{Introduction}
We assume that you have already installed MusiXTeX and PMX.
Before trying to install M-Tx from CTAN, check whether your TeX distribution
provides packages for M-Tx; this will be easier than doing it yourself.
But if your TeX distribution
doesn't have M-Tx (or doesn't have the most recent version), this distribution
of M-Tx is very easy to install, though
you may need to read the material on 
installation of (La)TeX files in the 
TeX FAQ\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html}},
particularly
the pages on 
which tree to use\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=what-TDS}}
and installing files\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=inst-wlcf}}.

\section{Installing \texttt{m-tx.tds.zip}}

In this distribution of M-Tx, many of the files to be installed 
(including macros and documentation are in 
\myurl{http://mirror.ctan.org/install/m-tx.tds.zip}. The file
\verb|mt-x.tds.zip| is a zipped TEXMF
hierarchy; simply download it and unzip in the root folder/directory of whichever TEXMF tree
you decide is most appropriate, likely a ``local'' or ``personal'' one.
This should work with any TDS\footnote{%
\myurl{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=tds}}
compliant TeX distribution, including MikTeX, TeXlive and teTeX,
but you must use an unzipping program that 
converts text files to the text-file format of your
platform, such as \verb\unzip -a ...\ on Linux.


After unzipping the archive, update the filename database as necessary,
for example, by executing \verb\texhash ~/texmf\ or 
clicking the button labelled ``Refresh FNDB" in the MikTeX settings program.

Documentation for M-Tx is installed under \verb\doc/generic/m-tx\
in the TEXMF tree.  

\section{Installing \texttt{prepmx}}

The next step in the installation is to install
the one essential file that can't be installed in a TEXMF tree: the \texttt{prepmx} preprocessor. 


\subsection{Pre-Compiled Executables}

On Windows
systems, one can install \texttt{prepmx.exe} 
in the \texttt{Windows32} or \texttt{Windows64} sub-directory; these are pre-compiled
executables and should be copied to any
folder on the PATH of executables. 
This might entail creating a suitable folder and adding that folder
to the PATH as follows: 
right click on the ``My Computer'' desktop icon,
left click on 
\begin{center}
Properties$\rightarrow$Advanced$\rightarrow$Environment Variables
\end{center}
in the ``System Variables'' section, scroll
down to ``path'', select it, click edit, and append the full path name you have selected for the new 
folder.  Also install the batch script \verb|m-tx.bat| in a folder on the PATH.

On the MAC OS-X platform, one can install the \texttt{prepmx} binary
that is in the 
\texttt{OSX} sub-directory.  Ensure that the file has the execute permission set:
\begin{list}{}{}\item
\verb\chmod +x prepmx\
\end{list}

\subsection{Compilation from Source}

On any platform with basic GNU tools (\texttt{tar}, \texttt{gunzip}, \texttt{make})
and \texttt{gcc} or \texttt{fpc}, you should be able to build the \texttt{prepmx} executable as follows:

\begin{enumerate}
\item Unpack the \texttt{mtx-\mtxVersion.tar.gz} archive:
\begin{list}{}{}
\item \texttt{tar zxvf mtx-\mtxVersion.tar.gz}
\end{list}
and move to the resulting \texttt{mtx-\mtxVersion} directory.
\item Configure:
\begin{list}{}{}
\item \verb\./configure --prefix=$HOME\
\end{list}
or just
\begin{list}{}{}
\item\verb\./configure \
\end{list}
if you have super-user privileges.
\item Build:
\begin{list}{}{}
\item \verb\make\
\end{list}
If this fails, you might want to install \texttt{fpc} (the Free Pascal Compiler\footnote{%
\myurl{http://www.freepascal.org/}}) and
try
\begin{list}{}{}
\item \verb\make -f Makefile.orig\
\end{list}

\item Install:
\begin{list}{}{}
\item \verb\make install\
\end{list}
This step should be executed as root if you need super-user privileges.
\end{enumerate}
You should now have an executable \verb\prepmx\ 
in your PATH.

\section{Usage}

To process an M-Tx source file, use the \verb|musixtex| script
which is included in the \verb|musixtex| package.
If applied to a file with \verb|.mtx| extension, it will by default execute \verb|prepmx| followed by
\verb|pmxab|, followed 
by \verb|etex|, \verb|musixflx|, and \verb|etex| again, followed by
conversion to PDF using \verb|dvips| and \verb|ps2pdf|.
There are many options to vary the default behaviour.

\section{Discussion}

Other pre-processor packages, additional documentation, additional
add-on packages, and many examples of M-Tx and MusiXTeX typesetting may be found
at the Werner Icking Music Archive\footnote{%
\myurl{http://icking-music-archive.org}}.
Support for users of MusiXTeX and related software may be obtained via
the MusiXTeX mail list\footnote{%
\myurl{http://tug.org/mailman/listinfo/tex-music}}.
M-Tx may be freely copied, duplicated and used in conformance to the
MIT License (see included file \verb\LICENSE\).

\end{document}

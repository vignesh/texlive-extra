
Bagpipe.tex is a macro-package for use on top of MusicTeX or        
MusixTeX, which are macro-packages for use with TeX or LaTeX. TeX   
is a language designed for typesetting technical documents, 
and is available for most platforms. Musi[c|x]TeX    
were written by Daniel Taupin, who is now deceased. They are          
available via anonymous ftp at CTAN sites and the Werner Icking     
Music Archive (WIMA). See WIMA for installation instructions.        

Version 3.02 of bagpipe.tex works with MusicTeX version 5.20 and    
MusixTeX version T.89. Since it redefines a few Musi(c|x)TeX        
macros, it may not work with later versions without modification.   
It definitely does not work with versions of MusicTeX earlier than  
4.7.                                                                

MusixTeX differs from MusicTeX in that it relies on a three pass
system to adjust the spacing instead of glue (stretchable space).
MusixTeX is also the version under active development and has
many features which MusicTeX does not. Most of these are
irrelevant for bagpipe music.

In this distribution:

bagpipe.tex   : the macro package.
bagpipe.txt   : same as above with a common extension
bagpipe.ini   : for generating a bagpipe format
bagpipex.ini  : for generating a bagpipex format
bagdoc.tex    : user's guide.

quickref.tex  : a reference 'card'.

Green.tex       }
GreenTwo.tex    } : sample tunes 
Bonnets.tex     } : (note that in bagpipe music, two sharps are  
Washer.tex      } :  understood and the key signature is usually omitted as
BlackDonald.tex } :  in these samples)

Notes: *.tex, *.ini, and *.mf are ascii.
You may have to modify some settings to get your browser to
download these files. The sample tunes and quickref in this
distribution are set up to use MusixTeX.

License is granted to distribute and use this product according
to the LaTeX Project Public License (lppl1.3). This project is
maintained by Walt Innes (walt (at) slac.stanford.edu).



# pst-dart: plotting a Dart Board

%% This program can be redistributed and/or modified under the terms
%% of the LaTeX Project Public License Distributed from CTAN archives
%% in directory macros/latex/base/lppl.txt.

Save the files pst-dart.sty|pro|tex in a directory, which is part of your 
local TeX tree. The pro file should go into $TEXMF/dvips/pstricks/
Then do not forget to run texhash to update this tree.
For more information  see the documentation of your TeX distribution 
on installing packages into your local TeX system or read the 
TeX Frequently Asked Questions:
(http://www.tex.ac.uk/cgi-bin/texfaq2html?label=instpackages).

pst-dart needs pstricks, which should already 
be part of your local TeX installation, otherwise get it from a 
CTAN server, http://mirror.ctan.org

PSTricks is PostScript Tricks, the documentation cannot be run
with pdftex, use the sequence latex->dvips->ps2pdf or
pdflatex with package auto-pst-pdf or xelatex.

%% $Id: README.md 713 2018-01-28 19:31:56Z herbert $

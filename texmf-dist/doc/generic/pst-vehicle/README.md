The pst-vehicle package Author: J. Gilg, M. Luque, T. Söll

Dated: 2017/09/16 Version 1.2

pst-vehicle contains the following:

1) pst-vehicle.sty
2) pst-vehicle.tex
3) ListVehicles.tex (in the same folder as pst-vehicle.tex)

T. Söll

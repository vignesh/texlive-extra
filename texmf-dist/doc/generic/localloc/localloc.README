Readme File for the package `localloc' by Bernd Raichle
Copyright (C) 1991,1994,1995 Bernd Raichle.  All rights reserved.

  The `localloc' package reimplements plain-TeX's basic allocation
macros to allow local allocation of registers.  It can be used with
 * plain-TeX,
 * LaTeX2e,
 * LaTeX 2.09,
and all other formats which are based on plain-TeX.
  In the past people complained about the restriction of 256
registers imposed by TeX and want either more registers or a more
clever way to maintain this limited resource.  This package shows
and implements a mechanism which doesn't remove this restrictions
but allows local allocation of registers.  Using this mechanism
you can often replace global register allocations by ones local
to a group allowing the register to be used for other purposes
after the group is left.


Installation:
-------------

This package contains 2 files, namely
 * localloc.README	This README file
 * localloc.dtx		The docstrip archive file containing the
			source for the macro file `localloc.sty'
			and the documentation.

To unpack the archive file you need `docstrip.tex' version 2.x or
newer.  You can find this file in the LaTeX2e release; it can be
used with plain-TeX _and_ LaTeX.  Run `localloc.dtx' through
plainTeX or LaTeX to unpack the archive file:
	tex localloc.dtx
or
	latex localloc.dtx


This package is supported by

Bernd Raichle
Stettener Str. 73
D-73732 Esslingen, FRG
Email: raichle@Informatik.Uni-Stuttgart.DE

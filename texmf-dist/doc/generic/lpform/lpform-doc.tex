\documentclass{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage{lpform}
\usepackage[parfill]{parskip}
\usepackage{fancyvrb}

\newcommand{\example}{\textbf{Example: }}

\begin{document}
\title{\textsc{lpform} Package Documentation}
\author{by Marijn Waltman}
\date{\today}

\maketitle
\tableofcontents

\section{Introduction}

The \textsc{lpform} package allows users to easily write linear programming formulations one restriction at a time. With this package, one can easily label equations, formulations can span multiple pages and several elements of the layout (such as spacing, texts and equation tags) are also customizable. Besides linear programming formulations, this package can also be used to display any series of aligned equations with easy labeling, referencing and other customization options.

To start using the \textsc{lpform} package, put this in your document preamble:

\begin{Verbatim}[frame=single]
\usepackage{lpform}
\end{Verbatim}

The `lpform.sty' file should also be in the same folder as your .tex file.

There are also the optional \textit{noforall} and \textit{alignedforall} arguments. Its purposes are explained in Section \ref{sec:optional}.

\section{Commands}

This is a list of all commands which are included in the package.

\begin{itemize}
\item \textbf{\textbackslash begin\{lpformulation\}[name] ... \textbackslash end\{lpformulation\}}

This is the environment in which the commands below should be placed. Each restriction is an equation and is aligned using an align environment.
\begin{itemize}
\item \textit{name} is an optional argument, which is the name given to the formulation (e.g. (P1)) and is displayed on the left of the formulation before the first line.
\end{itemize}

\item \textbf{\textbackslash lpobj[label]\{min/max\}\{objective function\}}

This displays the objective function, followed by the ``subject to'' text.
\begin{itemize}
\item \textit{label} is an optional argument which is the label text used for referencing.
\item \textit{min/max} is the text before the objective function, which is usually either min or max.
\item \textit{objective function} is where you put the objective function in math text.
\end{itemize}

\item \textbf{\textbackslash lpeq[label]\{constraint\}\{for all\}}

This displays a constraint as an equation with only an equation number (e.g. (1)).
\begin{itemize}
\item \textit{constraint} is the constraint, which is automatically formatted in a math environment.
\item \textit{for all} is the for all argument. The `for all' argument is automatically formatted in a math environment. If there is no for all argument for your constraint, then this argument should be left empty.
\end{itemize}

\item \textbf{\textbackslash lpnewsubeq[label]\{constraint\}\{for all\}}

This is the same as \textbackslash lpeq but it also starts a new list of subequations, which are indexed by the letters a,b,c,... (e.g. (2a)).

\item \textbf{\textbackslash lpsubeq[label]\{constraint\}\{for all\}}

This is the same as \textbackslash lpeq but it continues a previously started list of subequations (e.g. (2b)).

\item \textbf{\textbackslash lplabel\{label name\}}

This works the same as \textbackslash label and can be placed anywhere in the formulation to label it with \textit{label name}; the \textbackslash ref command then displays the name of the formulation given in lpformulation.

\item \textbf{\textbackslash lpnewline}

This can be placed inside the \textbackslash lpobj, \textbackslash lpeq, \textbackslash lpnewsubeq and \textbackslash lpsubeq commands in order to split the equation over multiple lines. For an example of using this command, see Section \ref{sec:split}.
\end{itemize}

The \textbackslash lpobj, \textbackslash lpeq, \textbackslash lpnewsubeq and \textbackslash lpsubeq commands all have starred versions, which hides the equation number at the end of the line.

\example Formulation \ref{lp:form} is a knapsack problem with additional constraints \eqref{eq:add1} and \eqref{eq:add2}.

\vspace{2ex}
\begin{Verbatim}[frame=single]
\begin{lpformulation}[(P)]
\lpobj{max}{\sum_{i \in I} u_ix_i}
\lpeq*{\sum_{i \in I} w_ix_i \le W}{}
\lpnewsubeq[eq:add1]{\sum_{i \in I} x_i \le 5}{}
\lpsubeq[eq:add2]{x_1 = 1}{}
\lpeq{x_i \in \{0,1\}}{i \in I}
\lplabel{lp:form}
\end{lpformulation}
\end{Verbatim}
\begin{lpformulation}[(P)]
\lplabel{lp:form}
\lpobj{max}{\sum_{i \in I} u_ix_i}
\lpeq*{\sum_{i \in I} w_ix_i \le W}{}
\lpnewsubeq[eq:add1]{\sum_{i \in I} x_i \le 5}{}
\lpsubeq[eq:add2]{x_1 = 1}{}
\lpeq{x_i \in \{0,1\}}{i \in I}
\end{lpformulation}

\textbf{Important:} When adding equations inside the lpformulation environment, it is important that there are NO empty lines, as this will cause an error to occur. An example of using empty lines:

\begin{Verbatim}[frame=single]
\begin{lpformulation}
\lpeq{5+1=6}{}

\lpeq{5+2=7}{}
\end{lpformulation}
\end{Verbatim}

\section{Optional arguments}
\label{sec:optional}

\subsection{\textit{noforall}}

The \textsc{lpform} package has an optional \textit{noforall} argument, which can be used by putting

\begin{Verbatim}[frame=single]
\usepackage[noforall]{lpform}
\end{Verbatim}

in your document preamble. This argument removes the `for all' arguments from the \textbackslash lpeq, \textbackslash lpnewsubeq and \textbackslash lpsubeq commands. This can be useful if you want to make a simple list of equations without having to leave the `for all' argument empty each time, or if you want to typeset the `for all' arguments manually.

\example An arbitrary list of equations (with \textit{noforall} on).

\vspace{2ex}
\begin{Verbatim}[frame=single]
\begin{lpformulation}
\lpeq{e^{i\pi} = -1}
\lpnewsubeq{\phi = \frac{1+\sqrt{5}}{2}}
\lpsubeq{\phi^2 - \phi - 1 = 0}
\end{lpformulation}
\end{Verbatim}
\begin{lpformulation}
\lpeq{e^{i\pi} = -1}{}
\lpnewsubeq{\phi = \frac{1+\sqrt{5}}{2}}{}
\lpsubeq{\phi^2 - \phi - 1 = 0}{}
\end{lpformulation}

\subsection{\textit{alignedforall}}
\label{sec:optionalalign}

The optional \textit{alignedforall} argument can be used by putting

\begin{Verbatim}[frame=single]
\usepackage[alignedforall]{lpform}
\end{Verbatim}

in your document preamble. This argument aligns the `for all' arguments of all equations in the \textbackslash lpformulation environment. The alignment is either around the center of the page or to the right of the longest equation if this equation is longer than half of the page. Using this argument is equivalent to using 

\begin{Verbatim}[frame=single]
\renewcommand{\lpforall}[1]{&& \forall #1}
\end{Verbatim} 

For examples of this argument in use, see Section \ref{sec:aligned}.

\section{Internal variables}

This package uses several internal variables to determine how the equations are displayed. These variables can also be redefined in order to customize the layout.

\begin{itemize}
\item \textbf{\textbackslash lpsubjectto}

This is the text which is shown in the `subject to'-line. Default value is `s.t.'. To change this variable to \textit{value}, use

\begin{Verbatim}[frame=single]
\renewcommand{\lpsubjectto}{value}
\end{Verbatim}

\item \textbf{\textbackslash lpforall}

This is the way the \textit{for all} argument is displayed. Default value is `\textbackslash quad \textbackslash forall \#1'. To change this variable to \textit{value}, use 

\begin{Verbatim}[frame=single]
\renewcommand{\lpforall}[1]{value} 
\end{Verbatim}

\item \textbf{\textbackslash lpindent}

This is the size of the left indent. Default value is `\textbackslash hspace\{20pt\}'. To change this variable to \textit{value}, use

\begin{Verbatim}[frame=single]
\renewcommand{\lpindent}{value}
\end{Verbatim}

\item \textbf{\textbackslash lpsubeqtag}

This is the way that sub-equations are tagged. Default value is `\textbackslash theequation\textbackslash alph\{subcounter\}', where subcounter is the counter of the sub-equation in the list. To change this variable to \textit{value}, use

\begin{Verbatim}[frame=single]
\renewcommand{\lpsubeqtag}{value}
\end{Verbatim}
\end{itemize}

\section{Custom layout examples}

\subsection{Custom names, tags and spacing}

This is an example of using the custom names, custom equation tags and custom spacing options. In this example the left indent has been removed, `max' has been changed to ``Maximize:'', the `subject to' line has been changed to ``Subject to:'' and the sub-equations are tagged by numbers instead of letters.

\example A knapsack problem with additional constraints.

\vspace{2ex}
\begin{Verbatim}[frame=single]
\renewcommand{\lpindent}{\hspace{0pt}}
\renewcommand{\lpsubjectto}{Subject to:}
\renewcommand{\lpsubeqtag}{\theequation .\arabic{subcounter}}
\begin{lpformulation}
\lpobj{Maximize:}{\sum_{i \in I} u_ix_i}
\lpeq*{\sum_{i \in I} w_ix_i \le W}{}
\lpnewsubeq{\sum_{i \in I} x_i \le 5}{}
\lpsubeq{x_1 = 1}{}
\lpeq{x_i \in \{0,1\}}{i \in I}
\end{lpformulation}
\end{Verbatim}
\renewcommand{\lpindent}{\hspace{0pt}}
\renewcommand{\lpsubjectto}{Subject to:}
\renewcommand{\lpsubeqtag}{\theequation .\arabic{subcounter}}
\begin{lpformulation}
\lpobj{Maximize:}{\sum_{i \in I} u_ix_i}
\lpeq*{\sum_{i \in I} w_ix_i \le W}{}
\lpnewsubeq{\sum_{i \in I} x_i \le 5}{}
\lpsubeq{x_1 = 1}{}
\lpeq{x_i \in \{0,1\}}{i \in I}
\end{lpformulation}

\subsection{Aligned `for all' arguments}
\label{sec:aligned}

Since each equation is put in an align environment, the `for all' arguments can be aligned by letting a new column start before the argument is shown. This can be achieved by using

\begin{Verbatim}[frame=single]
\renewcommand{\lpforall}[1]{&& \forall #1}
\end{Verbatim}

or by using the \textit{alignedforall} argument in the document preamble. The `for all' arguments will be aligned either around the center of the page or just to the right of the longest equation if this equation is longer than half of the page.

\example An arbitrary problem.

\vspace{2ex}
\begin{Verbatim}[frame=single]
\renewcommand{\lpforall}[1]{&& \forall #1}
\begin{lpformulation}
\lpobj{max}{\sum_{i \in I} u_ix_i
     - \sum_{i \in I} \sum_{j \in J} v_iy_{ij}}
\lpeq{\sum_{i \in I} y_{ij} - \sum_{i \in I} x_i
     \le 3.141592653589793238462643...}{j \in J}
\lpeq{\sum_{i \in I} x_i \le 5}{}
\lpeq{\sum_{j \in J} y_{ij} \ge 5}{i \in I}
\lpnewsubeq{x_i \in \{0,1\}}{i \in I}
\lpsubeq{y_{ij} \in \{0,1\}}{i \in I, j \in J}
\end{lpformulation}
\end{Verbatim}
\renewcommand{\lpindent}{\hspace{20pt}}
\renewcommand{\lpsubjectto}{s.t.}
\renewcommand{\lpsubeqtag}{\theequation\alph{subcounter}}
\renewcommand{\lpforall}[1]{&& \forall #1}
\begin{lpformulation}
\lpobj{max}{\sum_{i \in I} u_ix_i - \sum_{i \in I} \sum_{j \in J} v_iy_{ij}}
\lpeq{\sum_{i \in I} y_{ij} - \sum_{i \in I} x_i \le 3.141592653589793238462643...}{j \in J}
\lpeq{\sum_{i \in I} x_i \le 5}{}
\lpeq{\sum_{j \in J} y_{ij} \ge 5}{i \in I}
\lpnewsubeq{x_i \in \{0,1\}}{i \in I}
\lpsubeq{y_{ij} \in \{0,1\}}{i \in I, j \in J}
\end{lpformulation}

\subsection{Splitting long equations}
\label{sec:split}

Sometimes an equation can be too long to fit on just one line, which has some undesirable results. In these situations the \textbackslash lpnewline command can be used to break up the equation and divide it over multiple lines. Additional spacing can be added manually.

\example Breaking up a long equation.

\vspace{2ex}
\begin{Verbatim}[frame=single]
\begin{lpformulation}
\lpeq{1+\frac{1}{2}+\frac{1}{4}+\frac{1}{8}+\frac{1}{16}
     +\frac{1}{32}+\frac{1}{64}+\frac{1}{128}
     +\frac{1}{256}+\frac{1}{512}+\frac{1}{1024}
     +\frac{1}{2048}+...=2}{}
\lpeq{1+\frac{1}{2}+\frac{1}{4}+\frac{1}{8}+\frac{1}{16}
     +\frac{1}{32}+\frac{1}{64}+\frac{1}{128}+...
     \lpnewline\qquad ...+\frac{1}{256}+\frac{1}{512}
     +\frac{1}{1024}+\frac{1}{2048}+...=2}{}
\end{lpformulation}
\end{Verbatim}
\renewcommand{\lpforall}[1]{\quad \forall #1}
\begin{lpformulation}
\lpeq{1 + \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16} + \frac{1}{32} + \frac{1}{64} + \frac{1}{128} + \frac{1}{256} + \frac{1}{512} + \frac{1}{1024} + \frac{1}{2048} + ... = 2}{}
\lpeq{1 + \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16} + \frac{1}{32} + \frac{1}{64} + \frac{1}{128} + ... \lpnewline\qquad ... + \frac{1}{256} + \frac{1}{512} + \frac{1}{1024} + \frac{1}{2048} + ... = 2}{}
\end{lpformulation}

\end{document}
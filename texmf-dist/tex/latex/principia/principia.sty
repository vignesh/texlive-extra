%Package Identification
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{principia}[2020/10/25 principia package version 1.1] %This is the principia package is for representing notations in Whitehead and Russell's ``Principia Mathematica" close to their appearance in the original.
%Version 1.0 (superseded by Version 1.1): Covers typesetting of notation through Volume I. 2020/10/24
%Version 1.1 (updates): fixed the spacing of scope dots around parentheses; fixed spacing of theorem sign; fixed spacing around primitive proposition and definition signs. 2020/10/25
%Licensed under LaTeX Project Public License 1.3c. 
%Copyright Landon D. C. Elkind, 2020.  (https://landondcelkind.com/contact/).

%Principia package requirements
\RequirePackage{amssymb} %This loads the relation domain and converse domain limitation symbols.
\RequirePackage{amsmath} %This loads the circumflex, substitution into theorems, \text{}, \mathbf{}, \boldsymbol{}, \overleftarrow{}, \overrightarrow{}, etc.
\RequirePackage{graphicx} %This loads commands that flip iota for definite descriptions, Lambda for the universal class, and so on. The (superseded) graphics package should also work here, but is not recommended.
\RequirePackage{marvosym} %This loads the male and female symbol.
\RequirePackage{pifont} %This loads the symbols such as the eight-pointed asterisk.

%Meta-logical symbols
\newcommand{\pmdem}{\textit{Dem}.} %This notation begins a proof.
\newcommand{\pmsub}[2]{\bigg \lbrack \small \begin{array}{c} #1 \\ \hline #2 \end{array} \bigg \rbrack} %This is the substitution command.
\newcommand{\pmSub}[3]{\bigg \lbrack \normalsize #1 \text{ } \small \begin{array}{c} #2 \\ \hline #3 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmsubb}[4]{\bigg \lbrack \small \begin{array}{c c} #1, & #3 \\ \hline #2, & #4 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmSubb}[5]{\bigg \lbrack \normalsize #1 \text{ } \small \begin{array}{c c} #2, & #4 \\ \hline #3, & #5 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmsubbb}[6]{\bigg \lbrack \small \begin{array}{c c c} #1, & #3, & #5 \\ \hline #2, & #4, & #6 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmSubbb}[7]{\bigg \lbrack \normalsize #1 \text{ } \small \begin{array}{c c c} #2, & #4, & #6 \\ \hline #3, & #5, & #7 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmsubbbb}[8]{\bigg \lbrack \small \begin{array}{c c c c} #1, & #3, & #5, & #7 \\ \hline #2, & #4, & #6, & #8 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmSubbbb}[9]{\bigg \lbrack \normalsize #1 \text{ } \small \begin{array}{c c c c} #1, & #3, & #5, & #7 \\ \hline #2, & #4, & #6, & #8 \end{array}  \bigg \rbrack} %This is the substitution command.
\newcommand{\pmthm}{\mathpunct{\text{\scalebox{.5}[1]{$\boldsymbol\vdash$}}}} %This is the theorem sign.
\newcommand{\pmast}{\text{\resizebox{!}{.75\height}{\ding{107}}}} %This is the sign introducing a theorem number.
\newcommand{\pmcdot}{\text{\raisebox{.05cm}{$\boldsymbol\cdot$}}} %This is a sign introducing a theorem sub-number.
\newcommand{\pmiddf}{\mathbin{=}}
\newcommand{\pmdf}{\quad \text{Df}}
\newcommand{\pmpp}{\quad \text{Pp}}

%Square dots for scope, defined for up to six dots
\newcommand{\pmdot}{\mathrel{\hbox{\rule{.3ex}{.3ex}}}}
\newcommand{\pmdott}{\mathrel{\overset{\pmdot}{\pmdot}}}
\newcommand{\pmdottt}{\pmdott\hspace{.1em}\pmdot}
\newcommand{\pmdotttt}{\pmdott\hspace{.1em}\pmdott}
\newcommand{\pmdottttt}{\pmdott\hspace{.1em}\pmdott\hspace{.1em}\pmdot}
\newcommand{\pmdotttttt}{\pmdott\hspace{.1em}\pmdott\hspace{.1em}\pmdott}

%Logical connectives
\newcommand{\pmnot}{\mathord{\sim}}
\newcommand{\pmimp}{\mathbin{\boldsymbol{\supset}}}
\newcommand{\pmiff}{\mathbin{\equiv}}
\newcommand{\pmor}{\mathbin{\boldsymbol{\vee}}}
\newcommand{\pmall}[1]{(#1)}
\newcommand{\pmsome}[1]{(\text{\raisebox{.5em}{\rotatebox{180}{E}}}#1)}
\newcommand{\pmSome}{\text{\raisebox{.5em}{\rotatebox{180}{E}}}}
\newcommand{\pmand}{\mathrel{\hbox{\rule{.3ex}{.3ex}}}}
\newcommand{\pmandd}{\overset{\pmand}{\pmand}}
\newcommand{\pmanddd}{\pmandd\hspace{.1em}\pmand}
\newcommand{\pmandddd}{\pmandd\hspace{.1em}\pmandd}
\newcommand{\pmanddddd}{\pmandd\hspace{.1em}\pmandd\hspace{.1em}\pmand}
\newcommand{\pmandddddd}{\pmandd\hspace{.1em}\pmandd\hspace{.1em}\pmandd}

%Additional defined logic signs
\newcommand{\pmhat}[1]{\mathbf{\hat{\text{$#1$}}}}
\newcommand{\pmpf}[2]{#1#2} %for propositional functions of one variable
\newcommand{\pmpff}[3]{#1(#2, #3)} %for propositional functions of two variables
\newcommand{\pmpfff}[4]{#1(#2, #3, #4)} %for propositional functions of three variables
\newcommand{\pmshr}{\textbf{!}} %*12.1 and *12.11, used for predicative propositional functions
\newcommand{\pmpred}[2]{#1\pmshr#2} %for predicates (``predicative functions'') of one variable
\newcommand{\pmpredd}[3]{#1\pmshr(#2, #3)} %for predicates (``predicative functions'') of two variables
\newcommand{\pmpreddd}[4]{#1\pmshr(#2, #3, #4)} %for predicates (``predicative functions'') of three variables
\newcommand{\pmnid}{\mathrel{\ooalign{$=$\cr\hidewidth\footnotesize\rotatebox[origin=c]{210}{\textbf{/}}\hidewidth\cr}}} %*13.01
\newcommand{\pmiota}{\rotatebox[origin=c]{180}{$\iota$}} %the rotated Greek iota used in definite descriptions
\newcommand{\pmdsc}[1]{(\pmiota#1)} %*14.01
\newcommand{\pmDsc}{\pmiota} %*14.01
\newcommand{\pmexists}{\text{E}\pmshr} %*14.02

%Class signs
\newcommand{\pmcuni}{\text{\rotatebox[origin=c]{180}{$\Lambda$}}}
\newcommand{\pmcnull}{\Lambda}
\newcommand{\pmcls}[2]{\pmhat{#1}(#2)}
\newcommand{\pmCls}{\text{Cls}}
\newcommand{\pmClsn}[1]{\text{Cls}^{#1}}
\newcommand{\pmcexists}{\text{\raisebox{.5em}{\rotatebox{180}{E}}}\mathop{\pmshr}}
\newcommand{\pmccmp}[1]{\boldsymbol{-}#1}
\newcommand{\pmcmin}[2]{#1\boldsymbol{-}#2}
\newcommand{\pmcin}{\mathop{\epsilon}}
\newcommand{\pmccup}{\mathop{\scalebox{1.3}[1.75]{$\put(3, 2.5){\oval(4,4)[b]}\phantom{\circ}$}}}
\newcommand{\pmccap}{\mathop{\scalebox{1.3}[1.75]{$\put(3, 2){\oval(4,1)[t]}\phantom{\circ}$}}}
\newcommand{\pmcinc}{\mathop{\boldsymbol{\subset}}}

%Relation signs
\newcommand{\pmruni}{\dot{\text{\rotatebox[origin=c]{180}{$\Lambda$}}}}
\newcommand{\pmrnull}{\dot{\Lambda}}
\newcommand{\pmdscf}[2]{#1\textbf{`}#2}
\newcommand{\pmdscff}[2]{#1\textbf{`}\textbf{`}#2}
\newcommand{\pmdscfff}[2]{#1\textbf{`}\textbf{`}\textbf{`}#2}
\newcommand{\pmdscfr}[2]{#1_{\pmcin}\textbf{`}#2}
\newcommand{\pmdscfR}[1]{#1_{\pmcin}}
\newcommand{\pmdscfe}[2]{\mathop{\text{E}}\mathop{\pmshr\pmshr}\pmdscff{#1}{#2}}
\newcommand{\pmdm}[1]{\text{D}\textbf{`}#1}
\newcommand{\pmDm}{\text{D}}
\newcommand{\pmcdm}[1]{\text{\rotatebox[origin=c]{180}{D}}\textbf{`}#1}
\newcommand{\pmCdm}{\text{\rotatebox[origin=c]{180}{D}}}
\newcommand{\pmcmp}[1]{C\textbf{`}#1}
\newcommand{\pmCmp}{C}
\newcommand{\pmfld}[1]{F\textbf{`}#1}
\newcommand{\pmFld}{F}
\newcommand{\pmrel}[3]{\pmhat{#1}\pmhat{#2}#3}
\newcommand{\pmrele}[5]{#1\{\pmhat{#2}\pmhat{#3}#4(#2, #3)\}#5}
\newcommand{\pmrelep}[3]{#1\{#2\}#3}
\newcommand{\pmrcmp}[1]{\ooalign{$\hidewidth\raisebox{.25em}{$\cdot$}\hidewidth$\cr$\mathbf{\pmccmp}$}#1}
\newcommand{\pmrmin}[2]{#1\mathrel{\ooalign{$\hidewidth\raisebox{.25em}{$\cdot$}\hidewidth$\cr$\mathbf{\pmccmp}$}}#2}
\newcommand{\pmrexists}{\dot{\mathop{\text{\raisebox{.5em}{\rotatebox{180}{E}}}}}\mathop{\pmshr}}
\newcommand{\pmcrel}[1]{\breve{#1}}
\newcommand{\pmCnv}{\text{Cnv}}
\newcommand{\pmcnv}[1]{\breve{#1}}
\newcommand{\pmcnvr}[1]{\text{Cnv}\textbf{`}#1}
\newcommand{\pmrcup}{\mathrel{\ooalign{$\hidewidth\cdot\hidewidth$\cr$\mathbf{\pmccup}$}}}
\newcommand{\pmrcap}{\mathrel{\ooalign{$\hidewidth\raisebox{.3em}{$\cdot$}\hidewidth$\cr$\mathbf{\pmccap}$}}}
\newcommand{\pmrinc}{\mathrel{\ooalign{$\hidewidth\cdot\hidewidth$\cr$\mathbf{\pmcinc}$}}}
\newcommand{\pmrrf}[2]{\overset{\boldsymbol{\rightarrow}}{#1\textbf{`}}#2}
\newcommand{\pmRrf}[1]{\overset{\boldsymbol{\rightarrow}}{#1}}
\newcommand{\pmrrl}[2]{\overset{\boldsymbol{\leftarrow}}{#1\textbf{`}}#2}
\newcommand{\pmRrl}[1]{\overset{\boldsymbol{\leftarrow}}{#1}}
\newcommand{\pmsg}[1]{\text{sg}\textbf{`}#1}
\newcommand{\pmgs}[1]{\text{gs}\textbf{`}#1}
\newcommand{\pmSg}{\text{sg}}
\newcommand{\pmGs}{\text{gs}}
\newcommand{\pmRprd}{\mathop{|}}
\newcommand{\pmrprd}[2]{{#1}\mathop{|}{#2}}
\newcommand{\pmrprdn}[2]{#1^{#2}}
\newcommand{\pmrld}[2]{#1 \boldsymbol{\upharpoonleft} #2}
\newcommand{\pmrlcd}[2]{#1 \boldsymbol{\upharpoonright} #2}
\newcommand{\pmrlf}[3]{#1 \boldsymbol{\upharpoonleft} #2 \boldsymbol{\upharpoonright} #3}
\newcommand{\pmrl}[2]{#1 \boldsymbol{\uparrow} #2}
\newcommand{\pmrlF}[2]{#1 \mathbin{\ooalign{$\upharpoonright$\cr\hidewidth\rotatebox[origin=c]{180}{\text{$\upharpoonleft$}}\hidewidth\cr}} #2}
\newcommand{\pmop}{\mathop{\text{\Female}}}
\newcommand{\pmopc}[2]{#1 \mathop{\underset{\textbf{''}}{\text{\Female}}} #2}

%Products and sums of classes of classes or relations
\newcommand{\pmccsum}[1]{p\textbf{`}#1}
\newcommand{\pmccprd}[1]{s\textbf{`}#1}
\newcommand{\pmcrsum}[1]{\dot{p}\textbf{`}#1}
\newcommand{\pmcrprd}[1]{\dot{s}\textbf{`}#1}
\newcommand{\pmRprdd}{\mathop{||}}
\newcommand{\pmrprdd}[2]{{#1}\mathop{||}{#2}}

%Identity and Diversity
\newcommand{\pmrid}{I}
\newcommand{\pmrdiv}{J}
\newcommand{\pmcunit}[1]{\iota\textbf{`}#1}
\newcommand{\pmcUnit}{\iota}
\newcommand{\pmcunits}[1]{\breve{\iota}\textbf{`}#1}

%Cardinal numbers
\newcommand{\pmcn}[1]{#1}

%Ordinal numbers
\newcommand{\pmrn}[1]{#1_r}
\newcommand{\pmdn}[1]{\dot{#1}}
\newcommand{\pmoc}[2]{#1 \boldsymbol{\downarrow} #2}

%Subclasses and subrelations
\newcommand{\pmscl}[1]{\text{Cl}\textbf{`}#1}
\newcommand{\pmsCl}{\text{Cl}}
\newcommand{\pmscle}[1]{\text{Cl ex}\textbf{`}#1}
\newcommand{\pmsCle}{\text{Cl ex}}
\newcommand{\pmscls}[1]{\text{Cls}\textbf{`}#1}
\newcommand{\pmsrl}[1]{\text{Rl}\textbf{`}#1}
\newcommand{\pmsRl}{\text{Rl}}
\newcommand{\pmsrle}[1]{\text{Rl ex}\textbf{`}#1}
\newcommand{\pmsRle}{\text{Rl ex}}
\newcommand{\pmsrel}[1]{\text{Rel}\textbf{`}#1}
\newcommand{\pmRel}{\text{Rel}}
\newcommand{\pmReln}[1]{\text{Rel}^{#1}}
\newcommand{\pmrin}{\mathop{\epsilon}}

%Relative type symbols
\newcommand{\pmrt}[1]{t\textbf{`}#1}
\newcommand{\pmrti}[2]{t^{#1}\textbf{`}#2}
\newcommand{\pmrtc}[2]{t_{#1}\textbf{`}#2}
\newcommand{\pmrtri}[2]{t^{#1}\textbf{`}#2}
\newcommand{\pmrtrc}[2]{t_{#1}\textbf{`}#2}
\newcommand{\pmrtrci}[3]{t_{#1}^{\text{ }#2}\textbf{`}#3}
\newcommand{\pmrtric}[3]{^{#1}t_{#2}\textbf{`}#3}
\newcommand{\pmrtdi}[2]{#1_{#2}}
\newcommand{\pmrtdc}[2]{#1(#2)}
\newcommand{\pmrtdri}[2]{#1_{#2}}
\newcommand{\pmrtdrc}[2]{#1(#2)}

%Similarity relation signs
\newcommand{\pmrdc}[2]{#1\boldsymbol{\to}#2}
\newcommand{\pmsm}{\mathrel{\text{sm}}}
\newcommand{\pmsmbar}{\mathrel{\overline{\text{sm}}}}
\newcommand{\pmsmarr}{\overrightarrow{{\pmsm}}}
\newcommand{\pmonemany}{1\boldsymbol{\to}\pmCls}
\newcommand{\pmmanyone}{\pmCls\boldsymbol{\to}1}
\newcommand{\pmoneone}{1\boldsymbol{\to}1}

%Selections
\newcommand{\pmselp}[1]{P_{\small\Delta}\mathbf{`}#1}
\newcommand{\pmSelp}{P_{\Delta}}
\newcommand{\pmsele}[1]{\pmcin_{\small\Delta}\mathbf{`}#1}
\newcommand{\pmSele}{\pmcin_{\Delta}}
\newcommand{\pmself}[1]{F_{\small\Delta}\mathbf{`}#1}
\newcommand{\pmSelf}{F_{\Delta}}
\newcommand{\pmexc}{\text{Cls}^2 \mathop{\text{excl}}}
\newcommand{\pmexcc}[1]{\text{Cl} \mathop{\text{excl}}\textbf{`}#1}
\newcommand{\pmexcn}{\text{Cls} \mathop{\text{ex}}^2 \mathop{\text{excl}}}
\newcommand{\pmselc}[2]{#1 \mathrel{\rotatebox[origin=c]{270}{$\boldsymbol{\mapsto}$}} #2}
\newcommand{\pmmultr}{\mathop{\text{Rel}} \mathop{\text{Mult}}}
\newcommand{\pmmultc}{\mathop{\text{Cls}^2} \mathop{\text{Mult}}}
\newcommand{\pmmultax}{\mathop{\text{Mult}} \mathop{\text{ax}}}

%Inductive relations
\newcommand{\pmanc}[1]{#1_\pmast}
\newcommand{\pmancc}[1]{\pmcnv{#1}_\pmast}
\newcommand{\pmrst}[1]{#1_\text{st}}
\newcommand{\pmrts}[1]{#1_\text{ts}}
\newcommand{\pmpot}[1]{\text{Pot}\mathbf{`}#1}
\newcommand{\pmpotid}[1]{\text{Potid}\mathbf{`}#1}
\newcommand{\pmpo}[1]{#1_\text{po}}
\newcommand{\pmB}{B}
\newcommand{\pmmin}[1]{\text{min}_{#1}}
\newcommand{\pmmax}[1]{\text{max}_{#1}}
\newcommand{\pmMin}{\text{min}}
\newcommand{\pmMax}{\text{max}}
\newcommand{\pmgen}[1]{\text{gen}\mathbf{`}#1}
\newcommand{\pmGen}{\text{gen}}
\newcommand{\pmefr}[2]{#1\pmast#2}
\newcommand{\pmipr}[2]{I_{#1}\textbf{`}#2}
\newcommand{\pmjpr}[2]{J_{#1}\textbf{`}#2}
\newcommand{\pmfr}[2]{\overset{\boldsymbol{\leftrightarrow}}{#1}\textbf{`}#2}

%Cardinality 
\newcommand{\pmnc}[1]{\text{Nc}\textbf{`}#1}
\newcommand{\pmNc}{\text{Nc}}
\newcommand{\pmNC}{\text{NC}}
\newcommand{\pmnoc}[1]{\text{N}_0\text{c}\textbf{`}#1}
\newcommand{\pmNoc}{\text{N}_0\text{c}}
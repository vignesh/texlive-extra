%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{qxmlmtt.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{QX}{mlmtt}{\hyphenchar \font\m@ne}

\ifx\mlmtt@use@light@as@normal\@empty
% macro defined, so we use the light variant as medium (m), and
% medium as semi-bold (sb):
\DeclareFontShape{QX}{mlmtt}{sb}{n}
     {<-8.5>   qx-mlmtt8     <8.5-9.5> qx-mlmtt9
      <9.5-11> qx-mlmtt10    <11->     qx-mlmtt12
      }{}
\DeclareFontShape{QX}{mlmtt}{sb}{it}
     {<-> qx-mlmtti10}{}
\DeclareFontShape{QX}{mlmtt}{sb}{sl}
     {<-> qx-mlmtto10}{}
\DeclareFontShape{QX}{mlmtt}{sb}{sc}
     {<-> qx-mlmtcsc10}{}
\DeclareFontShape{QX}{mlmtt}{sb}{scsl}
     {<-> qx-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{QX}{mlmtt}{m}{n}
     {<-> qx-mlmtl10}{}
\DeclareFontShape{QX}{mlmtt}{m}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{QX}{mlmtt}{m}{sl}
     {<-> qx-mlmtlo10}{}
\DeclareFontShape{QX}{mlmtt}{c}{n}
     {<-> qx-mlmtlc10}{}
\DeclareFontShape{QX}{mlmtt}{c}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{QX}{mlmtt}{c}{sl}
     {<-> qx-mlmtlco10}{}
\else
% usual setup of variants:
\DeclareFontShape{QX}{mlmtt}{m}{n}
     {<-8.5>   qx-mlmtt8     <8.5-9.5> qx-mlmtt9
      <9.5-11> qx-mlmtt10    <11->     qx-mlmtt12
      }{}
\DeclareFontShape{QX}{mlmtt}{m}{it}
     {<-> qx-mlmtti10}{}
\DeclareFontShape{QX}{mlmtt}{m}{sl}
     {<-> qx-mlmtto10}{}
\DeclareFontShape{QX}{mlmtt}{m}{sc}
     {<-> qx-mlmtcsc10}{}
\DeclareFontShape{QX}{mlmtt}{m}{scsl}
     {<-> qx-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{QX}{mlmtt}{l}{n}
     {<-> qx-mlmtl10}{}
\DeclareFontShape{QX}{mlmtt}{l}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{QX}{mlmtt}{l}{sl}
     {<-> qx-mlmtlo10}{}
\DeclareFontShape{QX}{mlmtt}{lc}{n}
     {<-> qx-mlmtlc10}{}
\DeclareFontShape{QX}{mlmtt}{lc}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{QX}{mlmtt}{lc}{sl}
     {<-> qx-mlmtlco10}{}
\fi
% bold is always bold (b):
\DeclareFontShape{QX}{mlmtt}{b}{n}
     {<-> qx-mlmtk10}{}
\DeclareFontShape{QX}{mlmtt}{b}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{QX}{mlmtt}{b}{sl}
{<-> qx-mlmtko10}{}
\DeclareFontShape{QX}{mlmtt}{bx}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{QX}{mlmtt}{bx}{n}
     {<->ssub*mlmtt/b/n}{}
\DeclareFontShape{QX}{mlmtt}{bx}{sl}
     {<->ssub*mlmtt/b/sl}{}
\endinput
%%
%% End of file `qxmlmtt.fd'.

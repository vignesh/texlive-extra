%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{il2mlmtt.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{IL2}{mlmtt}{\hyphenchar \font\m@ne}

\ifx\mlmtt@use@light@as@normal\@empty
% macro defined, so we use the light variant as medium (m), and
% medium as semi-bold (sb):
\DeclareFontShape{IL2}{mlmtt}{sb}{n}
     {<-8.5>   cs-mlmtt8     <8.5-9.5> cs-mlmtt9
      <9.5-11> cs-mlmtt10    <11->     cs-mlmtt12
      }{}
\DeclareFontShape{IL2}{mlmtt}{sb}{it}
     {<-> cs-mlmtti10}{}
\DeclareFontShape{IL2}{mlmtt}{sb}{sl}
     {<-> cs-mlmtto10}{}
\DeclareFontShape{IL2}{mlmtt}{sb}{sc}
     {<-> cs-mlmtcsc10}{}
\DeclareFontShape{IL2}{mlmtt}{sb}{scsl}
     {<-> cs-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{IL2}{mlmtt}{m}{n}
     {<-> cs-mlmtl10}{}
\DeclareFontShape{IL2}{mlmtt}{m}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{IL2}{mlmtt}{m}{sl}
     {<-> cs-mlmtlo10}{}
\DeclareFontShape{IL2}{mlmtt}{c}{n}
     {<-> cs-mlmtlc10}{}
\DeclareFontShape{IL2}{mlmtt}{c}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{IL2}{mlmtt}{c}{sl}
     {<-> cs-mlmtlco10}{}
\else
% usual setup of variants:
\DeclareFontShape{IL2}{mlmtt}{m}{n}
     {<-8.5>   cs-mlmtt8     <8.5-9.5> cs-mlmtt9
      <9.5-11> cs-mlmtt10    <11->     cs-mlmtt12
      }{}
\DeclareFontShape{IL2}{mlmtt}{m}{it}
     {<-> cs-mlmtti10}{}
\DeclareFontShape{IL2}{mlmtt}{m}{sl}
     {<-> cs-mlmtto10}{}
\DeclareFontShape{IL2}{mlmtt}{m}{sc}
     {<-> cs-mlmtcsc10}{}
\DeclareFontShape{IL2}{mlmtt}{m}{scsl}
     {<-> cs-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{IL2}{mlmtt}{l}{n}
     {<-> cs-mlmtl10}{}
\DeclareFontShape{IL2}{mlmtt}{l}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{IL2}{mlmtt}{l}{sl}
     {<-> cs-mlmtlo10}{}
\DeclareFontShape{IL2}{mlmtt}{lc}{n}
     {<-> cs-mlmtlc10}{}
\DeclareFontShape{IL2}{mlmtt}{lc}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{IL2}{mlmtt}{lc}{sl}
     {<-> cs-mlmtlco10}{}
\fi
% bold is always bold (b):
\DeclareFontShape{IL2}{mlmtt}{b}{n}
     {<-> cs-mlmtk10}{}
\DeclareFontShape{IL2}{mlmtt}{b}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{IL2}{mlmtt}{b}{sl}
{<-> cs-mlmtko10}{}
\DeclareFontShape{IL2}{mlmtt}{bx}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{IL2}{mlmtt}{bx}{n}
     {<->ssub*mlmtt/b/n}{}
\DeclareFontShape{IL2}{mlmtt}{bx}{sl}
     {<->ssub*mlmtt/b/sl}{}
\endinput
%%
%% End of file `il2mlmtt.fd'.

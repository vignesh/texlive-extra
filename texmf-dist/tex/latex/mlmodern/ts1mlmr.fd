%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{ts1mlmr.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{TS1}{mlmr}{}
\DeclareFontShape{TS1}{mlmr}{m}{n}%
     {<-5.5>    ts1-mlmr5     <5.5-6.5> ts1-mlmr6
      <6.5-7.5> ts1-mlmr7     <7.5-8.5> ts1-mlmr8
      <8.5-9.5> ts1-mlmr9     <9.5-11>  ts1-mlmr10
      <11-15>   ts1-mlmr12
      <15-> ts1-mlmr17
      }{}
\DeclareFontShape{TS1}{mlmr}{m}{sl}%
     {<-8.5>    ts1-mlmro8    <8.5-9.5> ts1-mlmro9
      <9.5-11>  ts1-mlmro10   <11-15>   ts1-mlmro12
      <15-> ts1-mlmro17
      }{}
\DeclareFontShape{TS1}{mlmr}{m}{it}%
     {<-7.5>    ts1-mlmri7
      <7.5-8.5> ts1-mlmri8    <8.5-9.5> ts1-mlmri9
      <9.5-11>  ts1-mlmri10   <11->   ts1-mlmri12
      }{}
\DeclareFontShape{TS1}{mlmr}{m}{sc}%
     {<-> ts1-mlmcsc10}{}
\DeclareFontShape{TS1}{mlmr}{m}{ui}%
     {<-> ts1-mlmu10}{}
%
% Is this the right 'shape'?:
\DeclareFontShape{TS1}{mlmr}{m}{scsl}%
     {<-> ts1-mlmcsco10}{}
%%%%%%% bold series
\DeclareFontShape{TS1}{mlmr}{b}{n}
     {<-> ts1-mlmb10}{}
\DeclareFontShape{TS1}{mlmr}{b}{sl}
     {<-> ts1-mlmbo10}{}
%%%%%%% bold extended series
\DeclareFontShape{TS1}{mlmr}{bx}{n}
     {<-5.5>   ts1-mlmbx5      <5.5-6.5> ts1-mlmbx6
      <6.5-7.5> ts1-mlmbx7      <7.5-8.5> ts1-mlmbx8
      <8.5-9.5> ts1-mlmbx9      <9.5-11>  ts1-mlmbx10
      <11->   ts1-mlmbx12
      }{}
\DeclareFontShape{TS1}{mlmr}{bx}{it}
     {<-> ts1-mlmbxi10}{}
\DeclareFontShape{TS1}{mlmr}{bx}{sl}
     {<-> ts1-mlmbxo10}{}
%%%%%%% Font/shape undefined, therefore substituted
\DeclareFontShape{TS1}{mlmr}{b}{it}
     {<->sub * mlmr/b/sl}{}
\endinput
%%
%% End of file `ts1mlmr.fd'.

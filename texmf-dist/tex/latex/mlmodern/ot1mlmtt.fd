%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{ot1mlmtt.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{OT1}{mlmtt}{\hyphenchar \font\m@ne}

\ifx\mlmtt@use@light@as@normal\@empty
% macro defined, so we use the light variant as medium (m), and
% medium as semi-bold (sb):
\DeclareFontShape{OT1}{mlmtt}{sb}{n}
     {<-8.5>   rm-mlmtt8     <8.5-9.5> rm-mlmtt9
      <9.5-11> rm-mlmtt10    <11->     rm-mlmtt12
      }{}
\DeclareFontShape{OT1}{mlmtt}{sb}{it}
     {<-> rm-mlmtti10}{}
\DeclareFontShape{OT1}{mlmtt}{sb}{sl}
     {<-> rm-mlmtto10}{}
\DeclareFontShape{OT1}{mlmtt}{sb}{sc}
     {<-> rm-mlmtcsc10}{}
\DeclareFontShape{OT1}{mlmtt}{sb}{scsl}
     {<-> rm-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{OT1}{mlmtt}{m}{n}
     {<-> rm-mlmtl10}{}
\DeclareFontShape{OT1}{mlmtt}{m}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{OT1}{mlmtt}{m}{sl}
     {<-> rm-mlmtlo10}{}
\DeclareFontShape{OT1}{mlmtt}{c}{n}
     {<-> rm-mlmtlc10}{}
\DeclareFontShape{OT1}{mlmtt}{c}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{OT1}{mlmtt}{c}{sl}
     {<-> rm-mlmtlco10}{}
\else
% usual setup of variants:
\DeclareFontShape{OT1}{mlmtt}{m}{n}
     {<-8.5>   rm-mlmtt8     <8.5-9.5> rm-mlmtt9
      <9.5-11> rm-mlmtt10    <11->     rm-mlmtt12
      }{}
\DeclareFontShape{OT1}{mlmtt}{m}{it}
     {<-> rm-mlmtti10}{}
\DeclareFontShape{OT1}{mlmtt}{m}{sl}
     {<-> rm-mlmtto10}{}
\DeclareFontShape{OT1}{mlmtt}{m}{sc}
     {<-> rm-mlmtcsc10}{}
\DeclareFontShape{OT1}{mlmtt}{m}{scsl}
     {<-> rm-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{OT1}{mlmtt}{l}{n}
     {<-> rm-mlmtl10}{}
\DeclareFontShape{OT1}{mlmtt}{l}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{OT1}{mlmtt}{l}{sl}
     {<-> rm-mlmtlo10}{}
\DeclareFontShape{OT1}{mlmtt}{lc}{n}
     {<-> rm-mlmtlc10}{}
\DeclareFontShape{OT1}{mlmtt}{lc}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{OT1}{mlmtt}{lc}{sl}
     {<-> rm-mlmtlco10}{}
\fi
% bold is always bold (b):
\DeclareFontShape{OT1}{mlmtt}{b}{n}
     {<-> rm-mlmtk10}{}
\DeclareFontShape{OT1}{mlmtt}{b}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{OT1}{mlmtt}{b}{sl}
{<-> rm-mlmtko10}{}
\DeclareFontShape{OT1}{mlmtt}{bx}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{OT1}{mlmtt}{bx}{n}
     {<->ssub*mlmtt/b/n}{}
\DeclareFontShape{OT1}{mlmtt}{bx}{sl}
     {<->ssub*mlmtt/b/sl}{}
\endinput
%%
%% End of file `ot1mlmtt.fd'.

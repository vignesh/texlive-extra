%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{l7xmlmtt.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{L7x}{mlmtt}{\hyphenchar \font\m@ne}

\ifx\mlmtt@use@light@as@normal\@empty
% macro defined, so we use the light variant as medium (m), and
% medium as semi-bold (sb):
\DeclareFontShape{L7x}{mlmtt}{sb}{n}
     {<-8.5>   l7x-mlmtt8     <8.5-9.5> l7x-mlmtt9
      <9.5-11> l7x-mlmtt10    <11->     l7x-mlmtt12
      }{}
\DeclareFontShape{L7x}{mlmtt}{sb}{it}
     {<-> l7x-mlmtti10}{}
\DeclareFontShape{L7x}{mlmtt}{sb}{sl}
     {<-> l7x-mlmtto10}{}
\DeclareFontShape{L7x}{mlmtt}{sb}{sc}
     {<-> l7x-mlmtcsc10}{}
\DeclareFontShape{L7x}{mlmtt}{sb}{scsl}
     {<-> l7x-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{L7x}{mlmtt}{m}{n}
     {<-> l7x-mlmtl10}{}
\DeclareFontShape{L7x}{mlmtt}{m}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{L7x}{mlmtt}{m}{sl}
     {<-> l7x-mlmtlo10}{}
\DeclareFontShape{L7x}{mlmtt}{c}{n}
     {<-> l7x-mlmtlc10}{}
\DeclareFontShape{L7x}{mlmtt}{c}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{L7x}{mlmtt}{c}{sl}
     {<-> l7x-mlmtlco10}{}
\else
% usual setup of variants:
\DeclareFontShape{L7x}{mlmtt}{m}{n}
     {<-8.5>   l7x-mlmtt8     <8.5-9.5> l7x-mlmtt9
      <9.5-11> l7x-mlmtt10    <11->     l7x-mlmtt12
      }{}
\DeclareFontShape{L7x}{mlmtt}{m}{it}
     {<-> l7x-mlmtti10}{}
\DeclareFontShape{L7x}{mlmtt}{m}{sl}
     {<-> l7x-mlmtto10}{}
\DeclareFontShape{L7x}{mlmtt}{m}{sc}
     {<-> l7x-mlmtcsc10}{}
\DeclareFontShape{L7x}{mlmtt}{m}{scsl}
     {<-> l7x-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{L7x}{mlmtt}{l}{n}
     {<-> l7x-mlmtl10}{}
\DeclareFontShape{L7x}{mlmtt}{l}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{L7x}{mlmtt}{l}{sl}
     {<-> l7x-mlmtlo10}{}
\DeclareFontShape{L7x}{mlmtt}{lc}{n}
     {<-> l7x-mlmtlc10}{}
\DeclareFontShape{L7x}{mlmtt}{lc}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{L7x}{mlmtt}{lc}{sl}
     {<-> l7x-mlmtlco10}{}
\fi
% bold is always bold (b):
\DeclareFontShape{L7x}{mlmtt}{b}{n}
     {<-> l7x-mlmtk10}{}
\DeclareFontShape{L7x}{mlmtt}{b}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{L7x}{mlmtt}{b}{sl}
{<-> l7x-mlmtko10}{}
\DeclareFontShape{L7x}{mlmtt}{bx}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{L7x}{mlmtt}{bx}{n}
     {<->ssub*mlmtt/b/n}{}
\DeclareFontShape{L7x}{mlmtt}{bx}{sl}
     {<->ssub*mlmtt/b/sl}{}
\endinput
%%
%% End of file `l7xmlmtt.fd'.

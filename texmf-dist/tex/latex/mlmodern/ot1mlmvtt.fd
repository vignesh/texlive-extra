%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{ot1mlmvtt.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{OT1}{mlmvtt}{}

\ifx\mlmtt@use@light@as@normal\@empty
% macro defined, so we use the light variant as medium (m), and
% medium as semi-bold (sb):
\DeclareFontShape{OT1}{mlmvtt}{sb}{n}
     {<-> rm-mlmvtt10}{}
\DeclareFontShape{OT1}{mlmvtt}{sb}{sl}
     {<-> rm-mlmvtto10}{}
\DeclareFontShape{OT1}{mlmvtt}{sb}{it}
     {<->sub*mlmvtt/m/sl}{}
%%%%%%%% light (l) and dark (b) variants:
\DeclareFontShape{OT1}{mlmvtt}{m}{n}
     {<-> rm-mlmvtl10}{}
\DeclareFontShape{OT1}{mlmvtt}{m}{sl}
     {<-> rm-mlmvtlo10}{}
\DeclareFontShape{OT1}{mlmvtt}{m}{it}
     {<->sub*mlmvtt/l/sl}{}

\else
% usual setup of variants:
\DeclareFontShape{OT1}{mlmvtt}{m}{n}
     {<-> rm-mlmvtt10}{}
\DeclareFontShape{OT1}{mlmvtt}{m}{sl}
     {<-> rm-mlmvtto10}{}
\DeclareFontShape{OT1}{mlmvtt}{m}{it}
     {<->sub*mlmvtt/m/sl}{}
%%%%%%%% light (l) and dark (b) variants:
\DeclareFontShape{OT1}{mlmvtt}{l}{n}
     {<-> rm-mlmvtl10}{}
\DeclareFontShape{OT1}{mlmvtt}{l}{sl}
     {<-> rm-mlmvtlo10}{}
\DeclareFontShape{OT1}{mlmvtt}{l}{it}
     {<->sub*mlmvtt/l/sl}{}
\fi
% bold is always bold (b):
\DeclareFontShape{OT1}{mlmvtt}{b}{n}
     {<-> rm-mlmvtk10}{}
\DeclareFontShape{OT1}{mlmvtt}{b}{sl}
     {<-> rm-mlmvtko10}{}
\DeclareFontShape{OT1}{mlmvtt}{b}{it}
     {<->sub*mlmvtt/b/sl}{}
\DeclareFontShape{OT1}{mlmvtt}{bx}{n}
     {<->ssub*mlmvtt/b/n}{}
\DeclareFontShape{OT1}{mlmvtt}{bx}{sl}
     {<->ssub*mlmvtt/b/sl}{}
\DeclareFontShape{OT1}{mlmvtt}{bx}{it}
     {<->sub*mlmvtt/b/sl}{}
\endinput
%%
%% End of file `ot1mlmvtt.fd'.

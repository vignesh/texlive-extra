%% ----------------------------------------------------------------
%% ftc-notebook --- format for an FIRST Tech Challenge (FTC) engineering
%%                  notebook with daily entries, team story, bio,
%%                  and list of fig/table/tasks
%% Version:         Released 2019/02/16, Version 1.1
%% Authors:         FTC 9773, Team Robocracy
%% E-mail:          ftcrobocracy@gmail.com
%% ----------------------------------------------------------------
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{ftc9773}[2019/02/16 FIRST Tech Challenge (FTC) %
  package for engineering notebook by Robocracy]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Package Options

\RequirePackage{kvoptions}
\SetupKeyvalOptions{
  family=FTC,
  prefix=FTC@
}
\DeclareStringOption [FTC 000]             {Num}      [FTC 000]
\DeclareStringOption [Outstanding Team]    {Name}     [Outstanding Team]
\DeclareStringOption [logo.jpg]            {Logo}     [logo.jpg]
\DeclareStringOption [Engineering Notebook]{Notebook} [Engineering Notebook]
\DeclareStringOption [Strategy]            {KindOne}  [Strategy]
\DeclareStringOption [Design]              {KindTwo}  [Design]
\DeclareStringOption [Build]               {KindThree}[Build]
\DeclareStringOption [Math/Physics]        {KindFour} [Math/Physics]
\DeclareStringOption [Software]            {KindFive} [Software]
\DeclareStringOption [Team]                {KindSix}  [Team]
\DeclareBoolOption                         {Grid}
\DeclareBoolOption                         {Green}
\DeclareBoolOption                         {Orange}
\DeclareBoolOption                         {Blue}

\ProcessKeyvalOptions*

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% includes

%% general support
\RequirePackage{longtable}
\RequirePackage{datetime}
\newdateformat{monthyeardate}{ \monthname[\THEMONTH] \THEYEAR }
\RequirePackage[labelfont=bf, textfont=bf]{caption}
\RequirePackage{subcaption}
\RequirePackage{xparse}
\RequirePackage{float}
\RequirePackage{needspace}
\RequirePackage{mathptmx}
\RequirePackage{anyfontsize}
\RequirePackage{t1enc}
\RequirePackage{suffix}
\RequirePackage[absolute, overlay]{textpos}

%% support for tables
\RequirePackage{array}
\RequirePackage{multirow}
\RequirePackage{tabu}
\RequirePackage{paralist}

%% capitalization \capitalisewords{Will Get First Letters in Cap}
\RequirePackage{mfirstuc}
\MFUnocap{are}
\MFUnocap{or}
\MFUnocap{and}
\MFUnocap{for}
\MFUnocap{by}
\MFUnocap{a}
\MFUnocap{an}
\MFUnocap{in}
\MFUnocap{am}
\MFUnocap{pm}
\MFUnocap{to}
\MFUnocap{of}

%% page
\RequirePackage[letterpaper, portrait, margin=2cm]{geometry}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\RequirePackage{titlesec}

%% image
\RequirePackage{graphicx}
\graphicspath{{src/images/}}

%% support for color
\RequirePackage[table,dvipsnames]{xcolor}
\RequirePackage{colortbl}

%% support for callout (inlined below)
\RequirePackage{calc}
\setlength\arrayrulewidth{2pt}

%% for arrays of variables
\RequirePackage{arrayjobx}
\RequirePackage{multido}

%% conditional
\RequirePackage{ifthen}

%% to use apostroph as \textquotesingle
\RequirePackage{textcomp}
\RequirePackage[utf8]{inputenx}
\RequirePackage{newunicodechar}

%% for code listing ( \begin{lstlisting} \end{lstlisting}
\RequirePackage{listings}

 %% custom list
\RequirePackage{tocloft}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% start of inlined callout (because package is not aways present)
%% modified only to "un-package it." It was hardwired for the desired
%% color scheme, and the arrow was made wider. The original can be found
%% at CTAN.org


\RequirePackage{tikz}
\usetikzlibrary{calc}
\RequirePackage{xifthen}

\newcommand*{\focol}{white}
\newcommand*{\bgcol}{black}
\newcommand*{\arcol}{red}

%% ==================================================================

\newenvironment{annotate}[2]
{ \begin{tikzpicture}[scale=#2]
  % Annotate
  \node (pic) at (0,0) {#1};%
  \newdimen\xtic
  \newdimen\ytic
  \pgfextractx\xtic{\pgfpointanchor{pic}{east}}
  \pgfmathparse{int(\xtic/1cm)}
  \pgfmathsetmacro\xtic{\pgfmathresult}
  \pgfextracty\ytic{\pgfpointanchor{pic}{north}}
  \pgfmathparse{int(\ytic/1cm)}
  \pgfmathsetmacro\ytic{\pgfmathresult}
}%
{ \end{tikzpicture} }

%% ==================================================================

\newcommand{\helpgrid}[1][\bgcol]{
  \draw[help lines, color=#1] (pic.south west) grid (pic.north east);%
    \fill[#1] (0,0) circle (3pt);%
  \foreach \i in {-\xtic,...,\xtic} {%
    \node at (\i+0.2,0.2) {\color{#1} \tiny \i};}
  \foreach \i in {-\ytic,...,\ytic} {%
    \node at (0.2,\i+0.2) {\color{#1} \tiny \i};}
}

\newcommand{\callout}[3]{%
  \node [fill=\bgcol] (text) at (#1) {\scriptsize\color{\focol} #2};
  \draw [line width=0.9mm,\arcol,->] (text) -- (#3);
}

\newcommand{\note}[2]{%
  \node [fill=\bgcol] at (#1) {\scriptsize\color{\focol} #2};
}

\newcommand{\arrow}[2]{%
  \draw [\arcol,thick,->] (#1) -- (#2);
}

%% === EOF ================================================
%% end of inlined callout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% customizations arrays

\newarray\@TaskDate
\@TaskDate(1)={}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% counters (private)
\newcounter{TaskCounter} \setcounter{TaskCounter}{0}
\newcounter{TaskSection} \setcounter{TaskSection}{0}
\newcounter{TaskSubSection}[TaskSection] \setcounter{TaskSubSection}{0}
\newcounter{TaskSubSubSection}[TaskSubSection] \setcounter{TaskSubSubSection}{0}

\renewcommand{\theTaskSection}{\arabic{TaskSection}}
\renewcommand{\theTaskSubSection}{\arabic{TaskSection}.{\arabic{TaskSubSection}}}

\newcounter{SigneeCounter}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% new month
\NewDocumentCommand{\Month}{m m}
%% 1: month
%% 2: picture
{
  \cleardoublepage
  \newpage
  \@TaskDate(1)={#1,}
  \begin{flushleft}
  \tabulinesep=1.2mm
  \begin{tabu}{p{2cm}>{\raggedright\arraybackslash}p{14.7cm}}
      \multirow{2}{*}{\includegraphics[width=2cm]{\FTC@Logo}}
       & \textbf{\Large \color{TitleColor} \capitalisewords{#1}} \\
       & \\ \cline{2-2} \\
  \end{tabu}
  \vspace{10mm}  \\
  \end{flushleft}
  {\centering \includegraphics[width=0.85\textwidth]{#2} \\}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NewDocumentEnvironment{Meeting}{O{Meeting} m m m m m}
%% 1: Type Meeting/Pre-Season
%% 5 2: Title of Meeting
%% 2 3: Date
%% 3 4: Time
%% 4 5: Who participated
%% 6: Items
{
  %% arrays init
  \newarray\TaskTitle
  \newarray\TaskLabel
  \newarray\SigneeName
  \setcounter{SigneeCounter}{0}

  %% print first table with logo, meeting type, date, Title
  \clearpage
  \newpage
  \@TaskDate(1)={#3, Task \theTaskSection, }
   \begin{flushleft}
   \tabulinesep=1.2mm
   \begin{tabu}{p{2cm}>{\raggedright\arraybackslash}p{14.7cm}}
       \multirow{3}{*}{\includegraphics[width=2cm]{\FTC@Logo}}
           & \textbf{\Large \color{TitleColor} \capitalisewords{#1 -  #3.}} \\
           & {\Large \capitalisewords{#2.}} \\
           & \\ \cline{2-2} \\
   \end{tabu}
   \vspace{5mm}  \\
   %% print time and participant
   {\color{TitleColor} \textbf{Time:}} {\capitalisewords{#4.}} \\
   {\color{TitleColor} \textbf{Meeting Participants:}} {#5.} \\
   \vspace{5mm}
   %% print task box
   \rowcolors{1}{MainTableCellColor}{MainTableCellColor}
   \begin{tabu}{|>{\raggedright\arraybackslash}p{1cm}|>{\raggedright\arraybackslash}p{5.5cm}|>%
        {\raggedright\arraybackslash}p{9.7cm}|}
      \arrayrulecolor{TitleColor} \hline
      \cellcolor{MainTableHeaderColor} &
      \cellcolor{MainTableHeaderColor} \textbf{Task:} &
      \cellcolor{MainTableHeaderColor} \textbf{Goals and Reflections:} \\  \hline
      #6
  \end{tabu}
  \end{flushleft}
}
{
  \needspace{3cm}
  \begin{flushleft}
  \rowcolors{1}{MainTableCellColor}{MainTableCellColor}
  \tabulinesep=1.2mm
  \begin{tabu}{|>{\raggedright\arraybackslash}p{12.5cm}>{\raggedright\arraybackslash}p{4cm}|}
      \arrayrulecolor{TitleColor} \hline
      \cellcolor{MainTableHeaderColor} \textbf{Signed by: } \ListSignee &%
        \cellcolor{MainTableHeaderColor} \textbf{Date:} \\  \hline
      &  \\
      & #3   \\ \hline
  \end{tabu}
  \end{flushleft}
  %% delete array
  \delarray\TaskTitle
  \delarray\TaskLabel
  \delarray\SigneeName
  \ifnum\value{TaskCounter}=\value{TaskSection} \else
    \PackageError{Robocracy text}{More Task defined than described}{add text}
  \fi
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% People signing the entry are given in a \Signee{name} command
\NewDocumentCommand{\Signee}{m}
%% 1: name of person signing
{\stepcounter{SigneeCounter}%
\SigneeName(\theSigneeCounter)={#1}%
#1%
}                     

\NewDocumentCommand{\ListSignee}{}
{ %
  \ifnum\theSigneeCounter=0 \else %
    \multido{\i=1+1}{\theSigneeCounter}{ \SigneeName(\i)}. %
  \fi %
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task Info
\newcommand{\TaskInfo}[3] %
%% 1: title
%% 2: reference
%% 2: reflection
{
  \stepcounter{TaskCounter}
  \TaskTitle(\theTaskCounter)={#1}
  \TaskLabel(\theTaskCounter)={#2}
  \cellcolor{MainTableHeaderColor} \textbf{\arabic{TaskCounter}.} & \textbf{#1} & #3. \\ \hline
}

%% private
\newcommand{\@TypeColor}[4]
{%
 \ifthenelse%
   {\equal{#1}{#2}}%
   {\cellcolor{black}\textcolor{NormalTableCellColor}{\textbf{#4}}}%
   {\ifthenelse%
     {\equal{#1}{#3}}%
     {\cellcolor{black}\textcolor{NormalTableCellColor}{\textbf{#4}}}%
     {#4}%
   }%
}%

%% private
\ExplSyntaxOn
\DeclareExpandableDocumentCommand{\IfNoValueOrEmptyTF}{mmm}
{
 \IfNoValueTF{#1}
  {#2} %% true
  {\tl_if_empty:nTF {#1} {#2} {#3}} %% false
}
\ExplSyntaxOff

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% task section
\NewDocumentCommand{\Task}{o m O{-1}}
%% 1: optional label (dependent on tha task)
%% 2: kind number: 1 to 6
%% 3: optional second kind number
{
  \par
  \Needspace{5cm}
  \bigskip
  \begin{flushleft}
  \refstepcounter{TaskSection}
  \checkTaskLabel(\theTaskSection) %% was not able to use \TaskLabel(\theTaskSection)
                                   %% in label directly, works with \check & \cache
  \label{\cachedata}
  \rowcolors{1}{NormalTableCellColor}{NormalTableCellColor}
  \tabulinesep=3mm
  \begin{tabu}{|>{\centering\arraybackslash}p{2.5cm}|>{\centering\arraybackslash}p{2.5cm}|>%
      {\centering\arraybackslash}p{2.5cm}|>{\centering\arraybackslash}p{2.5cm}|>%
      {\centering\arraybackslash}p{2.5cm}|>{\centering\arraybackslash}p{2.5cm}|}
    \arrayrulecolor{TitleColor} \hline
    \multicolumn{6}{|l|}{\cellcolor{NormalTableHeaderColor} %
        \capitalisewords{\textbf{\large Task \theTaskSection: \TaskTitle(\theTaskSection).}}} \\
    \IfNoValueOrEmptyTF{#1}{}{\multicolumn{6}{|l|}{\cellcolor{NormalTableHeaderColor} %
       \small Continuing from:#1} \\} \hline
    \@TypeColor{1}{#2}{#3}{\FTC@KindOne} &
    \@TypeColor{2}{#2}{#3}{\FTC@KindTwo} &
    \@TypeColor{3}{#2}{#3}{\FTC@KindThree} &
    \@TypeColor{4}{#2}{#3}{\FTC@KindFour} &
    \@TypeColor{5}{#2}{#3}{\FTC@KindFive} &
    \@TypeColor{6}{#2}{#3}{\FTC@KindSix} \\ \hline
  \end{tabu}
  \end{flushleft}
  \checkTaskTitle(\theTaskSection) %% was not able to use \TaskLabel(\theTaskSection)
  \mycustomtask{\cachedata} %% for gen task entry
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task Section
\NewDocumentCommand{\@Section}{m}
 {
    \Needspace{4cm}
    \begin{flushleft}
       {\color{TitleColor} \large \textbf{#1}} \\
    \end{flushleft}
 }

\NewDocumentCommand{\Section}{sm}{%
  \IfBooleanTF#1
    {%% with star
      \@Section{#2}
    } {%% without star
      \refstepcounter{TaskSubSection}
      \@Section{\theTaskSubSection: #2}
    }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Meeting Summary
\NewDocumentCommand{\MeetingSummary}{}
  { \Section*{Meeting Summary} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task Subsection
\NewDocumentCommand{\@Subsection}{m}
{
  \needspace{3cm} %
  \begin{flushleft} %
    { \color{TitleColor} \large  \textbf{#1}}
    \vspace{-2mm}\\
  \end{flushleft} %
}

\NewDocumentCommand{\Subsection}{sm}{%
  \IfBooleanTF#1
    {%% with star
      \@Subsection{#2}
    } {%% without star
      \refstepcounter{TaskSubSubSection}
      \@Subsection{\arabic{TaskSection}.%
        \arabic{TaskSubSection}.\arabic{TaskSubSubSection}: #2}
    }
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task Subsection
\NewDocumentCommand{\Highlight}{m}
{
  %\needspace{3cm} %
  \begin{flushleft} %
    { \color{TitleColor} \textbf{#1}}
    \vspace{-2mm}\\
  \end{flushleft} %
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Enumerate with Title
\NewDocumentEnvironment{EnumerateWithTitle}{m} %
{
  \Subsection*{#1}
  \begin{enumerate}
}
{
  \end{enumerate}
}

\NewDocumentEnvironment{ItemizeWithTitle}{m} %
{
  \Subsection*{#1}
  \begin{itemize}
}
{
  \end{itemize}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% pictures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% private
\newcommand{\Callout}[3]{\callout{#1}{\large #2}{#3}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Picture (annotated)
\newcommand{\RawPict}[3]%
%% 1: image
%% 2: size in fraction of page width
%% 3: annotations
{  %
  \begin{minipage}{\linewidth}
    \centering %
    \begin{annotate}{\includegraphics[width=#2\textwidth]{#1}}{#2} %
      \ifFTC@Grid
        \helpgrid
      \fi
      %%  \callout{x , y of text}{Text}{x, y of arrow}
      #3
    \end{annotate}
  \end{minipage}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure with one Pict
\NewDocumentCommand{\PictFigure}{O{htb} m O{0.9} m  m o}%
%% 1 location (optional, everywhere)
%% 2 file
%% 3 size (optional, 90%)
%% 3 caption
%% 5 label
%% 6 annotation (optional)
{ %
  \begin{figure}[#1]
  \centering
  \RawPict{#2}{#3}{#6}
  \caption{#4.}
  \label{#5}
  \end{figure}
}

\newlength{\@ExplainedPictFigureTextLength}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure with one picture and explanations
%% private internal command
\NewDocumentCommand{\RawExplainedPict}{m O{0.6} m o}%
%% 1 file
%% 2 size pict (optional, default 0.6, must be < 0.95)
%% 3 explanation
%% 4 annotation (annotation)
{
  \setlength{\@ExplainedPictFigureTextLength}{0.95\textwidth -  #2\textwidth}
  \centering
  \begin{minipage}{#2\textwidth}
  \RawPict{#1}{.9}{#4}
  \end{minipage}%
  \begin{minipage}{\@ExplainedPictFigureTextLength}
    #3
  \end{minipage}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure with one picture and explanations
\NewDocumentCommand{\ExplainedPictFigure}{O{htb} m O{0.6} m m m o}%
%% 1 location (optional, everywhere)
%% 2 file
%% 3 size pict (optional, default 0.6, must be < 0.95)
%% 4 caption
%% 5 label
%% 6 explanation
%% 7 annotation (optional)
{
  \begin{figure}[#1]
    \RawExplainedPict{#2}[#3]{#6}[#7]
    \caption{#4.}
    \label{#5}
  \end{figure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure with one picture and explanations
\NewDocumentCommand{\PictSubfigure}{m O{0.4} m m o}%
%% 1 file
%% 2 size pict (optional, default 0.4, must be smaller than 0.95)
%% 3 caption
%% 4 label
%% 5 annotation (optional)
{
  \begin{subfigure}{#2\textwidth}
    \RawPict{#1}{.9}{#5}
    \caption{#3.}
    \label{#4}
  \end{subfigure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure with one picture and explanations
\NewDocumentCommand{\ExplainedPictSubfigure}{m O{0.6} m m m o}%
%% 1 file
%% 2 size pict (optional, default 0.6, must be < 0.95)
%% 3 caption
%% 4 label
%% 5 explanation
%% 6 annotation (optional)
{
  \begin{subfigure}{0.9\textwidth}
    \RawExplainedPict{#1}[#2]{#5}[#6]
    \caption{#3.}
    \label{#4}
  \end{subfigure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure with Multiple Figures
\NewDocumentEnvironment{GroupedFigures}{O{htb} m m}%
%% 1 location (optional, everywhere)
%% 2 caption
%% 3 label
{
  \begin{figure}[#1]
    \centering
}
{
    \caption{#2.}
    \label{#3}
  \end{figure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\arraystretch}{1.5}

%% internal command
\NewDocumentEnvironment{MyTable}{m m m m m} %
%% 1: color
%% 2: table column definition
%% 3: legend
%% 4: caption
%% 5: label
{
  \begin{center}
    \rowcolors{2}{#1}{#1}
    \begin{longtable}{#2}
      %
      \arrayrulecolor{TitleColor}
      \caption{#4.} \label{#5} \\
      \hline
      \rowcolor{NormalTableHeaderColor} #3 \\ \hline
      \endfirsthead
      %
      \arrayrulecolor{TitleColor}
      \hline
      \rowcolor{NormalTableHeaderColor} #3 \\ \hline
      \endhead
}
{
    \\ \hline
    \end{longtable}
  \end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% decision
\NewDocumentEnvironment{RawDecisionTable}{m m m m} %
%% 1: column format
%% 2: column titles
%% 3: caption
%% 4: label
{ \begin{MyTable}{NormalTableCellColor}{#1}{#2}{#3}{#4} }
{ \end{MyTable} }

\NewDocumentEnvironment{DecisionTable}{O{Option} O{Pro} O{Cons} m m} %
%% 1,2,3: column names (optional: all or none please)
%% 4: caption
%% 5: label
{ \begin{RawDecisionTable}{|p{4cm}|p{6.5cm}|p{6.5cm}|}%
    {\textbf{#1:} & \textbf{#2:} & \textbf{#3:}}{#4}{#5}
}
{ \end{RawDecisionTable} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% description Table
\NewDocumentEnvironment{RawDescriptionTable}{m m m m} %
%% 1: column format
%% 2: column titeles 
%% 3: caption
%% 4: label
{ \begin{MyTable}{White}{#1}{#2}{#3}{#4} }
{ \end{MyTable} }

%%    no star: small + large sized columns
%%    with star: 2 medium sized columns
\NewDocumentEnvironment{DescriptionTable}{m m m m} %
%% 1: first col title
%% 2: second col title
%% 3: caption
%% 4: label
{
  \begin{RawDescriptionTable}{|p{5cm}|p{12cm}|}%
    {\textbf{#1} & \textbf{#2}}{#3}{#4}
}
{ \end{RawDescriptionTable} }

\NewDocumentEnvironment{DescriptionTable*}{m m m m} %
%% 1: first col title
%% 2: second col title
%% 3: caption
%% 4: label
{
  \begin{RawDescriptionTable}{|p{8.5cm}|p{8.5cm}|}%
    {\textbf{#1} & \textbf{#2}}{#3}{#4}
}
{ \end{RawDescriptionTable} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% table entries
\NewDocumentCommand{\TableEntryTextTextText}{m m m}
%% 1,2,3: text, text, text entries (use in decision table)
{
  \begin{minipage}[t]{\linewidth}
    \vspace{1mm}
    \raggedright
    #1
    \vspace{2mm}
    \end{minipage}
    &
    \begin{minipage}[t]{\linewidth}
    \vspace{1mm}
    \raggedright
    #2
    \vspace{2mm}
  \end{minipage}
  &
  \begin{minipage}[t]{\linewidth}
    \vspace{1mm}
    \raggedright
    #3
    \vspace{2mm}
  \end{minipage}
}

\NewDocumentCommand{\TableEntryTextItemItem}{m m m}
%% 1,2,3: text, items, items entries (use in decision table)
{
  \TableEntryTextTextText%
    {#1}
    {\begin{compactitem} #2 \end{compactitem}}
    {\begin{compactitem} #3 \end{compactitem}}
}

\NewDocumentCommand{\TableEntryTextText}{m m}
%% 1, 2: text, text entries (use in description table)
{
  \begin{minipage}[t]{\linewidth}
    \vspace{1mm}
    \raggedright
    #1
    \vspace{2mm}
  \end{minipage}
  &
  \begin{minipage}[t]{\linewidth}
    \vspace{1mm}
    \raggedright
    #2
    \vspace{2mm}
  \end{minipage}
}

\NewDocumentCommand{\TableEntryTextItem}{m m}
%% 1,2: items, items entries (use in description table)
{
  \TableEntryTextText%
    {#1}
    {\begin{compactitem} #2 \end{compactitem}}
}

\NewDocumentCommand{\TableEntryItemItem}{m m}
%% 1,2: items, items entries (use in description table)
{
  \TableEntryTextText%
    {\begin{compactitem} #1 \end{compactitem}}
    {\begin{compactitem} #2 \end{compactitem}}
}

\NewDocumentCommand{\MyTableKey}{m} {\cellcolor{NormalTableHeaderColor} #1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% bio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 \NewDocumentEnvironment{Bio}{} %
{
  \cleardoublepage
  \newpage

  \addcontentsline{mcf}{mycustomtask}{Meet the Team}
  \begin{center}
    \rowcolors{1}{White}{White}
    \begin{longtable}{|>{\raggedright\arraybackslash}p{8.5cm}|%
        >{\raggedright\arraybackslash}p{8.5cm}|}
      %
      \arrayrulecolor{TitleColor}
      \hline
      \multicolumn{2}{|c|}{\cellcolor{NormalTableHeaderColor} %
        \textbf{\Large Meet the team}}
      \\ \hline
      \endhead
}
{
    \\ \hline
    \end{longtable}
  \end{center}
}

\NewDocumentCommand{\BioEntry}{m m O{Role} m O{Outreach} m m O{0.5} m}
%% 1, 2  Name, blurb below name
%% 3, 4  Role (optional), role description
%% 5, 6  Outreach role (optional), outreach description
%% 7, 8  pic, (optional) fractional size
%% 9     full bio
{
  \TableEntryTextText{%
    \begin{center}
      \RawPict{#7}{#8}{} \\
      \textbf{#1} \\
      #2 \vspace{3mm}\\
    \end{center}
    \textbf{#3:} #4\vspace{3mm} \\
    \textbf{#5:} #6
  } {
      #9
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% team story
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NewDocumentEnvironment{TeamStory}{O{Our Team Story} m}
%% 1: Title (default Our Team Story)
%% 2: Team one-liner description
{
  \cleardoublepage
  \newpage

  \addcontentsline{mcf}{mycustomtask}{#1}
  \begin{flushleft}
  \tabulinesep=1.2mm
  \begin{tabu}{p{3cm}>{\raggedright\arraybackslash}p{13.7cm}}
      \multirow{2}{*}{\includegraphics[width=3cm]{\FTC@Logo}}
       & \textbf{ \color{TitleColor} \textit{\fontsize{40}{50}\selectfont #1}} \\
       & \textbf{\LARGE ``#2''} \\ \cline{2-2} \\
  \end{tabu}
  \vspace{10mm}  \\
  \end{flushleft}
  \begin{Large}
}{
  \end{Large}
}

\NewDocumentCommand{\CoverPage}{m m O{14}}
%% 1: year
%% 2: picture
%% 3: vertical size of picture in cm
{
\newpage
%% text block that overlay info
\begin{textblock}{10}(2.5, 3.5)%
  \renewcommand{\arraystretch}{2}%
  \begin{tabu}{l}
    \multicolumn{1}{c}{{\Huge \FTC@Num ~}} \\
    {\fontsize{60}{70}\selectfont \textbf{\textsc{\FTC@Name}}} \\
    \includegraphics[height=#3cm]{#2} \\
    \multicolumn{1}{c}{\cellcolor{MainTableCellColor} %
      \fontsize{30}{40}\selectfont \textbf{\textsc{\FTC@Notebook}}} \\
  \end{tabu}
\end{textblock}
%%background table
\begin{tabular}[t]{p{9cm}>{\columncolor{MainTableHeaderColor}}p{8cm}}
  \multirow{3}{*}{\includegraphics[height=4cm]{\FTC@Logo}} & \\
   & \multicolumn{1}{r}{\cellcolor{MainTableHeaderColor} \textbf{ \Huge #1~}} \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
   & \\
\end{tabular}
\newpage

~
\begin{textblock}{12}(2, 14)%
  \noindent
  Document typeset in LaTeX with the \texttt{ftc-notebook} package created %
  by FTC 9773, Team Robocracy.
\end{textblock}

\newpage

}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Misc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% refs
\NewDocumentCommand{\TaskRef}{m}    {Task~\ref{#1} on page~\pageref{#1}}
\NewDocumentCommand{\FigureRef}{m}  {Figure~\ref{#1} on page~\pageref{#1}}
\NewDocumentCommand{\TableRef}{m}   {Table~\ref{#1} on page~\pageref{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% list of tasks
\newcommand{\listexamplename}{Table of Contents}
\newlistof{mycustomtask}{mcf}{\listexamplename}
\newcommand{\mycustomtask}[1]
{%
   \refstepcounter{mycustomtask}
   \addcontentsline{mcf}{mycustomtask}
   {\protect\numberline{\themycustomtask}#1}\par
}

\NewDocumentCommand{\listoftasks}{}
{
  \pagenumbering{roman}
  \cfoot{\thepage}
  \listofmycustomtask
}

\renewcommand\cftmycustomtaskfont{\large}
\renewcommand\cftfigfont{\large}
\renewcommand\cfttabfont{\large}
\renewcommand\cftloftitlefont{\Huge\bfseries}
\renewcommand\cftlottitlefont{\Huge\bfseries}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% start of doc
\NewDocumentCommand{\StartNotebook}{}
{
  \cfoot{}
  \lfoot{\FTC@Num, \FTC@Name, \FTC@Notebook.}
  \rfoot{\@TaskDate(1) Page \thepage.}
  \pagenumbering{arabic}
}

\NewDocumentCommand{\ListOfTasks}{}
{
  \listoftasks
  \newpage
}

\NewDocumentCommand{\ListOfFigures}{}
{
  \listoffigures
  \newpage
}

\NewDocumentCommand{\ListOfTables}{}
{
  \listoftables
  \newpage
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% defaults
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ifFTC@Blue
  %% title and array rules
  \definecolor{TitleColor}{rgb}{0.04, 0.32, 0.74}
  %% main table backgrounds
  \definecolor{MainTableHeaderColor}{rgb}{0.56, 0.69, 0.99}
  \definecolor{MainTableCellColor}{rgb}{0.26, 0.47, 0.79}
  %% normal table backgrounds
  \definecolor{NormalTableHeaderColor}{rgb}{0.38, 0.60, 0.79}
  \definecolor{NormalTableCellColor}{rgb}{0.68, 0.87, 0.99}
  \definecolor{NormalTableCellWhite}{rgb}{1.0, 1.0, 1.0}
\else
\ifFTC@Orange
  %% title and array rules
  \definecolor{TitleColor}{rgb}{0.71, 0.45, 0.01}
  %% main table backgrounds
  \definecolor{MainTableHeaderColor}{rgb}{0.96, 0.80, 0.51}
  \definecolor{MainTableCellColor}{rgb}{0.99, 0.69, 0.20}
  %% normal table backgrounds
  \definecolor{NormalTableHeaderColor}{rgb}{0.98, 0.69, 0.18}
  \definecolor{NormalTableCellColor}{rgb}{0.99, 0.89, 0.68}
  \definecolor{NormalTableCellWhite}{rgb}{1.0, 1.0, 1.0}
\else
  % default: green
  %% title and array rules
  \definecolor{TitleColor}{rgb}{0.65, 0.73, 0.29}
  %% main table backgrounds
  \definecolor{MainTableHeaderColor}{rgb}{0.84, 0.96, 0.29}
  \definecolor{MainTableCellColor}{rgb}{0.70, 0.82, 0.32}
  %% normal table backgrounds
  \definecolor{NormalTableHeaderColor}{rgb}{0.84, 0.96, 0.29}
  \definecolor{NormalTableCellColor}{rgb}{0.94, 0.99, 0.78}
  \definecolor{NormalTableCellWhite}{rgb}{1.0, 1.0, 1.0}
\fi
\fi

%% 
%% Copyright (c) 2019 FTC 9773, Team Robocracy
%% All rights reserved.
%% 
%% Developed by FTC 9773 Robocracy team members
%% Westchester County, NY
%% 2019
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `maintained'.
%% 
%% The Current Maintainer of this work is FTC 9773 Team Robocracy.
%% 
%% This work consists of the files ftc-notebook.dtx, ftc-notebook.ins,
%% ftc-notebook.pdf, ftc-notebook.sty, and newmeeting.sh and the derived files
%% 
%% This package includes the callout.sty package, which was lightly
%% adapted for our needs.  The original copyright of that package is
%% listed before the callout code. Original version of the callout.sty
%% is found on ctan.org
%% 
%%
%% End of file `ftc-notebook.sty'.

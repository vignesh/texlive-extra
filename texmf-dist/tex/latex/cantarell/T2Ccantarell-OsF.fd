%% T2Ccantarell-OsF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{T2Ccantarell-OsF.fd}[2019/06/24 Font definitions for T2C/cantarell-OsF.]

\expandafter\ifx\csname cantarell@scale\endcsname\relax
    \let\cantarell@@scale\@empty
\else
    \edef\cantarell@@scale{s*[\csname cantarell@scale\endcsname]}
\fi

\DeclareFontFamily{T2C}{cantarell-OsF}{}

\DeclareFontShape{T2C}{cantarell-OsF}{l}{n}{<-> \cantarell@@scale Cantarell-Light-T2C-OsF}{}
\DeclareFontShape{T2C}{cantarell-OsF}{b}{n}{<-> \cantarell@@scale Cantarell-Bold-T2C-OsF}{}
\DeclareFontShape{T2C}{cantarell-OsF}{el}{n}{<-> \cantarell@@scale Cantarell-Thin-T2C-OsF}{}
\DeclareFontShape{T2C}{cantarell-OsF}{m}{n}{<-> \cantarell@@scale Cantarell-Regular-T2C-OsF}{}
\DeclareFontShape{T2C}{cantarell-OsF}{eb}{n}{<-> \cantarell@@scale Cantarell-ExtraBold-T2C-OsF}{}
\DeclareFontShape{T2C}{cantarell-OsF}{l}{sl}{<-> \cantarell@@scale Cantarell-Light-T2C-OsF-Slanted}{}
\DeclareFontShape{T2C}{cantarell-OsF}{b}{sl}{<-> \cantarell@@scale Cantarell-Bold-T2C-OsF-Slanted}{}
\DeclareFontShape{T2C}{cantarell-OsF}{el}{sl}{<-> \cantarell@@scale Cantarell-Thin-T2C-OsF-Slanted}{}
\DeclareFontShape{T2C}{cantarell-OsF}{m}{sl}{<-> \cantarell@@scale Cantarell-Regular-T2C-OsF-Slanted}{}
\DeclareFontShape{T2C}{cantarell-OsF}{eb}{sl}{<-> \cantarell@@scale Cantarell-ExtraBold-T2C-OsF-Slanted}{}
\DeclareFontShape{T2C}{cantarell-OsF}{bx}{n}{<-> ssub * cantarell-OsF/b/n}{}
\DeclareFontShape{T2C}{cantarell-OsF}{l}{it}{<-> ssub * cantarell-OsF/l/sl}{}
\DeclareFontShape{T2C}{cantarell-OsF}{b}{it}{<-> ssub * cantarell-OsF/b/sl}{}
\DeclareFontShape{T2C}{cantarell-OsF}{bx}{sl}{<-> ssub * cantarell-OsF/b/sl}{}
\DeclareFontShape{T2C}{cantarell-OsF}{bx}{it}{<-> ssub * cantarell-OsF/b/sl}{}
\DeclareFontShape{T2C}{cantarell-OsF}{el}{it}{<-> ssub * cantarell-OsF/el/sl}{}
\DeclareFontShape{T2C}{cantarell-OsF}{m}{it}{<-> ssub * cantarell-OsF/m/sl}{}
\DeclareFontShape{T2C}{cantarell-OsF}{eb}{it}{<-> ssub * cantarell-OsF/eb/sl}{}

\endinput
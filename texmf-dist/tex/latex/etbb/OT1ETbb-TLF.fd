%%
\ProvidesFile{OT1ETbb-TLF.fd}
    [2020/02/15  Font definitions for OT1/ETbb-TLF.]

\expandafter\ifx\csname ETbb@@swashQ\endcsname\relax%
	\global\let\ETbb@@swashQ\@empty
\fi

\ifcsname s@fct@alias\endcsname\else
\gdef\s@fct@alias{\sub@sfcnt\@font@aliasinfo}
\gdef\@font@aliasinfo#1{%
    \@font@info{Font\space shape\space `\curr@fontshape'\space will
        \space be\space aliased\MessageBreak to\space `\mandatory@arg'}%
}
\fi

\edef\ETbb@@S{} % do not use fancy sharpS
\expandafter\ifx\csname @ETbb@sharpSfalse\endcsname\relax
\csname iftrue\endcsname
\else
	\if@ETbb@sharpS\edef\ETbb@@S{1}\fi % do use fancy sharpS
\fi
%\show\ETbb@@S
\expandafter\ifx\csname ETbb@scale\endcsname\relax
    \let\ETbb@@scale\@empty
\else
    \edef\ETbb@@scale{s*[\csname ETbb@scale\endcsname]}%
\fi

\DeclareFontFamily{OT1}{ETbb-TLF}{}


%   ----  bold  ----

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{nw}{
      <-> \ETbb@@scale ETbb-Bold-tlf-swash-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{sw}{
      <-> \ETbb@@scale ETbb-BoldItalic-tlf-swash-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{it}{
      <-> \ETbb@@scale ETbb\ETbb@@S-BoldItalic-tlf\ETbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{n}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Bold-tlf\ETbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{sc}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Bold-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{scit}{
      <-> \ETbb@@scale ETbb\ETbb@@S-BoldItalic-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{scsl}{
      <-> ssub * ETbb-TLF/bold/scit
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bold}{sl}{
      <-> ssub * ETbb-TLF/bold/it
}{}


%   ----  regular  ----

\DeclareFontShape{OT1}{ETbb-TLF}{regular}{scit}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Italic-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{regular}{sc}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Regular-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{regular}{it}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Italic-tlf\ETbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{regular}{n}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Regular-tlf\ETbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{regular}{scsl}{
      <-> ssub * ETbb-TLF/regular/scit
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{regular}{sl}{
      <-> ssub * ETbb-TLF/regular/it
}{}

%
%  Extra 'alias' rules to map the standard NFSS codes to our fancy names
%

%   m --> regular

\DeclareFontShape{OT1}{ETbb-TLF}{m}{sl}{
      <-> alias * ETbb-TLF/regular/sl
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{nw}{
      <-> alias * ETbb-TLF/regular/nw
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{sc}{
      <-> alias * ETbb-TLF/regular/sc
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{it}{
      <-> alias * ETbb-TLF/regular/it
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{n}{
      <-> alias * ETbb-TLF/regular/n
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{scsl}{
      <-> alias * ETbb-TLF/regular/scsl
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{scit}{
      <-> alias * ETbb-TLF/regular/scit
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{m}{sl}{
      <-> alias * ETbb-TLF/regular/sl
}{}


%   b --> bold

\DeclareFontShape{OT1}{ETbb-TLF}{b}{sw}{
      <-> alias * ETbb-TLF/bold/sw
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{nw}{
      <-> alias * ETbb-TLF/bold/nw
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{scsl}{
      <-> alias * ETbb-TLF/bold/scsl
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{scit}{
      <-> alias * ETbb-TLF/bold/scit
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{it}{
      <-> alias * ETbb-TLF/bold/it
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{n}{
      <-> alias * ETbb-TLF/bold/n
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{sc}{
      <-> alias * ETbb-TLF/bold/sc
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{b}{sl}{
      <-> alias * ETbb-TLF/bold/sl
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{sl}{
      <-> ssub * ETbb-TLF/b/sl
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{scit}{
      <-> ssub * ETbb-TLF/b/scit
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{scsl}{
      <-> ssub * ETbb-TLF/b/scsl
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{n}{
      <-> ssub * ETbb-TLF/b/n
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{sc}{
      <-> ssub * ETbb-TLF/b/sc
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{it}{
      <-> ssub * ETbb-TLF/b/it
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{nw}{
      <-> ssub * ETbb-TLF/b/nw
}{}

\DeclareFontShape{OT1}{ETbb-TLF}{bx}{sw}{
      <-> ssub * ETbb-TLF/b/sw
}{}

\endinput

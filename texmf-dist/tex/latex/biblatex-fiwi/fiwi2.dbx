%% Copyright 2017 Simon Spiegel
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%% 
%% The Current Maintainer of this work is Simon Spiegel
%%
%%


\ProvidesFile{fiwi2.dbx}[2017/11/21 1.7 film studies bibliography style data model]
\RequireBiber[3]

\DeclareDatamodelEntrytypes{archival}

\DeclareDatamodelFields[type=field, datatype=literal]{librarylocation}
\DeclareDatamodelFields[type=field, datatype=literal]{parttitle}
\DeclareDatamodelFields[type=field, datatype=literal]{duration}
\DeclareDatamodelFields[type=field, datatype=literal]{durationtype}
\DeclareDatamodelFields[type=field, datatype=literal]{alternatetitle}
\DeclareDatamodelFields[type=field, datatype=literal]{alternatetitlescript}
\DeclareDatamodelFields[type=list, datatype=name]{director}
\DeclareDatamodelFields[type=list, datatype=name]{scriptwriter}
\DeclareDatamodelFields[type=list, datatype=name]{actor}
\DeclareDatamodelFields[type=list, datatype=literal]{production}

\DeclareDatamodelEntryfields[archival]{librarylocation}

\DeclareDatamodelEntryfields[movie,misc,video]{
	duration,
	durationtype,
	alternatetitle,
	alternatetitlescript,
	director,
	scriptwriter,
	actor}

\DeclareDatamodelConstant[type=list]{nameparts}{prefix,family,suffix,given,trueprefix,truefamily,truesuffix,truegiven} 
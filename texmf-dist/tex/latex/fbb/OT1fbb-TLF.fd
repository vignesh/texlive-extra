%%
\ProvidesFile{OT1fbb-TLF.fd}
    [2020/07/01 (msharpe)  Font definitions for OT1/fbb-TLF.]

\expandafter\ifx\csname fbb@@swashQ\endcsname\relax%
	\global\let\fbb@@swashQ\@empty
\fi

\expandafter\ifx\csname fbb@scale\endcsname\relax
    \let\fbb@@scale\@empty
\else
    \edef\fbb@@scale{s*[\csname fbb@scale\endcsname]}%
\fi
\expandafter\ifx\csname fbb@altP\endcsname\relax
    \let\fbb@@altP\@empty
\else
    \let\fbb@@altP\fbb@altP
\fi

\DeclareFontFamily{OT1}{fbb-TLF}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{nw}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-tlf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{sw}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-tlf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{nw}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-tlf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{sw}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-tlf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{it}{
      <-> \fbb@@scale fbb-BoldItalic-tlf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{n}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-tlf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{scit}{
      <-> \fbb@@scale fbb-BoldItalic-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{sc}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{sl}{
      <-> ssub * fbb-TLF/b/it
}{}

\DeclareFontShape{OT1}{fbb-TLF}{b}{scsl}{
      <-> ssub * fbb-TLF/b/scit
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{n}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-tlf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{it}{
      <-> \fbb@@scale fbb-Italic-tlf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{scit}{
      <-> \fbb@@scale fbb-Italic-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{sc}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-tlf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{sl}{
      <-> ssub * fbb-TLF/m/it
}{}

\DeclareFontShape{OT1}{fbb-TLF}{m}{scsl}{
      <-> ssub * fbb-TLF/m/scit
}{}

\DeclareFontShape{OT1}{fbb-TLF}{bx}{scit}{
      <-> ssub * fbb-TLF/b/scit
}{}

\DeclareFontShape{OT1}{fbb-TLF}{bx}{scsl}{
      <-> ssub * fbb-TLF/b/scsl
}{}

\DeclareFontShape{OT1}{fbb-TLF}{bx}{sc}{
      <-> ssub * fbb-TLF/b/sc
}{}

\DeclareFontShape{OT1}{fbb-TLF}{bx}{n}{
      <-> ssub * fbb-TLF/b/n
}{}

\DeclareFontShape{OT1}{fbb-TLF}{bx}{sl}{
      <-> ssub * fbb-TLF/b/sl
}{}

\DeclareFontShape{OT1}{fbb-TLF}{bx}{it}{
      <-> ssub * fbb-TLF/b/it
}{}

\endinput

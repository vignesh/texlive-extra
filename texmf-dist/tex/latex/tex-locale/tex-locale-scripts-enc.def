%%
%% This is file `tex-locale-scripts-enc.def',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% tex-locale.dtx  (with options: `tex-locale-scripts-enc.def,package')
%% 
%%  tex-locale.dtx
%%  Copyright 2018 Nicola Talbot
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license or (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%% 
%%  The Current Maintainer of this work is Nicola Talbot.
%% 
%%  This work consists of the files tex-locale.dtx and tex-locale.ins and the derived files tex-locale.sty, tex-locale.tex, tex-locale-scripts-enc.def, tex-locale-encodings.def, tex-locale-support.def.
%% 
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%% arara: xetex: {shell: on}
%% arara: xetex: {shell: on}
\def\@locale@scriptenc@map#1#2{%
  \@tracklang@namedef{@locale@scriptenc@map@#1}{#2}%
}
\def\@locale@get@scriptenc@map#1{%
  \@tracklang@ifundef{@locale@scriptenc@map@#1}%
  {}%
  {\csname @locale@scriptenc@map@#1\endcsname}%
}
\def\@locale@if@scriptenc@map#1#2#3{%
  \@tracklang@ifundef{@locale@scriptenc@map@#1}%
  {#3}% false
  {#2}% true
}
\def\@locale@langenc@map#1#2{%
  \@tracklang@namedef{@locale@langenc@map@#1}{#2}%
}
\def\@locale@if@langenc@map#1#2#3{%
  \@tracklang@ifundef{@locale@langenc@map@#1}%
  {#3}% false
  {#2}% true
}
\def\@locale@get@langenc@map#1{%
  \@tracklang@ifundef{@locale@langenc@map@#1}%
  {}%
  {\csname @locale@langenc@map@#1\endcsname}%
}
\@locale@scriptenc@map{Latn}{T1}
\@locale@scriptenc@map{Latf}{T1}
\@locale@scriptenc@map{Latg}{T1}
\@locale@scriptenc@map{Cyrl}{T2A,T2B,T2C}
\@locale@langenc@map{vietnamese}{T5}
\@locale@langenc@map{polish}{OT4}
\@locale@langenc@map{armenian}{OT6}
\@locale@langenc@map{greek}{LGR}
\endinput
%%
%% End of file `tex-locale-scripts-enc.def'.

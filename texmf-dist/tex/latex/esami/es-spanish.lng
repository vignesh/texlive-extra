%%% Spanish translation by María Hernández Cifre


%%% Labels (esami.sty)

\def\es@parametermessage#1#2#3{El par\'ametro $#1$ se encuentra entre $#2$ y $#3$.}
\def\es@seedmessage#1{La semilla es igual a $#1$.}
\def\es@pointname{punto}
\def\es@pointsname{puntos}
\def\es@exercisename{Ejercicio}
\def\es@solutionname{\unexpanded{Soluci\'on}}

%%% Labels (xyz.cfg)

\def\profname{Prof.\ }
\def\studsignname{Firma del alumno}
\def\studlastname{Apellido}
\def\studfirstname{Nombre}
\def\studid{Identificaci\'on del alumno}
\def\versionname{\unexpanded{Versi\'on}}
\def\pagename{P\'agina}
\def\solutionsname{Soluciones}

%%% Error Messages

\def\es@cfgerrormessageshort{La opci\'{o}n \CurrentOption\space no existe}
\def\es@cfgerrormessagelong{El paquete esami necesita una opción para la configuración del fichero}
\def\es@numcompitierror{Es necesario especificar el número de versiones!}
\def\es@mastererror{AVISO: NO SE PUEDE COMPILAR EL FICHERO PRINCIPAL:\MessageBreak
        PRESIONE 'x' PARA SALIR Y RENOMBRE EL FICHERO}
\def\es@zerodenerror#1#2{El denominador de la fracción #1/#2 en el ejercicio \nome \space es 0}
\def\es@zeronumerrorshort#1#2{La fracción #1/#2 en el ejercicio \nome \space vale 0}
\def\es@zeronumerrorlong#1#2{Si la fracción puede ser 0, debe utilizar el comando "sempliz"}
\def\es@fraconewarn#1#2{La fracción #1/#2 en el ejercicio \nome \space vale 1}
\def\es@radicalerror{El radicando en el ejercicio \nome \space es negativo}
\def\es@fpsetparerrorshort{No se verifican todas las condiciones después de \maxLoopLimit\space intentos}
\def\es@EstraiMessage#1#2{\string\textbf\string{\string\noindent\space Las expresiones $#2i$, $#2ii$ \string\dots\space
                toman los valores #1 \string}\string\newline}
\def\es@fpsetparerrshort#1{No es posible definir el parámetro #1}
\def\es@fpsetparerrlong#1{En la definición del parámetro #1   la cota inferior es mayor de la cota superior}

\def\es@pointerrorshort{Wrong score}
\def\es@pointerrorlong{En la versión \thevers\space la puntuación total score \the\punteggio\space es diferente de la puntuación prevista \punti \MessageBreak}

%%% The date

\def\es@longdate#1/#2/#3\@empty{#3\space \ifcase#2\or
      Enero \or Febrero \or Marzo \or Abril
        \or Mayo \or Junio  \or Julio  \or Agosto
        \or Septiembre  \or Octubre  \or Noviembre
        \or Diciembre\fi \space #1}

\def\es@shortdate#1/#2/#3\@empty{#3/#2/#1}


%%% Useful Language dependent packages

\AtEndOfPackage{
\usepackage{geometry}
\geometry{a4paper,lmargin=6mm,rmargin=18mm,tmargin=6mm,bmargin=18mm,marginparsep=2mm,footskip=1cm}
\usepackage{icomma}
\usepackage{eurosym}
\usepackage[np,autolanguage]{numprint}
\newcommand*\npstylespanish{%
\npthousandsep{\,}%
\npdecimalsign{,}%
\npproductsign{\ensuremath{\cdot}}%
\npunitseparator{~}%
 \npthousandthpartsep{}
}
}

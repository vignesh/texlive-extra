%% LGRopensans-TOsF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{LGRopensans-TOsF.fd}[2019/06/24 Font definitions for LGR/opensans-TOsF.]

\expandafter\ifx\csname opensans@scale\endcsname\relax
    \let\opensans@@scale\@empty
\else
    \edef\opensans@@scale{s*[\csname opensans@scale\endcsname]}
\fi

\DeclareFontFamily{LGR}{opensans-TOsF}{}

\DeclareFontShape{LGR}{opensans-TOsF}{l}{it}{<-> \opensans@@scale OpenSans-LightItalic-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{eb}{n}{<-> \opensans@@scale OpenSans-ExtraBold-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{eb}{it}{<-> \opensans@@scale OpenSans-ExtraBoldItalic-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{bc}{n}{<-> \opensans@@scale OpenSansCondensed-Bold-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{l}{n}{<-> \opensans@@scale OpenSans-Light-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{b}{n}{<-> \opensans@@scale OpenSans-Bold-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{sb}{it}{<-> \opensans@@scale OpenSans-SemiBoldItalic-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{lc}{n}{<-> \opensans@@scale OpenSansCondensed-Light-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{lc}{it}{<-> \opensans@@scale OpenSansCondensed-LightItalic-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{m}{it}{<-> \opensans@@scale OpenSans-Italic-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{b}{it}{<-> \opensans@@scale OpenSans-BoldItalic-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{m}{n}{<-> \opensans@@scale OpenSans-Regular-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{sb}{n}{<-> \opensans@@scale OpenSans-SemiBold-LGR-TOsF}{}
\DeclareFontShape{LGR}{opensans-TOsF}{l}{sl}{<-> ssub * opensans-TOsF/l/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{eb}{sl}{<-> ssub * opensans-TOsF/eb/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{bx}{n}{<-> ssub * opensans-TOsF/b/n}{}
\DeclareFontShape{LGR}{opensans-TOsF}{sb}{sl}{<-> ssub * opensans-TOsF/sb/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{lc}{sl}{<-> ssub * opensans-TOsF/lc/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{m}{sl}{<-> ssub * opensans-TOsF/m/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{b}{sl}{<-> ssub * opensans-TOsF/b/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{bx}{it}{<-> ssub * opensans-TOsF/b/it}{}
\DeclareFontShape{LGR}{opensans-TOsF}{bx}{sl}{<-> ssub * opensans-TOsF/b/it}{}

\endinput
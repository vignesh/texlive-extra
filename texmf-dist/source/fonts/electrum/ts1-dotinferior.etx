%% Based on ts1.etx
%% See that file for commentary

\relax

\encoding

\setstr{codingscheme}{TEX TEXT COMPANION 1---TS1 DOTINFERIOR}

\ifisglyph{x}\then
   \setint{xheight}{\height{x}}
\else
   \setint{xheight}{500}
\fi

\ifisglyph{space}\then
   \setint{interword}{\width{space}}
\else\ifisglyph{i}\then
   \setint{interword}{\width{i}}
\else
   \setint{interword}{333}
\fi\fi

\setint{italicslant}{0}


\setint{fontdimen(1)}{\int{italicslant}}              % italic slant
\setint{fontdimen(2)}{\int{interword}}                % interword space
\setint{fontdimen(3)}{0}                              % interword stretch
\setint{fontdimen(4)}{0}                              % interword shrink
\setint{fontdimen(5)}{\int{xheight}}                  % x-height
\setint{fontdimen(6)}{1000}                           % quad
\setint{fontdimen(7)}{\int{interword}}                % extra space after .

\nextslot{0}
\setslot{capitalgrave.inferior}
   \comment{The grave accent `\capitalgrave{}', intended for use with
      capital letters.}
\endsetslot

\setslot{capitalacute.inferior}
   \comment{The acute accent `\capitalacute{}', intended for use with
      capital letters.}
\endsetslot

\setslot{capitalcircumflex.inferior}
   \comment{The circumflex accent `\capitalcircumflex{}', intended for
      use with capital letters.}
\endsetslot

\setslot{capitaltilde.inferior}
   \comment{The tilde accent `\capitaltilde{}', intended for use with
      capital letters.}
\endsetslot

\setslot{capitaldieresis.inferior}
   \comment{The umlaut or dieresis accent `\capitaldieresis{}',
      intended for use with capital letters.}
\endsetslot

\setslot{capitalhungarumlaut.inferior}
   \comment{The long Hungarian umlaut `\capitalhungarumlaut{}',
      intended for use with capital letters.}
\endsetslot

\setslot{capitalring.inferior}
   \comment{The ring accent `\capitalring{}', intended for use with
      capital letters.}
\endsetslot

\setslot{capitalcaron.inferior}
   \comment{The caron or h\'a\v cek accent `\capitalcaron{}', intended
      for use with capital letters.}
\endsetslot

\setslot{capitalbreve.inferior}
   \comment{The breve accent `\capitalbreve{}', intended for use with
      capital letters.}
\endsetslot

\setslot{capitalmacron.inferior}
   \comment{The macron accent `\capitalmacron{}', intended for use with
      capital letters.}
\endsetslot

\setslot{capitaldotaccent.inferior}
   \comment{The dot accent `\capitaldotaccent{}', intended for use with
      capital letters.}
\endsetslot

\setslot{cedilla.inferior}
   \comment{The cedilla accent `\capitalcedilla{}', intended for use
      with capital letters.}
\endsetslot

\setslot{ogonek.inferior}
   \comment{The ogonek accent `\capitalogonek{}', intended for use with
      capital letters.}
\endsetslot

\nextslot{13}
\setslot{quotesinglbase.inferior}
   \comment{A straight single quote mark on the baseline,
      `\textquotestraightbase'.}
\endsetslot

\nextslot{18}
\setslot{quotedblbase.inferior}
   \comment{A straight double quote mark on the baseline,
      `\textquotestraightdblbase'.}
\endsetslot

\nextslot{21}
\setslot{twelveudash.inferior}
   \comment{A 2/3~em dash, `\texttwelveudash'.}
\endsetslot

\setslot{threequartersemdash.inferior}
   \comment{A 3/4~em dash, `\textthreequartersemdash'.}
\endsetslot

\nextslot{23}
\setslot{capitalcompwordmark.inferior}
    \comment{An invisible glyph, with zero width and depth, but the
      height of capital letters.
      It is used to stop ligaturing in words like `shelf{}ful'.}
\endsetslot

\nextslot{24}
\setslot{arrowleft.inferior}
   \comment{A left pointing arrow, `\textleftarrow', unavailable in
      most PostScript fonts.}
\endsetslot

\setslot{arrowright.inferior}
   \comment{A right pointing arrow, `\textrightarrow', unavailable in
      most PostScript fonts.}
\endsetslot

\nextslot{26}
\setslot{tieaccentlowercase.inferior}
   \comment{The original tie accent `\t{}', intended for use with
      lowercase letters.}
\endsetslot

\setslot{tieaccentcapital.inferior}
   \comment{The tie accent `\capitaltie{}', intended for use with
      capital letters.}
\endsetslot

\setslot{newtieaccentlowercase.inferior}
   \comment{A new tie accent `\newtie{}', intended for use with
      lowercase letters.}
\endsetslot

\setslot{newtieaccentcapital.inferior}
   \comment{A new tie accent `\capitalnewtie{}', intended for use
      with capital letters.}
\endsetslot

\nextslot{31}
\setslot{ascendercompwordmark.inferior}
    \comment{An invisible glyph, with zero width and depth, but the
      height of lowercase letters with ascenders.
      It is used to stop ligaturing in words like `shelf{}ful'.}
\endsetslot

\nextslot{32}
\setslot{blank.inferior}
   \comment{The blank indicator `\textblank', similar to the letter `b'
      with an oblique bar throgh the stem.}
\endsetslot

\nextslot{36}
\setslot{dollar.inferior}
   \comment{The dollar sign `\textdollar'.}
\endsetslot

\nextslot{39}
\setslot{quotesingle.inferior}
   \comment{A straight single quote mark, `\textquotesingle'.}
\endsetslot

\nextslot{42}
\setslot{asteriskcentered.inferior}
   \comment{The centered asterisk, `\textasteriskcentered'.}
\endsetslot

\nextslot{44}
\setslot{comma.inferior}
   \comment{The decimal comma `,'.}
\endsetslot

\nextslot{45}
\setslot{hyphendbl.inferior}
   \comment{An alternate double hyphen, `\textdblhyphen'.}
\endsetslot

\nextslot{46}
\setslot{period.inferior}
   \comment{The decimal point `.'.}
\endsetslot

\nextslot{47}
\setslot{fraction.inferior}
   \comment{The fraction slash `\textfractionsolidus'.}
\endsetslot

\nextslot{48}
\setslot{zerooldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{0}'.}
\endsetslot

\setslot{oneoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{1}'.}
\endsetslot

\setslot{twooldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{2}'.}
\endsetslot

\setslot{threeoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{3}'.}
\endsetslot

\setslot{fouroldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{4}'.}
\endsetslot

\setslot{fiveoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{5}'.}
\endsetslot

\setslot{sixoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{6}'.}
\endsetslot

\setslot{sevenoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{7}'.}
\endsetslot

\setslot{eightoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{8}'.}
\endsetslot

\setslot{nineoldstyle.inferior}
   \comment{The oldstyle number `\oldstylenums{9}'.}
\endsetslot

\nextslot{60}
\setslot{angbracketleft.inferior}
   \comment{The opening angle bracket `\textlangle', unavailable in
      most PostScript fonts.}
\endsetslot

\nextslot{61}
\setslot{minus.inferior}
   \comment{The subtraction sign `\textminus'.}
\endsetslot

\nextslot{62}
\setslot{angbracketright.inferior}
   \comment{The closing angle bracket `\textrangle', unavailable in
      most PostScript fonts.}
\endsetslot

\nextslot{77}
\setslot{Omegainv.inferior}
   \comment{The inverted Ohm sign `\textmho', unavailable in most fonts.}
\endsetslot

\nextslot{79}
   \comment{A circle `\textbigcircle', big enough to enclose a letter
      as in `\textcopyright' or `\textregistered'.}
\setslot{bigcircle.inferior}
\endsetslot

\nextslot{87}
\setslot{Omega.inferior}
   \comment{The upright Ohm sign `\textohm', unavailable in most fonts.
      Even if it is available in Mac-encoded fonts, it isn't directly
      accessible in the 8r or 8y encodings.}
\endsetslot

\nextslot{91}
\setslot{openbracketleft.inferior}
   \comment{The opening double square bracket `\textlbrackdbl',
      unavailable in most PostScript fonts.}
\endsetslot

\nextslot{93}
\setslot{openbracketright.inferior}
   \comment{The closing double square bracket `\textrbrackdbl',
      unavailable in most PostScript fonts.}
\endsetslot

\nextslot{94}
\setslot{arrowup.inferior}
   \comment{An upwards pointing arrow `\textuparrow', unavailable in
      most PostScript fonts.}
\endsetslot

\nextslot{95}
\setslot{arrowdown.inferior}
   \comment{An downwards pointing arrow `\textdownarrow', unavailable
      in most PostScript fonts.}
\endsetslot

\nextslot{96}
\setslot{asciigrave.inferior}
   \comment{An ASCII-style grave `\textasciigrave'. This is supposed
      to be a character by itself rather than a combining accents.}
\endsetslot

\nextslot{98}
\setslot{born.inferior}
   \comment{The born symbol `\textborn', unavailable in most PostScript
      fonts.}
\endsetslot

\nextslot{99}
\setslot{divorced.inferior}
   \comment{The divorced symbol `\textdivorced', unavailable in most
      PostScript fonts.}
\endsetslot

\nextslot{100}
\setslot{died.inferior}
   \comment{The died symbol `\textdied', unavailable in most PostScript
      fonts.}
\endsetslot

\nextslot{108}
\setslot{leaf.inferior}
   \comment{The leaf symbol `\textleaf', unavailable in most PostScript
      fonts.}
\endsetslot

\nextslot{109}
\setslot{married.inferior}
   \comment{The married symbol `\textmarried', unavailable in most
      PostScript  fonts.}
\endsetslot

\nextslot{110}
\setslot{musicalnote.inferior}
   \comment{A musical note symbol `\textmusicalnote', unavailable in
      most PostScript fonts.}
\endsetslot

\nextslot{126}
\setslot{tildelow.inferior}
   \comment{A lowered tilde `\texttildelow'.  In most PostScript fonts
      it can be substituted by `asciitilde', while `\textasciitilde'
      is supposed to be a raised `tilde'.}
\endsetslot

\nextslot{127}
\setslot{hyphendblchar.inferior}
    \comment{The glyph used for hyphenation in this font, which will
      almost always be the same as `hyphendbl'.}
\endsetslot

\nextslot{128}
\setslot{asciibreve.inferior}
   \comment{An ASCII-style breve `\textasciibreve'. This is supposed
      to be a character by itself rather than a combining accents.}
\endsetslot

\setslot{asciicaron.inferior}
   \comment{An ASCII-style caron `\textasciicaron'. This is supposed
      to be a character by itself rather than a combining accents.}
\endsetslot

\setslot{asciiacutedbl.inferior}
   \comment{An ASCII-style double tick mark, `\textacutedbl'.}
\endsetslot

\setslot{asciigravedbl.inferior}
   \comment{An ASCII-style double backtick mark, `\textgravedbl'.}
\endsetslot

\setslot{dagger.inferior}
   \comment{The single dagger `\textdagger'.}
\endsetslot

\setslot{daggerdbl.inferior}
   \comment{The double dagger `\textdaggerdbl'.}
\endsetslot

\setslot{bardbl.inferior}
   \comment{The double vertical bar `\textbardbl'.}
\endsetslot

\setslot{perthousand.inferior}
   \comment{The perthousand sign `\textperthousand'.}
\endsetslot

\setslot{bullet.inferior}
   \comment{The centered bullet `\textbullet'.}
\endsetslot

\setslot{centigrade.inferior}
   \comment{The degree centigrade symbol `\textcelsius'.}
\endsetslot

\setslot{dollaroldstyle.inferior}
   \comment{An oldstyle dollar sign `\textdollaroldstyle'.}
\endsetslot

\setslot{centoldstyle.inferior}
   \comment{An oldstyle cent sign `\textcentoldstyle'.}
\endsetslot

\setslot{florin.inferior}
   \comment{The florin sign `\textflorin'.}
\endsetslot

\setslot{colonmonetary.inferior}
   \comment{The Colon currency sign `\textcolonmonetary', similar to
      a capital `C' with a vertical bar through the middle.}
\endsetslot

\setslot{won.inferior}
   \comment{The Won currency sign `\textwon', similar to a capital `W'
      with two horizontal bars.}
\endsetslot

\setslot{naira.inferior}
   \comment{The Naira currency sign `\textnaira', similar to a
      capital `N' with two horizontal bars.}
\endsetslot

\setslot{guarani.inferior}
   \comment{The Guarani currency sign `\textguarani',  similar to
      a capital `G' with a vertical bar through the middle.}
\endsetslot

\setslot{peso.inferior}
   \comment{The Peso currency sign `\textpeso', similar to a capital `P'
      with a horizontal bar through the bowl or below the bowl.}
\endsetslot

\setslot{lira.inferior}
   \comment{The Lira currency sign `\textlira', similar to a sterling
      sign `\textsterling' with two horizontal bars.}
\endsetslot

\setslot{recipe.inferior}
   \comment{The recipe symbol `\textrecipe', similar to a capital `R'
      with an oblique bar through the tail.}
\endsetslot

\setslot{interrobang.inferior}
   \comment{The interrobang symbol `\textinterrobang', similar to
      a combination of an exclamation mark and a question mark.}
\endsetslot

\setslot{interrobangdown.inferior}
   \comment{The inverted interrobang symbol `\textinterrobangdown',
      similar to a combination of an inverted exclamation mark
      and an inverted question mark.}
\endsetslot

\setslot{dong.inferior}
   \comment{The Dong currency sign `\textdong', similar to a lowercase
      `d'  with a horizontal bar through the stem and another bar below
      the letter.}
\endsetslot

\setslot{trademark.inferior}
   \comment{The trademark sign `\texttrademark', similar to the raised
     letters `TM'.}
\endsetslot

\setslot{pertenthousand.inferior}
   \comment{The pertenthousand sign `\textpertenthousand', unavailable
     in most PostScript fonts.}
\endsetslot

\setslot{pilcrow.inferior}
   \comment{The pilcrow mark `\textpilcrow', similar to a paragraph
      mark `\textparagraph' with a single stem.}
\endsetslot

\setslot{baht.inferior}
   \comment{The Baht currency sign `\textbaht', similar to a capital `B'
      with a vertical bar through the middle.}
\endsetslot

\setslot{numero.inferior}
   \comment{The numero sign `\textnumero', similar to the letter `N'
      with a raised `o', unavailable in most PostScript fonts.}
\endsetslot

\setslot{discount.inferior}
   \comment{The discount sign `\textdiscount', similar to a stylized
      percent sign, unavailable in most PostScript fonts.}
\endsetslot

\setslot{estimated.inferior}
   \comment{The estimated sign `\textestimated', similar to an enlarged
      lowercase `e', unavailable in most PostScript fonts.}
\endsetslot

\setslot{openbullet.inferior}
   \comment{The centered open bullet `\textopenbullet'', unavailable
      in most PostScript fonts.}
\endsetslot

\setslot{servicemark.inferior}
   \comment{The service mark sign `\textservicemark', similar to the
      raised letters `SM', unavailable in most PostScript fonts.}
\endsetslot

\nextslot{160}
\setslot{quillbracketleft.inferior}
   \comment{The opening quill bracket `\textlquill', unavailable in
      most PostScript fonts.}
\endsetslot

\setslot{quillbracketright.inferior}
   \comment{The closing quill bracket `\textrquill', unavailable in
      most PostScript fonts.}
\endsetslot

\setslot{cent.inferior}
   \comment{The cent sign `\textcent'.}
\endsetslot

\setslot{sterling.inferior}
   \comment{The British currency sign, `\textsterling'.}
\endsetslot

\setslot{currency.inferior}
   \comment{The international currency sign, `\textcurrency'.}
\endsetslot

\setslot{yen.inferior}
   \comment{The Japanese currency sign, `\textyen'.}
\endsetslot

\setslot{brokenbar.inferior}
   \comment{A broken vertical bar, `\textbrokenbar', similar to
      `\textbar' with a gap through the middle.}
\endsetslot

\setslot{section.inferior}
   \comment{The section mark `\textsection'.}
\endsetslot

\setslot{asciidieresis.inferior}
   \comment{An ASCII-style dieresis `\textasciidieresis'. This is
       supposed to be character by itself  rather than an accents.}
\endsetslot

\setslot{copyright.inferior}
   \comment{The copyright sign `\textcopyright',  similar to a small
       letter `C' enclosed by a circle.}
\endsetslot

\setslot{ordfeminine.inferior}
   \comment{The raised letter `\textordfeminine'.}
\endsetslot

\setslot{copyleft.inferior}
   \comment{The reversed copyright sign `\textcopyleft', similar to
      a small reversed `C' enclosed by a circle.}
\endsetslot

\setslot{logicalnot.inferior}
   \comment{The logical not sign `\textlnot'.}
\endsetslot

\setslot{circledP.inferior}
   \comment{A small letter `P' enclosed by a circle, `\textcircledP',
      unavailable in most fonts.}
\endsetslot

\setslot{registered.inferior}
   \comment{The registered trademark sign `\textregistered', similar to
      a small letter `R' enclosed by a circle.}
\endsetslot

\setslot{asciimacron.inferior}
   \comment{An ASCII-style macron `\textasciimacron'. This is supposed
      to be a character by itself rather than a combining accents.}
\endsetslot

\setslot{degree.inferior}
   \comment{The degree sign `\textdegree'.}
\endsetslot

\setslot{plusminus.inferior}
   \comment{The plus or minus sign `\textpm'.}
\endsetslot

\setslot{two.superior}
   \comment{The raised digit `\texttwosuperior'.}
\endsetslot

\setslot{three.superior}
   \comment{The raised digit `\textthreesuperior'.}
\endsetslot

\setslot{asciiacute.inferior}
   \comment{An ASCII-style acute `\textasciiacute'. This is supposed
      to be a character by itself rather than a combining accents.}
\endsetslot

\setslot{mu.inferior}
   \comment{The lowercase Greek letter `\textmu', intended  for use as
      a prefix `micro' in physical units.}
\endsetslot

\setslot{paragraph.inferior}
   \comment{The paragraph mark `\textparagraph'.}
\endsetslot

\setslot{periodcentered.inferior}
   \comment{The centered period `\textperiodcentered'.}
\endsetslot

\setslot{referencemark.inferior}
   \comment{The reference mark `\textreferencemark', similar to
      a combination of the `multiply' and `divide' symbols.}
\endsetslot

\setslot{one.superior}
   \comment{The raised digit `\textonesuperior'.}
\endsetslot

\setslot{ordmasculine.inferior}
   \comment{The raised letter `\textordmasculine'.}
\endsetslot

\setslot{radical.inferior}
   \comment{The radical sign `\textsurd', unavailable in most fonts.
      Even if it is available in Mac-encoded fonts, it isn't directly
      accessible in the 8r or 8y encodings.}
\endsetslot

\setslot{onequarter.inferior}
   \comment{The fraction `\textonequarter'.}
\endsetslot

\setslot{onehalf.inferior}
   \comment{The fraction `\textonehalf'.}
\endsetslot

\setslot{threequarters.inferior}
   \comment{The fraction `\textthreequarters'.}
\endsetslot

\setslot{Euro.inferior}
   \comment{The European currency sign, similar to `\texteuro'.}
\endsetslot


\nextslot{214}
\setslot{multiply.inferior}
   \comment{The multiplication sign `\texttimes'.
      This symbol was originally intended to be put into slot~215,
      but ended up in this slot by mistake, at which time it was
      considered too late to change it.}
\endsetslot

\nextslot{246}
\setslot{divide.inferior}
   \comment{The divison sign `\textdiv'.
      This symbol was originally intended to be put into slot~247,
      but ended up in this slot by mistake, at which time it was
      onsidered too late to change it.}
\endsetslot

\endencoding


%%
%% This is file `fcltxdoc.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% fcltxdoc.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% Copyright (FC) 2010-2011 - lppl
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file fcltxdoc.dtx
%% and the derived files
%%    fcltxdoc.sty, fcltxdoc.pdf, fcltxdoc.ins, fcltxdoc.drv.
%% 
%% fcltxdoc: 2011/03/12 v1.0 - Private additional ltxdoc support (FC)
%% 
%% This package is not intended for public use.
%% It is required to compile some of my documentations files.
%% 
%% ------------------------------------------------------------------------------
\def\thisinfo {Private additional ltxdoc support}
\def\thisversion {1.0}
\PassOptionsToPackage{}{hyperref}
\PassOptionsToPackage{linegoal,full}{tabu}
\RequirePackage [latin1]{inputenc}
\RequirePackage [\detokenize{��},scrartcl]{fcltxdoc}
\RequirePackage {calc}
\csname endofdump\endcsname
\let\microtypeYN=n
\RequirePackage {tabu}
\CheckDates {interfaces=2011/02/12,linegoal=2011/02/25}
\documentclass[a4paper,oneside]{ltxdoc}
\AtBeginDocument{\catcode164=14}
\RequirePackage{ragged2e,lmodern,pifont,upgreek}
\ifx y\microtypeYN
   \usepackage[expansion=all,stretch=20,shrink=60]{microtype}\fi            % font (microtype)
\usepackage {logbox}
\lstset{
backgroundcolor=\color{GhostWhite!200},
commentstyle=\ttfamily\color{violet},
keywordstyle=[3]{\color{black}\bfseries},
morekeywords=[3]{&,\\},
keywordstyle=[4]{\color{red}\bfseries},
%%    morekeywords=[4]{red},
keywordstyle=[5]{\color{blue}\bfseries},
morekeywords=[5]{blue},
keywordstyle=[6]{\color{green}\bfseries},
morekeywords=[6]{green},
keywordstyle=[7]{\color{yellow}\bfseries},
morekeywords=[7]{yellow},
keywordstyle=[8]{\color{FireBrick!80}\bfseries},
morekeywords=[8]{
},
keywordstyle=[9]{\color{pkgcolor}\bfseries},
morekeywords=[9]{
},
keywordstyle=[10]{\color{pkgcolor}\bfseries\slshape},
morekeywords=[10]{},
extendedchars={true},
alsoletter={&},
}
\CodelineNumbered
\RequirePackage {geometry}
\geometry {top=0pt,includeheadfoot,headheight=.6cm,headsep=.6cm,bottom=.6cm,footskip=.5cm,left=30mm,right=1.5cm}
\hypersetup{%
  pdfsubject={Private additional ltxdoc support},
  pdfauthor={Florent CHERVET},
  pdfstartview=FitH 0,
  pdfkeywords={tex, e-tex, latex, package, doc, doc.sty, dtx, documentation},
  }
\begin{document}
   \DocInput{\thisfile.dtx}
\end{document}
\endinput
%%
%% End of file `fcltxdoc.drv'.

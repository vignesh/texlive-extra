% \iffalse
% +AMDG  This document was begun on E October 11EE, the
% feast of the Maternity of the Blessed Virgin Mary, and it
% is humbly dedicated to her and to her Immaculate Heart for
% her prayers, and to the Sacred Heart of Jesus for His
% mercy.
%
% This document is copyright 2015 by Donald P. Goodman, and is
% released publicly under the LaTeX Project Public License.  The
% distribution and modification of this work is constrained by the
% conditions of that license.  See
% 	http://www.latex-project.org/lppl.txt
% for the text of the license.  This document is released
% under version 1.3 of that license, and this work may be distributed
% or modified under the terms of that license or, at your option, any
% later version.
% 
% This work has the LPPL maintenance status 'maintained'.
% 
% The Current Maintainer of this work is Donald P. Goodman
% (dgoodmaniii@gmail.com).
% 
% This work consists of xellipsis.dtx, xellipsis.ins, and
% derived files xellipsis.sty and xellipsis.pdf.
% \fi

% \iffalse
%<package>\NeedsTeXFormat{LaTeX2e}[1999/12/01]
%<package>\ProvidesPackage{xellipsis}[2015/11/01 v2.0 support for highly configurable ellipses of arbitrary symbol, number, and distance]
%<*driver>
\documentclass{ltxdoc}

\usepackage{doc}
\usepackage{array}
\usepackage{lettrine}
	\setcounter{DefaultLines}{3}
	\setlength{\DefaultFindent}{2pt}
	\renewcommand{\LettrineFontHook}{\color{red}}
\usepackage{url}
\usepackage{spverbatim}
\usepackage[typeone]{dozenal}
\usepackage{lmodern}
\usepackage[]{xellipsis}
\usepackage[colorlinks]{hyperref}
\usepackage{makeidx}
\EnableCrossrefs
\PageIndex
\CodelineNumbered
\RecordChanges
\makeindex

\long\def\example#1#2{%
	\bigskip%
	\hrule%
	\hbox to\linewidth{%
		\hbox to0.5\linewidth{%
			\vbox to2in{\vfil#1\vfil}%
		}\hbox to0.5\linewidth{%
			\vbox to2in{\vfil#2\vfil}%
		}%
	}%
	\hrule%
	\bigskip%
}

\tracingmacros=3 \begin{document} \DocInput{xellipsis.dtx}
\end{document}
%</driver> \fi
%
% \title{The |xellipsis| Package, v2.0} \author{Donald P.\
% Goodman III} \date{\today}
%
% \maketitle
%
% \begin{abstract} \TeX\ users have long been remarking that
% the default characteristics of ellipses, whether produced
% by |\dots|, |\ldots|, or some other command, just aren't
% quite right.  While some packages have attempted to
% resolve this issue (e.g., |lips| and |ellipsis|), these
% have never quite fit my use cases.  |xellipsis| attempts
% to fill this gap by providing absurdly configurable
% ellipses, along with preconfigured options for the Chicago
% Manual of Style (and Turabian), MLA, and the Bluebook.  \end{abstract}
%
% \tableofcontents
%
% \section{Introduction}
%
% Typography has long made use of these strange sequences of
% (usually) dots called \emph{ellipses} (singular
% \emph{ellipsis}).  An ellipsis typically indicates an
% omission from a quotation (indeed, the Greek root means
% ``omission''), but they have also been used in a huge
% variety of other ways over the years:
%
% \begin{itemize} \item The aforementioned omission.  ``Four
% score and\xelip seven years ago.'' \item An unfinished
% thought.  ``Our forefathers brought forth\xelip I don't
% know; something, I guess.'' \item Indications of preceding
% matter.  ``\xelip a new nation, conceived in liberty.''
% \item A pause.  ``I think\xelip therefore, I am.'' \item
% Probably lots of other things, as well.  \end{itemize}
%
% \LaTeX, by default, produces ellipses with the |\dots|
% (\dots) or |\ldots| (\ldots) commands.  These are fine, as
% far as they go; however, they suffer a few clear faults.
% For example, there is much less space at the beginning
% than at the end, which many people don't like; they are
% fairly close together, which not only do many people not
% like, but which goes against some important style manuals;
% they cannot have four or more dots, which some style
% manuals require in certain circumstances; and their
% spacing is not in any way configurable.
%
% So |xellipsis| gives you all the configuration options for
% your ellipses that you could possibly want, and probably
% lots more than you'll ever need.  It also comes
% prepackaged with a few common formats, which are selected
% as package options at load time.
%
% |xellipsis| is packaged according to the \LaTeX\
% \textsc{docstrip} utility, which allows automatic
% extraction of code and documentation from the same files.
%
% \section{Preconfigured Formats}
% \label{sect:preconf}
%
% |xellipsis| comes with some preconfigured ellipsis formats
% for the convenience of users.  These are all set as
% package options; so, for example, when loading the package
% one states |\usepackage{xellipsis}| for the default
% behavior, but |\usepackage[latex]{xellipsis}| to specify
% the |latex| format.
%
% Please note that the usage of these ellipses is not always
% clear, even according to the manuals.  I've done my best
% to get them right here, but if they're wrong on some
% detail, please let me know and I'll try to fix them.
% |xellipsis| offers ample configuration options to get
% these things right.
%
% \DescribeMacro{latex}
% The |latex| option sets up |\xelip| to behave identically
% to the default \LaTeX\ setting of |\dots| or |\ldots|.
% (The \LaTeX\ kernel defines |\ldots| as
% |\let\ldots=\dots|, so the two are the same.)  In a
% convoluted way, |\dots| is defined in terms of
% |\textellipsis|, which itself is simply three dots
% separated by the current font's |\fontdimen3|, which is
% the stretchability of interword space.  Not the interword
% space itself, mind you, but just the stretchability of
% that space.  This leads to pretty tightly spaced ellipses
% for most fonts; but there we are.  It also specifies no
% particular space before the ellipsis, but the same space
% as the gaps afterwards.  This option yields the following:
%	\begin{quote}
%		{\xelipbef=0pt\xelipaft=\fontdimen3\font\xelipgap=\fontdimen3\font
% 		This is\xelip pretty tight, really. (|\xelip|)
%
% 		This is\ldots pretty tight, really. (|\ldots|)
%		}
%	\end{quote}
%
% \DescribeMacro{chicago}
% \textit{The Chicago Manual of Style} has some very
% specific rules about ellipses; specifically, that they
% should be three periods plus two nonbreaking spaces.  So
% |xellipsis| defines them precisely that way, in terms of
% |\fontdimen2| (the non-glue portion of the interword space
% for the font), and adds no space before the first dot.
% The \textsc{cmos} also specifies that, at the end of a
% natural sentence, the period of the sentence should remain
% \emph{prior} to the ellipsis; this works fine with our
% definition.
% This system yields:
%	\begin{quote}
%		{\xelipbef=0pt\xelipaft=\fontdimen2\font\xelipgap=\fontdimen2\font
% 		This is\xelip pretty loose, really. (|\xelip|)
%
% 		I like that.\ \xelip It makes some sense. (|.\ \xelip|)
%		}
%	\end{quote}
%
% \DescribeMacro{mla}
% The \textsc{mla} advises similar ellipses to the \textsc{cmos},
% except that there should always be a space before the
% first period.
%	\begin{quote}
%		{\xelipbef=\fontdimen2\font\xelipaft=\fontdimen2\font\xelipgap=\fontdimen2\font
% 		This is\xelip pretty loose, really. (|\xelip|)
%		}
%	\end{quote}
%
% \DescribeMacro{oldmla}
% The \textsc{mla} formerly advised that ellipses indicating
% omissions should be surrounded by square brackets ([]).
% The materials I've seen seem to indicate that this might
% still be required when the quotation already contains
% ellipses in the original material; however, usually they
% are not required.  Either way, here's the option:
%	\begin{quote}
%		{\xelipbef=\fontdimen2\font\xelipaft=\fontdimen2\font\xelipgap=\fontdimen2\font\def\xelipprechar{[}\def\xelippostchar{]}\xelipprebef=\fontdimen2\font\xelippostaft=\fontdimen2\font
% 		This is\xelip how it's supposed to work, I guess? (|\xelip|)
%		}
%	\end{quote}
%
% \DescribeMacro{bluebook}
% Finally, in the United States legal citations are governed
% by our overlords at the Harvard Law Review, who publish
% \textit{The Bluebook}.  Their ellipses are formatted just
% like the \textsc{mla} ellipses:
%	\begin{quote}
%		{\xelipbef=\fontdimen2\font\xelipaft=\fontdimen2\font\xelipgap=\fontdimen2\font
% 		This is\xelip pretty loose, really. (|\xelip|)
%		}
%	\end{quote}
%
% Finally, there is a special command
% \DescribeMacro{\xelipend}|\xelipend|.  This is identical
% to |\xelip| except that the closing space is omitted.
%
%	\begin{quote}
% 		``This is pretty neat\xelipend'' (|\xelipend|)
%
% 		``This is pretty neat\xelip'' (|\xelip|)
%	\end{quote}
%
% |\xelipend| is primarily useful immediately preceding some
% quoting character; some people prefer there to be no or
% much more limited spacing in this location.  Use
% |\xelipend| if you prefer this type of behavior.
%
% \section{The Nitty-Gritty:  Configuration}
% \label{sect:nittygritty}
%
% We've already seen examples of the |xellipsis| defaults
% used (any time in this document that we haven't been
% explicitly demonstrating something else), so let's get
% down-and-dirty with the internals and do some customizing.
%
% The first is the most obvious:  the character from which
% the ellipsis is constructed.  This defaults to ``.'' (a
% period), and is held in the variable
% \DescribeMacro{\xelipchar}|\xelipchar|.  Simply redefine
% |\xelipchar| in the normal way to get something different
% from the ordinary:
%
% \bigskip \hbox to\linewidth{% \hfil% \hbox
% to0.4\linewidth{% \hfil% \vbox{%
% \hbox{|\def\xelipchar{*}|} \hbox{|\xelip|} } \hfil% }\hbox
% to0.4\linewidth{% \hfil% \vbox{% \def\xelipchar{*}\xelip }
% \hfil% } \hfil% } \bigskip
%
% \def\xelipchar{*} This was actually quite common in older
% legal documents (using asterisks for ellipses, that is):
% ``And the ruling of the lower court\xelip is hereby
% \textsc{affirmed}.'' \def\xelipchar{.}  So this sort of
% thing has some real functionality at times.
%
% The same effect can be achieved by using the
% \DescribeMacro{char}|char| package option; e.g., by
% loading |xellipsis| with |\usepackage[char=*]{xellipsis}|.
% Indeed, as of version 2.0, all these settings can be made
% at the time of package loading.
%
% The \emph{number} of characters in an ellipsis is governed
% by \DescribeMacro{\xelipnum}|\xelipnum|.   |\xelipnum| is
% a \emph{counter}, not a macro, so if you change it, do so
% with a simple |=|:
%
% \bigskip \hbox to\linewidth{% \hfil% \hbox
% to0.4\linewidth{% \hfil% \vbox{% \hbox{|\xelipnum=6|}
% \hbox{|\xelip|} } \hfil% }\hbox to0.4\linewidth{% \hfil%
% \vbox{% \xelipnum=6 \xelip } \hfil% } \hfil% } \bigskip
%
% Most likely this would be 3 or 4, but I'm not going to try
% to limit you.  |\xelipnum| defaults to 3.  The package
% option is \DescribeMacro{num}|num|.
%
% There are three lengths responsible for controlling the
% spacing of the ellipses.  The space \emph{before} the
% first character of the ellipses is
% \DescribeMacro{\xelipbef}|\xelipbef|; the space
% \emph{after} the last character of the ellipses is
% \DescribeMacro{\xelipaft}|\xelipaft|; and the space
% \emph{between} the characters of the ellipses is
% \DescribeMacro{\xelipgap}|\xelipgap|.  Each of these
% default to |3pt|.  Their package options are
% \DescribeMacro{before}|before|,
% \DescribeMacro{after}|after|, and
% \DescribeMacro{gap}|gap|.
%
% The benefit of having each of these independently
% configurable is that you don't have to worry about
% inserting special spacing before the ellipsis.
% Furthermore, you don't have to worry about |\xelip| eating the space
% after your command unless you prevent that with |{}| or 
% |\ |; it \emph{does}
% eat the space, but that's okay, because the appropriate
% spacing is built into it.  So you can say 
% |I went\xelip to the store| and you'll get 
% ``I went\xelip to the store'', exactly as you undoubtedly
% wanted.  No need for |\xelip{}| or such expedients.
%
% Remember that these lengths are |dimen|s, so they are
% reset using a simple |=|; you can also use the \LaTeX\
% |\setlength| incantation if you'd prefer:
%
% \bigskip
% \hbox to\linewidth{%
% 	\hfil%
% 	\hbox to0.49\linewidth{%
% 		\hfil%
% 		\vbox{%
% 			\hbox{|\xelipbef=1em|}
% 			\hbox{|\xelipgap=2em|}
% 			\hbox{|\setlength{\xelipaft}{2pt}|}
% 			\hbox{|This is\xelip strange.|}
% 		}%
% 		\hfil%
% 	}\hbox to0.49\linewidth{%
% 		\hfil%
% 		\vbox{%
% 			\xelipbef=1em
% 			\xelipgap=2em
% 			\setlength{\xelipaft}{2pt}
% 			This is\xelip strange.
% 		}
% 		\hfil%
% 	}%
% 	\hfil%
% }
% \bigskip
%
% Finally, some style guides (including the MLA, until
% recently) require some surrounding punctuation for an
% ellipsis.  |xellipsis| provides for this, too.  The
% character that is to go \emph{before} the ellipsis is
% \DescribeMacro{\xelipprechar}|\xelipprechar|, while the
% character that is to go \emph{after} the ellipsis is
% \DescribeMacro{\xelippostchar}|\xelippostchar|.  Both of
% these default to be empty.  Their package options are
% \DescribeMacro{prechar}|prechar| and
% \DescribeMacro{postchar}|postchar|.
%
% \bigskip
% \hbox to\linewidth{%
% 	\hfil%
% 	\hbox to0.49\linewidth{%
% 		\hfil%
% 		\vbox{%
% 			\hbox{|\def\xelipprechar{[}|}
% 			\hbox{|\def\xelippostchar{]}|}
% 			\hbox{|This is\xelip strange.|}
% 		}%
% 		\hfil%
% 	}\hbox to0.49\linewidth{%
% 		\hfil%
% 		\vbox{%
% 			\def\xelipprechar{[}
% 			\def\xelippostchar{]}
% 			This is\xelip strange.
% 		}
% 		\hfil%
% 	}%
% 	\hfil%
% }
% \bigskip
%
% As with |\xelipchar|, you can use really anything for
% this, even things that would be truly bizarre (daggers?
% fleur-de-lis?); but this is a typical use case.
%
% The unsurprisingly-named
% \DescribeMacro{\xelipprebef}|\xelipprebef|,
% \DescribeMacro{\xelippreaft}|\xelippreaft|,
% \DescribeMacro{\xelippostbef}|\xelippostbef|, and
% \DescribeMacro{\xelippostaft}|\xelippostaft| control the
% spacing around these two characters; by default, all four
% are set to |0pt|.  They are |dimen|s and can be set in
% either the \TeX\ way or the \LaTeX\ |\setlength| way.
% Their package options are
% \DescribeMacro{prebefore}|prebefore|,
% \DescribeMacro{preafter}|preafter|,
% \DescribeMacro{postbefore}|postbefore|, and
% \DescribeMacro{postafter}|postafter|,
%
% \bigskip
% \hbox to\linewidth{%
% 	\hfil%
% 	\hbox to0.49\linewidth{%
% 		\hfil%
% 		\vbox{%
% 			\hbox{|\def\xelipprechar{[}|}
% 			\hbox{|\def\xelippostchar{]}|}
% 			\hbox{|\xelipprebef=6pt|}
% 			\hbox{|\xelippreaft=3pt|}
% 			\hbox{|\setlength{\xelippostbef}{3pt}|}
% 			\hbox{|\setlength{\xelippostaft}{6pt}|}
% 			\hbox{|This is\xelip strange.|}
% 		}%
% 		\hfil%
% 	}\hbox to0.49\linewidth{%
% 		\hfil%
% 		\vbox{%
% 			\def\xelipprechar{[}
% 			\def\xelippostchar{]}
% 			\xelipprebef=6pt
% 			\xelippreaft=3pt
% 			\setlength{\xelippostbef}{3pt}
% 			\setlength{\xelippostaft}{6pt}
% 			This is\xelip strange.
% 		}
% 		\hfil%
% 	}%
% 	\hfil%
% }
% \bigskip
%
% Please be aware that |\xelippreaft| \emph{stacks}
% with |\xelipbef|, and |\xelippostbef| stacks with
% |\xelipaft|; so their sum should be the spacing you want.
% Often this means simply leaving the two of them alone, as
% you've already got the appropriate space in the
% shorter-named |dimen|s.
%
% And that's it; if |xellipsis| can't meet your ellipsis
% needs somehow, please contact me and let me know, and I'll
% endeavor to add what you require.  Happy \TeX{}ing!
%
% \section{Implementation}
%
% Our first task, as usual, is to define our options.  We
% have options for default \LaTeX\ ellipsis behavior,
% Chicago Manual of Style behavior, old MLA behavior, and
% current MLA behavior.  These all default to \emph{off},
% and the default |xellipsis| behavior described above is
% \emph{on}.  We define the conditions for the options, then
% process the options themselves.  We use |xkeyval| to get
% these options, so we can easily give them key-value
% arguments.  We also need to define our default characters.
%    \begin{macrocode}
\RequirePackage{xkeyval}
\def\xelipchar{.}
\def\xelipprechar{}
\def\xelippostchar{}
%    \end{macrocode}
% Proceed to define the dimens that we will need.  These are
% mostly self-explanatory; the first is for the gap
% between the characters of the ellipsis, the second is for
% the space \emph{before} the ellipsis begins, and the third
% is for the space \emph{after} the ellipsis ends.  The
% fourth and fifth are for the spaces around the
% pre-ellipsis character, and the sixth and seventh are for
% the spaces around the post-ellipsis character.
%    \begin{macrocode}
\newdimen\xelipgap\xelipgap=3pt
\newdimen\xelipbef\xelipbef=3pt
\newdimen\xelipaft\xelipaft=3pt
\newdimen\xelipprebef\xelipprebef=0pt
\newdimen\xelippreaft\xelippreaft=0pt
\newdimen\xelippostbef\xelippostbef=0pt
\newdimen\xelippostaft\xelippostaft=0pt
%    \end{macrocode}
% Now we define the counter which holds the number of
% ellipsis characters we want.  Defaults to 3.
%    \begin{macrocode}
\newcount\xelipnum\xelipnum = 3%
%    \end{macrocode}
%    \begin{macrocode}
\newif\ifxel@latex\xel@latexfalse
\newif\ifxel@chicago\xel@chicagofalse
\newif\ifxel@oldmla\xel@oldmlafalse
\newif\ifxel@mla\xel@mlafalse
\newif\ifxel@bluebook\xel@bluebookfalse
\DeclareOptionX{latex}{\xel@latextrue}
\DeclareOptionX{chicago}{\xel@chicagotrue}
\DeclareOptionX{oldmla}{\xel@oldmlatrue}
\DeclareOptionX{mla}{\xel@mlatrue}
\DeclareOptionX{bluebook}{\xel@bluebooktrue}
\DeclareOptionX{char}{\def\xelipchar{#1}}
\DeclareOptionX{num}{\xelipnum=#1}
\DeclareOptionX{before}{\xelipbef=#1}
\DeclareOptionX{after}{\xelipaft=#1}
\DeclareOptionX{gap}{\xelipgap=#1}
\DeclareOptionX{prechar}{\def\xelipprechar{#1}}
\DeclareOptionX{postchar}{\def\xelippostchar{#1}}
\DeclareOptionX{prebefore}{\xelipprebef=#1}
\DeclareOptionX{preafter}{\xelippreaft=#1}
\DeclareOptionX{postbefore}{\xelippostbef=#1}
\DeclareOptionX{postafter}{\xelippostaft=#1}
\ProcessOptionsX
%    \end{macrocode}
% Now we define the loop variable.
%    \begin{macrocode}
\newcount\xel@loopi\xel@loopi = 0%
%    \end{macrocode}
% Next, we define the box which will hold the ellipsis; this
% way we can be sure that it won't break across lines.
%    \begin{macrocode}
\def\xelip{%
	\nobreak\hskip0pt\hbox{%
%    \end{macrocode}
% Put it the code for the |\xelipprechar| and its spacing:
%    \begin{macrocode}
		\hskip\xelipprebef\xelipprechar\hskip\xelippreaft%
%    \end{macrocode}
% Now, we use |\xelipbef| to skip the pre-ellipsis distance.
%    \begin{macrocode}
		\hskip\xelipbef\xelipchar%
%    \end{macrocode}
% Now we loop, printing |\xelipchar| and skipping
% |\xelipgap| as many times as |\xelipnum| requires.  We
% start be resetting the |\xel@loopi| variable, just in
% case.
%    \begin{macrocode}
		\xel@loopi = 1%
		\loop\ifnum\xelipnum>\xel@loopi%
			\advance\xel@loopi by1%
			\hskip\xelipgap%
			\xelipchar%
		\repeat
%    \end{macrocode}
% Lastly, we skip |\xelipaft|, then skip for
% |\xelippostchar| and its |dimen|s, close our box, and sit
% back in the satisfaction of an ellipsis well-made.
%    \begin{macrocode}
		\hskip\xelipaft%
		\hskip\xelippostbef\xelippostchar\hskip\xelippostaft%
	}%
}%
%    \end{macrocode}
% Set up a special command, |\xelipend|, which omits the
% spacing at the end and can be used immediately before
% quotations or the like.
%    \begin{macrocode}
\def\xelipend{%
	\begingroup%
	\xelipaft=0pt
	\xelip%
	\endgroup%
}%
% 
% Set up the |latex| option.
%    \begin{macrocode}
\ifxel@latex
	\xelipbef=0pt%
	\xelipaft=\fontdimen3\font%
	\xelipgap=\fontdimen3\font%
\fi
%    \end{macrocode}
% \textit{The Chicago Manual of Style} option.
%    \begin{macrocode}
\ifxel@chicago
	\xelipbef=0pt%
	\xelipaft=\fontdimen2\font%
	\xelipgap=\fontdimen2\font%
\fi
%    \end{macrocode}
% The \textsc{mla} option.
%    \begin{macrocode}
\ifxel@mla
	\xelipbef=\fontdimen2\font%
	\xelipaft=\fontdimen2\font%
	\xelipgap=\fontdimen2\font%
\fi
%    \end{macrocode}
% \textit{The Bluebook} option.
%    \begin{macrocode}
\ifxel@bluebook
	\xelipbef=\fontdimen2\font%
	\xelipaft=\fontdimen2\font%
	\xelipgap=\fontdimen2\font%
\fi
%    \end{macrocode}
% The old \textsc{mla} option.
%    \begin{macrocode}
\ifxel@oldmla
	\xelipbef=\fontdimen2\font%
	\xelipaft=\fontdimen2\font%
	\xelipgap=\fontdimen2\font%
	\def\xelipprechar{[}%
	\def\xelippostchar{]}%
	\xelipprebef=\fontdimen2\font%
	\xelippostaft=\fontdimen2\font%
\fi
%		}
%    \end{macrocode}
% And there's the |xellipsis| package.  I hope it proves
% useful to someone besides myself.  Happy \TeX{}ing!
%
% \PrintIndex

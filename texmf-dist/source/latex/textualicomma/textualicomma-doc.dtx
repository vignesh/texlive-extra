%\iffalse
%%  Doc-Source file to use with LaTeX2e
%%  Copyright (C) 2017-18 Sebastian Friedl
%%
%%  This work is subject to the LaTeX Project Public License, Version 1.3c or -- at
%%  your option -- any later version of this license.
%%  The work consists of the files textualicomma.dtx, textualicomma.ins,
%%  textualicomma-doc.dtx and the derived file textualicomma.sty
%%
%%  This work has the LPPL maintenance status ‘maintained’.
%%  Current maintainer of the work is Sebastian Friedl.
%%
%%  -------------------------------------------------------------------------------------------
%%
%%  Use the textual comma character as decimal separator in math mode
%%
%%  -------------------------------------------------------------------------------------------
%%
%%  Please report bugs and other problems as well as suggestions for improvements
%%  to my email address (sfr682k@t-online.de).
%%
%%  -------------------------------------------------------------------------------------------
%\fi

% !TeX spellcheck=en_US

\documentclass[11pt]{ltxdoc}

\usepackage{iftex}
\RequireLuaTeX

\usepackage[no-math]{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage[english]{selnolig}



\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{hologo}
\usepackage{multicol}

\usepackage{verbatim}


\usepackage[a4paper,left=4.50cm,right=2.75cm,top=3.25cm,bottom=2.75cm,nohead]{geometry}


\usepackage[unicode, pdfborder={0 0 0}, linktoc=all, hyperindex=false]{hyperref}


\usepackage[erewhon,vvarbb]{newtxmath}
\setmainfont{Source Serif Pro}
\setsansfont[Scale=MatchLowercase]{Source Sans Pro}
\setmonofont[Scale=MatchLowercase]{Hack}



\newcommand*{\cls}[1]{\textsf{#1}}
\newcommand*{\sty}[1]{\textsf{#1}}
\newcommand*{\opt}[1]{\texttt{#1}\index{#1=\texttt{#1}~~option|main}}

\def\DescribeOption#1{\marginpar{\raggedleft\PrintDescribeMacro{#1}}}

\renewcommand*{\usage}[1]{\hyperpage{#1}}
\renewcommand*{\main}[1]{\hyperpage{#1}}

\PageIndex
\RecordChanges

\parindent0pt

\MakeShortVerb{"}
\CheckSum{101}


\title{The \sty{textualicomma} package \\ {\large\url{https://gitlab.com/SFr682k/textualicomma}}}
\author{Sebastian Friedl \\ \href{mailto:sfr682k@t-online.de}{\ttfamily sfr682k@t-online.de}}
\date{2018/08/23 (v1.1)}

\hypersetup{pdftitle={The textualicomma package}, pdfauthor={Sebastian Friedl}}


\begin{document}
    \maketitle
    
    \begin{abstract}\noindent
        Use the textual comma character as decimal separator in math mode. \\[\medskipamount]
        \sty{textualicomma} is based on the \href{https://www.ctan.org/pkg/icomma}{\sty{icomma} package}
        and intended as a solution for cases where the comma character used in horizontal (i.e.~text) mode
        discerns from the one used in math mode. \\
        Note: Escaping to text mode every time a comma is used in math mode might slow down the compilation of huge documents.
        Please check, whether \href{https://www.ctan.org/pkg/icomma}{\sty{icomma}} already satisfies your needs.
    \end{abstract}


    \tableofcontents
    
    
    
    \clearpage
    \subsection*{Dependencies and other requirements}
    \addcontentsline{toc}{subsection}{Dependencies and other requirements}
    \sty{textualicomma} requires \LaTeXe\ and depends on the \sty{amstext} package. \\
    Also, the three NFSS commands "\rmfamily", "\sffamily" and "\ttfamily" have to be defined
    (which holds true for every common document class).
    
    
    
    \subsection*{Installation}
    \addcontentsline{toc}{subsection}{Installation}
    Extract the \textit{package} file first:
    \begin{enumerate}
        \item Run \LaTeX\ over the file "textualicomma.ins"
        \item Move the resulting ".sty" file to "TEXMF/tex/latex/textualicomma/"
    \end{enumerate}
    
    Then, you can compile the \textit{documentation} yourself by executing \\[\smallskipamount]
    "lualatex textualicomma-doc.dtx" \\
    "makeindex -s gind.ist textualicomma-doc.idx" \\
    "makeindex -s gglo.ist -o textualicomma-doc.gls textualicomma-doc.glo" \\
    "lualatex textualicomma-doc.dtx" \\
    "lualatex textualicomma-doc.dtx"
    
    \smallskip
    or just use the precompiled documentation shipped with the source files. \\
    In both cases, copy the files "textualicomma-doc.pdf" and "README.md" to \\
    "TEXMF/doc/latex/textualicomma/"
    
    
    
    \subsection*{License}
    \addcontentsline{toc}{subsection}{License}
    \begin{small}
        \textcopyright\ 2017-18 Sebastian Friedl
        
        \smallskip
        This work may be distributed and/or modified under the conditions of the \LaTeX\ Project Public License, either version 1.3c of this license or (at your option) any later version.
        
        \smallskip
        The latest version of this license is available at \url{http://www.latex-project.org/lppl.txt} and version 1.3c or later is part of all distributions of \LaTeX\ version 2008-05-04 or later.
        
        \smallskip
        This work has the LPPL maintenace status \enquote*{maintained}. \\
        Current maintainer of this work is Sebastian Friedl.
        
        \medskip
        This work consists of the following files:
        \begin{itemize} \itemsep 0pt
            \item "textualicomma.sty",
            \item "textualicomma.ins",
            \item "textualicomma-doc.dtx" and
            \item the derived file "textualicomma.sty"
        \end{itemize}
    \end{small}

    
    




    \clearpage
    \part{The documentation}
    \section{Introduction}
    Using the comma as decimal separator in ordinary \LaTeX\ has always been proble\-matic: since the comma is treated as punctuation,\TeX\ adds some space after it. \\
    The solution was defining an \enquote*{intelligent} comma recognizing whether it's used as decimal separator or punctuation character.
    
    \medskip
    The common package implementing this solution is \sty{icomma} by Walter Schmidt~\cite{icomma}.
    However, some fonts, e.g. Source Serif Pro, don't provide any math support. \\
    Although there are fonts looking very similar to Source Serif and providing math support (like Utopia and its ‘clones’ Fourier and Erewhon), more or less obvious differences are visible (compare \enquote*{$,$} to~\enquote*{,} at high magnifications). The only solution solving this \enquote*{problem} is escaping to text mode.
    
    
    
    \section{The \sty{textualicomma} package}
    \subsection{\sty{textualicomma}'s behaviour}
    \sty{textualicomma} makes some changes and additions to \sty{icomma}'s code in order to provide automated escaping to text mode.
    The behaviour remains the same:
    \begin{itemize}
        \item
            The comma is treated as decimal separator if it is directly followed by a number.
            Thus, a number is to be entered as, for instance, \\
            "3,142"
        \item
            whereas mathematical expressions like $(x\text{,}\,y)$ are to be written with a space after the comma: \\
            \verb*|(x, y)|
    \end{itemize}

    Also, the period is redefined so that it adapts to the used font.
    This feature might be useful with \LaTeX\ \cls{beamer}'s sans-serif math.
    
    
    \subsection{Package options}
    By default, the font used for typesetting the comma is the current roman text font. This behavior can be modified by using package options:
    
    \medskip\leavevmode%
    \DescribeOption{sffamily}%
    Passing the \opt{sffamily} option to \sty{textualicomma} results in the comma character being taken from the current sans serif text font
    
    \medskip\leavevmode%
    \DescribeOption{ttfamily}%
    Passing the \opt{ttfamily} option to \sty{textualicomma} results in the comma character being taken from the current mono-spaced text font
    
    
    \bigskip
    \DescribeMacro{\textualicommafont}
    Both options change the used font family \textit{for the whole document}.
    Changes \emph{inside} the document can be made using the "\textualicommafont" command: \\[\smallskipamount]
    "\textualicommafont{\rmfamily}" \qquad\rightarrow~~ roman comma in math mode \\
    "\textualicommafont{\sffamily}" \qquad\rightarrow~~ sans-serif comma in math mode \\
    "\textualicommafont{\ttfamily}" \qquad\rightarrow~~ monospaced comma in math mode
    
    
    
    \section{Bugs and problems}
    Generally, since the comma is made ‘active’, problems are not unlikely.
    
    \medskip
    For example, when using the ‘intelligent comma’ together with the \sty{dcolumn} package, a comma to be \textit{printed} as the decimal separator in a column of type "D" is to be specified as "{\mathord\mathcomma}", rather than "{,}", since the latter leads to an error. For example:
    \begin{center}
        \verb|\begin{tabular}{... D{,}{\mathord\mathcomma}{2} ...}|
    \end{center}

    Note that specifying the comma as the related input character works as usual.
    
    
    
    \phantomsection
    \addcontentsline{toc}{section}{References}
    \begin{thebibliography}{1}
        \bibitem{icomma}
        Walter Schmidt.
        The \sty{icomma} package for \LaTeXe.
        Package and Documentation available on CTAN (\url{https://www.ctan.org/pkg/icomma})
    \end{thebibliography}
    
    
    
    
    
    % Start determining the checksum from here
    \StopEventually{%
        \clearpage
        \PrintChanges
        \setcounter{IndexColumns}{2}
        \phantomsection
        \addcontentsline{toc}{part}{Index}%
        \IndexPrologue{\section*{Index}}
        \PrintIndex}
    
    \clearpage
    \part{The package code}
    \CodelineNumbered
    \DocInput{textualicomma.dtx}
    
    
    \Finale
\end{document}
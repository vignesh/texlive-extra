% \iffalse meta-comment
%
% Copyright (C) 2017 by Kim Wittenburg
%
% This file may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either
% version 1.3 of this license or (at your option) any later
% version. The latest version of this license is in:
%
%     http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of
% LaTeX version 2005/12/01 or later.
%
% \fi


% \iffalse
%<driver>\ProvidesFile{uhhassignment.dtx}
%
%<class>\NeedsTeXFormat{LaTeX2e}[2005/12/01]
%<class>\ProvidesClass{uhhassignment}[2017/04/22 v1.0 The UHH-Assignment Class]
%<*driver>
\documentclass{ltxdoc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}

\usepackage{idxlayout}
\usepackage{hypdoc}
\usepackage{enumitem}

\usepackage{lmodern}

\CodelineIndex
\EnableCrossrefs
\RecordChanges
\begin{document}
    \DocInput{uhhassignment.dtx}
\end{document}
%</driver>
% \fi


% \CheckSum{409}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}


% \changes{v1.0}{2017/04/12}{Initial version}
%
% \DoNotIndex{\#,\$,\%,\&,\@,\\,\{,\},\^,\_,\~,\ ,\relax}
% \DoNotIndex{\@ne,\newif,\fi,\gdef,\let,\newcommand,\renewcommand,\ifdefempty,
%             \ifstrempty,\RequirePackage}
% \DoNotIndex{\LoadClassWithOptions,\ClassWarning,\DeclareOption,
%             \ExecuteOptions,\ProcessOptions}
% \DoNotIndex{\advance,\begingroup,\catcode,\closein,\begin}
% \DoNotIndex{\end,\closeout,\day,\def,\edef,\else,\empty,\endgroup}
% \DoNotIndex{\newcounter,\arabic,\setcounter,\stepcounter}
% \DoNotIndex{\baselineskip,\headheight,\hfill,\par,\parskip,\setlength,\vspace,
%             \widthof}
% \DoNotIndex{\bfseries,\Huge,\footnotesize,\LARGE,\Large,\large,\normalfont,
%             \raggedright,\raggedleft,\RedeclareSectionCommands,\scriptsize,
%             \setkomafont,\sffamily,\textbf,\textnormal,\textsc,\ttfamily,
%             \usekomafont}
% \DoNotIndex{\ifoot,\ihead,\cfoot,\clearpairofpagestyles,\newpagestyle,\color,
%             \colorbox,\marginpar,\ofoot,\ohead,\pagestyle,\thispagestyle}
% \DoNotIndex{\pagemark,\pageref}
% \DoNotIndex{\protect,\space}

% \title{The \textsf{uhhassignment} document class}
% \author{Kim Wittenburg \\ \texttt{5wittenb@informatik.uni-hamburg.de}}
%
% \maketitle
%
% \begin{abstract}
%   The \textsf{uhhassignment} document class is a document class designed to be
%   used for assignments in university (or school). The class predominantly
%   implements a custom layout for titles but also featues some helpful new
%   commands. The class was originally designed to be used at the university of
%   Hamburg.
% \end{abstract}
%
% \addcontentsline{toc}{section}{Change History}
% \PrintChanges
%
% \tableofcontents
%
% \section{Usage}
%
% \subsection{Class Options}
%
% Currently the \textsf{uhhassignment} class supports three (or rather six)
% options:
%
% \index{unnumberedsheets}
% By default the \textsf{uhhassignment} class expects the assignment sheets to
% be numbered and issues a warning if the |\sheetno| is missing. However if you
% do not want to specify a sheet number you can use the
% \texttt{unnumberedsheets} class option to suppress the warning. The package
% option also removes any text indicating the sheet number from the output.
%
% \index{numberedsheets}
% The opposite option \texttt{numberedsheets} is used by default.
%
% \index{repeatauthor}
% You can specify the \texttt{repeatauthor} class option to display a short
% version of the |\author| on each page in the footer (including the first
% page). The short version of the author is specified with the |\author| command
% as an optional first parameter. By default the normal |\author| is used as
% short author as well. By default the short author will not be displayed at
% all.
%
% \index{shorttitle}\index{longtitle}
% You can pass \texttt{shorttitle} class option to display the short university
% name and short course name in the title. You could also explicitly pass
% \texttt{longtitle} which is the default.
%
% \index{shortheader}\index{longheader}
% Similarly you cann pass \texttt{shortheader} or \texttt{longheader} to display
% the short or regular university and course name in the header on each
% subsequent page.
%
% \subsection{The Title}
%
% The main feature of the \textsf{uhhassignment} class is the custom title
% layout. The layout implements some new fields like the name of the university
% (or school), the name of the teacher or the number of the exercise sheet. The
% full list of supported fields is listed below. The actual title layout may be
% different depending on which of the following fields are actually specified.
%
% \begingroup
% \setlist[description]{
%     font={\normalfont}
% }
%
% \begin{description}
%   \item[|\textbackslash university|\oarg{short uni}\marg{uni}]
%                                                   \DescribeMacro{\university}
%           Lets you specify the name of the university (or school). The default
%           value is \texttt{Universität Hamburg}.
%   \item[|\textbackslash course|\oarg{short course} \marg{course}]
%                                                       \DescribeMacro{\course}
%           Lets you specify the name of the course of the assignment. The
%           course is sometimes referred to as a \emph{module}.
%   \item[|\textbackslash teacher|\marg{teacher}] \DescribeMacro{\teacher}
%           Lets you specify the name of the teacher or teachers of the course.
%           If you want to specify multiple teachers it is recommended that you
%           separate them with the |\\| command.
%   \item[|\textbackslash group|\marg{group number}] \DescribeMacro{\group}
%           The exercises for many university courses are divided into multiple
%           exercise groups. Using this command you can specify the number of
%           your group.
%   \item[|\textbackslash sheetno|\marg{sheet number}] \DescribeMacro{\sheetno}
%           Lets you specify the current exercise sheet. If no |\title| is
%           provided the sheet number is used as title of the document.
% \end{description}
% \endgroup
%
% In addition to the above new commands you can also specify a |\title|, |\date|
% and one or more |\author|s.
%
% \DescribeMacro{\id}
% Sometimes you have to provide your student ID with your solutions to an
% exercise sheet. The \textsf{uhhassignment} class provides the |\id|\marg{id}
% command for this case. This command should be used when specifying the authors
% with the |\author| command. It might look like this:
% |\author{John Doe\id{1234567}}|
%
% \subsection{Tasks and Subtasks}
%
% The \textsf{uhhassignment} class is based on \textsf{srcarctl} from the KOMA
% packages so its main sectioning level are sections (not chapters). Sections
% and subsections however do not always accurately reflect the semantics of
% solutions to an assignment.  Instead the \textsf{uhhassignment} class provides
% the following commands:
%
% \smallskip
%
% \DescribeMacro{\task}|\task|\oarg{task number}\marg{task name}\\
% The |\task| command replaces the |\section| command. It incrementally numbers
% the tasks (starting at 1). If you want to skip a task you can use the optional
% \meta{task number} argument to set the task number to a specific value.
% Subsequent tasks are then incrementally numbered.
%
% \smallskip
%
% \DescribeMacro{\subtask}|\subtask|\oarg{task number}\marg{task name}\\
% \DescribeMacro{\subsubtask}|\subsubtask|\oarg{task number}\marg{task name}\\
% The |\subtask| and |\subsubtask| replace |\subsection| and |\subsubsection|
% respectively. They work analogous to |\task|.
%
% \smallskip
%
% The standard sectioning commands are not modified by the
% \textsf{uhhassignment} class so it is still possible to use them. In fact the
% |\task| commands do create sections and subsections internally. You should
% however be careful in using |\section|s yourself because it may be difficult
% to distinguish |\task|s from |\section|s.
%
% \subsection{Todos}
%
% The \textsf{uhhassignment} class offers rudimentary support for todo notes.
% Currently todo support is very limited and may not work in some cases.
%
% \smallskip
%
% \DescribeMacro{\todo}|\todo|\oarg{todo text}\\
% The |\todo| command does two things:
% \begin{enumerate}
%   \item It puts a todo marker in the text where the |\todo| command is
%       specified. The todo marker is defined by the |\todomark| command.
%       \DescribeMacro{\todomark}
%   \item It puts a todo note at the page margin so that you can easily spot
%       open todos in the PDF. The text of the todo note can be customized using
%       the \meta{todo text} optional argument.
% \end{enumerate}
%
% \StopEventually{
%   \addcontentsline{toc}{section}{Index}
%   \PrintIndex
% }
%
% \section{Implementation}
%
% In addition to the options provided by \textsf{scrartcl} the
% \textsf{uhhassignment} class supports the following options.
%    \begin{macrocode}
\newif\ifuhhassignment@numberedsheets
\DeclareOption{unnumberedsheets}{
    \uhhassignment@numberedsheetsfalse
}

\DeclareOption{numberedsheets}{
    \uhhassignment@numberedsheetstrue
}

\newif\ifuhhassignment@repeatauthor
\uhhassignment@repeatauthorfalse
\DeclareOption{repeatauthor}{
    \uhhassignment@repeatauthortrue
}

\newif\ifuhhassignment@shorttitle
\DeclareOption{shorttitle}{
    \uhhassignment@shorttitletrue
}

\DeclareOption{longtitle}{
    \uhhassignment@shorttitlefalse
}

\newif\ifuhhassignment@shortheader
\DeclareOption{shortheader}{
    \uhhassignment@shortheadertrue
}

\DeclareOption{longheader}{
    \uhhassignment@shortheaderfalse
}

\ExecuteOptions{longtitle,longheader,numberedsheets}

\ProcessOptions\relax
%    \end{macrocode}
%
% The \textsf{uhhassignment} class is based on \textsf{scrartcl}.
%    \begin{macrocode}
\LoadClassWithOptions{scrartcl}
%    \end{macrocode}

% \iffalse %%%%%%%%%%%%%%%%%%%
%%%%%%%% Dependencies %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%\fi

% \subsection{Dependencies}
%
%    \begin{macrocode}
\RequirePackage[utf8]{inputenc} % UTF-8 Encoding
\RequirePackage{cmap} % Special Characters
\RequirePackage[T1]{fontenc} % Font Encoding
\RequirePackage[ngerman]{babel} % German Language
\RequirePackage{etoolbox} % Programming
\RequirePackage{calc} % Calculations

\RequirePackage[a4paper,
                left=25mm,
                right=25mm,
                top=20mm,
                bottom=50mm]{geometry} % Page Margins

\RequirePackage[headsepline]{scrlayer-scrpage} % Header and Footer
\RequirePackage{lastpage} % Page Numbers

\RequirePackage{amsmath} % Maths
\RequirePackage{amssymb} % Maths
\RequirePackage{amsthm} % Beweise
\RequirePackage{enumitem} % Customizable Enumerations

\RequirePackage{xcolor} % Colors
\RequirePackage{framed} % Colored Boxes
\RequirePackage[colorlinks,linkcolor=black]{hyperref} % References
\RequirePackage{lmodern} % Better Font
%    \end{macrocode}

% \iffalse %%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Constants %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%\fi

% \subsection{Constants}

% The following constants are used throughout the template. They can be
% overridden for customizations.
%    \begin{macrocode}
\def\uhhassignment@sheetid{Aufgabenblatt}
\def\uhhassignment@groupid{Übungsgruppe}
%    \end{macrocode}

% \iffalse %%%%%%%%%%%%%%%%%%%
%%%%%%% Custom Commands %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%\fi

% \subsection{Definitions}
%
% \subsubsection{Title Elements}
%
% \begin{macro}{\university}
% The |\university| command stores the name of the university for later use.
% \begin{macrocode}
\newcommand{\university}[2][]{
    \ifstrempty{#1}{
        \gdef\@shortuniversity{#2}
    }{
        \gdef\@shortuniversity{#1}
    }
    \gdef\@university{#2}
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\course}
% The |\course| command stores the name of the course for later use.
%    \begin{macrocode}
\newcommand{\course}[2][]{
    \ifstrempty{#1}{
        \gdef\@shortcourse{#2}
    }{
        \gdef\@shortcourse{#1}
    }
    \gdef\@course{#2}
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\teacher}
% The |\teacher| command stores the name of the teacher for later use.
%    \begin{macrocode}
\newcommand{\teacher}[1]{
    \gdef\@teacher{#1}
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\group}
% The |\group| command stores the name of the group for later use.
%    \begin{macrocode}
\newcommand{\group}[1]{
    \gdef\@group{#1}
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\sheetno}
% The |\sheetno| command stores the assignment sheet number for later use.
%    \begin{macrocode}
\newcommand{\sheetno}[1]{
    \gdef\@sheetno{#1}
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\id}
% The |\id| macro prints the id of an author. The default implementation puts
% the id on a new line in parentheses.
%    \begin{macrocode}
\newcommand{\id}[1]{\\\ttfamily\footnotesize(#1)}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\author}
% The |\author| command is overridden by this class to support a short author
% that may be printed at the bottom of each page.
%    \begin{macrocode}
\renewcommand{\author}[2][]{
    \ifstrempty{#1}{
        \gdef\@shortauthor{#2}
    }{
        \gdef\@shortauthor{#1}
    }
    \gdef\@author{#2}
}
%    \end{macrocode}
% \end{macro}

% The default values for most fields are empty. The default university is the
% university of Hamburg.
%    \begin{macrocode}
\university[Uni Hamburg]{Universität Hamburg}
\def\@course{
    \ClassWarning{uhhassignment}{No \protect\course\space given}
}
\let\@shortcourse\@course
\group{}
\def\@teacher{
    \ClassWarning{uhhassignment}{No \protect\teacher\space given}
}
\let\@shortteacher\@teacher
\def\@sheetno{
    \ClassWarning{uhhassignment}{No \protect\sheetno\space given}
}
\def\@shortauthor{
    \ClassWarning{uhhassignment}{No short \protect\author\space given}
}
\title{}

%    \end{macrocode}

% \subsubsection{Todos}

% \begin{macro}{\todomark}
% The |\todomark| is placed in the text when the |\todo| command is used. The
% default implementation draws a purple box with the text "Todo" inside.
%    \begin{macrocode}
\newcommand{\todomark}{%
    \colorbox{purple}{%
        \textnormal\ttfamily\bfseries\color{white}%
        TODO%
    }%
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\todo}
% The |\todo| macro places the |\todomark| and puts a todo text in a marginpar.
%    \begin{macrocode}
\newcommand{\todo}[1][]{%
    \ifstrempty{#1}{%
        \def\todotext{Todo}%
    }{%
        \def\todotext{Todo: #1}%
    }%
    \todomark%
    {%
        \marginpar{%
            \raggedright\normalfont\sffamily\scriptsize\todotext%
        }%
    }%
}
%    \end{macrocode}
% \end{macro}

% \subsubsection{Tasks}

% The task mechanism of the \textsf{uhhassignment} class implements its own
% numbering mechanism. This is neccessary because the task numbers do not
% necessarily correlate with section numbers.
%    \begin{macrocode}
\setcounter{secnumdepth}{0}
%    \end{macrocode}

% \begin{macro}{\task}
% The |\task| command is basically just a wrapper around a section with a
% semi-customizable section title.
%    \begin{macrocode}
\newcounter{task}
\newcommand{\task}[2][]{%
    \ifstrempty{#1}{%
        \stepcounter{task}%
    }{%
        \setcounter{task}{#1}%
    }%
    \ifstrempty{#2}{%
        \section{Aufgabe \arabic{task}}%
    }{%
        \section{Aufgabe \arabic{task}: #2}%
    }%
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\subtask}
% The equivalent of |\subsection| for the \textsf{uhhassignment} class.
% |\subtask| essentially wraps |\subsection| with a semi-customizable title.
%    \begin{macrocode}
\newcounter{subtask}[task]
\newcommand{\subtask}[2][]{%
    \ifstrempty{#1}{%
        \stepcounter{subtask}%
    }{%
        \setcounter{subtask}{#1}%
    }%
    \ifstrempty{#2}{%
        \subsection{Teilaufgabe \arabic{task}.\arabic{subtask}}%
    }{%
        \subsection{\arabic{task}.\arabic{subtask}. #2}%
    }%
}
%    \end{macrocode}
% \end{macro}

% \begin{macro}{\subsubtask}
% The equivalent of |\subsubsection| for the \textsf{uhhassignment} class.
% |\subsubtask| essentially wraps |\subsection| with a semi-customizable title.
% It probably is not neccessary to use this very often.
%    \begin{macrocode}
\newcounter{subsubtask}[subtask]
\newcommand{\subsubtask}[2][]{%
    \ifstrempty{#1}{%
        \stepcounter{subsubtask}%
    }{%
        \setcounter{subsubtask}{#1}%
    }%
    \ifstrempty{#2}{%
        \subsection{\arabic{task}.\arabic{subtask}.\arabic{subsubtask}}
    }{%
        \subsubsection{\arabic{task}.\arabic{subtask}.\arabic{subsubtask}. #2}%
    }%
}
%    \end{macrocode}
% \end{macro}

% \iffalse %%%%%%%%%%%%%%%%%%%
%%%%%%%%% Page Setup %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%\fi

% \subsection{Page Setup}

% The \textsf{uhhassignment} class defines its own header and footer using
% \textsf{scrlayer-scrpage}. The default setup shows the course, sheet number,
% university and date on every page.
%    \begin{macrocode}
\setlength{\headheight}{50pt}
\pagestyle{scrheadings}
\clearpairofpagestyles

% Header
\ifuhhassignment@shortheader
    \ifuhhassignment@numberedsheets
        \ihead{\textbf{\@shortcourse}\\\uhhassignment@sheetid{} \@sheetno}
    \else
        \ihead{\textbf{\@shortcourse}}
    \fi
    \ohead{\textbf{\@shortuniversity}\\\@date}
\else
    \ifuhhassignment@numberedsheets
        \ihead{\textbf{\@course}\\\uhhassignment@sheetid{} \@sheetno}
    \else
        \ihead{\textbf{\@course}}
    \fi
    \ohead{\textbf{\@university}\\\@date}
\fi

% Footer
\setkomafont{pagefoot}{\textnormal}
\ifuhhassignment@repeatauthor
    \ifoot[\@shortauthor]{\@shortauthor}
    \ofoot[Seite \pagemark{} von \pageref{LastPage}]{Seite \pagemark{} von \pageref{LastPage}}
\else
    \cfoot[Seite \pagemark{} von \pageref{LastPage}]{Seite \pagemark{} von \pageref{LastPage}}
\fi

\newpagestyle{firstpage}[]{%
    \ihead{}
    \cfoot{\pagemark{}}
}
%    \end{macrocode}

% \iffalse %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Layout %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%\fi

% \subsection{Layout}

% The \textsf{uhhassignment} class overrides some font settings and adjusts the
% paragraph spacing to create a more distinct layout.
%
%    \begin{macrocode}
\setlength{\parskip}{5pt}

\setkomafont{section}{\normalfont\bfseries\LARGE}
\setkomafont{subsection}{\normalfont\bfseries\Large}
\setkomafont{subsubsection}{\normalfont\bfseries\large}
\RedeclareSectionCommands[afterskip=.25\baselineskip]{section}
\RedeclareSectionCommands[
    beforeskip=0.5\baselineskip,
    afterskip=.125\baselineskip]{subsection}
%    \end{macrocode}

% \begin{macro}{\maketitle}
% The \textsf{uhhassignment} class completely redefines the |\maketitle| macro.
% This implementation actually defines the layout of the final title depending
% on which information was specified by the user (group number, a title, \dots).
%
% We differentiate two \emph{states} of the title. If the user does not specify
% a title and does not specify a group the title will be rearranged to not
% display too much empty space. This is implemented via the |\ifextended|
% definition.
%    \begin{macrocode}
\renewcommand*{\maketitle}{%
    \thispagestyle{plain}%

    \newif\ifextended
    \extendedtrue

    \ifuhhassignment@shorttitle
        \let\university\@shortuniversity
        \let\course\@shortcourse
    \else
        \let\university\@university
        \let\course\@course
    \fi
%    \end{macrocode}

% First we define the different layout parts. The title has six \emph{slots}
% each of which can hold a piece of text:
% \begin{description}
%   \item[Top Left] On the top left the course name ist printed in a bold
%                   font.
%   \item[Top Right] On the top right the name of the university is printed
%                    in a bold font.
%   \item[Meta Left] Below the course name a piece of information is
%                    printed. The contents of this slot depend on the
%                    specified information and can be either the group
%                    number or the teacher.
%   \item[Meta Right] Similar to meta left but below the university name.
%                     This slot typically holds the teacher or the date.
%   \item[Title Left] Above the actual title there is a row of secondary
%                     titles. On the left of this row the sheet number is
%                     printed.
%   \item[Title Right] Similarly to title left this slot may contain the
%                      date.
% \end{description}
% Below the header the actual document title and the author(s) are printed.
% If no |\title| is specified the sheet number becomes the title (|\title|
% however remains empfy).
%    \begin{macrocode}
    \ifdefempty{\@title}{%
        \ifdefempty{\@group}{%
            \def\metaleft{\@teacher}%
            \def\metaright{\@date}%
            \def\titleright{}%
            \extendedfalse
        }{%
            \def\metaleft{\uhhassignment@sheetid{} \@group}%
            \def\metaright{\@teacher}%
            \def\titleright{\@date}%
        }%
        \def\titleleft{}%
        \ifuhhassignment@numberedsheets
            \def\title{\uhhassignment@sheetid{} \@sheetno}%
        \else
            % Title remains user-specified
        \fi
    }{%
        \ifdefempty{\@group}{%
            \def\metaleft{}%
            \def\metaright{\@teacher}%
        }{%
            \def\metaleft{\uhhassignment@groupid{} \@group}%
            \def\metaright{\@teacher}%
        }%
        \ifuhhassignment@numberedsheets
            \def\titleleft{\uhhassignment@sheetid{} \@sheetno}%
        \else
            \def\titleleft{}
        \fi
        \def\titleright{\@date}%
        \def\title{\@title}%
    }%
%    \end{macrocode}

% The following code prints the title with the information specified above.
% Depending on the value of |\ifextended| the title will be longer or
% shorter.
%
% Each field (heading, secondary title, title, author) is put in a separate
% group to avoid interference.
%    \begin{macrocode}

    \begingroup% Heading Group
        \usekomafont{pageheadfoot}%
        \usekomafont{pagehead}%
        \setlength{\parskip}{0pt}%

        \textbf{\course} \hfill \textbf{\university}\par%

        \begin{minipage}[t]{\widthof{\metaleft}}%
            \metaleft%
        \end{minipage}%
        \hfill%
        \begin{minipage}[t]{\widthof{\metaright}}%
            \raggedleft%
            \metaright%
        \end{minipage}%
    \endgroup%

    \ifextended%
        \vspace{\baselineskip}%
    \fi%

    \begingroup% Secondary Title Groupt
        \Large\bfseries\titleleft\hfill\titleright%
    \endgroup%

    \begingroup% Title Group
        \begin{center}%
            \Huge\textsc{\title}%
        \end{center}%
    \endgroup%

    \begingroup% Author Group
        \setlength{\baselineskip}{2.75\baselineskip}%
        \vspace{-\baselineskip}%
        \begin{center}%
            \begin{tabular}[t]{c}%
                \@author%
            \end{tabular}%
        \end{center}%
    \endgroup%
}
%    \end{macrocode}
% \end{macro}
% \Finale

\endinput

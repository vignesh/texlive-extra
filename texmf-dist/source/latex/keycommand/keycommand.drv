%%
%% This is file `keycommand.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% keycommand.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% keycommand : key-value interface for commands and environments in LaTeX [v3.1415 2010/04/27]
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file keycommand.dtx
%% and the derived files
%%    keycommand.sty, keycommand.pdf, keycommand.ins,
%%    keycommand-example.tex
%% 
%% keycommand : an easy way to define commands with optional keys
%% Copyright (C) 2009-2010 by Florent Chervet <florent.chervet@free.fr>
%% 
\edef\thisfile{\jobname}
\def\thisinfo{key-value interface for commands and environments in \LaTeX.}
\def\thisdate{2010/04/27}
\def\thisversion{3.1415}
\let\loadclass\LoadClass
\def\LoadClass#1{\loadclass[abstracton]{scrartcl}\let\scrmaketitle\maketitle\AtEndOfClass{\let\maketitle\scrmaketitle}}
\documentclass[a4paper,oneside]{ltxdoc}
\usepackage[latin9]{inputenc}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{etex,etoolbox,holtxdoc,geometry,tocloft,graphicx,xspace,fancyhdr,color,bbding,embedfile,framed,multirow,txfonts,makecell,enumitem,arydshln}
\CodelineNumbered
\usepackage{keyval}\makeatletter\let\keyval@setkeys\setkeys\makeatother
\usepackage{xkeyval}\let\xsetkeys\setkeys
\usepackage{kvsetkeys}
\usepackage{fancyvrb}
\lastlinefit999
\geometry{top=2cm,headheight=1cm,headsep=.3cm,bottom=1.4cm,footskip=.5cm,left=2.5cm,right=1cm}
\hypersetup{%
  pdftitle={The keycommand package},
  pdfsubject={key-value interface for commands and environments in LaTeX.},
  pdfauthor={Florent CHERVET},
  colorlinks,linkcolor=reflink,
  pdfstartview={FitH},
  pdfkeywords={tex, e-tex, latex, package, keys, keycommand, newcommand, keyval, kvsetkeys, programming},
  bookmarksopen=true,bookmarksopenlevel=3}
\embedfile{\thisfile.dtx}
\begin{document}
   \DocInput{\thisfile.dtx}
\end{document}
\endinput
%%
%% End of file `keycommand.drv'.

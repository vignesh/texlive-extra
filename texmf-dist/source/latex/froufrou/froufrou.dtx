% \iffalse meta-comment
% Copyright 2020 Nelson Lago <lago@ime.usp.br>
%
% This work may be distributed and/or modified under the conditions of the
% LaTeX Project Public License, either version 1.3 of this license or (at
% your option) any later version. The latest version of this license can be
% found at http://www.latex-project.org/lppl.txt and version 1.3 or later
% is part of all distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Nelson Lago <lago@ime.usp.br>.
%
% \fi
% \iffalse
%
%<package>\NeedsTeXFormat{LaTeX2e}[2015/01/01]
%<package>\ProvidesPackage{froufrou}[2020/12/22 1.2 Visual section separator]
%
%<*driver>

\documentclass{ltxdoc}

\usepackage[hyperref,svgnames,x11names,table]{xcolor}
\usepackage{url}
\urlstyle{sf}
\usepackage{hyperref}
\hypersetup{
  pdfborder={0 0 .6},
  pdfborderstyle={/S/U/W .6},
  urlbordercolor=DodgerBlue,
  citebordercolor=White,
  linkbordercolor=White,
  filebordercolor=White,
}

\usepackage{froufrou}

\usepackage{libertinus}
\usepackage[scale=.85]{sourcecodepro}

%%\EnableCrossrefs
%%\CodelineIndex
\RecordChanges

\OnlyDescription

\begin{document}
\DocInput{froufrou.dtx}
\end{document}

%</driver>
%
% \fi
%
% \CheckSum{0}
%
% \changes{v1.0}{2019/04/24}{Prerelease}
% \changes{v1.1}{2020/02/03}{Prerelease}
% \changes{v1.2}{2020/12/22}{First public version}
%
% \GetFileInfo{froufrou.sty}
%
% \title{The \textsf{froufrou} package\thanks{This document
% corresponds to \textsf{froufrou}~\fileversion, dated~\filedate.}}
%
% \author{
% Nelson Lago\\
% \texttt{lago@ime.usp.br}\\
% ~\\
% \url{https://gitlab.com/lago/froufrou}
% }
%
% \maketitle
%
% \begin{abstract}
%
% This package provides fancy separators, which are visual cues that
% indicate a change of subject or context without actually starting
% a new chapter or section.
%
% \end{abstract}
%
% \section{Introduction}
%
% This package allows the user to create fancy separators that visually
% indicate text subdivisions without a title, i.e., they are similar to
% \verb|\section| but do not provide a name to the new section. Apparently,
% there is no definite name for such separators; they may be regarded as
% thought breaks or anonymous sections, but often borrow their name from
% the ornament that embodies them, such as asterism, fleuron, or dinkus.
% An example:
%
% \froufrou
%
% Such separators never appear at the top of the page (that would be
% confusing), but may appear at the bottom. They should work equally
% well with one or two-column text and with single or doublespacing.
% The package offers a few predefined ornaments to be used as separators,
% but the user may create others.
%
% \section{Usage}
%
% After \verb|\usepackage{froufrou}|, just call \verb|\froufrou| to create
% a subdivision. By default, the separator behaves like \verb|\section|,
% suppressing indentation in the following paragraph. With \verb|\froufrou*|,
% this feature is disabled and the following paragraph is indented normally.
%
% You may select a different separator appearance either by
% supplying the name of a predefined ornament as a package
% option (\verb|\usepackage[ornament]{froufrou}|) or by calling
% \verb|\setfroufrou{ornament}| anywhere in the document. You may
% also use \verb|\froufrou[ornament]| to only affect that specific
% separator. \verb|ornament| is one of:
%
% \makeatletter
% \begin{description}
%
% \item[fleuron,] the default\footnote{From the \verb|fancyhdr| docs.}: \quad\@froufrouFleuron
%
% \item[simplefleuron,] similar to the default, but without the side lines: \quad\@froufrouSimpleFleuron
%
% \item[asterism,] three asterisks forming a triangle\footnote{
% From \verb|symbols-a4| (search for ``asterism'').}: \quad\@froufrouAsterism
%
% \item[tightasterism,] similar, but with tighter spacing: \quad\@froufrouTightAsterism
%
% \item[trueasterism,] also similar, but using unicode character
%      U+2042\footnote{This only works with a unicode engine
%      (luatex, xetex) and with a font that actually provides the
%      glyph (such as libertinus or libertine).}: \quad\@froufrouTrueAsterism
%
% \item[dinkus,] three asterisks forming a line: \quad\@froufrouDinkus
%
% \end{description}
% \makeatother
%
% Beyond the predefined ornaments, you may also use
% \verb|\setfroufrou{SOMETHING}| or \verb|\froufrou[SOMETHING]|
% to make \verb|SOMETHING| be the separator. However, please note
% that \verb|\usepackage[SOMETHING]{froufrou}| does \emph{not} work!
%
% \section{See also}
%
% \begin{itemize}
% \item \url{http://tug.org/TUGboat/tb32-2/tb101glister.pdf} ;
% \item The \verb|pgfornament| package, which may aid in creating separators;
% \item The \verb|novel| class, which offers the somewhat similar \verb|\ChapterDeco| command;
% \item The \verb|memoir| class, which offers the somewhat similar \verb|\fancybreak| command;
% \end{itemize}
%
% \StopEventually{\PrintChanges}
%
% \section{The implementation}
%
%    \begin{macrocode}

\RequirePackage{etoolbox}
\RequirePackage{expl3}

% .1\textwidth works well for one and two-column text.
\RequirePackage{fourier-orns}
\newcommand\@froufrouFleuron{%
    \bgroup
    \footnotesize
    \rule[.7mm]{.1\textwidth}{0.5pt}%
    \enspace\,%
    \decofourleft\decotwo\decofourright
    \enspace\,%
    \rule[.7mm]{.1\textwidth}{0.5pt}%
    \egroup
}

\newcommand\@froufrouSimpleFleuron{%
    \bgroup
    \footnotesize
    \decofourleft\decotwo\decofourright
    \egroup
}

\newcommand{\@froufrouTightAsterism}{%
  \bgroup
  \smash{%
    \raisebox{-.5ex}{%
      \setlength{\tabcolsep}{-.5pt}%
      \begin{tabular}{@{}cc@{}}%
        \multicolumn2c*\\[-2ex]*&*%
      \end{tabular}%
    }%
  }%
  \egroup
}

\newcommand{\@froufrouAsterism}{%
  \bgroup
  \smash{%
    \raisebox{-.5ex}{%
      \setlength{\tabcolsep}{0pt}%
      \begin{tabular}{@{}cc@{}}%
        \multicolumn2c*\\[-1.8ex]*&*%
      \end{tabular}%
    }%
  }%
  \egroup
}

% Unicode character U+2042 (\char"2042), only with a unicode engine
% (luatex/xetex) and a font that has this glyph (such as libertinus
% or libertine)
\newcommand\@froufrouTrueAsterism{⁂}

\newcommand{\@froufrouDinkus}{\relax*\quad*\quad*}

\ExplSyntaxOn
\newcommand\setfroufrou[1]{%
  \str_case:nnF{#1}{
      {}{\relax}
      {fleuron}{\def\@froufrouOrnament{\@froufrouFleuron}}
      {simplefleuron}{\def\@froufrouOrnament{\@froufrouSimpleFleuron}}
      {asterism}{\def\@froufrouOrnament{\@froufrouAsterism}}
      {tightasterism}{\def\@froufrouOrnament{\@froufrouTightAsterism}}
      {trueasterism}{\def\@froufrouOrnament{\@froufrouTrueAsterism}}
      {dinkus}{\def\@froufrouOrnament{\@froufrouDinkus}}%
  }{\def\@froufrouOrnament{#1}}%
}
\ExplSyntaxOff

% I see no reason to make these different, but feel free to change this :)
\def\@froufrouspacebefore{\vskip 22pt plus 7pt minus 5pt}
\def\@froufrouspaceafter{\@froufrouspacebefore}

% The starred form means "indent the first line of the next paragraph"
% (\if@afterindent is read by \@afterheading)
\newcommand{\froufrou}{%
  \@ifstar{\@afterindenttrue\@realfroufrou}{\@afterindentfalse\@realfroufrou}%
}

% For compatibility with older versions of this package
\newcommand\frufru{\froufrou}

\newcommand{\@realfroufrou}[1][]{%
  % Make sure we left horizontal mode.
  \nopagebreak[4]\par

  \nopagebreak[4]\@froufrouspacebefore\nopagebreak[4]

  % Start a new group to (1) reset \doublespacing, \parskip, and
  % \baselineskip, so spacing is independent from the previous text
  % settings and (2) so that using #1 to change the ornament only
  % affects the current call. Then, start yet another group only
  % for the ornament because font size changes in it should not
  % affect spacing either.
  \bgroup
    \setfroufrou{#1}%
    \normalsize
    \ifdefvoid{\setstretch}{}{\setstretch{\setspace@singlespace}}% normally 1
    \setlength{\parskip}{0pt}
    \noindent\centering\bgroup\@froufrouOrnament\egroup\par
  \egroup

  \nopagebreak[4]\@froufrouspaceafter\nopagebreak[4]

  % Next paragraph's \doublespacing, \baselineskip, and \parskip
  % should not affect spacing.
  \@froufrouFixSpacingAfter

  % Breaking after the separator is acceptable as a last resort. Since
  % we use a lot of vertical space before/after it, not allowing this
  % break might lead to poor results in some cases.
  \nopagebreak[3]

  % Emulate the behavior of \section, \subsection etc: prevent
  % the first line of the next paragraph from being indented
  % and absolutely forbid a club line in the next paragraph.
  \@afterheading
}

\newcommand\@froufrouFixSpacingAfter{
  % The next \par will add \parskip and \baselineskip. We want to
  % (1) eliminate \parskip so that spacing does not change if it
  % is nonzero and (2) add the "normal" \baselineskip, even if the
  % text is set using \doublespacing. These adjustments may be off
  % if font size, \parskip or \baselineskip are changed before the
  % next paragraph.

  \nopagebreak[4]\vskip -\parskip

  \ifdefvoid{\setstretch}
    {}
    {
      \bgroup
        % \singlespacing may add an extra \baselineskip; let's
        % avoid surprises and use \setstrech instead.
        \setstretch {\setspace@singlespace}%  normally 1
        \nopagebreak[4]\vskip \baselineskip
      \egroup

      \nopagebreak[4]\vskip -\baselineskip
    }
}

% Trying to accept a custom ornament as a package option is not such a
% great idea: first, it has to be expandable; second, this may become
% a problem if we want to add more options to the class in the future.
\DeclareOption{fleuron}{\setfroufrou{fleuron}}
\DeclareOption{simplefleuron}{\setfroufrou{simplefleuron}}
\DeclareOption{asterism}{\setfroufrou{asterism}}
\DeclareOption{tightasterism}{\setfroufrou{tightasterism}}
\DeclareOption{trueasterism}{\setfroufrou{trueasterism}}
\DeclareOption{dinkus}{\setfroufrou{dinkus}}
\DeclareOption*{\PackageWarning{froufrou}{Unknown option `\CurrentOption'}}
\ExecuteOptions{fleuron}
\ProcessOptions\relax

%    \end{macrocode}
% \Finale

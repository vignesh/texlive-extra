%%
%% This is file `showlabels.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% showlabels.dtx  (with options: `driver')
%% Showlabels: Style to display labels in the margin
%% This is the showlabels package
%%
%%%% Copyright 1999, 2001-09, 2013-16, Norman Gray
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public Licence, either version 1.3
%% of this licence or (at your option) any later version.
%% The latest version of this licence is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainer of this work is Norman Gray <http://nxg.me.uk>
%%
%% This work consists of the files showlabels.dtx and showlabels.ins,
%% and the derived file showlabels.sty.

%%%% File: showlabels.dtx
%%%% Source: 95a544e6b41b, 2016-06-09T16:51:03+01:00

\def\filename{showlabels}
\def\fileversion{1.8}
\def\filedate{2016/06/09}
\def\docdate{2016 June 9}
\def\filemaintainer{norman@astro.gla.ac.uk}
\documentclass{ltxdoc}
\EnableCrossrefs
\newcommand\Lopt[1]{\textsf{#1}} % package options
\newcommand\file[1]{\texttt{#1}} % filename
\newcommand\Lenv[1]{\textsl{\{#1\}}}
\newcommand\Lpackage[1]{\textsf{\{#1\}}} % packages
%% \url macro (url.sty does this better, but we don't want extra dependencies)
\def\setpathdots{\discretionary{.}{}{.}}
\def\setpathslash{\discretionary{/}{}{/}}
{\catcode`\.=\active
 \catcode`\/=\active
  \gdef\pathcats{%
    \catcode`\%=12      \catcode`\~=12
    \catcode`\.=\active  \let.\setpathdots
    \catcode`\/=\active \let/\setpathslash
    \catcode`\#=12      \catcode`\_=12}%
    }
\def\setpath#1{\ttfamily <\nobreak #1\nobreak>\endgroup}
\def\url{\begingroup\pathcats\setpath}
\begin{document}
\DocInput{showlabels.dtx}
\PrintIndex
\end{document}
\endinput
%%
%% End of file `showlabels.drv'.

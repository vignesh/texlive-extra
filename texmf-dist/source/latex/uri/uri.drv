%%
%% This is file `uri.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% uri.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% Project: uri
%% Version: 2018/09/06 v2.0b
%% 
%% Copyright (C) 2011 - 2018 by
%%     H.-Martin M"unch <Martin dot Muench at Uni-Bonn dot de>
%% Portions of code copyrighted by other people as marked.
%% 
%% IMPORTANT NOTICE: The package takes options.
%% 
%% The usual disclaimer applies:
%% If it doesn't work right that's your problem.
%% (Nevertheless, send an e-mail to the maintainer
%%  when you find an error in this package.)
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. This version of this license is in
%%    http://www.latex-project.org/lppl/lppl-1-3c.txt
%% and the latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status "maintained".
%% 
%% The Current Maintainer of this work is H.-Martin Muench.
%% 
%% This work consists of the main source file uri.dtx,
%% the README, and the derived files
%%    uri.sty, uri.pdf, uri.ins, uri.drv,
%%    uri-example.tex, uri-example.pdf.
%% 
%% In memoriam
%%  Claudia Simone Barth + 1996/01/30
%%  Tommy Muench + 2014/01/02
%%  Hans-Klaus Muench + 2014/08/24
%% 
\NeedsTeXFormat{LaTeX2e}[2015/01/01]
\ProvidesFile{uri.drv}[2018/09/06 v2.0b
            Hyperlinks URIs like DOI,HDL,NBN,PubMed (HMM)]
\documentclass{ltxdoc}[2015/03/26]% v2.0w
\usepackage{pdflscape}[2008/08/11]% v0.10
\usepackage{holtxdoc}[2012/03/21]%  v0.24
%% uri may work with earlier versions of LaTeX2e and those
%% class and packages, but this was not tested.
%% Please consider updating your LaTeX, class, and package
%% to the most recent version (if they are not already the most
%% recent version).
\hypersetup{%
 pdfsubject={Hyperlinks URIs like DOI, HDL, NBN, PubMed (HMM)},%
 pdfkeywords={LaTeX, uri, hyperlink, arXiv, ASIN, DOI, HDL, NBN, OCLC, OID, PubMed, TINY, TINY:P, preview, WebCite, citeurl, mailto, ukoeln, uref, Hans-Martin Muench},%
 pdfencoding=auto,%
 pdflang={en},%
 breaklinks=true,%
 linktoc=all,%
 pdfstartview=FitH,%
 pdfpagelayout=OneColumn,%
 bookmarksnumbered=true,%
 bookmarksopen=true,%
 bookmarksopenlevel=3,%
 pdfmenubar=true,%
 pdftoolbar=true,%
 pdfwindowui=true,%
 pdfnewwindow=true%
}
\CodelineIndex
\hyphenation{created document doc-u-ment-a-tion every-thing ignored}
\gdef\unit#1{\mathord{\thinspace\mathrm{#1}}}
\begin{document}
  \DocInput{uri.dtx}%
\end{document}
\endinput
%%
%% End of file `uri.drv'.
